README
------
This is the historical/archive SIGLEX-MWE section website repository.
The [historical/archive website](http://multiword.sourceforge.net) has been replaced by the new [SIGLEX-MWE section website](https://multiword.org/) in January 2021.
This repository has ensured the versioning of the section's website hosted at [Sourceforge](https://sourceforge.net) until January 2021.
Updates in the future should remain exceptional, but the documentation below can help if this is necessary.
Please remember that sourceforge and gitlab accounts are necessary to edit the website: ask the current [project members](https://gitlab.com/siglex-mwe/siglex-mwe/-/project_members) if you require edit access to the website.

Website structure
-------
 * The website source files are stored in `htdocs/`
 * Each name of a directory and subdirectory starting with `**CONF**` is automatically displayed as the website left hand side menu or submenu item. 
 * The number following the `CONF` prefix is used to sort pages in the menu.
 * The name of the menu item is composed from the directory/file name by deleting the `CONF_number` prefix, replacing the underscores with blank spaces, and lb/rb with paretheses. 
 * The `body*.inc` file contained in a subdirectory becomes the contents of the webpage attached to the given menu item. 
 * For instance:
    * File: `CONF_05_MWE_2017_\_\_lb__EACL_\_rb_\_/CONF_25_Program/body_program.inc`
    * Menu item: _MWE 2017 (EACL)_ -> _Program_
    * Webpage: the contents of `body_program.inc`
 * If you want to make a page invisible, change the prefix of its directory so as not to start with `CONF`.
 * Directories not prefixed by CONF are used to handle redirections of short URLs to true URLs. For instance, the `mwe2017/index.html` file redirects `http://multiword.sourceforge.net/mwe2017/` to `http://multiword.sourceforge.net/PHITE.php?sitesig=CONF&page=CONF_05_MWE_2017_\_\_lb_\_EACL_\_rb_\_`

Gitlab use
------
 * To do once:
     * Create an account on [Gitlab](https://gitlab.com/)
     * (If you wish to avoid typing your password when connection to the server) Install your SSH public key on [Gitlab](https://gitlab.com/) (see [manual](https://docs.gitlab.com/ce/ssh/README.html)).
     * Clone the Gitlab repo to a local directory (choose a name and use it as `your-local-repo`)
         * `git clone git@gitlab.com:siglex-mwe/siglex-mwe.git your-local-repo`
     * Copy a Gitlab hook which will remind you about pushing and running `./toServer` each time you commit (see below):
         *  `cd your-local-repo`
         *  `cp githooks/post-commit .git/hooks/`
         *  `chmod a+x .git/hooks/post-commit`
 * To do **each time** you need to modify the website:
     * Update the local version of the repository
         * `git pull`
     * Edit the source files locally
     * Commit local changes to the repo
         * `git add file` (if you created a new file)
         * `git commit -m "Description of your modifications" modified-file`
         * `git push`
     * Don't forget to synchronise your local repository with Sourceforge (see below):
          * `./toServer your-sourceforge-login`
 * Other useful git commands:
      * Checking if there is anything to push
         * `git status`

Sourceforge use
-------
 * To do once:
     * Create an account on [Sourceforge](https://sourceforge.net)
     * (If you wish to avoid typing your password when connection to the server) Install you SSH public key on [Sourceforge](https://sourceforge.net):
         * Go to _Me -> Account settings -> SSH settings_
         * Insert the public RSA key of your local machine (usually found in `.ssh/id_rsa.pub`)
 * To do **each time** you have modified your website
     *  Synchronise your local changes with Sourceforge (this is based on a the `rsync` command, excluding some cofiguration files like `.git` etc.):
         * `cd ~/your-local-repo`
         * `./toServer.sh your-sourceforge-login`
 * In case you need to acces the website source codes via FTP (usually not needed):
    * `sftp your-sourceforge-login,multiword@web.sf.net`
    * `cd htdocs`

Adding a new event
------
 * Create a subdirectory with the name conforming to the event and the right rank, e.g.:
     * `cd your-local-repo/htdocs`
     * `mkdir CONF_04_MWE_2018___lb__ACL__rb__`
 * Copy the files from a similar previous event and adapt them, e.g.:
     * `cp -r CONF_05_MWE_2017___lb__EACL__rb__/* CONF_04_MWE_2018___lb__ACL__rb__/*`
     * edit files
 * Create a short URL, e.g.:
     * `mkdir mwe2018`
     * `cp mwe2017/index.html mwe2018`
     * edit `mwe2018/index.html` to update the redirection URLs
 * Synchronise with Sourceforge and Gitlab, e.g.:
     * `cd your-local-repo/`
     * `git add CONF_04_MWE_2018___lb__ACL__rb_ mwe2018`
     * `git commit -m "Adding a new event"`
     * `git push`
     * `./toServer your-sourceforge-login`
     
Contact
-------
The [SIGLEX-MWE section](http://multiword.sourceforge.net/) website and its repository is usually managed by the Section's representative on the [SIGLEX board](https://siglex.org/board/2020.html).
* 2016-2019: [Agata Savary](http://www.info.univ-tours.fr/~savary/); backup by [Carlos Ramisch](http://pageperso.lis-lab.fr/carlos.ramisch/)
* 2020-2022: [Carlos Ramisch](https://pageperso.lis-lab.fr/carlos.ramisch); backup by [Jelena Mitrović](http://jelena.mitrovic.rs/)
