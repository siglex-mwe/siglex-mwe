<?php

$globalsitename="SIGLEX-MWE (archive)";

$sites['MWE'] = array(
#		'name'=>"Multiword Expressions",
		'name'=>"Historical website",
#		"homepagename"=>'Multiword Expressions',
		"homepagename"=>'SIGLEX-MWE Section - Historical website',
		'def_skin'=>'GilaTwo'
	);

$sites['CONF'] = array(
		'name'=>"Workshops",
		"homepagename"=>'Workshops',
		'def_skin'=>'GilaTwo'
	);

$sites['FILES'] = array(
		'name'=>"Software &amp; Resources",
		"homepagename"=>'Software &amp; Resources',
		'def_skin'=>'GilaTwo'
	);
	
$sites['SPECIAL'] = array(
		'name'=>"ACM TSLP Special Issue",
		"homepagename"=>'ACM TSLP Special Issue',
		'def_skin'=>'GilaTwo'
	);	
	
$sites['PMWE'] = array(
		'name'=>"PMWE volume",
		"homepagename"=>'PMWE volume',
		'def_skin'=>'GilaTwo'
	);	

?>
