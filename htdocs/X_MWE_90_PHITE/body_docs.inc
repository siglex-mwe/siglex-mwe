<h2>Instructions for site editors</h2>

<p>
	In order to simplify collaborative Web site management, <a href="http://multiword.sourceforge.net/" target="_blank">multiword.sf.net</a> uses a modified version of the <a href="http://sourceforge.net/projects/phite" title="PHITE Homepage" target="_blank">PHITE</a> template engine written in PHP.  The following instructions are intended for site editors and explain how to modify and add individual pages and subpages.  For advanced editing, you should also consult the original <a href="http://www.dreammask.com/PHITE.php?sitesig=PT&amp;page=PT_003_Basic_Concepts" target="_blank">PHITE documentation</a>, but keep in mind that the version used here has been modified substantially.
</p>

<h1>The PHITE engine</h1>

<p>
	PHITE automatically generates a navigation menu from the directory structure of the site, shown on the left-hand side of the screen in the default layout (<em>skin</em>).  Each directory corresponds to a page, and may contain subdirectories that appear as subpages in the navigation menu.  In contrast to the original PHITE framework, this version divides the Web site into sections (called <em>subsites</em> internally) with separate navigation menus.  A navigation bar below the main title allows readers to switch between different sections (other skins may place the navigation bar elsewhere).
</p>

<p>
	Page content is stored in one or more text files in the page's subdirectory.  In addition to the main body of the page, small text blocks in separate files can be displayed on the left (below the navigation bar), below the main body, or in highlighted boxes on the right-hand sight of the screen.  One of the distinctive features of PHITE is that text blocks are automatically inherited from the parent page, section or global site defaults if no blocks of the respective type are defined locally.  For instance, the global default body text says <em>“page not available yet”</em>, and a default text box below the navigation menu displays the required SourceForge logo.  Within the <em>Multiword Expressions</em> section, a message box on the right-hand side shows the latest news, but is overridden on this page and its subpages.
</p>

<p>
	More detailed information on the directory layout, editing pages, and some more advanced topics can be accessed through the subpage entries in the navigation menu on the left.
</p>