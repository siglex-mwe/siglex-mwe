<h2>Defining text blocks</h2>

<p>
	In addition to the main body text of a page, PHITE offers a flexible mechanism for assembling content from small text blocks defined in separate files.   There are three different types of blocks: <em>left blocks</em> (LB) appear below the navigation menu on the left and are formatter in a similar, unobtrusive style (in the default skin); <em>center blocks</em> (CB) are appended to the main body text and look like regular sections with a <code>&lt;h1&gt</code> heading; <em>right blocks</em> (RB) appear as highlighted information boxes on the right-hand side of the page and should be used sparingly.  Examples of all three block types are shown on this page.
</p>

<p>
	Text block files may contain the same formats (plain text, HTML code and PHP) as the main page body.  If no text blocks of a given type are defined for the current page, they are inherited from the parent page or the section's main page.
</p>

<h1>Naming conventions for text blocks</h1>

<p>
	Text block files use the same naming conventions as PHITE directories.  Filenames must have the form
</p>
<pre>
  <i>TT</i>_<i>dd</i>_Text_block_heading.inc
</pre>
<p>
	where <code><i>TT</i></code> identifies the block type (<code>LB</code>, <code>CB</code> or <code>RB</code>) and the two-digit number <code><i>dd</i></code> determines the ordering of multiple blocks of the same type.  The rest of the filename is used as a heading for the text block, with underscores and named entities converted in the same way as for directories (details are repeated below).  Note the mandatory <code>.inc</code> filename suffix.
</p>
<p>
	The text block examples on this page are defined in files named
</p>
<pre>
  ./LB_10_Left_block_example.inc
  ./LB_20_More_left_blocks.inc
  ./LB_30_SourceForge_site_logo.inc
  ./CB_10_Center_block_example.inc
  ./CB_90_Special_characters.inc
  ./RB_10_Right_block_example.inc
  ./RB_20_Lists_in_message_boxes___lb__right_blocks__rb__.inc
</pre>
