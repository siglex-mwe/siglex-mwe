<p>This is an example of a message box you can add on the right-hand side of a page.</p>
<p>Put text in paragraphs to ensure nice spacing (or just have a single block of plain HTML text).</p>
