<p>
	Note that the site logo required by SourceForge is defined as a global left block, so it is not displayed on pages that define their own left blocks (and their subpages).
</p>