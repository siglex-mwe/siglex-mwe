<p>
	Message boxes can also contain bulleted
	<ul>
		<li>list item 1</li>
		<li>list item 2</li>
	</ul>
	as well as enumerated lists:
	<ol>
		<li>one</li>
		<li>two</li>
		<li>three</li>
	</ol>
	These are not guaranteed to format nicely in all broswers yet and should be used sparingly.
</p>
