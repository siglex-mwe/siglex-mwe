<p>
	Left blocks are less obtrusive than message boxes (right blocks) and do not disrupt page formatting.
</p>
<p>	
	They can be deployed more liberally, but should not be used for important messages because they are visually similar to the navigation menu and may be perceived as “boilerplate”.
</p>