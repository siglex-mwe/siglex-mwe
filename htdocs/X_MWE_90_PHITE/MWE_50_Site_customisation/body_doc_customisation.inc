<h2>Customising the appearance of a PHITE site</h2>

<p>
	Since the pages of a PHITE Web site are generated from HTML text blocks and navigation menus, their appearance can be changed almost completely by modifying the global formatting templates (<em>skins</em>).  The <a href="http://www.dreammask.com/PHITE.php?sitesig=PT&amp;page=PT_010_Choose_Skin" target="_blank">skin chooser</a> on the PHITE homepage gives an impressive demonstration of this feature.
</p>

<p>
	The modified PHITE version used here includes a single, professionally designed skin.  The original PHITE skins <em>will not work</em> because of changes in the page structure.  If you wish to change the appearance of the Web site, make a copy of the default skin and modify it according to your requirements.  This will involve some trial &amp; error, as there is no documentation for skin authors yet.  It is probably a good idea to familiarise yourself with the <a href="http://templatepower.codocad.com/" target="_blank">TemplatePower</a> system used by PHITE.
</p>

<p>
	Some parts of the site boilerplate can easily be changed by all editors: (a) a copyright message or contact information shown on the left-hand side of the footer bar, and (b) the page metadata returned as part of the HTML header.  These items are specified in files <code>GLOBAL/footer.inc</code> and <code>GLOBAL/meta.inc</code>, but can be overridden for individual subsites or pages in the usual way (i.e. by putting a file of the same name in the respective directory).
</p>

<h1>Defining skins</h1>

<p>
	Skins are defined by <em>template files</em> in the directory <code>GLOBAL/skin</code>.  For instance, the default skin <em>SinorcaGrey</em> is implemented by the file <code>GLOBAL/skin/SincorcaGrey.tpl</code> (note that the filename corresponds to the name of the skin).  If skins require additional support files (such as CSS stylesheets or background images), these files should be kept in an appropriate subdirectory of <code>GLOBAL/skin_files</code>, e.g. <code>GLOBAL/skin_files/SinorcaGrey</code> for the default skin.
</p>

<p>
	All PHITE skins are globally defined for the entire Web site, but the individual subsites can have different default skins (e.g. if you want to separate them visually with different colour schemes).
</p>

<p>
	From here on, you are on your own.  The best strategy is to make a copy of the default skin <code>SinorcaGrey.tpl</code> (don't forget to copy the support files, too!), try to understand its structure and the included template instructions, and then start by making small changes to the layout.
</p>