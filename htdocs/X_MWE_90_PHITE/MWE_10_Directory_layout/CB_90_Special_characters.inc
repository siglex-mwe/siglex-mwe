<p>
	Since the pages and subpages of a PHITE Web site must be valid directory names, they should only contain ASCII alphanumeric characters, underscores and dashes.  All other characters may not be fully portable, and whitespace should generally be avoided in filenames.  In order to allow for special characters in page names, PHITE automatically converts all underscore (<code>_</code>) characters in the page name part of a directory into blanks.  In addition, certain “named entities” delimited by double underscores can e used to insert special characters such as parentheses.  By default the following entities are defined:
</p>
<pre>
  __apos__     &rarr; '
  __quot__     &rarr; &quot;
  __amp__      &rarr; &amp;
  __question__ &rarr; ?
  __exclaim__  &rarr; !
  __colon__    &rarr; :
  __comma__    &rarr; ,
  __period__   &rarr; .
  __lb__       &rarr; (
  __rb__       &rarr; )
  __ldots__    &rarr; &hellip;
</pre>
<p>
	Note that standard HTML entities are not recognised automatically!  Further named entities can be added by modyfing the file <code>special_chars.inc.php</code> in the top-level directory (the syntax should be obvious).
</p>
<p>
	As an example, the directory <code>EXTRA_10_R__amp__D___lb__2008__rb__</code> defines a page named <em>R&amp;D (2008)</em>.
</p>