<h2>PHITE directory layout</h2>

<p>
	PHITE automatically generates navigation menus from the directory structure of a Web site.  The site can be divided into sections, which are internally called <em>subsites</em> and identified by short <em>signature</em> strings such as <code>CONF</code> (for the <em>Conferences</em> section).  Top-level directories correspond to pages (note that there are no separate directories for the sections of a site), and may contain subdirectories that appear as subpages in the navigation menu.  No further nesting is allowed.
</p>

<p>
	Directory names must follow the general pattern
</p>
<pre>
  <i>SIG</i>_<i>dd</i>_Name_of_the_page
</pre>
<p>
	where <code><i>SIG</i></code> is the subsite signature, <code><i>dd</i></code> a two-digit number that determines the ordering of pages in the menu (other sort codes may also be used), and the rest of the directory name appears as a page name in the navigation menu.  In addition, each section has a start page in the top-level directory <code><i>SIG</i>_MAIN</code> (shown as <em>Overview</em> in the menu) and the directory <code>GLOBAL</code> contains default text blocks for the entire Web site.
</p>

<h1>Adding pages and sections</h1>

<p>
	To add a page, simply create a new directory or subdirectory according to the naming conventions explained above (and don't forget to make it group writable).  Initially, the page will show the same content as its parent or the corresponding section start page.  You can now add the main body text of the page in a file named <code>body.inc</code> (or <code>body_some_mnemonic_text.inc</code>), and optionally further text blocks in separate files.  See <em>Editing pages</em> and <em>Text blocks</em> for further information.
</p>

<p>
	Adding a section is slightly more complicated and requires editing the file <code>subsites.inc.php</code>, where the signatures, names and order of the individual sections are defined.  Choose a new subsite signature, say <code>EXTRA</code>, then copy one of the existing entries in the file and adjust the signature, name (shown as main title of the page) and homepage name (shown in the section navigation bar).  In our example, the new entry might look like this:
</p>
<pre>
  $sites['EXTRA'] = array(
         'name' => "Multiword Expressions: Goodies",
         "homepagename" => 'Extras &amp;amp; Goodies',
         'def_skin' => 'default'
      );
</pre>
<p>
	After editing <code>subsites.inc.php</code>, you must also create a group writable directory for the start page of the section, named <code>EXTRA_MAIN</code> in our case.
</p>