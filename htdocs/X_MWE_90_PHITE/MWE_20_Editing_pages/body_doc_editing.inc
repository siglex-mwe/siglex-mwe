<h2>Editing pages</h2>

<p>
	The main body text of a page is contained in a file <code>body.inc</code> in the page directory (see <em>Directory layout</em> for more information).  Optionally, a mnemonic suffix introduced by an underscore character (<code>_</code>) may be added before the extension <code>.inc</code>, e.g. <code>body_conf_2008.inc</code>.  There must be only a single body text file in each directory, though (otherwise PHITE will silently use the first one found).  If the directory does not contain a <code>body.inc</code> file, the page inherits its body text from the parent page or the start page of its section.
</p>

<p>
	The <code>body.inc</code> file may contain either plain text (in HTML format, i.e. special characters must be escaped as entities), HTML code (as you would put in the <code>&lt;body&gt;</code> element of a HTML page, but without the <code>&lt;body&gt; &hellip; &lt;/body&gt;</code> tags) or HTML code mixed with PHP (in the usual <code>&lt;?php &hellip; ?&gt;</code> delimiters).  Note that all pages <em>must use UTF-8 encoding</em> if they contain non-ASCII characters.  Links to other subpages can be inserted by copying URLs from the automatically generated navigation menu, but should generally be avoided as they will break when pages are renamed.  The recommended strategy is to point readers to navigation menu entries in the text, e.g. “click on <em>Text blocks</em> for more information”.
</p>

<p>
	Links to images and other files cannot use relative paths within the page or subpage directory, since the page is always accessed through the <code>PHITE.php</code> script in the top-level directory.  It is recommended to put such files in a global top-level directory and then specify a relative path, e.g. <code>&lt;img src="images/face-smile.png"&gt</code> for <img src="images/face-smile.png">.  Advanced users can also embed PHP code to access local data directories (see <em>Advanced features</em>).
</p>

<h1>Links as menu entries</h1>

<p>
	Entries in the navigation menu can also directly link to remote pages.  For this purpose, put a file named <code>redirect.inc</code> or <code>redirect_some_mnemonic_suffix.inc</code> in the page directory instead of <code>body.inc</code>.  This redirect file only contains the URL of the target page.  A trick can be used to open such links in new windows, viz. by specifying the URL in the form
</p>
<pre>
http://www.sourceforge.net/" target="_blank
</pre>