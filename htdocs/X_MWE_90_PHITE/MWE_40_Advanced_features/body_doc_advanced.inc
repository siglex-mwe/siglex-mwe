<h2>Advanced features</h2>

<p>
	Since the page body and all text blocks may contain arbitrary PHP code, experienced editors can create very complex and dynamic pages.  CGI parameters and some internal information are provided in the dictionary <code>$PHITE_vars</code>, which hast to be included with the statement <code>&lt;?php global $PHITE_vars; ?&gt;</code>.  See the <a href="http://www.dreammask.com/PHITE.php?sitesig=PT&amp;page=PT_003_Basic_Concepts&amp;subpage=PT_020_PHITE_Environment" target="_blank">PHITE documentation</a> for more information on the contents of this dictionary.
</p>

<p>
	The appearance of a PHITE site can be changed drastically by allowing readers to select between different skins (see <em>Site customisation</em>).  The desired skin is indicated by a CGI parameter or a cookie that can be set automatically by a short piece of PHP code.  A sample skin chooser is included in the <a href="http://www.dreammask.com/PHITE.php?sitesig=PT&amp;page=PT_010_Choose_Skin" target="_blank">PHITE documentation</a>, but has not yet been adapted to the modified framework used here.  Currently, only a single skin is available, as the original PHITE skins cannot be used with the modified framework.
</p>

<h1>Site management &amp; backup</h1>

<p>
	PHP sites can easily be edited with most SFTP clients or in a SSH shell.  Some caution has to be exercised to avoid collisions and possible data loss, since SourceForge implements no precautions to keep multiple users from editing the same file at the same time.  For this reason, it is also important to make regular backups of the entire Web site (SourceForge does not back up any data on the Web server).
</p>
<p>
	Most SFTP clients can download a full site automatically, and editors should create such <em>mirrors</em> of the Web site at regular intervals on their desktop machine or local server (older versions of the mirror can be kept as compressed archives for additional safety).  The main goal of mirroring is to be able to recreate the entire Web site quickly after catastrophic data loss or recover individual files that have accidentally been deleted or corrupted.
</p>
<p>
	A more sophisticated approach is to <em>synchronise</em> a local copy of the Web site with the main version of the SourceForge server, using <code>rsync</code> or similar software (the <a href="http://www.cogsci.uni-osnabrueck.de/~severt/Software.html#HotSync" target="_blank">HotSync</a> utility is a convenient front-end to <code>rsync</code> for this purpose).  Synchronised copies can be edited off-line and the changes can later be uploaded to the server.  Since there is a considerable risk of data loss (changes made by other users on the main server might be overwritten from the local copy), only advanced users should work with off-line copies.  In any case, it is important to coordinate well with the other Web site editors!
</p>
<p>
	In general, files and directories should not be renamed once they have been created, especially if some editors have synchronised copies of the Web site.  File deletion also has to be coordinated in this case, as “ghost copies” of the deleted files might otherwise reappear on the main Web server.
</p>