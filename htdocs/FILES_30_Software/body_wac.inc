<h2>Legacy page</h2>

<p>Please go to our new website <a href="https://multiword.org/">https://multiword.org/</a> for up-to-date information</p>

<h2>Software</h2>

<p>This page lists some useful (free) tools for MWE processing, specially for MWE identification.</p>

<table width="100%" border="0" cellpadding="2">
  <tr>
    <td valign="top" align="left"><strong>Tool name</strong></td>
    <td valign="top" align="left"><strong>Description</strong></td>
    <td valign="top" align="left"><strong>Link</strong></td>    
  </tr>
  
  <tr>
    <td valign="top" align="left">UCS toolkit</td>
    <td valign="top" align="left">Collection of libraries and scripts for the statistical analysis of cooccurrence data</td>
    <td valign="top" align="left"><a href="http://www.collocations.de/software.html" target="_blank">Website</a></td>    
  </tr>
  
  <tr>
    <td valign="top" align="left">mwetoolkit</td>
    <td valign="top" align="left">Research tool that aids in the automatic extraction of multiword units </td>
    <td valign="top" align="left"><a href="http://mwetoolkit.sf.net/" target="_blank">Website</a></td>    
  </tr>
  
  <tr>
    <td valign="top" align="left">Text-NSP</td>
    <td valign="top" align="left">Extract collocations and Ngrams from text</td>
    <td valign="top" align="left"><a href="http://search.cpan.org/dist/Text-NSP/" target="_blank">Website</a></td>    
  </tr>  
  
  <tr>
    <td valign="top" align="left">jMWE</td>
    <td valign="top" align="left">jMWE is a Java library for detecting Multi-Word Expressions (MWE) in text. jMWE is written for Java 1.5.0.</td>
    <td valign="top" align="left"><a href="http://projects.csail.mit.edu/jmwe/" target="_blank">Website</a></td>    
  </tr>    
  <tr>
    <td valign="top" align="left">StringNet</td>
    <td valign="top" align="left">A searchable online knowledgebase of English MWEs.</td>
    <td valign="top" align="left"><a href="http://nav.stringnet.org/" target="_blank">Website</a></td>    
  </tr>   
  <tr>  
  	<td valign="top" align="left">Stanford parser MWE module</td>
    <td valign="top" align="left">A tool for converting French MWEs into a representation that can be used by Stanford parser (included in main parser download file, instructions in README, details in <a href="http://aclweb.org/anthology-new/D/D11/D11-1067.pdf" target="_blank">paper</a>)</td>
    <td valign="top" align="left"><a href="http://nlp.stanford.edu/software/lex-parser.shtml" target="_blank">Website</a></td>   
  </tr>
  
  
</table>  

<p>If you think that your software should be on this list, please send us an <a href="mailto:mwe2010workshop[[--aT--]gmail.com">email</a>.</p>
