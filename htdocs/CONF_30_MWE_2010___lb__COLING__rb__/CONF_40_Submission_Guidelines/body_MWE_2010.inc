<h2>Multiword Expressions: from Theory to Applications (MWE 2010)</h2>

<h3>Workshop at COLING 2010 (Beijing, China), August 28, 2010</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.clres.com/siglex.html" target="_blank">SIGLEX</a>)</p>

<p><small>Last updated: August 03, 2010</small></p>

<h1>Instructions for authors</h1>

<h2>Submission for review</h2>

<p>We invite submissions of original and unpublished work as full
papers. All submissions must follow the format of COLING 2010
proceedings, available at <a href="http://www.coling-2010.org/SubmissionGuideline.htm" target="_blank">http://www.coling-2010.org/SubmissionGuideline.htm</a>.
We strongly advise the use of the provided Word or LaTeX template
files. Full papers should not exceed eight (8) pages for the
content, with one (1) extra page for references, thus the total
lenght should not exceed nine pages (9) inclusive of references.
Reviewing will be double-blind, and thus no author information
should be included in the papers; self-reference should be avoided
as well.</p>

<p>Submission will be electronic in PDF format through the workshop
START system, available at <a href="https://www.softconf.com/coling2010/mwe2010/" target="_blank">https://www.softconf.com/coling2010/mwe2010/</a>.
The papers must be submitted no later than June 6, 2010
at 23:59 PDT (GMT-7).</p>

<p><strong>Submitted papers that do not conform to these requirements will be
rejected without review.</strong> Accepted papers will appear in the workshop
proceedings and will be presented orally.</p>

<h2> Camera-ready version</h2>

<p>The final submission should have no more than:</p>
<ul>
<li><strong>Oral presentations:</strong> eight (8) content pages plus one (1) additional page for references;</li>
<li><strong>Poster presentations:</strong> four (4) pages including both content and references.</li>
</ul>

<p>All final submissions must follow the format of COLING 2010
proceedings, available at <a href="http://www.coling-2010.org/SubmissionGuideline.htm" target="_blank">http://www.coling-2010.org/SubmissionGuideline.htm</a>.
The guidelines have been updated since the first submission: papers must
follow the last guidelines on the COLING website.</p>

<p>Camera-ready papers should include author names and affiliations BUT should not include
country names. The papers should use A4 page format; US letter format is
not acceptable.  The final manuscript should be uploaded in PDF format through the workshop
START system, available at <a href="https://www.softconf.com/coling2010/mwe2010/" target="_blank">https://www.softconf.com/coling2010/mwe2010/</a>.
The final version must be uploaded no later than <strong>July 10, 2010 at 23:59 PDT (GMT-7)</strong>. 
At least one co-author must register to attend the workshop and present
the paper.  Registration should be done through <a href="http://www.coling-2010.org/Registration.htm" target="_blank">COLING 2010 online system</a>.
</p>

<h2> Poster instructions</h2>

The authors of short papers (accepted as posters) should follow the instructions of the main conference for the poster: usable board area of 110cm x 180cm, in portrait orientation. Up-to-date details can be obtained directly at <a href="http://www.coling-2010.org/Poster.htm" target="_blank"/>COLING</a> website.

<h1>Important dates</h1>

<table>
<tr><td><span style="text-decoration:line-through">May 30, 2010</span></td><td><span style="text-decoration:line-through">Paper submission deadline 23:59 PDT (GMT-7)</span></td></tr>
<tr><td><span style="text-decoration:line-through">Jun 06, 2010 (extended)</span></td><td><span style="text-decoration:line-through">Paper submission deadline 23:59 PDT (GMT-7)</span></td></tr>
<tr><td><span style="text-decoration:line-through">Jul 03, 2010</span></td><td><span style="text-decoration:line-through">Notification of acceptance</span></td></tr>
<tr><td><span style="text-decoration:line-through">Jul 10, 2010</span></td><td><span style="text-decoration:line-through">Camera-ready full papers due</span></td></tr>
<tr><td><span style="text-decoration:line-through">Aug 28, 2010</span></td><td><span style="text-decoration:line-through">Workshop</span></td></tr>
</table>


<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwe2010workshop at gmail.com">mwe2010workshop at gmail.com</a></p>

