<h2>Multiword Expressions: from Theory to Applications (MWE 2010)</h2>

<h3>Workshop at COLING 2010 (Beijing, China), August 28, 2010</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.clres.com/siglex.html" target="_blank">SIGLEX</a>)</p>

<p><small>Last updated: August 03, 2010</small></p>

<h1> Workshop Program</h1>

<h3>Saturday, August 28, 2010</h3>

<table>
<tr><td>08:30-08:40 </td><td> Welcome </td></tr>
<tr><td>08:40-09:40 </td><td> Being Theoretical is Being Practical: Multiword Units and Terminological Structure Revitalised </td></tr>
<tr><td>            </td><td> <em>Invited talk by Kyo Kageura </em> </td></tr>
<tr><td>            </td><td> <strong>Session I - Lexical Representation </strong></td></tr>
<tr><td>            </td><td> Chair: Pavel Pecina</td></tr>
<tr><td>09:40-10:05 </td><td> Computational Lexicography of Multi-Word Units. How Efficient Can It Be? </td></tr>
<tr><td>            </td><td><em> Filip Graliński, Agata Savary, Monika Czerepowicka and Filip Makowiecki </em> </td></tr>
<tr><td>10:05-10:30 </td><td> Construction of Chinese Idiom Knowledge-base and Its Applications </td></tr>
<tr><td>            </td><td><em> Lei Wang and Shiwen Yu </em> </td></tr>
<tr><td>10:30-11:00 </td><td> BREAK </td></tr>
<tr><td>            </td><td> <strong>Session II - Identification and Extraction </strong></td></tr>
<tr><td>            </td><td> Chair: Aline Villavicencio</td></tr>
<tr><td>11:00-11:25 </td><td> Automatic Extraction of Arabic Multiword Expressions </td></tr>
<tr><td>            </td><td><em>Mohammed Attia, Antonio Toral, Lamia Tounsi, Pavel Pecina and Josef van Genabith</em> </td></tr>
<tr><td>11:25-11:50 </td><td> Sentence Analysis and Collocation Identification </td></tr>
<tr><td>            </td><td><em> Eric Wehrli, Violeta Seretan and Luka Nerima </em> </td></tr>
<tr><td>11:50-12:15 </td><td> Automatic Extraction of Complex Predicates in Bengali </td></tr>
<tr><td>            </td><td><em>Dipankar Das, Santanu Pal, Tapabrata Mondal, Tanmoy Chakraborty and Sivaji Bandyopadhyay</em> </td></tr>
<tr><td>12:15-13:50 </td><td> LUNCH </td></tr>
<tr><td>            </td><td> <strong>Session III - Applications </strong></td></tr>
<tr><td>            </td><td> Chair: Eric Wehrli</td></tr>
<tr><td>13:50-14:15 </td><td> Handling Named Entities and Compound Verbs in Phrase-Based Statistical Machine Translation </td></tr>
<tr><td>            </td><td><em>Santanu Pal, Sudip Kumar Naskar, Pavel Pecina, Sivaji Bandyopadhyay and Andy Way</em> </td></tr>
<tr><td>14:15-14:40 </td><td> Application of the Tightness Continuum Measure to Chinese Information Retrieval </td></tr>
<tr><td>            </td><td><em> Ying Xu, Randy Goebel, Christoph Ringlstetter and Grzegorz Kondrak </em> </td></tr>
<tr><td>14:40-15:05 </td><td> Standardizing Complex Functional Expressions in Japanese Predicates: Applying Theoretically-Based Paraphrasing Rules </td></tr>
<tr><td>            </td><td><em>Tomoko Izumi, Kenji Imamura, Genichiro Kikui and Satoshi Sato</em> </td></tr>
<tr><td>15:05-15:30 </td><td> <strong> Poster Session </strong></td></tr>
<tr><td>            </td><td> Chair: Carlos Ramisch</td></tr>
<tr><td>            </td><td> <li> Identification of Reduplication in Bengali Corpus and their Semantic Analysis: A Rule Based Approach</li> </td></tr>
<tr><td>            </td><td> <em> Tanmoy Chakraborty and Sivaji Bandyopadhyay </em> </td></tr>
<tr><td>            </td><td> <li>Contrastive Filtering of Domain-Specific Multi-Word Terms from Different Types of Corpora </li></td></tr>
<tr><td>            </td><td> <em> Francesca Bonin, Felice Dell’Orletta, Giulia Venturi and Simonetta Montemagni </em> </td></tr>
<tr><td>            </td><td> <li> A Hybrid Approach for Functional Expression Identification in a Japanese Reading Assistant </li></td></tr>
<tr><td>            </td><td> <em> Gregory Hazelbeck and Hiroaki Saito </em> </td></tr>
<tr><td>            </td><td> <li> An Efficient, Generic Approach to Extracting Multi-Word Expressions from Dependency Trees</li></td></tr>
<tr><td>            </td><td> <em> Scott Martens and Vincent Vandeghinste </em> </td></tr>
<tr><td>15:30-16:00 </td><td> BREAK </td></tr>
<tr><td>16:00-17:00 </td><td> Multiword Expressions as Discourse Relation Markers (DRMs) </td></tr>
<tr><td>            </td><td> <em>Invited talk by Aravind Joshi </em> </td></tr>
<tr><td>17:00-17:50 </td><td> Panel: Multiword Expressions - from Theory to Applications </td></tr>
<tr><td>            </td><td> Moderator: Aline Villavicencio </td></tr>
<tr><td>            </td><td> <li><em>Mona Diab, Columbia University </em></li> </td></tr>
<tr><td>            </td><td> <li><em>Valia Kordoni, Saarland University </em></li> </td></tr>
<tr><td>            </td><td> <li><em>Hans Uszkoreit, Saarland University </em></li> </td></tr>
<tr><td>17:50-18:00 </td><td> Closing Remarks </td></tr>
</table>

<h1> List of accepted papers </h1>

<h2>Oral presentations</h2>

<ul>

<li>Computational Lexicography of Multi-Word Units. How Efficient Can It Be?<br/>
<em>Filip Graliński, Agata Savary, Monika Czerepowicka and Filip Makowiecki</em></li>

<li>Construction of Chinese Idiom Knowledge-base and Its Applications<br/>
<em>Lei Wang and Shiwen Yu</em></li>

<li>Automatic Extraction of Arabic Multiword Expressions <br/>
<em>Mohammed Attia, Antonio Toral, Lamia Tounsi, Pavel Pecina and Josef van Genabith </em></li>

<li>Sentence Analysis and Collocation Identification<br/>
<em>Eric Wehrli, Violeta Seretan and Luka Nerima</em></li>

<li>Automatic Extraction of Complex Predicates in Bengali <br/>
<em>Dipankar Das, Santanu Pal, Tapabrata Mondal, Tanmoy Chakroborty and Sivaji Bandyopadhyay </em></li>

<li>Handling Named Entities and Compound Verbs in Phrase-Based Statistical Machine Translation<br/>
<em>Santanu Pal, Sudip Kumar Naskar, Pavel Pecina and Sivaji Bandyopadhyay</em></li>

<li>Application of the Tightness Continuum Measure to Chinese Information Retrieval<br/>
<em>Ying Xu, Randy Goebel, Christoph Ringlstetter and Grzegorz Kondrak</em></li>

<li>Standardizing Complex Functional Expressions in Japanese Predicates: Applying Theoretically-Based Paraphrasing Rules<br/>
<em>Tomoko Izumi, Kenji Imamura, Genichiro Kikui and Satoshi Sato</em></li>


</ul>

<h2>Poster presentations</h2>

<ul>

<li>Identification of Reduplication in Bengali Corpus and their Semantics Analysis: A Rule Based Approach<br/>
<em>Tanmoy Chakraborty and Sivaji Bandyopadhyay</em></li>

<li>Contrastive Filtering of Domain-Specific Multi-Word Terms from Different Types of Corpora  <br/>
<em>Francesca Bonin, Felice Dell’Orletta, Giulia Venturi and Simonetta Montemagni </em></li>

<li>A Hybrid Approach for Functional Expression Identification in a Japanese Reading Assistant <br/>
<em>Gregory Hazelbeck and Hiroaki Saito </em></li>

<li>An Efficient, Generic Approach to Extracting Multi-Word Expressions from Dependency Trees <br/>
<em>Scott Martens and Vincent Vandeghinste </em></li>
</ul>



