<h2>Multiword Expressions: from Theory to Applications (MWE 2010)</h2>

<h3>Workshop at COLING 2010 (Beijing, China), August 28, 2010</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.clres.com/siglex.html" target="_blank">SIGLEX</a>)</p>

<p><small>Last updated: July 15, 2010</small></p>

<h1>Program Committee</h1>

<ul>
<li>I&ntilde;aki Alegria                     <em>(University of the Basque Country, Spain)</em></li>
<li>Dimitra Anastasiou                <em>(Limerick University, Ireland)</em></li>
<li>Timothy Baldwin                   <em>(University of Melbourne, Australia)</em></li>
<li>Colin Bannard                     <em>(University of Texas at Austin, USA)</em></li>
<li>Francis Bond                      <em>(Nanyang Technological University, Singapore)</em></li>
<li>Paul Cook                         <em>(University of Toronto, Canada)</em></li>
<li>B&eacute;atrice Daille                   <em>(Nantes University, France)</em></li>
<li>Ga&euml;l Dias                         <em>(Beira Interior University, Portugal)</em></li>
<li>Stefan Evert                      <em>(University of Osnabrueck, Germany)</em></li>
<li>Roxana Girju                      <em>(University of Illinois at Urbana-Champaign, USA)</em></li>
<li>Nicole Gr&eacute;goire                   <em>(University of Utrecht, The Netherlands)</em></li>
<li>Chikara Hashimoto                 <em>(National Institute of Information and Communications Technology, Japan)</em></li>
<li>Marti Hearst                      <em>(University of California at Berkeley, USA)</em></li>
<!--<li>Ulrich Heid                       <em>(Stuttgart University, Germany)</em></li>-->
<li>Kyo Kageura                       <em>(University of Tokyo, Japan)</em></li>
<li>Min-Yen Kan                       <em>(National University of Singapore, Singapore)</em></li>
<li>Adam Kilgarriff                   <em>(Lexical Computing Ltd, UK)</em></li>
<li>Su Nam Kim                        <em>(University of Melbourne, Australia)</em></li>
<li>Anna Korhonen                     <em>(University of Cambridge, UK)</em></li>
<li>Zornitsa Kozareva	              <em>(University of Southern California, USA)</em></li>
<li>Brigitte Krenn                    <em>(Austrian Research Institute for Artificial Intelligence, Austria)</em></li>
<li>Cvetana Krstev                    <em>(University of Belgrade, Serbia)</em></li>
<!--<li>Begona Villada Moiron             <em>(Q-go, The Netherlands)</em></li>-->
<li>Rosamund Moon                     <em>(University of Birmingham, UK)</em></li>
<li>Diarmuid &Oacute; S&eacute;aghdha <em>(University of Cambridge, UK)</em></li>
<li>Jan Odijk                         <em>(University of Utrecht, The Netherlands)</em></li>
<li>Stephan Oepen                     <em>(University of Oslo, Norway)</em></li>
<li>Darren Pearce                     <em>(London Knowledge Lab, UK)</em></li>
<li>Pavel Pecina                      <em>(Dublin City University, Ireland)</em></li>
<li>Scott Piao                        <em>(Lancaster University, UK)</em></li>
<li>Thierry Poibeau                   <em>(CNRS and &Eacute;cole Normale Sup&eacute;rieure, France)</em></li>
<li>Elisabete Ranchhod                <em>(University of Lisbon, Portugal)</em></li>
<li>Barbara Rosario                   <em>(Intel Labs, USA)</em></li>
<li>Violeta Seretan                   <em>(University of Geneva, Switzerland)</em></li>
<li>Stan Szpakowicz                   <em>(University of Ottawa, Canada)</em></li>
<li>Beata Trawinski                   <em>(University of Vienna, Austria)</em></li>
<li>Vivian Tsang                      <em>(Bloorview Research Institute, Canada)</em></li>
<li>Kyioko Uchiyama                   <em>(National Institute of Informatics, Japan)</em></li>
<li>Ruben Urizar                      <em>(University of the Basque Country, Spain)</em></li>
<li>Tony Veale                        <em>(University College Dublin, Ireland)</em></li>
</ul>

<h1>Workshop Organizers</h1>

<ul>
<li><a href="http://igm.univ-mlv.fr/~laporte/index_en.html" target="_blank">Eric Laporte</a>           <em>(Universit&eacute; Paris-Est, France)</em></li>
<li><a href="http://www.comp.nus.edu.sg/~nakov/" target="_blank">Preslav Nakov</a>          <em>(National University of Singapore, Singapore)</em></li>
<li><a href="http://www.inf.ufrgs.br/~ceramisch/" target="_blank">Carlos Ramisch</a>         <em>(University of Grenoble, France and Federal University of Rio Grande do Sul, Brazil)</em></li>
<li><a href="http://www.inf.ufrgs.br/~avillavicencio/" target="_blank">Aline Villavicencio</a>    <em>(Federal University of Rio Grande do Sul, Brazil)</em></li>
</ul>

<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwe2010workshop at gmail.com">mwe2010workshop at gmail.com</a></p>

