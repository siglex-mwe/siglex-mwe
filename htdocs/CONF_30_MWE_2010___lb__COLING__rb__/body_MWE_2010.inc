<h2>Multiword Expressions: from Theory to Applications (MWE 2010)</h2>

<h3>Workshop at COLING 2010 (Beijing, China), August 28, 2010</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.clres.com/siglex.html" target="_blank">SIGLEX</a>)</p>

<p><small>Last updated: September 10, 2010</small></p>

<h1>News</h1>

<ul>
	<li><em>September 10, 2010:</em> The <a href="download/Presentations_MWE2010.zip"/>presentation slides</a> are now available!</li>
	<li><em>August 3, 2010:</em> The online <a href="PHITE.php?sitesig=CONF&page=CONF_30_MWE_2010___lb__COLING__rb__&subpage=CONF_50_Online_Proceedings"/>proceedings</a> are now available!</li>
	<li><em>August 3, 2010:</em> The <a href="PHITE.php?sitesig=CONF&page=CONF_30_MWE_2010___lb__COLING__rb__&subpage=CONF_10_Program"/>workshop program</a> was updated with invited talks, session chairs and panelists information.</li>
	<li><em>August 3, 2010:</em> The MWE 2010 workshop is due to proceed as scheduled (in spite of rumors about cancelled workshops). </li>
	<li><em>August 3, 2010:</em> The abstract of Aravind K. Joshi's <a href="PHITE.php?sitesig=CONF&page=CONF_30_MWE_2010___lb__COLING__rb__&subpage=CONF_20_Keynote_Speakers"/>invited talk</a> is now online.</li>
	<li><em>August 3, 2010:</em> Authors of short papers should check the <a href="PHITE.php?sitesig=CONF&page=CONF_30_MWE_2010___lb__COLING__rb__&subpage=CONF_40_Submission_Guidelines"/>poster guidelines</a> section.</li>
	<li><em>July 14, 2010:</em> More details about the <a href="PHITE.php?sitesig=CONF&page=CONF_30_MWE_2010___lb__COLING__rb__&subpage=CONF_20_Keynote_Speakers"/>invited talks</a> are now online.</li>
	<li><em>July 14, 2010:</em> The <a href="PHITE.php?sitesig=CONF&page=CONF_30_MWE_2010___lb__COLING__rb__&subpage=CONF_10_Program"/>workshop program</a> is now online.</li>
	<li><em>July 5, 2010:</em> List of <a href="PHITE.php?sitesig=CONF&page=CONF_30_MWE_2010___lb__COLING__rb__&subpage=CONF_10_Program"/>accepted papers</a> online.</li>
	<li><em>July 5, 2010:</em> Confirmed <a href="PHITE.php?sitesig=CONF&page=CONF_30_MWE_2010___lb__COLING__rb__&subpage=CONF_20_Keynote_Speakers"/>invited speakers</a>.</li>
</ul>

<!--<img width="24" alt="  New: " src="images/new.gif"/>-->
<h1> Description</h1>

<!--<p><a href="http://www.coling-2010.org/Registration.htm" target="_blank">Online registration</a> is open until August 6!</p>-->

<p>The COLING 2010 Workshop on Multiword Expressions: from Theory to 
Applications (MWE 2010) took place on August 28, 2010 in Beijing, China, 
following the 23rd International Conference on Computational Linguistics 
(COLING 2010). The workshop has been held every year since 2003 in conjunction 
with ACL, EACL and LREC; this is the first time that it has been co-located 
with COLING.</p>

<p>Multiword Expressions (MWEs) are a ubiquitous component of natural
languages and appear steadily on a daily basis, both in specialized
and in general-purpose communication. While easily mastered by
native speakers, their interpretation poses a major challenge for
automated analysis due to their flexible and heterogeneous nature.
Therefore, the automated processing of MWEs is desirable for any
natural language application that involves some degree of semantic
interpretation, e.g., Machine Translation, Information Extraction,
and Question Answering.</p>

<p>In spite of the recent advances in the field, there is a wide range of open 
problems that prevent MWE treatment techniques from full integration in current 
NLP systems. In MWE’2010, we were interested in major challenges in the overall 
process of MWE treatment. We thus asked for original research related but not 
limited to the following topics:</p>

<ul>
  <li><strong>MWE resources</strong>: Although underused in most current 
  state-of-the-art approaches, resources are key for developing real-world 
  applications capable of interpreting MWEs. We thus encouraged submissions 
  describing the process of building MWE resources, constructed both manually 
  and automatically from text corpora; we were also interested in assessing the 
  usability of such resources in various MWE tasks.</li>

  <li><strong>Hybrid approaches</strong>: We further invited research on 
  integrating heterogeneous MWE treatment techniques and resources in NLP 
  applications. Such hybrid approaches can aim, for example, at the combination 
  of results from symbolic and statistical approaches, at the fusion of manually 
  built and automatically extracted resources, or at the design of language 
  learning techniques. </li>

  <li><strong> Domain adaptation</strong>: Real-world NLP applications need to 
  be robust to deal with texts coming from different domains. Thus, its is 
  important to assess the performance of MWE methods across domains or 
  describing domain adaptation techniques for MWEs.</li>

  <li><strong> Multilingualism</strong>: Parallel and comparable corpora are 
  gaining popularity as a resource for automatic MWE discovery and treatment. We 
  were thus interested in the integration of MWE processing in multilingual 
  applications such as machine translation and multilingual information 
  retrieval, as well as in porting existing monolingual MWE approaches to new 
  languages.</li>
</ul>

<p>We received 18 submissions, and, given our limited capacity as a one-day 
workshop, we were only able to accept eight full papers for oral presentation: 
an acceptance rate of 44%. We further accepted four papers as posters. The 
regular papers were distributed in three sessions: Lexical Representation, 
Identification and Extraction, and Applications. The workshop also featured two 
invited talks (by Kyo Kageura and by Aravind K. Joshi), and a panel discussion 
(involving Mona Diab, Valia Kordoni and Hans Uszkoreit).</p>

<p>We would like to thank the members of the Program Committee for their timely 
reviews. We would also like to thank the authors and participants for their 
valuable contributions.</p>


<h1>Workshop Organizers</h1>

<ul>
<li><a href="http://igm.univ-mlv.fr/~laporte/index_en.html" target="_blank">Eric Laporte</a>           <em>(Universite Paris-Est, France)</em></li>
<li><a href="http://www.comp.nus.edu.sg/~nakov/" target="_blank">Preslav Nakov</a>          <em>(National University of Singapore, Singapore)</em></li>
<li><a href="http://www.inf.ufrgs.br/~ceramisch/" target="_blank">Carlos Ramisch</a>         <em>(University of Grenoble, France and Federal University of Rio Grande do Sul, Brazil)</em></li>
<li><a href="http://www.inf.ufrgs.br/~avillavicencio/" target="_blank">Aline Villavicencio</a>    <em>(Federal University of Rio Grande do Sul, Brazil)</em></li>
</ul>

<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwe2010workshop at gmail.com">mwe2010workshop at gmail.com</a></p>
