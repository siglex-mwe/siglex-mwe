<h2>Submission information</h2>

<p>There will be three different types of submissions to the workshop:</p>

<ul>
	<li>short papers describing data sets and other evaluation resources made freely available on the community Web page;</li>
	<li>shared task participants, who evaluate an algorithm or MWE extraction system on multiple data sets and discuss implications of their results;</li>
	<li>regular papers addressing the evaluation and comparison of multiword extraction algorithms, but not limited to these topics. Papers that make use of the <a href="http://multiword.sourceforge.net/PHITE.php?sitesig=FILES&page=FILES_20_Data_Sets" target="_blank">collected resources</a> are encouraged.</li>
</ul>

<h1>Format</h1>

<p>Papers must adhere to the format of LREC proceedings (preferably using the <a href="http://www.lrec-conf.org/lrec2008/-Submissions-.html" target="_blank">style files</a> provided on the conference Web site) and must not exceed four (4) pages for short papers (resource and shared task) and eight (8) pages for regular papers. Only submissions in PDF format will be considered. The papers must be submitted no later than 23:59 GMT on <span style="color:red;">3 March 2008</span> (14 March 2008 for shared task papers). Papers submitted after that time cannot be reviewed.</p>

<p>Regular papers will be reviewed by the Program Committee. As reviewing of regular papers will be blind, the paper should not include the authors' names and affiliations. Furthermore, self-citations and other references (e.g. to projects, corpora, or software) that could reveal the author's identity should be avoided. For example, instead of "We previously showed (Smith, 1991) ...", write "Smith previously showed (Smith, 1991) ...".</p>

<p>Short papers are not anonymous and will be briefly reviewed by the workshop organisers for complicance with the shared task requirements.</p>

<p>Please submit your paper here: <a href="https://www.softconf.com/LREC2008/MWE2008/submit.html" target="_blank">https://www.softconf.com/LREC2008/MWE2008/submit.html</a></p>

<p>All accepted papers will be published in the proceedings. The workshop schedule will include short presentations for short papers (with separate sessions for resources and shared task participants), and longer presentations for full papers.</p>

<h1>Deadlines</h1>

<table border="0" cellspacing="4" cellpadding="2">
		<tr>
		<td valign="top">8-page regular paper</td>
		<td width="1em">&nbsp;</td>
		<td valign="top">Mon, 3 March 2008</td>
	</tr>
	<tr>
		<td valign="top">4-page resource paper</td>
		<td width="1em">&nbsp;</td>
		<td valign="top">Fri, 6 March 2008</td>
	</tr>
		<tr>
		<td valign="top">4-page shared task paper</td>
		<td width="1em">&nbsp;</td>
		<td valign="top">Fri, 14 March 2008</td>
	</tr>
</table>