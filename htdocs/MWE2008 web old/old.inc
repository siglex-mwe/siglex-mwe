<h2>Towards a Shared Task for Multiword Expressions (MWE 2008)</h2>

<h3>Workshop at the LREC 2008 Conference (Marrakech, Morocco)</h3>

<p>
	The MWE 2008 Workshop &ndash; <em>Towards a Shared Task for Multiword Expressions</em> &ndash; will take place on Sunday, 1 June 2008 in Marrakech, Morocco, as part of the <a href="http://www.lrec-conf.org/lrec2008/" target="_blank">LREC 2008 Conference</a>.
</p>

<h3>Important dates</h3>

<table border="0" cellspacing="4" cellpadding="2">
	<tr>
		<td valign="top"><s>Fri, 1 Feb 2008</s></td>
		<td width="1em">&nbsp;</td>
		<td valign="top"><s>Resource submission deadline</s></td>
	</tr>
		<tr>
		<td valign="top"><s>Mon, 3 March 2008</s></td>
		<td width="1em">&nbsp;</td>
		<td valign="top"><s>Regular paper (8 pages) extended submission deadline</s></td>
	</tr>
	<tr>
		<td valign="top"><s>Fri, 7 March 2008</s></td>
		<td width="1em">&nbsp;</td>
		<td valign="top"><s>Extended deadline for resource descriptions (4-page paper)</s></td>
	</tr>
	<tr>
		<td valign="top"><s>Wed, 19 March 2008</s></td>
		<td width="1em">&nbsp;</td>
		<td valign="top"><s>Extended deadline for shared task papers (4 pages)</s></td>
	</tr>
	<tr>
		<td valign="top">Wed, 26 March 2008</td>
		<td width="1em">&nbsp;</td>
		<td valign="top">Notification of acceptance</td>
	</tr>
	<tr>
		<td valign="top"><span style="color:red;">Fri, 4 April 2008</span></td>
		<td width="1em">&nbsp;</td>
		<td valign="top">Camera-ready papers due</td>
	</tr>
	<tr>
		<td valign="top">Sun, 1 June 2008</td>
		<td width="1em">&nbsp;</td>
		<td valign="top">Workshop date</td>
	</tr>
</table>

<p>Please note that resource submission remains open after 1 February 2008. People who have submitted their resource before the deadline were invited to submit a 4-page resource description paper to the workshop.</p>