<h2>Call for Evaluation Resources <span style="color:red;">(closed)</span></h2>

<p>
	<span style="font-size:130%; font-weight:bold;">The MWE 2008 Workshop: <i style="color:#0000BB">Towards a Shared Task for Multiword Epressions</i> (at LREC 2008)</span><br />
	<b>endorsed by the ACL Special Interest Group on the Lexicon (SIGLEX)</b>
</p>

<table border="0" cellspacing="4" cellpadding="2">
	<tr><td width="2.5em">&nbsp;</td><td><i>Date:</i></td><td>Sunday, 1 June 2008</td></tr>
	<tr><td width="2.5em">&nbsp;</td><td><i>Location:</i></td><td>Marrakech, Morocco</td></tr>
	<tr><td width="2.5em">&nbsp;</td><td><i>Web page:</i></td><td><a href="http://multiword.sf.net/mwe2008/">http://multiword.sf.net/mwe2008/</a></td></tr>
	<tr><td width="2.5em">&nbsp;</td><td><i>Deadline:</i></td><td>Friday, 1 February 2008 (resources) / <span style="color:red;">Friday, 6 March 2008</span> (papers)</td></tr>
</table>

<h1>Description</h1>

<p>In recent years, considerable progress has been made in our understanding of multiword expressions (MWE), the development of algorithms for their automatic extraction from corpora, and the automatic identification of additional properties such as morphosyntactic preferences or the interpretation of semi-compositional expressions. </p>

<p>It is difficult to compare results of the many published studies on MWEs and obtain a broader perspective, though, because algorithms and implemented systems have been evaluated on vastly different gold standards and corpora, in different languages, for different subtypes of MWEs, etc. In order to make the next big step forward, the field of MWE research needs a shared task in which different approaches are applied to the same data sets, allowing completely new insights to be gained. Since there is as yet not a clear and universally accepted definition of multiword expressions, the first instalment of this shared task will be of a more exploratory nature than the competitions that have been carried out in other areas of computational linguistics. </p>

<p>The MWE 2008 workshop is primarily intended as a forum for collecting, sharing and exploiting MWE evaluation resources. We solicit contributions of such resources from the MWE community, in particular:</p>

<ol>
	<li>manually annotated data sets (MWE candidates marked as true and false positives, or as different subtypes of MWEs);</li>
	<li>data sets of MWEs annotated with additional properties; and</li>
	<li>lists of known MWEs, e.g. from machine-readable dictionaries.</li>
</ol>

<p>In addition, candidate data obtained from corpora with sophisticated proprietary NLP tools may be of interest, helping researchers to apply their statistical MWE identification techniques to a broad range of languages. </p>

<p>The contributed resources will be made available freely for research purposes on <a href="http://multiword.sf.net/">multiword.sf.net</a>, and should be accompanied by documentation (e.g. annotation guidelines) on the SourceForge project wiki. Contributors will be invited to submit a short paper (4 pages) describing their resource and summarising previous research carried out on these data. </p>

<p> After collection of the resources, teams participating in the shared task can evaluate their MWE extraction algorithms on multiple data sets and discuss implications for their generalisability and further development. At the workshop, the evaluation results of the different teams will be summarised and compared. A call for papers and participation in the shared task is being distributed separately. </p>

<h1>Submission information</h1>

<p><strong>Evaluation resources</strong></p>

<p>Please send your resource and documentation to Nicole Grégoire (<a href="mailto:Nicole.Gregoire@let.uu.nl">Nicole.Gregoire@let.uu.nl</a>).  You will then receive an account for the MWE wiki on which you can publish basic information about your resource.</p>

<p>The resources will be made available as a downloadable package on our <a href="http://sourceforge.net/projects/multiword">SourceForge project page</a> under a suitable open-source license (please specify if you require different licensing terms). A list of all available resources will be published on the <a href="http://multiword.sf.net/">multiword.sf.net</a> Web site.</p>

<p>To give shared task participants sufficient time to re-evaluate their models, we set the deadline for submitting resources on 1 February 2008.  Submissions made before the deadline are invited to submit a short paper for the workshop proceedings. Resource submissions after the deadline (and even after the workshop) are of course possible and welcome.</p>

<p><strong>Papers</strong></p>

<p>Short papers describing evaluation resources must adhere to the format of LREC proceedings (preferably using the <a href="http://www.lrec-conf.org/lrec2008/-Submissions-.html" target"=_blank">style files</a> provided on the conference Web site) and must not exceed four (4) pages, including references. Only submissions in PDF format will be considered. The papers must be submitted no later than 23:59 GMT on February 29, 2008. Papers submitted after that time cannot be reviewed.</p>

<p>Please submit your paper here: <a href="https://www.softconf.com/LREC2008/MWE2008/submit.html">https://www.softconf.com/LREC2008/MWE2008/submit.html</a></p>


<h1>Important dates</h1>

<table border="0" cellspacing="4" cellpadding="2">
	<tr><td>01.02.08</td>
		<td>Resource submission deadline</td></tr>
	<tr><td>29.02.08</td>
		<td>Paper submission deadline</td></tr>
	<tr><td>26.03.08</td>
		<td>Notification of acceptance</td></tr>
	<tr><td>04.04.08</td>
		<td>Camera-ready papers due</td></tr>
	<tr><td>01.06.08</td>
		<td>Workshop date</td></tr>
</table>

<h1>Workshop chairs</h1>

<ul>
	<li>Nicole Grégoire, University of Utrecht, The Netherlands</li>
	<li>Stefan Evert, University of Osnabrück, Germany</li>
	<li>Brigitte Krenn, Austrian Research Institute for Artificial Intelligence (ÖFAI), Austria</li>
</ul>

<h1>Contact</h1>

<p>For any inquiries regarding the workshop please contact <a href="Nicole.Gregoire@let.uu.nl">Nicole Grégoire</a>.</p>
