<h2>Call for Papers <span style="color:red;">(closed)</span></h2>

<p>
	<span style="font-size:130%; font-weight:bold;">The MWE 2008 Workshop: <i style="color:#0000BB">Towards a Shared Task for Multiword Epressions</i> (at LREC 2008)</span><br />
	<b>endorsed by the ACL Special Interest Group on the Lexicon (SIGLEX)</b>
</p>

<table border="0" cellspacing="4" cellpadding="2">
	<tr><td width="2.5em">&nbsp;</td><td><i>Date:</i></td><td>Sunday, 1 June 2008</td></tr>
	<tr><td width="2.5em">&nbsp;</td><td><i>Location:</i></td><td>Marrakech, Morocco</td></tr>
	<tr><td width="2.5em">&nbsp;</td><td><i>Web page:</i></td><td><a href="http://multiword.sf.net/mwe2008/">http://multiword.sf.net/mwe2008/</a></td></tr>
	<tr><td width="2.5em">&nbsp;</td><td><i>Deadline:</i></td><td style="color:red;">Monday, 3 March 2008</td></tr>
</table>

<h1>Description</h1>

<p>In recent years, considerable progress has been made in our understanding of multiword expressions (MWE), the development of algorithms for their automatic extraction from corpora, and the automatic identification of additional properties such as morphosyntactic preferences or the interpretation of semi-compositional expressions. </p>

<p>It is difficult to compare results of the many published studies on MWEs and obtain a broader perspective, though, because algorithms and implemented systems have been evaluated on vastly different gold standards and corpora, in different languages, for different subtypes of MWEs, etc. In order to make the next big step forward, the field of MWE research needs a shared task in which different approaches are applied to the same data sets, allowing completely new insights to be gained. Since there is as yet not a clear and universally accepted definition of multiword expressions, the first instalment of this shared task will be of a more exploratory nature than the competitions that have been carried out in other areas of computational linguistics. </p>

<p>The MWE 2008 workshop is primarily intended as a forum for collecting, sharing and exploiting MWE evaluation resources. We have solicited contributions of such resources in a separate call. After collection of the resources, teams are invited to participate in a shared task by evaluating their MWE extraction algorithms on data sets downloaded from <a href="http://multiword.sf.net/">http://multiword.sf.net/</a>. Further instructions will be made available on the workshop Web site, and the full collection of data sets will be online by February 1, 2008.</p>

<p>With this call, we invite submissions of regular papers, in particular (but not limited to) research on:</p>

<ol>
	<li>Linguistic analysis of MWEs based on language resources (such as corpora) and the impact that these studies have on NLP applications. We particularly welcome papers that perform a cross-linguistic analysis of MWEs, identify variation across languages, text types, domains, etc. or investigate the variability of MWEs.</li>
	<li>Typologies of MWEs: Papers that describe classes of MWEs and their representation in language resources, discuss different approaches to the definition and classification of MWEs, or apply new MWE typologies to the evaluation of computational techniques.</li>
	<li>The evaluation and comparison of multiword extraction techniques: Do methods generalise across languages, text types, different classes of MWEs, etc.? How useful and essential is linguistic knowledge and a theoretical understanding of MWEs? Is fully automatic extraction feasible or will manual intervention always be necessary?</li>
	<li>Evaluation methodology and the creation of gold standards for MWEs. Papers should address theoretical and technical issues, while descriptions of existing resources may be submitted as short papers for the shared task. Topics of particular interest are novel types of gold standards (such as human ratings from expert and non-expert subjects, language resources derived from the Web, etc.), inter-annotator agreement in the manual validation of candidate lists (which is often fairly low) and the task-based evaluation of MWE resources in NLP applications.</li>
</ol>

<h1>Submission information</h1>

<p>Regular papers must adhere to the format of LREC proceedings (preferably using the <a href="http://www.lrec-conf.org/lrec2008/-Submissions-.html" target"=_blank">style files</a> provided on the conference Web site) and must not exceed eight (8) pages, including references. Short papers describing evaluation resources and shared task participants will be allowed four (4) pages, using the same formatting. Only submissions in PDF format will be considered.</p>

<p>As reviewing will be blind, the paper should not include the authors' names and 
affiliations. Furthermore, self-citations and other references (e.g. to projects, 
corpora, or software) that could reveal the author's identity should be avoided. For 
example, instead of "We previously showed (Smith, 1991) ...", write "Smith previously 
showed (Smith, 1991) ...".</p>

<p>The papers must be submitted no later than 23:59 GMT on February 29, 2008. Papers submitted after that time cannot be reviewed.</p>

<p>Please submit your paper here: <a href="https://www.softconf.com/LREC2008/MWE2008/submit.html">https://www.softconf.com/LREC2008/MWE2008/submit.html</a></p>


<h1>Important dates</h1>

<table border="0" cellspacing="4" cellpadding="2">
	<tr><td>29.02.08</td>
		<td>Paper submission deadline</td></tr>
	<tr><td>26.03.08</td>
		<td>Notification of acceptance</td></tr>
	<tr><td>04.04.08</td>
		<td>Camera-ready papers due</td></tr>
	<tr><td>01.06.08</td>
		<td>Workshop date</td></tr>
</table>

<h1>Programme committee</h1>

<ul>
	<li>Iñaki Alegria, <i>University of the Basque Country (Spain)</i></li>
	<li>Timothy Baldwin, <i>Stanford University (USA); U of Melbourne (Australia)</i></li>
	<li>Colin Bannard, <i>Max Planck Institute (Germany)</i></li>
	<li>Francis Bond, <i>NTT Communication Science Laboratories (Japan)</i></li>
	<li>Gaël Dias, <i>Beira Interior University (Portugal)</i></li>
	<li>Ulrich Heid, <i>Stuttgart University (Germany)</i></li>
	<li>Kyo Kageura, <i>University of Tokyo (Japan)</i></li>
	<li>Rosamund Moon, <i>University of Birmingham (UK)</i></li>
	<li>Diana McCarthy, <i>University of Sussex (UK)</i></li>
	<li>Eric Laporte, <i>University of Marne-la-Vallee (France)</i></li>
	<li>Preslav Nakov, <i>University of California, Berkeley (USA)</i></li>
	<li>Jan Odijk, <i>University of Utrecht (The Netherlands)</i></li>
	<li>Stephan Oepen, <i>Stanford University (USA); U of Oslo (Norway)</i></li>
	<li>Darren Pearce, <i>University of Sussex (UK)</i></li>
	<li>Pavel Pecina, <i>Charles University (Czech Republic)</i></li>
	<li>Scott Piao, <i>University of Manchester (UK)</i></li>
	<li>Violeta Seretan, <i>University of Geneva (Switzerland)</i></li>
	<li>Suzanne Stevenson	University of Toronto (Canada)</i></li>
	<li>Beata Trawinski, <i>University of Tuebingen (Germany)</i></li>
	<li>Kiyoko Uchiyama, <i>Keio University (Japan)</i></li>
	<li>Begoña Villada Moirón, <i>University of Groningen (The Netherlands)</i></li>
	<li>Aline Villavicencio, <i>Federal University of Rio Grande do Sul (Brazil)</i></li>
</ul>

<h1>Workshop chairs</h1>

<ul>
	<li>Nicole Grégoire, <i>University of Utrecht, The Netherlands</i></li>
	<li>Stefan Evert, <i>University of Osnabrück, Germany</i></li>
	<li>Brigitte Krenn, <i>Austrian Research Institute for Artificial Intelligence (ÖFAI), Austria</i></li>
</ul>

<h1>Contact</h1>

<p>For any inquiries regarding the workshop please contact <a href="Nicole.Gregoire@let.uu.nl">Nicole Grégoire</a>.</p>
