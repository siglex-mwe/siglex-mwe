<h1>Programme committee</h1>

<ul>
	<li>Iñaki Alegria, <i>University of the Basque Country (Spain)</i></li>
	<li>Timothy Baldwin, <i>Stanford University (USA); U of Melbourne (Australia)</i></li>
	<li>Colin Bannard, <i>Max Planck Institute (Germany)</i></li>
	<li>Francis Bond, <i>NTT Communication Science Laboratories (Japan)</i></li>
	<li>Gaël Dias, <i>Beira Interior University (Portugal)</i></li>
	<li>Ulrich Heid, <i>Stuttgart University (Germany)</i></li>
	<li>Kyo Kageura, <i>University of Tokyo (Japan)</i></li>
	<li>Rosamund Moon, <i>University of Birmingham (UK)</i></li>
	<li>Diana McCarthy, <i>University of Sussex (UK)</i></li>
	<li>Eric Laporte, <i>University of Marne-la-Vallee (France)</i></li>
	<li>Preslav Nakov, <i>University of California, Berkeley (USA)</i></li>
	<li>Jan Odijk, <i>University of Utrecht (The Netherlands)</i></li>
	<li>Stephan Oepen, <i>Stanford University (USA); U of Oslo (Norway)</i></li>
	<li>Darren Pearce, <i>University of Sussex (UK)</i></li>
	<li>Pavel Pecina, <i>Charles University (Czech Republic)</i></li>
	<li>Scott Piao, <i>University of Manchester (UK)</i></li>
	<li>Violeta Seretan, <i>University of Geneva (Switzerland)</i></li>
	<li>Suzanne Stevenson	University of Toronto (Canada)</i></li>
	<li>Beata Trawinski, <i>University of Tuebingen (Germany)</i></li>
	<li>Kiyoko Uchiyama, <i>Keio University (Japan)</i></li>
	<li>Begoña Villada Moirón, <i>University of Groningen (The Netherlands)</i></li>
	<li>Aline Villavicencio, <i>Federal University of Rio Grande do Sul (Brazil)</i></li>
</ul>

<h1>Workshop chairs</h1>

<ul>
	<li>Nicole Grégoire, <i>University of Utrecht, The Netherlands</i></li>
	<li>Stefan Evert, <i>University of Osnabrück, Germany</i></li>
	<li>Brigitte Krenn, <i>Austrian Research Institute for Artificial Intelligence (ÖFAI), Austria</i></li>
</ul>

