<h2>Towards a Shared Task for Multiword Expressions (MWE 2008)</h2>

<h3>Workshop at the LREC 2008 Conference (Marrakech, Morocco)</h3>

<p>
	Endorsed by the ACL Special Interest Group on the Lexicon (SIGLEX)
</p>

<table border="0" cellspacing="4" cellpadding="2">
	<tr><td width="2.5em">&nbsp;</td><td><span style="color:#0000BB">Date:</span></td><td>Sunday, 1 June 2008</td></tr>
	<tr><td width="2.5em">&nbsp;</td><td><span style="color:#0000BB">Location:</span></td><td>Marrakech, Morocco</td></tr>
	<tr><td width="2.5em">&nbsp;</td><td><span style="color:#0000BB">Web page:</span></td><td><a href="http://multiword.sf.net/mwe2008/">http://multiword.sf.net/mwe2008/</a></td></tr>
</table>

<h1>Proceedings</h1>

<p>Download the <a href="download/MWE_2008_Proceedings.pdf" target="_blank"><strong>workshop proceedings</strong></a> (PDF, 2MB).</p>

<h1>Programme</h1>
<br>  
 <table cellspacing="0" cellpadding="5" border="0">
<tr><td valign=top>09:15&#8211;09:30</td><td valign=top>Opening</td></tr>
<tr><td valign=top>09:30&#8211;10:30</td><td valign=top>Resource session I</td></tr>

<tr><td valign=top>&nbsp;</td><td valign=top><span style="color:#0000BB">A Resource for Evaluating the Deep Lexical Acquisition of English<br> Verb-Particle Constructions </span><br>
Timothy Baldwin</td></tr>

<tr><td valign=top>&nbsp;</td><td valign=top><span style="color:#0000BB">A lexicographic evaluation of German adjective-noun collocations</span></a><br>
Stefan Evert</td></tr>

<tr><td valign=top>&nbsp;</td><td valign=top><span style="color:#0000BB">Description of evaluation resource -- German PP-verb data</span><br>
Brigitte Krenn</td></tr>

<tr><td valign=top>&nbsp;</td><td valign=top><span style="color:#0000BB">Reference Data for Czech Collocation Extraction</span><br>
Pavel Pecina</td></tr>
<tr><td valign=top>10:30&#8211;11:00</td><td valign=top>Coffee break</td></tr>
<tr><td valign=top>11:00&#8211;13:30</td><td valign=top>Resource session II</td></tr>

<tr><td valign=top>&nbsp;</td><td valign=top><span style="color:#0000BB">A Lexicon of shallow-typed German-English MW-Expressions<br> and a German Corpus of MW-Expressions annotated Sentences</span><br>
Dimitra Anastasiou and Michael Carl</td></tr>

<tr><td valign=top>&nbsp;</td><td valign=top><span style="color:#0000BB">The VNC-Tokens Dataset</span><br>
Paul Cook, Afsaneh Fazly and Suzanne Stevenson</td></tr>

<tr><td valign=top>&nbsp;</td><td valign=top><span style="color:#0000BB">Multi-Word Verbs of Estonian: a Database and a Corpus</span><br>
Heiki-Jaan Kaalep and Kadri Muischnek</td></tr>

<tr><td valign=top>&nbsp;</td><td valign=top><span style="color:#0000BB">A French Corpus Annotated for Multiword Nouns</span><br>
Eric Laporte, Takuya Nakamura and Stavroula Voyatzi</td></tr>

<tr><td valign=top>&nbsp;</td><td valign=top><span style="color:#0000BB">An Electronic Dictionary of French Multiword Adverbs</span><br>
Eric Laporte and Stavroula Voyatzi</td></tr>

<tr><td valign=top>&nbsp;</td><td valign=top><span style="color:#0000BB">Cranberry Expressions in English and in German</span><br>
Beata Trawinski, Manfred Sailer, Jan-Philipp Soehn,<br> Lothar Lemnitzer and Frank Richter</td></tr>

<tr><td valign=top>&nbsp;</td><td valign=top><span style="color:#0000BB">Standardised Evaluation of English Noun Compound Interpretation</span><br>
Su Nam Kim and Timothy Baldwin</td></tr>

<tr><td valign=top>&nbsp;</td><td valign=top><span style="color:#0000BB">Interpreting Compound Nominalisations</span><br>
Jeremy Nicholson and Timothy Baldwin</td></tr>

<tr><td valign=top>&nbsp;</td><td valign=top><span style="color:#0000BB">Paraphrasing Verbs for Noun Compound Interpretation</span><br>
Preslav Nakov</td></tr>

<tr><td valign=top>13:30&#8211;14:30</td><td valign=top>Lunch break</td></tr>

<tr><td valign=top>14:30&#8211;14:50</td><td valign=top>Introduction to shared task and baseline results</td></tr>

<tr><td valign=top>14:50&#8211;15:40</td><td valign=top>Shared task participants</td></tr>

<tr><td valign=top>&nbsp;</td><td valign=top><span style="color:#0000BB">An Evaluation of Methods for the Extraction of Multiword Expressions</span><br>
Carlos Ramisch, Paulo Schreiner, Marco Idiart and Aline Villavicencio</td></tr>

<tr><td valign=top>&nbsp;</td><td valign=top><span style="color:#0000BB">A Machine Learning Approach to Multiword Expression Extraction</span><br>
Pavel Pecina</td></tr>

<tr><td valign=top>15:40&#8211;16:00</td><td valign=top>Thoughts on the first MWEVAL</td></tr>

<tr><td valign=top>16:00&#8211;16:30</td><td valign=top>Coffee break</td></tr>

<tr><td valign=top>16:30&#8211;17:30</td><td valign=top>
		<span style="color:#0000BB">Interactive group discussion</span>
		<ol>
			<li>Plans &amp; tasks for the first MWEVAL competition (Evert)</li>
			<li>What other types of resources &amp; software can be shared?<br>
				Community building: Web site, mailing list, etc. (Gr&eacute;goire)</li>
			<li>Representation formats for MWE resources (Krenn)</li>
		</ol>
	</td></tr>

<tr><td valign=top>17:30&#8211;</td><td valign=top>Workshop summary and General discussion</td></tr>
</table> 

<h1>Workshop format</h1>
 
<p>The time slot for each resource description is 15 minutes which includes one or two questions.</p>
<p>The format of the presentations of shared task participants is 20+5 minutes, i.e. 20 minutes for the presentation and 5 minutes for discussion</p>

<h1>Workshop chairs</h1>

<ul>
	<li>Nicole Grégoire, University of Utrecht, The Netherlands</li>
	<li>Stefan Evert, University of Osnabrück, Germany</li>
	<li>Brigitte Krenn, Austrian Research Institute for Artificial Intelligence (ÖFAI), Austria</li>
</ul>

<h1>Contact</h1>

<p>For any inquiries regarding the workshop please contact <a href="Nicole.Gregoire@let.uu.nl">Nicole Grégoire</a>.</p>
