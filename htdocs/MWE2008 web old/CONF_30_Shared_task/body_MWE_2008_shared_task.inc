<h2>The MWE 2008 Shared Task: <span style="color:#0000BB">Ranking MWE Candidates</span> <span style="color:red;">(closed)</span></h2>

<table border="0" cellspacing="4" cellpadding="2">
	<tr><td width="2.5em">&nbsp;</td><td><i>Submission:</i></td><td>Short paper (4 pages) describing evaluation results (<a href="PHITE.php?sitesig=CONF&amp;page=CONF_50_MWE_2008___lb__LREC__rb__&amp;subpage=CONF_40_Submission_information">submission details</a>)</td></tr>
	<tr><td width="2.5em">&nbsp;</td><td><i>Deadline:</i></td><td><span style="color:red">Wednesday, 19 March 2008</span> (extended)</td></tr>
</table>

<!--
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFF88">
	<tr>
		<td valign="center" align="left"><img src="images/construction_50.png"></td>
		<td valign="center" align="center">
			<span style="font-size:120%; font-weight:bold;">
				<span style="color:red;">This page is currently being edited!!</span><br/>
				Some of the information might not be up to date.
			</span>
		</td>
		<td valign="center" align="right"><img src="images/construction_50.png"></td>
	</tr>
</table>
-->

<h1>Overview</h1>

<p>
	This first, exploratory evaluation campaign focusses on the straightforward task of <strong>ranking MWE candidates</strong>, in order to enable a wide range of systems (any that include a ranking component) to participate in the shared task, bypass lengthy discussions on evaluation paradigms, and give teams more time to adapt their algorithms. As a consequence, only a few of the submitted resources were included in the shared task. We expect a full-fledged competition with a more varied range of tasks to be organised in the near future, making use of the other data sets from this workshop.
</p>

<p> 
	Participating teams have to apply their MWE candidate ranking algorithm to at least <strong>three different data sets</strong>, provided in the form of a list of MWE candidates that have been manually annotated as true positives (TP) and false positives (FP). <strong>Evaluation</strong> is carried out by the teams themselves in terms of n-best precision, precision-recall graphs and average precision. Further analysis and experiments are strongly encouraged (e.g. on different subsets of the candidates, or for different subtypes of MWE). Baseline results for standard association measures will be presented by the workshop organisers.
</p>

<p>
	Teams planning to participate in the shared task should join the <a href="https://lists.sourceforge.net/lists/listinfo/multiword-expressions" target="_blank">multiword mailing list</a> for up-to-date information and discussions, and send a brief <strong>expression of interest</strong> to the list.
</p>

<p>
	Shared task participants will submit a <strong>short paper</strong> (4 pages) with the complete evaluation results of their ranking algorithm on at least
three different data sets. Submissions are not anonymous and will be briefly reviewed by the workshop organisers for complicance with
the shared task requirements (<a
href="PHITE.php?sitesig=CONF&amp;page=CONF_50_MWE_2008___lb__LREC__rb__&amp;subpage=CONF_40_Submission_information">submission
details</a>).
	In addition, participants should make available their full candidate rankings and, if possible, an implementation of their ranking algorithm, to encourage further experiments. Details will be discussed on the multiword mailing list.
</p>

<h1>Task description</h1>

<p>
	Participating teams will evaluate their MWE ranking algorithm on <em>at least two</em> of the data sets listed below, but are encouraged to include all four data sets in their analysis.  Each data set is provided as a list of MWE candidates (which may already have been filtered according to certain criteria) with manual annotation of true and false positives. Further information can be found in the detailed README file inside each archive.
</p>

<p>
	The goal is to re-rank each list such that the “best” candidates are concentrated at the top of the list. If corpus frequency data are provided (links in the data set table below), participating systems should only use these data in order to allow a better comparison. Otherwise, they are free to use any information sources available (such as frequency data from various corpora and Web search engines), <em>except for manual annotations and lexical resources</em>. Always keep in mind that the focus of this shared task is on exploration and <em>better understanding</em>, not on <em>winning a competition</em>.
</p>

<p>
	Supervised learning approaches and algorithms requiring parameter-tuning are allowed to use up to 10% of the candidates as training data. Evaluation results must be provided for the remaining 90% of candidates as test set. Ideally, 10-fold cross-validation should be used, averaging precision and recall across all 10 folds.
</p>

<h1>Data sets</h1>

<p>
	<a href="http://sourceforge.net/project/showfiles.php?group_id=212049&amp;package_id=263330" target="_blank">Click here</a> to download a package with the shared task data sets detailed below. (NB: the English_VPC_Baldwin data set used in the shared task is different from the one in the main package.)
</p>

<table width="100%" border="0" cellpadding="2">
  <tr>
    <td valign="top" align="left"><strong>File name / Contributor</strong></td>
    <td valign="top" align="left"><strong>Description / Further Information</strong></td>
    <tr>
    <td valign="top" align="left"><span style="color:#0000BB">English_VPC_Baldwin_SharedTask</span><br/>
		<a href="mailto:tim@csse.unimelb.edu.au">Timothy Baldwin</a></td>
    <td valign="top" align="left">English Verb-Preposition Combinations<br/>
		<a href="http://multiword.wiki.sourceforge.net/English_VPC_Baldwin_SharedTask" target="_blank">Wiki</a></td>
  </tr>
      <tr>
    <td valign="top" align="left"><span style="color:#0000BB">German_AN_La11t</span><br/>
		<a href="mailto:stefan.evert@uos.de">Stefan Evert</a></td>
    <td valign="top" align="left">Langenscheidt German Adj-N Collocations<br/>
		<a href="http://multiword.wiki.sourceforge.net/German_AN_La11t" target="_blank">Wiki</a> &ndash;
		<a href="http://www.collocations.de/data/Germa_AN_Data_FR.zip">Frequency data</a> (.zip, 32 MB)</td>
  </tr>
     <tr>
    <td valign="top" align="left"><span style="color:#0000BB">German_PNV_Krenn</span><br/>
		<a href="mailto:brigitte.krenn@researchstudio.at">Brigitte Krenn</a></td>
    <td valign="top" align="left">Brigitte Krenn's German PP-Verb Collocations<br/>
		<a href="http://multiword.wiki.sourceforge.net/German_PNV_Krenn" target="_blank">Wiki</a> &ndash;
		<a href="http://www.collocations.de/data/German_PNV_Data_FR.zip">Frequency data</a> (.zip, 56 MB)<br/>
		<em>Evaluation on FR-30 subset is recommended!</em></td>
  </tr>
     <tr>
    <td valign="top" align="left"><span style="color:#0000BB">Czech_Bigram_Pecina</span><br/>
		<a href="mailto:pecina@ufal.mff.cuni.cz">Pavel Pecina</a></td>
    <td valign="top" align="left">Czech MWE Bigrams from Prague Dependency Treebank<br/>
		<a href="http://multiword.wiki.sourceforge.net/Czech_Bigram_Pecina" target="_blank">Wiki</a> &ndash;
		<a href="http://www.collocations.de/data/Czech_Bigram_PDT.zip">Frequency data</a> (.zip, 870 KB)<br/>
		<em>To obtain the complete PDT 2.0, fill in this <a href="http://ufal.mff.cuni.cz/corp-lic/pdt20-reg.html" target="_blank">license form</a> and contact <a href="mailto:pecina@ufal.ms.mff.cuni.cz">Pavel Pecina</a> for download details.</em></td>
  </tr>
</table>

<h1 id="Evaluation">Evaluation</h1>

<p>
	Evaluation follows the paradigm recommended by Evert &amp; Krenn (<a href="http://purl.org/stefan.evert/PUB/EvertKrenn2001.pdf" target="_blank">2001</a>, <a href="http://purl.org/stefan.evert/PUB/EvertKrenn2005.pdf" target="_blank">2005</a>). Evaluation results must be given in terms of <strong>precision-recall graphs</strong> (which plot recall on the x-axis against precision on the y-axis), as well as tables of <strong>n-best precision</strong> and/or <strong>average precision</strong> (the area under precision-recall curve) as an overall measure.
</p>

<p>
	Contestants are encouraged to perform further experiments for a detailed analysis of their algorithm, such as: (1) precision achieved for different frequency bands; (2) application of various filters (e.g. using the “light verb” annotation in  German_PNV_Krenn); (3) precision achieved for different definitions or subtypes of MWE (see comments in README files).
</p>

<p>
	If you have questions about the evaluation paradigm or need assistance, please send an e-mail to the multiword mailing list.
</p>

<ul>
	<li>Download <a href="http://www.collocations.de/data/MWE2008_Evaluation_Package.zip">evaluation package</a> (ZIP archive, 60 kB),  with <a href="http://www.r-project.org/" target="_blank" class="external">R</a> script for precision-recall graphs and average precision, as well as example data.</li>
</ul>

