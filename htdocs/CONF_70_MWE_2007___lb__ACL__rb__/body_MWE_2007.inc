<h2>A Broader Perspective on Multiword Expressions</h2>

<h3>Workshop at the ACL 2007 Conference (Prague, Czech Republic)</h3>
<p>
	<b>endorsed by the ACL Special Interest Group on the Lexicon (SIGLEX)</b>
</p>

<h1>Papers and slides</h1>

<p>Full proceedings: <a href="http://www.aclweb.org/anthology-new/W/W07/W07-11.pdf" target="_blank">pdf</a> - <a href="http://www.aclweb.org/anthology-new/W/W07/W07-11.bib" target="_blank">bib</a><br>Presentation slides: <a href="http://sourceforge.net/project/showfiles.php?group_id=212049&package_id=286671" target="_blank">zip</a></p>

<table cellspacing="0" cellpadding="5" border="0">
<tr><td valign=top></td>
<td valign=top><i>A Measure of Syntactic Flexibility for Automatically Identifying Multiword Expressions in Corpora</i><br>
Colin Bannard</td></tr>
<tr><td valign=top></td>
<td valign=top><i>Distinguishing Subtypes of Multiword Expressions Using Linguistically-Motivated Statistical Measures</i><br>
Afsaneh Fazly and Suzanne Stevenson</td></tr>
<tr><td valign=top></td>
<td valign=top><i>Design and Implementation of a Lexicon of Dutch Multiword Expressions</i><br>
Nicole Gr&eacute;goire</td></tr>
<tr><td valign=top></td>
<td valign=top><i>Semantics-based Multiword Expression Extraction</i><br>
Tim Van de Cruys and Bego&ntilde;a Villada Moir&oacute;n</td></tr>
<tr><td valign=top></td>
<td valign=top><i>Spanish Adverbial Frozen Expressions</i><br>
Dolors Catal&agrave; and Jorge Baptista</td></tr>
<tr><td valign=top></td>
<td valign=top><i>Pulling their Weight: Exploiting Syntactic Forms for the Automatic Identification of Idiomatic Expressions in Context</i><br>
Paul Cook, Afsaneh Fazly and Suzanne Stevenson</td></tr>
<tr><td valign=top></td>
<td valign=top><i>Pauses as an Indicator of Psycholinguistically Valid Multi-Word Expressions (MWEs)?</i><br>
Irina Dahlmann and Svenja Adolphs</td></tr>
<tr><td valign=top></td>
<td valign=top><i>Co-occurrence Contexts for Noun Compound Interpretation</i><br>
Diarmuid &Oacute; S&eacute;aghdha and Ann Copestake</td></tr>
<tr><td valign=top></td>
<td valign=top><i>Learning Dependency Relations of Japanese Compound Functional Expressions</i><br>
Takehito Utsuro, Takao Shime, Masatoshi Tsuchiya, Suguru Matsuyoshi and Satoshi Sato</td></tr>
<tr><td valign=top></td>
<td valign=top><i>Semantic Labeling of Compound Nominalization in Chinese</i><br>
Jinglei Zhao, Hui Liu and Ruzhan Lu</td></tr>
</table> 


<h1>Programme Committee</h1>

<ul>
	<li>Iñaki Alegria, <i>University of the Basque Country (Spain)</i></li>
	<li>Timothy Baldwin, <i>Stanford University (USA); U of Melbourne (Australia)</i></li>
	<li>Francis Bond, <i>NTT Communication Science Laboratories (Japan)</i></li>
	<li>Beatrice Daille, <i>Nantes University (France)</i></li>
	<li>Gaël Dias, <i>Beira Interior University (Portugal)</i></li>
	<li>Kyo Kageura, <i>University of Tokyo (Japan)</i></li>
    <li>Anna Korhonen, <i>University of Cambridge (UK)</i></li>
	<li>Rosamund Moon, <i>University of Birmingham (UK)</i></li>
	<li>Diana McCarthy, <i>University of Sussex (UK)</i></li>
	<li>Eric Laporte, <i>University of Marne-la-Vallee (France)</i></li>
	<li>Preslav Nakov, <i>University of California, Berkeley (USA)</i></li>
	<li>Jan Odijk, <i>University of Utrecht (The Netherlands)</i></li>
	<li>Stephan Oepen, <i>Stanford University (USA); U of Oslo (Norway)</i></li>
	<li>Darren Pearce, <i>University of Sussex (UK)</i></li>
	<li>Scott Piao, <i>University of Manchester (UK)</i></li>
	<li>Violeta Seretan, <i>University of Geneva (Switzerland)</i></li>
	<li>Suzanne Stevenson	University of Toronto (Canada)</i></li>
	<li>Beata Trawinski, <i>University of Tuebingen (Germany)</i></li>
    <li>Vivian Tsang, <i>University of Toronto (Canada)</i></li>
	<li>Kiyoko Uchiyama, <i>Keio University (Japan)</i></li>
	<li>Ruben Urizar, <i>University of the Basque Country (Spain)</i></li>
	<li>Begoña Villada Moirón, <i>University of Groningen (The Netherlands)</i></li>
	<li>Aline Villavicencio, <i>Federal University of Rio Grande do Sul (Brazil)</i></li>
</ul>

<h1>Workshop chairs</h1>

<ul>
	<li>Nicole Grégoire, University of Utrecht, The Netherlands</li>
	<li>Stefan Evert, University of Osnabrück, Germany</li>
	<li>Su Nam Kim, University of Melbourne, Australia</li>
</ul>