<h2>The 11th Workshop on Multiword Expressions (MWE 2015)</h2>

<h3>Workshop at NAACL 2015 (Denver, Colorado, USA), June 4, 2015</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.siglex.org/" target="_blank">SIGLEX</a>) and SIGLEX's Multiword Expressions Section (<a href="http://multiword.sourceforge.net/PHITE.php?sitesig=MWE" target="_blank">SIGLEX-MWE</a>).
</p>


<p><small>Last updated: Apr 24, 2015</small></p>

<!-- ------------------------------------------------------------------------- -->

<h1><a href="mwe2015/invited-talk/Constructional_Meaning_NAACL2.pdf">How Constructions Mean</a></h1>
<p>by <strong><a href="http://www1.icsi.berkeley.edu/~kay" target="_blank">Paul Kay</a></strong> – University of California, Berkeley<br/>
and <strong><a href="http://spot.colorado.edu/~michaeli/index.htm" target="_blank">Laura A. Michaelis</a></strong> – University of Colorado Boulder</p>

<p><strong>Abstract</strong></p>
<p>
   One of the major motivations for a construction-based approach to syntax is that a given rule of syntactic formation can often be associated with more than one semantic specification. For example, a pair of expressions like purple plum and alleged thief call on different rules of semantic combination. The first involves something related to intersection of sets: a purple plum is a member of the set of purple things and of the set of plums. But an alleged thief is not a member of the intersection of the set of thieves and the set of alleged things. Indeed, that intersection is empty, since only a proposition can be alleged and a thief is never a proposition. Constructional approaches recognize as instances of compositionality cases in which two different meanings for the same syntactic form are licensed by two different collections of form-meaning licensors, i.e., by two different collections of constructions. Construction-based grammars are nevertheless compositional in the usual sense: if you know the meanings of the words and you know all the rules that combine words and phrases into larger formal units, while simultaneously combining the meanings of the smaller units into the meanings of the larger ones, then you know the forms and meanings of all the larger units, including all the sentences. Constructional approaches focus on the fact that there are many such rules, and especially on the rules that assign meanings to complex structures. Such approaches do not draw a theoretical distinction between those rules thought to be in the ‘core’ and those considered ‘peripheral’. The construction grammarian conceives of a language as a continuum of generality of expressions; a construction grammar models this continuum with an array of constructions of correspondingly graded generality (Fillmore et al. 1988).</p>
<p>
   This talk surveys the various ways meanings can be assembled in a construction-based grammar, with a focus on the continuum of idiomaticity, a gradient of lexical fixity stretching from frozen idioms, like the salt of the, earth, in the doghouse and under the weather, on the one hand, to fully productive rules on the other, e.g., the rule licensing Kim blinked (the Subject-Predicate construction). The semantics of constructions is the semantics to be discovered along the full length of this gamut. Meanings discussed include: literal meaning, the meanings of constructions that regulate argument expression, context indexation, less commonly recognized illocutionary forces, metalinguistic commentary and topic-focus alignment. We conclude that the seamless integration of relatively idiomatic constructions with more productive ones in actual sentences undermines the notion of a privileged ‘core’ grammar.</p>
