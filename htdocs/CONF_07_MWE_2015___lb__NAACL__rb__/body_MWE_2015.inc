<h2>The 11th Workshop on Multiword Expressions (MWE 2015)</h2>

<h3>Workshop at NAACL 2015 (Denver, Colorado, USA), June 4, 2015</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.siglex.org/" target="_blank">SIGLEX</a>) and SIGLEX's Multiword Expressions Section (<a href="http://multiword.sourceforge.net/PHITE.php?sitesig=MWE" target="_blank">SIGLEX-MWE</a>).
</p>


<p><small>Last updated: Apr 22, 2015</small></p>

<h1>News</h1>

<ul>
  <li><em>Nov 20, 2014:</em> First call for papers</li>
  <li><em>Feb 23, 2015:</em> Second and final call for papers</li>
  <li><em>Mar 4, 2015:</em><b> New submission deadline: Mar 15!</b></li>
  <li><em>Apr 22, 2015:</em> The workshop programme is now available</li>
</ul>

<h1>Important Dates</h1>

<table>
<tr><td><del>Mar 15, 2015</td><td>Submission <strong>deadline</strong> for long &amp; short papers at 23:59 East Coast USA time - GMT-5</del></td></tr>
<tr><td><del>Apr 7, 2015</td><td>Notification of acceptance</del></td></tr>
<tr><td><del>Apr 15, 2015</td><td>Camera-ready papers due</del></td></tr>
<tr><td>Jun 4, 2015</td><td>MWE 2015 Workshop</td></tr>
</table>

<p>Submission and reviewing will be electronic, managed by the START system:</p>
<p align="center"><a href="https://www.softconf.com/naacl2015/mwe" target="_blank">https://www.softconf.com/naacl2015/mwe</a></p>

<h1>Call For Papers</h1>

<p>Under the denomination "multiword expression", one assumes a wide range of linguistic constructions such as idioms (“storm in a teacup”, “sweep under the rug”), fixed phrases (“in vitro”, “by and large”, “rock'n roll”), noun compounds (“olive oil”, “laser printer”), compound verbs (“take a nap”, “bring  about”), etc. While easily mastered by native speakers, their interpretation poses a major challenge for computational systems, due to their flexible and heterogeneous nature.</p>

<p>For a start, MWEs are not nearly as frequent in NLP resources as they are in real-world
text, and this problem of coverage may impact the performance of many NLP tasks. Moreover, treating MWEs also involves problems like determining their semantics, which is not always compositional (“to kick the bucket” meaning “to die”). In sum, MWEs are a key issue and a current weakness for natural language parsing and generation, as well as real-life applications depending on language technology, such as machine translation, just to name a prominent one among many. Thanks to the joint efforts of researchers from several fields working on MWEs, significant progress has been made in recent years, especially concerning the construction of large-scale language resources. For instance, there is a large number of recent papers which focus on acquisition of MWEs from corpora, and others that describe a variety of techniques to find paraphrases for MWEs. Current methods use a plethora of tools such as association measures, machine learning, syntactic patterns, web queries, etc. A considerable body of techniques, resources and tools to perform these tasks are now available, and are indicative of the growing importance of the field within the NLP community.</p>

<p>Many of these advances are described as part of the annual workshop on MWEs, which attracts the attention of an ever-growing community working on a variety of languages and MWE types. The workshop has been held since 2001 in conjunction with major computational linguistics conferences (ACL, EACL, NAACL, COLING, LREC), providing an important venue for the community to interact, share resources and tools and collaborate on efforts for advancing the computational treatment of MWEs. Additionally, special issues on MWEs have been published by leading journals in computational linguistics. The latest such effort is the special issue on “Multiword Expressions: from Theory to Practice and Use”, which has recently been published by the <a href="http://multiword.sourceforge.net/tslp2011si" target="_blank">ACM Transactions on Speech and Language Processing</a>.</p>

<p>MWE 2015 will be the 11th event in the series. We will be interested in major challenges in the overall process of MWE treatment, both from the theoretical and the computational viewpoint, focusing on original research related (but not limited) to the following topics:</p>

<ul>
<li> Lexicon-grammar interface for MWEs </li>
<li> Parsing techniques for MWEs </li>
<li> Hybrid parsing of MWEs </li>
<li> Annotating MWEs in treebanks </li>
<li> MWEs in Machine Translation and Translation Technology </li>
<li> Manually and automatically constructed resources </li>
<li> Representation of MWEs in dictionaries and ontologies </li>
<li> MWEs and user interaction </li>
<li> Multilingual acquisition </li>
<li> Multilingualism and MWE processing </li>
<li> Models of first and second language acquisition of MWEs </li>
<li> Crosslinguistic studies on MWEs </li>
<li> The role of MWEs in the domain adaptation of parsers </li>
<li> Integration of MWEs into NLP applications </li>
<li> Evaluation of MWE treatment techniques </li>
<li> Lexical, syntactic or semantic aspects of MWEs </li>

</ul>

<h1>Submission modalities</h1>

<p>For MWE 2015, we accept the following two types of submissions:</p>
<ul>
<li>Regular long papers (8 content pages + 1 page for references): Long papers should report on solid and finished research including new experimental results, resources and/or techniques.</li> 
<li>Regular short papers (4 content pages + 1 page for references): Short papers should report on small experiments, focused contributions, ongoing research, negative results and/or philosophical discussion.</li>
</ul>

<p>The reported research should be substantially original. The papers will be presented orally or as posters. The decision as to which papers will be presented orally and which as posters will be made by the program committee based on the nature rather than on the quality of the work. All submissions must be in PDF format and must follow the NAACL 2015 formatting requirements (style files available at the <a href=“http://naacl.org/naacl-pubs/” target=“_blank”> NAACL 2015 website</a>). We strongly advise the use of the provided Word or LaTeX template files.</p>

<p>Reviewing will be double-blind, and thus no author information should be included in the papers; self-reference should be avoided as well. </p>

<p>Resources submitted with the papers should be anonymized for submission. Papers and/or resources that do not conform to these requirements will be rejected without review. Accepted papers will appear in the workshop proceedings, where no distinction will be made between papers presented orally or as posters. </p>

<p>Submission and reviewing will be electronic, managed by the START system:</p>
<p align="center"><a href="https://www.softconf.com/naacl2015/mwe" target="_blank">https://www.softconf.com/naacl2015/mwe</a></p>
<p>Submissions must be uploaded onto the START system by the submission deadline: <b>March 15th, 2015</b> (11:59pm, GMT-5).
Please choose the appropriate submission type from the starting submission page, according to the category of your paper.</p>


<h1>Workshop Organizers</h1>

<ul>
<li><a href="https://www.angl.hu-berlin.de/staff/kordoni" target="_blank">Valia Kordoni</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="https://www.angl.hu-berlin.de/staff/1687478" target="_blank">Kostadin Cholakov</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="https://www.angl.hu-berlin.de/staff/markus_egg" target="_blank">Markus Egg</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="http://www.ilsp.gr/en/profile/staff?view=member&id=38&task=show" target="_blank">Stella Markantonatou</a><em> (Institute for Language and Speech Processing (ILSP) - Athena Research Center, Greece)</em></li>
<li><a href="http://cs.haifa.ac.il/~shuly/Shuly_Wintner/Home.html" target="_blank">Shuly Wintner </a><em> (University of Haifa, Israel)</em></li>
</ul>


<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwe2015.naacl@gmail.com">mwe2015.naacl@gmail.com</a></p>

