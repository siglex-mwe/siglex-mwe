<h2>The 11th Workshop on Multiword Expressions (MWE 2015)</h2>

<h3>Workshop at NAACL 2015 (Denver, Colorado, USA), June 4, 2015</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.siglex.org/" target="_blank">SIGLEX</a>) and SIGLEX's Multiword Expressions Section (<a href="http://multiword.sourceforge.net/PHITE.php?sitesig=MWE" target="_blank">SIGLEX-MWE</a>).
</p>


<p><small>Last updated: Nov 20, 2014</small></p>


<h1>Program Committee</h1>

<ul>
<li> Dimitra Anastasiou, University of Bremen (Germany) </li>
<li> Eleftherios Avramidis, DFKI GmbH (Germany)</li>
<li> Tim Baldwin, University of Melbourne (Australia)</li>
<li> Núria Bel, Pompeu Fabra University (Spain)</li>
<li> Lars Borin, University of Gothenburg (Sweden) </li>
<li> Jill Burstein, ETS (USA)</li>
<li> Aoife Cahill, ETS (USA)</li>
<li> Helena Caseli, Federal University of Sao Carlos (Brazil)</li>
<li> Ken Church, IBM Research (USA)</li>
<li> Paul Cook, University of New Brunswick (Canada)</li>
<li> Béatrice Daille, Nantes University (France)</li>
<li> Gaël Dias, University of Caen Basse-Normandie (France)</li>
<li> Stefan Evert, Friedrich-Alexander-Universität Erlangen-Nürnberg (Germany)</li>
<li> Roxana Girju, University of Illinois at Urbana-Champaign (USA)</li>
<li> Ed Hovy, Carnegie Mellon University (USA)</li>
<li> Kyo Kageura, University of Tokyo (Japan)</li>
<li> Su Nam Kim, Monash University (Australia)</li>
<li> Dimitrios Kokkinakis, University of Gothenburg (Sweden)</li>
<li> Ioannis Korkontzelos, University of Manchester (UK)</li>
<li> Lori Levin, Carnegie Mellon University (USA)</li>
<li> Patricia Lichtenstein, University of California, Merced (USA)</li>
<li> Marie-Catherine de Marneffe, The Ohio State University (USA)</li>
<li> Takuya Matsuzaki, Nagoya University (Japan)</li>
<li> Yusuke Miyao, National Institute of Informatics (Japan)</li>
<li> Preslav Nakov, Qatar Computing Research Institute - Qatar Foundation (Qatar) </li>
<li> Malvina Nissim, University of Bologna (Italy)</li>
<li> Joakim Nivre, University of Uppsala (Sweden)</li>
<li> Diarmuid Ó Séaghdha, University of Cambridge and VocalIQ (UK)</li>
<li> Jan Odijk, University of Utrecht (The Netherlands)</li>
<li> Yannick Parmentier, Universite d’Orleans (France)</li>
<li> Pavel Pecina, Charles University Prague (Czech Republic)</li>
<li> Scott Piao, Lancaster University (UK)</li>
<li> Barbara Plank, University of Copenhagen (Denmark)</li>
<li> Carlos Ramisch, Aix-Marseille University (France)</li>
<li> Martin Riedl, University of Darmstadt (Germany)</li>
<li> Will Roberts, Humboldt University Berlin (Germany)</li>
<li> Agata Savary, Université François Rabelais Tours (France)</li>
<li> Violeta Seretan, University of Geneva (Switzerland)</li>
<li> Ekaterina Shutova, University of California, Berkeley (USA)</li>
<li> Beata Trawinski, IDS Mannheim (Germany)</li>
<li> Yulia Tsvetkov, Carnegie Mellon University (USA)</li>
<li> Yuancheng Tu, Microsoft (USA)</li>
<li> Aline Villavicencio, Federal University of Rio Grande do Sul (Brazil)</li>
<li> Veronika Vincze, Hungarian Academy of Sciences (Hungary)</li>
<li> Martin Volk, University of Zurich (Switzerland)</li>
<li> Tom Wasow, Stanford University (USA)</li>
<li> Eric Wehrli, University of Geneva (Switzerland)</li>
</ul>


<h1>Workshop Organizers</h1>

<ul>
<li><a href="https://www.angl.hu-berlin.de/staff/kordoni" target="_blank">Valia Kordoni</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="https://www.angl.hu-berlin.de/staff/1687478" target="_blank">Kostadin Cholakov</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="https://www.angl.hu-berlin.de/staff/markus_egg" target="_blank">Markus Egg</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="http://www.ilsp.gr/en/profile/staff?view=member&id=38&task=show" target="_blank">Stella Markantonatou</a><em> (Institute for Language and Speech Processing (ILSP) - Athena Research Center, Greece)</em></li>
<li><a href="http://cs.haifa.ac.il/~shuly/Shuly_Wintner/Home.html" target="_blank">Shuly Wintner </a><em> (University of Haifa, Israel)</em></li>
</ul>

<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwe2015.naacl@gmail.com">mwe2015.naacl@gmail.com</a></p>
