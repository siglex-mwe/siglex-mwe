<h2>The 11th Workshop on Multiword Expressions (MWE 2015)</h2>

<h3>Workshop at NAACL 2015 (Denver, Colorado, USA), June 4, 2015</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.siglex.org/" target="_blank">SIGLEX</a>) and SIGLEX's Multiword Expressions Section (<a href="http://multiword.sourceforge.net/PHITE.php?sitesig=MWE" target="_blank">SIGLEX-MWE</a>).
</p>


<p><small>Last updated: Apr 22, 2015</small></p>

<h1>Workshop Program</h1>

<h3>Thursday, 4 June 2015</h3>

<table>
  <tr><td></td><td><strong>Oral Session 1</strong></td></tr>
  <tr></tr>
  <tr><td valign="top">09:00–09:30</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">A Method of Accounting Bigrams in Topic Models</span><br/>
    Michael Nokel and Natalia Loukachevitch
  </td>
  <tr><td valign="top">09:30–10:00</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Multiword Expression Identification with Recurring Tree Fragments and Association Measures</span><br/>
    Federico Sangati and Andreas van Cranenburgh
  </td></tr>
  <tr><td valign="top">10:00–10:30</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">How to Account for Idiomatic German Support Verb Constructions in Statistical Machine Translation</span><br/>
    Fabienne Cap, Manju Nirmal, Marion Weller and Sabine Schulte im Walde
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td valign="top">10:30–11:00</td><td valign="top">
    Coffee Break
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
<tr><td></td><td><strong>Oral Session 2</td></tr>
  <tr><td valign="top">11:00–11:20</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">A Multiword Expression Data Set: Annotating Non-Compositionality and Conventionalization for English Noun Compounds</span><br/>
    Meghdad Farahmand, Aaron Smith and Joakim Nivre
  </td></tr>
<tr><td valign="top">11:20–11:40</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Modeling the Statistical Idiosyncrasy of Multiword Expressions</span><br/>
    Meghdad Farahmand and Joakim Nivre
  </td></tr>
<tr><td valign="top">11:40–12:00</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Clustering-based Approach to Multiword Expression Extraction and Ranking</span><br/>
    Elena Tutubalina
  </td></tr>
<tr><td></td><td>&nbsp;</td></tr>
  <tr><td valign="top">12:00–13:00</td><td valign="top">
    Invited Talk: <span style="font-style: italic; color: #2255AA"><a href="mwe2015/invited-talk/Constructional_Meaning_NAACL2.pdf">How constructions mean</a></span><br/>
    Laura A. Michaelis (joint work with Paul Kay)
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
    <tr><td valign="top">13:00–14:00</td><td valign="top">
    Lunch
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td>14:00–14:30</td><td>
    <strong>Poster Booster Session</strong> (5 minutes per poster)
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Never-Ending Multiword Expressions Learning</span><br/>
    Alexandre Rondon, Helena Caseli and Carlos Ramisch
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">The Impact of Multiword Expression Compositionality on Machine Translation Evaluation</span><br/>
    Bahar Salehi, Nitika Mathur, Paul Cook and Timothy Baldwin
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">The Bare Necessities: Increasing Lexical Coverage for Multi-Word Domain Terms with Less Lexical Data</span><br/>
    Branimir Boguraev, Esme Manandise and Benjamin Segal
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Phrase translation using a bilingual dictionary and n-gram data: A case study from Vietnamese to English</span><br/>
    Khang Nhut Lam, Feras Al Tarouti and Jugal Kalita
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Annotation and Extraction of Multiword Expressions in Turkish Treebanks</span><br/>
    Gül¸sen Eryi˘git, Kübra ADALI, Dilara Toruno˘glu-Selamet, Umut Sulubacak and
Tu˘gba Pamay
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Event Categorization beyond Verb Senses</span><br/>
    Aron Marvel and Jean-Pierre Koenig
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td>14:30–15:30</td><td><strong>Poster Session</strong></td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td valign="top">15:30–16:00</td><td valign="top">
    Coffee Break
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
<tr><td></td><td><strong>Oral Session 3</strong></td></tr>
  <tr></tr>
  <tr><td valign="top">16:00–16:30</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Muddying The Multiword Expression Waters: How Cognitive Demand Affects Multiword Expression Production</span><br/>
    Adam Goodkind and Andrew Rosenberg
  </td>
  <tr><td valign="top">16:30–17:00</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Building a Lexicon of Formulaic Language for Language Learners</span><br/>
    Julian Brooke, Adam Hammond, David Jacob, Vivian Tsang, Graeme Hirst and Fraser Shein
  </td></tr>
</table>

<!--
<table>
  <tr><td style="width: 10em;"></td><td>&nbsp;</td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td></td><td><strong>Session:</strong></td></tr>
  <tr><td valign="top">00:00–00:00</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">TITLE</span><br/>
    AUTHORS
  </td></tr>
</table>
-->
