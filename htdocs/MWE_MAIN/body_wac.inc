<h2>Historical SIGLEX-MWE section website</h2>

<h2>Please go to our new website <a href="https://multiword.org/">https://multiword.org/</a></h2>

<!--------------------------------------------------------------------------------------------->

<!--
<p>SIGLEX-MWE is a section of the <a href="http://www.siglex.org">Special Interest Group on the Lexicon (SIGLEX)</a> of the <a href="http://www.aclweb.org/">Association for Computational Linguistics (ACL)</a> .
   It is dedicated to promoting scientific activity on <strong>multiword expressions</strong> (MWEs) in computational linguistics.
   Its main activity is the coordination of the <a href="?sitesig=CONF">MWE workshop series</a> and related events such as the PARSEME shared tasks.
 </p>-->

<!--------------------------------------------------------------------------------------------->

<!--
<h3><a name="membership"></a>Membership</h3>

<p>
  Anyone interested in research in multiword expressions is welcome to <strong>join the section</strong> (free of charge):
  <ul>
  <li>[Mandatory] Register or update your profile on the <a href="https://siglex.org/members.html">SIGLEX member directory</a>.</li>
  <li>[Recommended] Subscribe to the <a href="https://sourceforge.net/projects/multiword/lists/multiword-expressions">MWE mailing list</a>.</li>
  </ul>
</p>
-->

<!--------------------------------------------------------------------------------------------->

<!--
<h3><a name="constitution"></a>Constitution</h3>

<p>Since its ratification on August 23, 2017, the section has its own <a href="MWE_MAIN/SIGLEX-MWE-section-constitution-2020-11-21.pdf">constitution</a>.</p>
-->

<!--------------------------------------------------------------------------------------------->

<!--
<h3><a name="sc"></a>Standing committee</h3>

<p>The section is coordinated by a standing committee composed of:
<ul>
<li><a href="http://cs.unb.ca/~ccook1/">Paul Cook</a> (University of New Brunswick, Canada) - nominated officer in 2020-2022</li>
<li><a href="http://bultreebank.org/en/our-team/petya-osenova/">Petya Osenova</a> (Sofia University and Bulgarian Academy of Sciences, Bulgaria) - nominated officer in 2019-2021</li>
<li><a href="https://shivaat.github.io/">Shiva Taslimipoor</a> (University of Cambridge, UK) - nominated officer in 2020-2022</li>
<li><a href="http://web.iitd.ernet.in/~avaidya/">Ashwini Vaidya</a> (Indian Institute of Technology, Delhi, India) - nominated officer in 2019-2021</li>
<li><a href="https://pageperso.lis-lab.fr/carlos.ramisch/">Carlos Ramisch</a> (Aix Marseille University, France) - elected section representative at the <a href="https://siglex.org/board/2020.html">SIGLEX board</a> in 2020-2022</li>
</ul>
</p>

<p>New officers are welcome every year. If you are a section member, you have published research on MWEs, and you wish to serve the MWE community, please, contact us. Calls for candidates will also be published yearly on the MWE <a href="https://sourceforge.net/projects/multiword/lists/multiword-expressions">mailing list</a>.
</p>

<p>To contact the standing committee, please write to <a href="mailto:siglex-mwe-board@googlegroups.com">siglex-mwe-board@googlegroups.com</a>.</p>

-->

<!--------------------------------------------------------------------------------------------->

<!--

<h3>Past officers</h3>

<ul>
<li><a href="https://www.linkedin.com/in/jelena-mitrovi%C4%87-78354711/">Jelena Mitrović</a> (University of Passau, Germany) - nominated officer in 2018-2020</li>
<li><a href="https://sites.google.com/site/carlaparraescartin/">Carla Parra Escartín</a> (Iconic Translation Machines, Ireland) - nominated officer in 2018-2020</li>
<li><a href="http://www.info.univ-tours.fr/~savary/">Agata Savary</a> (Université François Rabelais Tours, France) - elected section representative at the <a href="https://siglex.org/board/2016.html">SIGLEX board</a> in 2016-2020</li>
<li><a href="http://www.ilsp.gr/en/profile/staff?view=member&id=38&task=show">Stella Markantonatou</a> (Institute for Language and Speech Processing, Greece) - nominated officer in 2017-2019</li>
<li><a href="http://www3.ntu.edu.sg/home/fcbond/">Francis Bond</a> (Nanyang Technological University, Singapore) - nominated officer in 2017-2019</li>
<li><a href="https://www.linkedin.com/in/johanna-monti-03553310/">Johanna Monti</a> ("L'Orientale" University of Naples, Italy) - nominated officer in 2016-2018</li>
<li><a href="https://pageperso.lis-lab.fr/carlos.ramisch/">Carlos Ramisch</a> (Aix Marseille University, France) - nominated officer in 2016-2018</li>
<li><a href="https://www.angl.hu-berlin.de/department/staff-faculty/other/kordoni">Valia Kordoni</a> (Humboldt University of Berlin, Germany) - section representative at the <a href="https://siglex.org/board/2013.html">SIGLEX board</a> in 2013-2016</li>
</ul>

<p>The section's past officers (not currently serving in the standing committee) compose the section's advisory committee.</p>

-->

<!--------------------------------------------------------------------------------------------->

<!--

<h3><a name="reports"></a>Yearly reports</h3>
The standing committee reports yearly on the section's activities to its members.</p>
<ul>
<li><a href="MWE_MAIN/2020-SIGLEX-MWE-yearly-report.pdf">2020 yearly report</a></li>
<li><a href="MWE_MAIN/2019-SIGLEX-MWE-yearly-report.pdf">2019 yearly report</a></li>
<li><a href="MWE_MAIN/2018-SIGLEX-MWE-yearly-report.pdf">2018 yearly report</a></li>
<li><a href="MWE_MAIN/2017-SIGLEX-MWE-yearly-report.pdf">2017 yearly report</a></li>
</ul>
-->
