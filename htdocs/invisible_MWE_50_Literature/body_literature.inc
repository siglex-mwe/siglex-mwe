<h2>Literature related to Multiword Expressions</h2>

The literature section is divided in subsections:

<ul>
	<li>Literature related to the extraction of (subclasses of) MWEs and their properties from corpora.</li>
	<li>Literature related to the linguistic analysis of MWEs, e.g. compositionality, flexibility, etc.</li>
	<li>Literature related to subclasses and definitions.</li>
	<li>Literature related to the representation of (subclasses of) MWEs and their properties from corpora.</li>
	<li>Miscellaneous literature.</li>
</ul>

Feel free to add relevant references.