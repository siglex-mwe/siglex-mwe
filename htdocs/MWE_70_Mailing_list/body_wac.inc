<h2>Mailing list</h2>


<h3>ACTION REQUIRED before March 1st, 2021- mailing list migration</h3>

<p>
The historical mailing list <a href="https://lists.sourceforge.net/lists/listinfo/multiword-expressions">https://lists.sourceforge.net/lists/listinfo/multiword-expressions</a> will be <strong>shut down on March 1, 2021</strong>
</p>

<p>
Please, register to the <a href="https://multiword.org/mailinglist">new mailing list</a>.
</p>
