<h2>Community</h2>

<p>The Multiword Expressions website is intended as a community resource. Anyone interested can contribute. To do so:
<ul>
<li>create a <a href="https://sourceforge.net/user/registration" target="_blank">SourceForge</a> (SF) account,</li>
<li>send your SF login to the <a href="mailto:mwe2017workshop at gmail.com">website maintainers</a>, who will add you to the siglex-mwe project,</li>
<li>login to SF and follow the siglex-mwe <a href="https://gitlab.com/siglex-mwe/siglex-mwe/" target="_blank">manual</a>,
<li>also check the <a href="?sitesig=MWE&page=MWE_90_PHITE_docs">PHITE documentation</a> online.</li>
</ul>
</p>

<h2>Website creators</h2>

<ul>
<li>Stefan Evert</li>
<li>Nicole Grégoire</li>
</ul>

<h2>Website maintainers</h2>

<ul>
<li>Carlos Ramisch</li>
<li>Agata Savary</li>
</ul>
