<h2>Other sites related to Multiword Expressions</h2>

<ul>
	<li>The <a href="http://www.siglex.org/" target="_blank">Special Interest Group on the Lexicon</a> (SIGLEX) of the <a href="http://www.aclweb.org/" target="_blank">Association for Computational Linguistics</a> (ACL) is the main professional organisation for researchers interested in multiword expression.</li>
	<li><a href="http://www.parseme.eu/" target="_blank">PARSEME</a> is a European scientific network on parsing and multiword expressions. It was funded by the <a href="http://www.cost.eu/COST_Actions/ict/IC1207" target="_blank">IC1207 COST action</a> in 2013-2017. Beyond the action duration a good part of its activities were transferred to the SIGLEX MWE section. See also its <a href="https://typo.uni-konstanz.de/parseme/index.php/related-links/spin-off-and-related-national-projects" target="_blank">spin-off and related national projects</a>.</li>
	<li><a href="http://langsci-press.org/catalog/series/pmwe" target="_blank">PMWE</a> (Phraseology and Multiword Expressions) is an Open Access book series at <a href="http://langsci-press.org" target="_blank">Language Science Press</a>.</li>
<!--	<li>The <a href="http://multiword.wiki.sourceforge.net/" target="_blank">MWE wiki</a> hosted by this SourceForge project is a place where researchers can share ideas and document their open-source software and data sets.</li> -->
	<li>The <a href="http://mwe.stanford.edu/" target="_blank">Stanford MWE project</a> offers some links to resources and other Web sites, but there have been no updates since 2004.</li>
	<li>The <a href="http://www.collocations.de/" target="_blank">www.collocations.de</a> site contains comprehensive reference documentation on statistical association measures as well as some other information.  It is slowly going out of date, but plans are under way to replace the old Web site by a collocations wiki that can be updated more easily.</li>
	<li>Have a look at our <a href="http://sourceforge.net/projects/multiword" target="_blank">SourceForge project page</a> for downloads (software &amp; data sets) and further information about the multiword project.</li>
</ul>
