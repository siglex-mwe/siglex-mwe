<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en-US">
	<head>
		<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="{SKIN_FILES}/gila-screen.css" media="screen" title="Gila (screen)" />
		<link rel="stylesheet" type="text/css" href="{SKIN_FILES}/gila-print.css" media="print" />
		<title>{GLOBALSITENAME} {SITETITLE}: {PAGETITLE}</title>
		{META}
	</head>

	<body>
		<!-- For non-visual user agents: -->
		<div id="top">
			<a href="#main-copy" class="doNotDisplay doNotPrint">Skip to main content.</a>
		</div>

		<!-- ##### Header ##### -->

		<div id="header">
			<h1 class="headerTitle">
				<a href="{HOMELINK}">{GLOBALSITENAME} <span>{SITENAME}<span class="doNotDisplay">: {PAGETITLE}</span></span></a>
			</h1>
			<div class="subHeader">
				<span class="doNotDisplay">Main navigation: </span>
<!-- START BLOCK : SUBSITE_NAV -->
				{SUBSITE_SEPARATOR}
				<a href="{SUBSITE_LINK}" class="{SUBSITE_HIGHLIGHT}">{SUBSITE_NAME}</a>
<!-- END BLOCK : SUBSITE_NAV -->
			</div>
		</div>
		
		<div id="side-bar">

			<!-- ##### Left Sidebar ##### -->

			<div class="leftSideBar">
				<p class="sideBarTitle"><a href="{HOMELINK}" class="{HOME_NAV_HIGHLIGHT}">Overview</a></p>
<!-- START BLOCK : LEFT_NAV -->
				<p class="sideBarTitle"><a href="{OPTION_LINK}" class="{OPTION_HIGHLIGHT}">{OPTION_NAME}</a></p>
<!-- START BLOCK : SUB_NAV -->
				<ul>
				<li>
					<a href="{SUB_LINK}" class="{SUB_HIGHLIGHT}">{SUB_NAME}</a>
				</li>
				</ul>
<!-- END BLOCK : SUB_NAV -->
<!-- END BLOCK : LEFT_NAV -->
<!-- START BLOCK : LEFT_BOX -->	  
				<div class="sideBarBlock">
					<p class="sideBarTitle">
						{LB_TITLE}
					</p>
					<div class="sideBarText">
						{LB_CONTENT}
					</div>
				</div>
<!-- END BLOCK : LEFT_BOX -->
			</div>

			<!-- ##### Right Sidebar ##### -->

			<div class="rightSideBar">
<!-- START BLOCK : RIGHT_BOX -->
				<p class="sideBarTitle">
					{RB_TITLE}
				</p>
				<div class="sideBarText">
					{RB_CONTENT}
				</div>
<!-- END BLOCK : RIGHT_BOX -->
			</div>
		</div>

		<!-- ##### Main Copy ##### -->

		<div id="main-copy">      
			{BODY}
			<!-- START BLOCK : CENTER_BOX -->
			<h1>{CENTER_TITLE}</h1>
			{CENTER_CONTENT}
			<!-- END BLOCK : CENTER_BOX -->
		</div>

		<!-- ##### Footer ##### -->

		<div id="footer">
			<div>
				{FOOTER}
			</div>
			
			<div>
				Powered by <a href="http://sourceforge.net/projects/phite" title="PHITE Homepage">PHITE</a> framework developed
				by <a href="http://sourceforge.net/users/fatveg/" title="Chris Robson's page at SourceForge">Chris Robson</a>.
				|
				Design based on 
				<a href="http://capmex.biz/resources/gila" title="Gila Two Homepage">Gila Two</a>
				by <a href="http://capmex.biz/resources/designs-by-haran" title="Designs by Haran">Haran</a>.
			</div>
		</div>
	</body>
</html>
