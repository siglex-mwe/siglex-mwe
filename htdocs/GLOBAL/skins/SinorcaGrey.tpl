<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en-US">
	<head>
		<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="{SKIN_FILES}/css/sinorca-screen.css" media="screen" title="Sinorca (screen)" />
		<link rel="stylesheet" type="text/css" href="{SKIN_FILES}/css/sinorca-print.css" media="print" />
		<title>{SITENAME}: {PAGETITLE}</title>
		{META}
	</head>
	<body>
		<!-- For non-visual user agents: -->
		<div id="top">
			<a href="#main-copy" class="doNotDisplay doNotPrint">Skip to main content.</a>
		</div>
		<!-- ##### Header ##### -->
		<div id="header">
			<div class="superHeader">
				<div class="left">
					<span class="doNotDisplay">SourceForge sites:</span>
					<a href="http://sourceforge.net/projects/multiword">SF Project Page</a> | 
					<a href="http://multiword.wiki.sourceforge.net/">MWE Wiki</a> | 
					<a href="http://www.siglex.org/">ACL SIGLEX</a>
				</div>
				<div class="right">
					<a href="http://multiword.sourceforge.net/"><strong>Welcome to the Multiword Expressions Web</strong></a>
				</div>
			</div>
			<div class="midHeader">
				<h1 class="headerTitle">
					{SITENAME}<span class="doNotDisplay">: {PAGETITLE}</span>
				</h1>
			</div>
			<div class="subHeader">
				<span class="doNotDisplay">Main navigation:</span>
<!-- START BLOCK : SUBSITE_NAV -->
				{SUBSITE_SEPARATOR}
				<a href="{SUBSITE_LINK}" class="{SUBSITE_HIGHLIGHT}">{SUBSITE_NAME}</a>
<!-- END BLOCK : SUBSITE_NAV -->
			</div>
		</div>
		<!-- ##### Side Bar ##### -->
		<div id="side-bar">
			<div class="sideBarFirstBlock">
				<p class="sideBarTitle">
					{HOMENAME}
				</p>
				<ul>
					<li>
						<a href="{HOMELINK}" class="{HOME_NAV_HIGHLIGHT}">Overview</a>
					</li>
<!-- START BLOCK : LEFT_NAV -->
					<li>
						<a href="{OPTION_LINK}" class="{OPTION_HIGHLIGHT}">{OPTION_NAME}</a>
					</li>
<!-- START BLOCK : SUB_NAV -->
					<li>
						<a href="{SUB_LINK}" class="{SUB_HIGHLIGHT}">{SUB_NAME}</a>
					</li>
<!-- END BLOCK : SUB_NAV -->
<!-- END BLOCK : LEFT_NAV -->
				</ul>
			</div>
			<!-- <div class="lighterBackground doNotPrint">
				<p class="sideBarText"></p>
			</div> -->
<!-- START BLOCK : LEFT_BOX -->	  
			<div class="sideBarBlock">
				<p class="sideBarTitle">
					{LB_TITLE}
				</p>
				<div class="sideBarText">
					{LB_CONTENT}
				</div>
			</div>
<!-- END BLOCK : LEFT_BOX -->
		</div>
		<!-- ##### Text Blocks on RHS ##### -->
		<div id="right-blocks">
<!-- START BLOCK : RIGHT_BOX -->
			<div class="rightBlock">
				<p class="rightBlockTitle">
					{RB_TITLE}
				</p>
				<div class="rightBlockText">
					{RB_CONTENT}
				</div>
			</div>
<!-- END BLOCK : RIGHT_BOX -->
		</div>
		<!-- ##### Main Copy ##### -->
		<div id="main-copy">
			{BODY}
			<!-- START BLOCK : CENTER_BOX -->
			<h1>{CENTER_TITLE}</h1>
			{CENTER_CONTENT}
			<!-- END BLOCK : CENTER_BOX -->
		</div>
		<!-- ##### Footer ##### -->
		<div id="footer">
			<div class="left">
				{FOOTER}
			</div>
			<br class="doNotDisplay doNotPrint" />
			<div class="right doNotPrint">
				Powered by <a href="http://sourceforge.net/projects/phite" title="PHITE Homepage">PHITE</a> framework developed
				by <a href="http://sourceforge.net/users/fatveg/" title="Chris Robson's page at SourceForge">Chris Robson</a>.
				<br />
				Design based on 
				<a href="http://capmex.biz/resources/sinorca-gradient" title="Sinorca Gradient Homepage">Sinorca Gradient</a>
				by <a href="http://capmex.biz/resources/designs-by-haran" title="Designs by Haran">Haran</a> and Edgard.
			</div>
		</div>
	</body>
</html>
