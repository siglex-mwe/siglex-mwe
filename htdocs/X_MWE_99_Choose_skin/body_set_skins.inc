<?php global $PHITE_vars;?>

<h1>Choose Skin</h1>

<p>
	If cookies are enabled in your browser, you can select a different layout for the site here:
	<form method="post" enctype="multipart/form-data" action=<?php
		print("'".$PHITE_vars['PHITE_callself']."'"); 
	?>>
		<select name="skin" size="1">
		<?php
			$setskin=$PHITE_vars['PHITE_skin']; 
			if (isset($setskin))
			{
				setcookie('PHITE_skin',$setskin);
			}
			$skindir = './GLOBAL/skins/';
			$d=dir($skindir);
			while($entry=$d->read())
			{
				if (substr($entry,strrpos($entry,'.')+1)=="tpl" && substr($entry,0,1)!='_' && $entry != 'default.tpl')
				{
					$entry = substr($entry,0,strrpos($entry,'.'));
					$selected = (isset($setskin) && $setskin == $entry) ? ' selected="selected"' : ""; 
					print('<option'.$selected.'>'.$entry.'</option>');
				}
			}
		?>
		</select>
		<input type="submit" VALUE="Set Skin" />
	</form>
</p>
