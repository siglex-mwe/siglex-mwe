<h2>Multiword Expressions: from Parsing and Generation to the Real World (MWE 2011)</h2>

<h3>Workshop at ACL 2011 (Portland, Oregon, USA), June 23, 2011</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.clres.com/siglex.html" target="_blank">SIGLEX</a>)</p>

<p><small>Last updated: May 20, 2011</small></p>

<h2>Invited Talks</h2>

<h1><em>How Many Multiword Expressions do People Know?</em></h1>
<p>by <strong><a href="http://http://www.cs.jhu.edu/~kchurch/" target="_blank">Kenneth Church</a></strong> - Johns Hopkins University, MD, USA</p>


<h3>Abstract</h3>

<p>What is a multiword expression (MWE) and how many are there? What is a MWE? What is many? Mark Liberman gave a great invited talk at ACL-89 titled &quot;how many words do people know?&quot; where he spent the entire hour questioning the question. Many of these same questions apply to multiword expressions. What is a word? What is many? What is a person? What does it mean to know? Rather than answer these questions, this talk will use these questions as Liberman did, as an excuse for surveying how such issues are addressed in a variety of fields: computer science, web search, linguistics, lexicography, educational testing, psychology, statistics, etc.
</p>

<h3>Short Bio</h3>

<p>Ken Church is currently the Chief Scientist at HLTCOE (Human Language Technology Center of Excellence) at Hopkins.  He was previously at Microsoft Research and AT&amp;T Labs-Research. He has worked on many topics in computational linguistics including: web search, language modeling, text analysis, spelling correction, word-sense disambiguation, terminology, translation, lexicography, compression, speech (recognition and synthesis) and more.  Honors: AT&T Fellow.  He is the VP of ACL and the President of SIGDAT (the special interest group that runs EMNLP).</p>


<h1><em>MWEs and Topic Modelling: Enhancing Machine Learning with Linguistics</em></h1>
<p>by <strong><a href="http://www.csse.unimelb.edu.au/~tim/" target="_blank">Timothy Baldwin</a></strong> - University of Melbourne, Australia</p>


<h3>Abstract</h3>

<p>Topic modelling is a popular approach to joint clustering of documents and terms, e.g. via Latent Dirichlet Allocation. The standard document representation in topic modelling is a bag of unigrams, ignoring both macro-level document structure and micro-level constituent structure. In this talk, I will discuss recent work on consolidating the micro-level document representation with multiword expressions, and present experimental results which demonstrate that linguistically-richer document representations enhance topic modelling.
</p>

<h3>Short Bio</h3>

<p>Tim Baldwin is an Associate Professor and Deputy Head of the Department of Computer Science and Software Engineering, University of Melbourne and a contributed research staff member of the NICTA Victoria Research Laboratories. He has previously held visiting positions at the University of Washington,  University of Tokyo, University of Saarland, and NTT Communication Science Laboratories. His research interests cover topics including deep linguistic processing, multiword expressions, deep lexical acquisition, computer-assisted language learning, information extraction and web mining, with a particular interest in the interface between computational and theoretical linguistics. Current projects include web user forum mining, information personalisation in museum contexts, biomedical text mining, online linguistic exploration, and intelligent interfaces for Japanese language learners. He is President of the Australasian Language Technology Association in 2011-2012.</p>

<p>Tim completed a BSc(CS/Maths) and BA(Linguistics/Japanese) at the University of Melbourne in 1995, and an MEng(CS) and PhD(CS) at the Tokyo Institute of Technology in 1998 and 2001, respectively. Prior to commencing his current position at the University of Melbourne, he was a Senior Research Engineer at the Center for the Study of Language and Information, Stanford University (2001-2004).
</p>


