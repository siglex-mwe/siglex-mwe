<h2>Multiword Expressions: from Parsing and Generation to the Real World (MWE 2011)</h2>

<h3>Workshop at ACL 2011 (Portland, Oregon, USA), June 23, 2011</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.clres.com/siglex.html" target="_blank">SIGLEX</a>)</p>

<p><small>Last updated: May 03, 2011</small></p>

<h1>Instructions for authors</h1>

<h2>Camera-ready instructions</h2>
<!--<img width="24" alt="  New: " src="images/new.gif"/>-->

<p>The final submission should have no more than:</p>
<ul>
<li><strong>Long papers:</strong> eight (8) content pages plus one (1) additional page for references;</li>
<li><strong>Short papers:</strong> five (5) content pages plus one (1) additional page for references;</li>
<li><strong>Demo papers:</strong> two (2) content pages plus one (1) additional page for references.</li>
</ul>

<p>We are allowing one extra page for short papers than we did for the initial submissions, so that the authors can take the reviews into account. Paper length is independent of presentation format (poster or oral presentation).</p>

<p>As for initial submissions, all final submissions must be in PDF format and must follow the ACL 2011 formatting requirements <strong>STRICTLY</strong> (available at <a href="http://www.acl2011.org/call.shtml#submission" target="_blank"/>http://www.acl2011.org/call.shtml#submission</a>). Camera-ready papers should include author names and affiliations. The final manuscript should be uploaded in PDF format through the workshop START system, available at <a href="https://www.softconf.com/acl2011/mwe" target="_blank">www.softconf.com/acl2011/mwe</a>. The final version must be uploaded no later than <strong>May 09, 2011 at 23:59 PDT (GMT-7)</strong>. At least one co-author must register to attend the workshop and present the paper. Registration should be done through <a href="http://www.aclweb.org/membership/acl2011reg.php" target="_blank">ACL 2011 online system</a>.
</p>

<p>Please check this website periodically when preparing the camera-ready version: we will post up-to-date instructions here.</p>

<h2>Poster instructions</h2>

The authors of long and short papers accepted as posters should follow the instructions of the main conference for the poster. It should be no larger than 48 inches wide and 36 inches high. Up-to-date details can be obtained directly at <a href="http://www.acl2011.org/authors.shtml" target="_blank"/>ACL 2011</a> website.

<h2>Demo session</h2>

<p>Demo presenters who wish to prepare experiments on MWE identification based on a toy corpus may use this <a href="download/demo2011.zip">preprocessed toy corpus</a>. The toy corpus is a fragment of the English part of the <a href="http://statmt.org/europarl/">Europarl corpus</a>, containing the first 20,000 sentences of an old version (Europarl v3). It is made available as a text file in UTF-8 character encoding, contains one sentence per line, was automatically tokenized and lowercased (except for proper names) and has placeholders for numbers.</p>

<p>This toy corpus is provided as a suggestion to demonstrate how the systems work, but authors are free to chose whether they want to use it and, if they do, how they will process the corpus (possibly in combination with external sources).</p>

<h2>Submission for review</h2>

<!--<p>We invite submissions of original and unpublished work as full
papers. All submissions must follow the format of COLING 2010
proceedings, available at <a href="http://www.coling-2010.org/SubmissionGuideline.htm" target="_blank">http://www.coling-2010.org/SubmissionGuideline.htm</a>.
We strongly advise the use of the provided Word or LaTeX template
files. Full papers should not exceed eight (8) pages for the
content, with one (1) extra page for references, thus the total
lenght should not exceed nine pages (9) inclusive of references.
Reviewing will be double-blind, and thus no author information
should be included in the papers; self-reference should be avoided
as well.</p>

<p>Submission will be electronic in PDF format through the workshop
START system, available at <a href="https://www.softconf.com/coling2010/mwe2010/" target="_blank">https://www.softconf.com/coling2010/mwe2010/</a>.
The papers must be submitted no later than June 6, 2010
at 23:59 PDT (GMT-7).</p>

<p><strong>Submitted papers that do not conform to these requirements will be
rejected without review.</strong> Accepted papers will appear in the workshop
proceedings and will be presented orally.</p>-->



<!--
<h1>Important dates</h1>

<table>
<tr><td><span style="text-decoration:line-through">May 30, 2010</span></td><td><span style="text-decoration:line-through">Paper submission deadline 23:59 PDT (GMT-7)</span></td></tr>
<tr><td><span style="text-decoration:line-through">Jun 06, 2010 (extended)</span></td><td><span style="text-decoration:line-through">Paper submission deadline 23:59 PDT (GMT-7)</span></td></tr>
<tr><td><span style="text-decoration:line-through">Jul 03, 2010</span></td><td><span style="text-decoration:line-through">Notification of acceptance</span></td></tr>
<tr><td><span style="text-decoration:line-through">Jul 10, 2010</span></td><td><span style="text-decoration:line-through">Camera-ready full papers due</span></td></tr>
<tr><td><span style="text-decoration:line-through">Aug 28, 2010</span></td><td><span style="text-decoration:line-through">Workshop</span></td></tr>
</table>
-->

<p><strong>A selection of papers will be chosen for a journal publication.</strong></p>

<p>MWE 2011 introduces three different submission modalities:</p>

<ul>
<li><strong>Regular long papers</strong> <em>(8 content pages + 1 page for references):</em> Long papers should report on solid and finished research including new experimental results, resources and/or techniques.</li>
<li><strong>Regular short papers</strong> <em>(4 content pages + 1 page for references):</em>
Short papers should report on small experiments, focused contributions, ongoing research, negative results and/or philosophical discussion.</li>
<li><strong>System demonstration</strong> <em>(2 pages):</em> System demonstration papers should describe and document the demonstrated system or resources. We encourage the demonstration of both early research prototypes and mature systems, that will be presented in a separate demo session.</li>
</ul>

<p>All submissions must be in PDF format and must follow the ACL 2011 formatting requirements (available at <a href="http://www.acl2011.org/call.shtml#submission" target="_blank"/>http://www.acl2011.org/call.shtml#submission</a>). We strongly advise the use of the provided Word or LaTeX template files. For regular long and short papers, the reported research should be substantially original. The papers will be presented orally or as posters. The decision as to which paper will be presented orally and which as poster will be made by the program committee based on the nature rather than on the quality of the work.</p>

<p>Following the example of major conferences like ACL-HLT 2011, this year we will also accept papers accompanied by the resource (software or data) described in the paper. Resources will be reviewed separately and the final acceptance decision will be made based on both the resource reviews and the paper reviews. The software or data resources submitted should be ready for release and should contain at a README file. All resources will be made available to the MWE community.</p>

<p>Reviewing will be double-blind, and thus no author information should be included in the papers; self-reference should be avoided as well. Resources submitted with the papers should be anonymized for submission. Papers and/or resources that do not conform to these requirements will be rejected without review. Accepted papers will appear in the workshop proceedings, where no distinction will be made between papers presented orally or as posters.</p>

<p>Submission will be electronic in PDF format through the workshop START system, available at:</p> 

<p><a href="https://www.softconf.com/acl2011/mwe/" target="_blank">https://www.softconf.com/acl2011/mwe/</a></p>

<p> Please chose the appropriate submission type from the starting submission page, according to the category of your paper, and remember that long and short papers must be submitted no later than <span style="text-decoration:line-through">March 4, 2011</span> April 1, 2011 at 23:59 PDT (GMT-7) and demo papers must be submitted no later than <span style="text-decoration:line-through">March 11, 2011</span> April 8, 2011 at 23:59 PDT (GMT-7).</p>

<h1>Important dates</h1>

<table>
<tr><td><span style="text-decoration:line-through">Mar 4, 2011</span></td><td><span style="text-decoration:line-through">Long paper submission deadline 23:59 PDT (GMT-7)</span></td></tr>
<tr><td><span style="text-decoration:line-through">Mar 11, 2011</span></td><td><span style="text-decoration:line-through">Short paper and demo submission deadline 23:59 PDT (GMT-7)</span></td></tr>
<tr><td><span style="text-decoration:line-through">Apr 01, 2011</span></td><td><span style="text-decoration:line-through">EXTENDED long paper and short paper deadline 23:59 PDT (GMT-7)</span></td></tr>
<tr><td><span style="text-decoration:line-through">Apr 08, 2011</span></td><td><span style="text-decoration:line-through">EXTENDED demo submission deadline 23:59 PDT (GMT-7)</span></td></tr>
<tr><td><span style="text-decoration:line-through">May 2, 2011</span></td><td><span style="text-decoration:line-through">Notification of acceptance</span></td></tr>
<tr><td><span style="text-decoration:line-through">May 9, 2011</span></td><td><span style="text-decoration:line-through">Camera-ready deadline</span></td></tr>
<tr><td><span style="text-decoration:line-through">Jun 23, 2011</span></td><td><span style="text-decoration:line-through">Workshop</span></td></tr>
</table>

<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwe2011 at gmail.com">mwe2011 at gmail.com</a></p>



