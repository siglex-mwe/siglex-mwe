<h2>Multiword Expressions: from Parsing and Generation to the Real World (MWE 2011)</h2>

<h3>Workshop at ACL 2011 (Portland, Oregon, USA), June 23, 2011</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.clres.com/siglex.html" target="_blank">SIGLEX</a>)</p>

<!--<p><strong>Submission deadline EXTENDED:<br/> <span style="text-decoration:line-through">Long &amp; short papers - Apr 01, 2011 at 23:59 PDT (GMT-7)</span><br/> <span style="text-decoration:line-through">Demo papers - Apr 08, 2011 at 23:59 PDT (GMT-7)</span></strong></p>-->

<!--<p>Electronic submission: <a href="https://www.softconf.com/acl2011/mwe/" target="_blank">https://www.softconf.com/acl2011/mwe/</a></p>-->

<p><small>Last updated: Aug 24, 2011</small></p>

<h1>News</h1>

<ul>
	<li><em>August 24, 2011:</em> The <a href="download/Presentations_MWE2011.zip"/>presentation slides</a> are now available!</li>
    <li><em>June 20, 2011: </em>The proceedings are available at the <a href="http://aclweb.org/anthology-new/W/W11/#0800" >ACL anthology</a>.</li>
    <li><em>May 20, 2011: </em>Updated <a href="PHITE.php?sitesig=CONF&page=CONF_20_MWE_2011___lb__ACL__rb__&subpage=CONF_10_Program">program</a> and <a href="PHITE.php?sitesig=CONF&page=CONF_20_MWE_2011___lb__ACL__rb__&subpage=CONF_20_Keynote_Speakers">keynote speakers</a>.</li>
    <li><em>May 10, 2011: </em>The preliminary program is <a href="PHITE.php?sitesig=CONF&page=CONF_20_MWE_2011___lb__ACL__rb__&subpage=CONF_10_Program">ONLINE</a>!</li>
    <li><em>May 03, 2011: </em>The acceptance notifications have been sent! The list of accepted papers can be viewed <a href="PHITE.php?sitesig=CONF&page=CONF_20_MWE_2011___lb__ACL__rb__&subpage=CONF_10_Program">here</a>. Authors should check the up-to-date <a href="PHITE.php?sitesig=CONF&page=CONF_20_MWE_2011___lb__ACL__rb__&subpage=CONF_40_Submission_Guidelines">submission guidelines</a></li>
    <li><em>April 18, 2011: </em>Due to the deadline extension, the acceptance notifications will be sent on April 29, 2011. Check the updated dates below.</li>
    <li><em>April 4, 2011: </em>Ken Church and Tim Baldwin are our <a href="PHITE.php?sitesig=CONF&page=CONF_20_MWE_2011___lb__ACL__rb__&subpage=CONF_20_Keynote_Speakers">keynote speakers</a>!</li>
    <li><em>April 1, 2011: </em>The submission system will stay open for demo papers until April 8, 2011. However, we ask the authors to submit an abstract or a preliminary version until April 4, 2011. Also check our new <a href="PHITE.php?sitesig=CONF&page=CONF_20_MWE_2011___lb__ACL__rb__&subpage=CONF_40_Submission_Guidelines">submission guidelines</a> for demo presentations.</li>    
    <li><em>March  25, 2011: </em>Due to the problems in Japan, we have extended the long paper submission deadline to the 1st of April, 2011.</li>
    <li><em>March  03, 2011: </em>Deadline extension! :-)</li> 
    <li><em>January  13, 2011: </em>The START submission system is now open! Please check the detailed <a href="PHITE.php?sitesig=CONF&page=CONF_20_MWE_2011___lb__ACL__rb__&subpage=CONF_40_Submission_Guidelines">submission guidelines</a>.</li> 
    <li><em>December 09, 2010: </em>The website and first CFP of <em>MWE 2011: from Parsing and Generation to the real world</em> are now online!</li>      
<!--	<li><em>September 10, 2010:</em> The <a href="download/Presentations_MWE2010.zip"/>presentation slides</a> are now available!</li>
	<li><em>August 3, 2010:</em> The online <a href="PHITE.php?sitesig=CONF&page=CONF_30_MWE_2010___lb__COLING__rb__&subpage=CONF_50_Online_Proceedings"/>proceedings</a> are now available!</li>
	<li><em>August 3, 2010:</em> The <a href="PHITE.php?sitesig=CONF&page=CONF_30_MWE_2010___lb__COLING__rb__&subpage=CONF_10_Program"/>workshop program</a> was updated with invited talks, session chairs and panelists information.</li>
	<li><em>August 3, 2010:</em> The MWE 2010 workshop is due to proceed as scheduled (in spite of rumors about cancelled workshops). </li>
	<li><em>August 3, 2010:</em> The abstract of Aravind K. Joshi's <a href="PHITE.php?sitesig=CONF&page=CONF_30_MWE_2010___lb__COLING__rb__&subpage=CONF_20_Keynote_Speakers"/>invited talk</a> is now online.</li>
	<li><em>August 3, 2010:</em> Authors of short papers should check the <a href="PHITE.php?sitesig=CONF&page=CONF_30_MWE_2010___lb__COLING__rb__&subpage=CONF_40_Submission_Guidelines"/>poster guidelines</a> section.</li>
	<li><em>July 14, 2010:</em> More details about the <a href="PHITE.php?sitesig=CONF&page=CONF_30_MWE_2010___lb__COLING__rb__&subpage=CONF_20_Keynote_Speakers"/>invited talks</a> are now online.</li>
	<li><em>July 14, 2010:</em> The <a href="PHITE.php?sitesig=CONF&page=CONF_30_MWE_2010___lb__COLING__rb__&subpage=CONF_10_Program"/>workshop program</a> is now online.</li>
	<li><em>July 5, 2010:</em> List of <a href="PHITE.php?sitesig=CONF&page=CONF_30_MWE_2010___lb__COLING__rb__&subpage=CONF_10_Program"/>accepted papers</a> online.</li>
	<li><em>July 5, 2010:</em> Confirmed <a href="PHITE.php?sitesig=CONF&page=CONF_30_MWE_2010___lb__COLING__rb__&subpage=CONF_20_Keynote_Speakers"/>invited speakers</a>.</li>-->
</ul>


<h1>Call For Participation</h1>

<p><a href="http://www.aclweb.org/membership/acl2011reg.php" target="_blank">Early bird registration</a> deadline is May 23, 2011!</p>

<!--<p>The COLING 2010 Workshop on Multiword Expressions: from Theory to 
Applications (MWE 2010) took place on August 28, 2010 in Beijing, China, 
following the 23rd International Conference on Computational Linguistics 
(COLING 2010). The workshop has been held every year since 2003 in conjunction 
with ACL, EACL and LREC; this is the first time that it has been co-located 
with COLING.</p>

<p>Multiword Expressions (MWEs) are a ubiquitous component of natural
languages and appear steadily on a daily basis, both in specialized
and in general-purpose communication. While easily mastered by
native speakers, their interpretation poses a major challenge for
automated analysis due to their flexible and heterogeneous nature.
Therefore, the automated processing of MWEs is desirable for any
natural language application that involves some degree of semantic
interpretation, e.g., Machine Translation, Information Extraction,
and Question Answering.</p>

<p>In spite of the recent advances in the field, there is a wide range of open 
problems that prevent MWE treatment techniques from full integration in current 
NLP systems. In MWE’2010, we were interested in major challenges in the overall 
process of MWE treatment. We thus asked for original research related but not 
limited to the following topics:</p>

<ul>
  <li><strong>MWE resources</strong>: Although underused in most current 
  state-of-the-art approaches, resources are key for developing real-world 
  applications capable of interpreting MWEs. We thus encouraged submissions 
  describing the process of building MWE resources, constructed both manually 
  and automatically from text corpora; we were also interested in assessing the 
  usability of such resources in various MWE tasks.</li>

  <li><strong>Hybrid approaches</strong>: We further invited research on 
  integrating heterogeneous MWE treatment techniques and resources in NLP 
  applications. Such hybrid approaches can aim, for example, at the combination 
  of results from symbolic and statistical approaches, at the fusion of manually 
  built and automatically extracted resources, or at the design of language 
  learning techniques. </li>

  <li><strong> Domain adaptation</strong>: Real-world NLP applications need to 
  be robust to deal with texts coming from different domains. Thus, its is 
  important to assess the performance of MWE methods across domains or 
  describing domain adaptation techniques for MWEs.</li>

  <li><strong> Multilingualism</strong>: Parallel and comparable corpora are 
  gaining popularity as a resource for automatic MWE discovery and treatment. We 
  were thus interested in the integration of MWE processing in multilingual 
  applications such as machine translation and multilingual information 
  retrieval, as well as in porting existing monolingual MWE approaches to new 
  languages.</li>
</ul>

<p>We received 18 submissions, and, given our limited capacity as a one-day 
workshop, we were only able to accept eight full papers for oral presentation: 
an acceptance rate of 44%. We further accepted four papers as posters. The 
regular papers were distributed in three sessions: Lexical Representation, 
Identification and Extraction, and Applications. The workshop also featured two 
invited talks (by Kyo Kageura and by Aravind K. Joshi), and a panel discussion 
(involving Mona Diab, Valia Kordoni and Hans Uszkoreit).</p>

<p>We would like to thank the members of the Program Committee for their timely 
reviews. We would also like to thank the authors and participants for their 
valuable contributions.</p>-->

<p>Under the denomination &quot;Multiword Expression&quot;, one can hang a wide range of linguistic constructions such as idioms (a frog in the throat, kill some time), fixed phrases (per se, by and large, rock'n roll), noun compounds (telephone booth, cable car), compound verbs (give a presentation, go by [a name]), etc. While easily mastered by native speakers, their interpretation poses a major challenge for computational systems, due to their flexible and heterogeneous nature. Surprisingly enough, MWEs are not nearly as frequent in NLP resources (dictionaries, grammars) as they are in real-word text, where they have been reported to account for over 70% of the terms in a domain. Thus, MWEs are a key issue and a current weakness for tasks like Natural Language Parsing (NLP) and Generation (NLG), as well as real-life applications such as Machine Translation.</p>

<!--<p>Research in the field of MWEs has made significant progress in recent years, especially concerning the construction of large-scale language resources, and this is reflected on the large number of papers that focus on data-driven (semi-)automatic identification, extraction and/or acquisition of multiword units from corpora. Current identification methods use a plethora of computational and statistical tools such as association measures, machine learning, syntactic patterns, web text mining, word alignment, etc. A considerable body of techniques, resources and tools to perform automatic extraction of expressions from text are now available and are indicative of the growing importance of the field of automatic MWE acquisition within the NLP community.</p>

<p>A clear evidence of this "change of status" is that the annual workshop on MWEs attracts the attention of an ever-growing community. It has been held since 2001 in conjunction with major computational linguistics conferences (ACL, COLING, LREC and EACL). Additionally, special issues on MWEs have been published by leading journals in Computational Linguistics. Researchers from several fields view MWEs as a key problem in current NLP technology. And yet, there are still important and urgent matters to be solved.</P>-->

<p>MWE 2011 will be the 8th event in the series, and the time has come to move from basic preliminary research and theoretical results to actual applications in real-world NLP tasks. Therefore, following further the trend of previous MWE workshops, we propose a turn towards MWEs on NLP applications, specifically towards Parsing and Generation of MWEs, as
there is a wide range of open problems that prevent MWE treatment techniques to be fully integrated in current NLP systems. We will be asking for original research related (but not limited) to the following topics:</p>

<ul>
<li> <strong>Lexical representations:</strong> In spite of several proposals for MWE representation ranging along the continuum from  words-with-spaces to compositional approaches
connecting lexicon and grammar, to date, it remains unclear how MWEs should be represented in electronic dictionaries, thesauri and grammars. New methodologies that take into account the type of MWE and its properties are needed for efficiently handling manually and/or automatically acquired expressions in NLP systems. Moreover, we also need strategies to represent deep attributes and semantic properties for these multiword entries.</li>

<li> <strong> Application-oriented evaluation:</strong> Evaluation is a crucial aspect for MWE research. Various evaluation techniques have been proposed, from manual inspection of top-n candidates to classic precision/recall measures. However, only application-oriented techniques can give a clear indication of whether the acquired MWEs are really useful. We call for submissions that study the impact of MWE handling in applications such as Parsing, Generation, Information Extraction, Machine Translation, Summarization, etc.</li>

<li> <strong> Type-dependent analysis:</strong> While there is no unique definition or classification of MWEs, most researchers agree on some major classes such as named entities,
collocations, multiword terminology and verbal expressions. These, though, are very heterogeneous in terms of syntactic and semantic properties, and should thus be treated differently by applications. Type-dependent analyses could shed some light on the best methodologies to integrate MWE knowledge in our analysis and generation systems.</li>

<li><strong> MWE engineering:</strong> Where do my MWEs go after being extracted? Do they belong to the lexicon and/or to the grammar? In the pipeline of linguistic analysis and/or generation, where should we insert MWEs? And even more important: HOW? Because all the effort put in automatic MWE extraction will not be useful if we do not know how to employ these rich resources in our real-life NLP applications!</li>
</ul>

<!--<h1>Submissions</h1>

<p>MWE 2011 introduces three different submission modalities:</p>

<ul>
<li><strong>Regular long papers</strong> <em>(8 content pages + 1 page for references):</em> Long papers should report on solid and finished research including new experimental results, resources and/or techniques.</li>
<li><strong>Regular short papers</strong> <em>(4 content pages + 1 page for references):</em>
Short papers should report on small experiments, focused contributions, ongoing research, negative results and/or philosophical discussion.</li>
<li><strong>System demonstration</strong> <em>(2 pages):</em> System demonstration papers should describe and document the demonstrated system or resources. We encourage the demonstration of both early research prototypes and mature systems, that will be presented in a separate demo session.</li>
</ul>

<p>All submissions must be in PDF format and must follow the ACL 2011 formatting requirements (available at <a href="http://www.acl2011.org/call.shtml#submission" target="_blank"/>http://www.acl2011.org/call.shtml#submission</a>). We strongly advise the use of the provided Word or LaTeX template files. For regular long and short papers, the reported research should be substantially original. The papers will be presented orally or as posters. The decision as to which paper will be presented orally and which as poster will be made by the program committee based on the nature rather than on the quality of the work.</p>

<p>Following the example of major conferences like ACL-HLT 2011, this year we will also accept papers accompanied by the resource (software or data) described in the paper. Resources will be reviewed separately and the final acceptance decision will be made based on both the resource reviews and the paper reviews. The software or data resources submitted should be ready for release and should contain at a README file. All resources will be made available to the MWE community.</p>

<p>Reviewing will be double-blind, and thus no author information should be included in the papers; self-reference should be avoided as well. Resources submitted with the papers should be anonymized for submission. Papers and/or resources that do not conform to these requirements will be rejected without review. Accepted papers will appear in the workshop proceedings, where no distinction will be made between papers presented orally or as posters.</p>

<p>More details about the submission procedure (e.g. online submission system) will be available soon.</p>-->

<h1>Important dates</h1>

<table>
<tr><td><span style="text-decoration:line-through">Mar 4, 2011</span></td><td><span style="text-decoration:line-through">Long paper submission deadline 23:59 PDT (GMT-7)</span></td></tr>
<tr><td><span style="text-decoration:line-through">Mar 11, 2011</span></td><td><span style="text-decoration:line-through">Short paper and demo submission deadline 23:59 PDT (GMT-7)</span></td></tr>
<tr><td><span style="text-decoration:line-through">Apr 01, 2011</span></td><td><span style="text-decoration:line-through">EXTENDED long paper and short paper deadline 23:59 PDT (GMT-7)</span></td></tr>
<tr><td><span style="text-decoration:line-through">Apr 08, 2011</span></td><td><span style="text-decoration:line-through">EXTENDED demo submission deadline 23:59 PDT (GMT-7)</span></td></tr>
<tr><td><span style="text-decoration:line-through">May 2, 2011</span></td><td><span style="text-decoration:line-through">Notification of acceptance</span></td></tr>
<tr><td><span style="text-decoration:line-through">May 9, 2011</span></td><td><span style="text-decoration:line-through">Camera-ready deadline</span></td></tr>
<tr><td><span style="text-decoration:line-through">Jun 23, 2011</span></td><td><span style="text-decoration:line-through">Workshop</span></td></tr>
</table>

<!--<h1>Program Committee</h1>

<ul>
<li>I&ntilde;aki Alegria                <em>(University of the Basque Country, Spain)</em></li>
<li>Dimitra Anastasiou                  <em>(University of Bremen, Germany)</em></li>
<li>Timothy Baldwin                     <em>(University of Melbourne, Australia)</em></li>
<li>Srinivas Bangalore                  <em>(AT&T Labs-Research, USA)</em></li>
<li>Colin Bannard                       <em>(University of Texas at Austin, USA)</em></li>
<li>Francis Bond                        <em>(Nanyang Technological University, Singapore)</em></li>
<li>Aoife Cahill                        <em>(IMS University of Stuttgart, Germany)</em></li>
<li>Paul Cook                           <em>(University of Toronto, Canada)</em></li>
<li>B&eacute;atrice Daille              <em>(Nantes University, France)</em></li>
<li>Mona Diab                           <em>(Columbia University, USA)</em></li>
<li>Gael Dias                           <em>(Beira Interior University, Portugal)</em></li>
<li>Stefan Evert                        <em>(University of Osnabrueck, Germany)</em></li>
<li>Roxana Girju                        <em>(University of Illinois at Urbana-Champaign, USA)</em></li>
<li>Chikara Hashimoto                   <em>(National Institute of Information and Communications Technology, Japan)</em></li>
<li>Ulrich Heid                         <em>(Stuttgart University, Germany)</em></li>
<li>Kyo Kageura                         <em>(University of Tokyo, Japan)</em></li>
<li>Adam Kilgarriff                     <em>(Lexical Computing Ltd., UK)</em></li>
<li>Anna Korhonen                       <em>(University of Cambridge, UK)</em></li>
<li>Ioannis Korkontzelos                <em>(University of York, UK)</em></li>
<li>Zornitsa Kozareva                   <em>(University of Southern California, USA)</em></li>
<li>Brigitte Krenn                      <em>(Austrian Research Institute for Artificial Intelligence, Austria)</em></li>
<li>Takuya Matsuzaki                    <em>(University of Tokyo, Japan)</em></li>
<li>Diana McCarthy                      <em>(Lexical Computing Ltd., UK)</em></li>
<li>Yusuke Miyao                        <em>(National Institute of Informatics, Japan)</em></li>
<li>Rosamund Moon                       <em>(University of Birmingham, UK)</em></li>
<li>Diarmuid &Oacute; S&eacute;aghdha   <em>(University of Cambridge, UK)</em></li>
<li>Jan Odijk                           <em>(University of Utrecht, The Netherlands)</em></li>
<li>Darren Pearce-Lazard                <em>(University of Sussex, UK)</em></li>
<li>Pavel Pecina                        <em>(Dublin City University, Ireland)</em></li>
<li>Scott Piao                          <em>(Lancaster University, UK)</em></li>
<li>Elisabete Ranchhod                  <em>(University of Lisbon, Portugal)</em></li>
<li>Barbara Rosario                     <em>(Intel Labs, USA)</em></li>
<li>Agata Savary                        <em>(Universit&eacute; François Rabelais de Tours, France)</em></li>
<li>Violeta Seretan                     <em>(University of Edinburgh, UK)</em></li>
<li>Suzanne Stevenson                   <em>(Univesity of Toronto, Canada)</em></li>
<li>Sara Stymne                         <em>(Link&oumlaut;ping University, Sweden)</em></li>
<li>Stan Szpakowicz                     <em>(University of Ottawa, Canada)</em></li>
<li>Beata Trawinski                     <em>(University of Vienna, Germany)</em></li>
<li>Vivian Tsang                        <em>(Bloorview Research Institute, Canada)</em></li>
<li>Kyioko Uchiyama                     <em>(National Institute of Informatics, Japan)</em></li>
<li>Ruben Urizar                        <em>(University of the Basque Country, Spain)</em></li>
<li>Gertjan van Noord                   <em>(University of Groningen, The Netherlands)</em></li>
<li>Tony Veale                          <em>(University College Dublin, Ireland)</em></li>
<li>Bego&ntilde;a Villada Moir&oacute;n <em>(Q-go, The Netherlands)</em></li>
<li>Yi Zhang                            <em>(DFKI GmbH &amp; Saarland University, Germany)</em></li>
</ul>

<h1>Consulting Body</h1>

<ul>
<li><a href="http://sunamkim.me/" target="_blank">Su Nam Kim</a><em> (University of Melbourne, Australia)</em></li>
<li><a href="http://www.comp.nus.edu.sg/~nakov/" target="_blank">Preslav Nakov</a><em> (National University of Singapore, Singapore)</em></li>
</ul>-->

<h1>Workshop Organizers</h1>

<ul>
<li><a href="http://www.coli.uni-saarland.de/~kordoni/" target="_blank">Valia Kordoni</a><em> (DFKI GmbH &amp; Saarland University, Germany)</em></li>
<li><a href="http://www.inf.ufrgs.br/~ceramisch/" target="_blank">Carlos Ramisch</a><em> (University of Grenoble, France and Federal University of Rio Grande do Sul, Brazil)</em></li>
<li><a href="http://www.inf.ufrgs.br/~avillavicencio/" target="_blank">Aline Villavicencio</a><em> (Federal University of Rio Grande do Sul, Brazil)</em></li>
</ul>

<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwe2011 at gmail.com">mwe2011 at gmail.com</a></p>
