<h2>Multiword Expressions: from Parsing and Generation to the Real World (MWE 2011)</h2>

<h3>Workshop at ACL 2011 (Portland, Oregon, USA), June 23, 2011</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.clres.com/siglex.html" target="_blank">SIGLEX</a>)</p>

<p><small>Last updated: May 20, 2011</small></p>

<h1>Workshop Program</h1>
<!--<img width="24" alt="  New: " src="images/new.gif"/>-->

<h3>Thursday, June 23, 2011</h3>

<table>
<tr><td>08:15-08:30</td><td>Welcome</td></tr>
<tr><td>08:30-09:30</td><td>MWEs and Topic Modelling: Enhancing Machine Learning with Linguistics</td></tr>
<tr><td>           </td><td><em>Invited talk by Tim Baldwin</em></td></tr>
<tr><td>           </td><td><strong>Session I - Short Papers</strong></td></tr>
<tr><td>           </td><td>Chair: Eric Wherli</td></tr>
<tr><td>09:30-09:45</td><td>Automatic Extraction of NV Expressions in Basque: Basic Issues on Cooccurrence Techniques</td></tr>
<tr><td>           </td><td><em>Antton Gurrutxaga and I&ntilde;aki Alegria</em></td></tr>
<tr><td>09:45-10:00</td><td>Semantic Clustering: an Attempt to Identify Multiword Expressions in Bengali</td></tr>
<tr><td>           </td><td><em>Tanmoy Chakraborty, Dipankar Das and Sivaji Bandyopadhyay</em></td></tr>
<tr><td>10:00-10:15</td><td>Decreasing Lexical Data Sparsity in Statistical Syntactic Parsing - Experiments with Named Entities</td></tr>
<tr><td>           </td><td><em>Deirdre Hogan, Jennifer Foster and Josef van Genabith</em></td></tr>
<tr><td>10:15-10:30</td><td>Detecting Multi-Word Expressions Improves Word Sense Disambiguation</td></tr>
<tr><td>           </td><td><em>Mark Finlayson and Nidhi Kulkarni</em></td></tr>
<tr><td>10:30-11:00</td><td>MORNING BREAK</td></tr>
<tr><td>           </td><td><strong>Session II - Identification and Representation</strong></td></tr>
<tr><td>           </td><td>Chair: Francis Bond</td></tr>
<tr><td>11:00-11:25</td><td>Tree-Rewriting Models of Multi-Word Expressions</td></tr>
<tr><td>           </td><td><em>William Schuler and Aravind Joshi</em></td></tr>
<tr><td>11:25-11:50</td><td>Learning English Light Verb Constructions: Contextual or Statistical</td></tr>
<tr><td>           </td><td><em>Yuancheng Tu and Dan Roth</em></td></tr>
<tr><td>11:50-12:15</td><td>Two Types of Korean Light Verb Constructions in a Typed Feature Structure Grammar</td></tr>
<tr><td>           </td><td><em>Juwon Lee</em></td></tr>
<tr><td>12:15-13:50</td><td>LUNCH BREAK</td></tr>
<tr><td>           </td><td><strong>Session III - Tasks and Applications</strong></td></tr>
<tr><td>           </td><td> Chair: Ted Pedersen</td></tr>
<tr><td>13:50-14:15</td><td>MWU-Aware Part-of-Speech Tagging with a CRF Model and Lexical Resources</td></tr>
<tr><td>           </td><td><em>Matthieu Constant and Anthony Sigogne</em></td></tr>
<tr><td>14:15-14:40</td><td>The Web is not a PERSON, Berners-Lee is not an ORGANIZATION, and African-Americans are <br/>
not LOCATIONS: An Analysis of the Performance of Named-Entity Recognition</td></tr>
<tr><td>           </td><td><em>Robert Krovetz, Paul Deane and Nitin Madnani</em></td></tr>
<tr><td>14:40-15:05</td><td>A Machine Learning Approach to Relational Noun Mining in German</td></tr>
<tr><td>           </td><td><em>Berthold Crysmann</em></td></tr>
<tr><td>15:05-15:30</td><td><strong>Poster and Demo Session</strong></td></tr>
<tr><td>           </td><td> Chair: I&ntilde;aki Alegria</td></tr>
<tr><td>           </td><td><strong>Long Papers</strong></td></tr>
<tr><td>           </td><td><li>Identifying and Analyzing Brazilian Portuguese Complex Predicates</td></tr>
<tr><td>           </td><td><em>Magali Sanches Duran, Carlos Ramisch, Sandra Maria Alu&iacute;sio and Aline Villavicencio</em></li></td></tr>
<tr><td>           </td><td><li>An N-gram Frequency Database Reference to Handle MWE Extraction in NLP Applications</td></tr>
<tr><td>           </td><td><em>Patrick Watrin and Thomas Fran&ccedil;ois</em></li></td></tr>
<tr><td>           </td><td><li>Extracting Transfer Rules for Multiword Expressions from Parallel Corpora</td></tr>
<tr><td>           </td><td><em>Petter Haugereid and Francis Bond</em></li></td></tr>
<tr><td>           </td><td><li>Identification and Treatment of Multiword Expressions Applied to Information Retrieval</td></tr>
<tr><td>           </td><td><em>Otavio Acosta, Aline Villavicencio and Viviane Moreira</em></li></td></tr>
<tr><td>           </td><td><strong>Short Papers</strong></td></tr>
<tr><td>           </td><td><li>Stepwise Mining of Multi-Word Expressions in Hindi</td></tr>
<tr><td>           </td><td><em>Rai Mahesh Sinha</em></li></td></tr>
<tr><td>           </td><td><li>Detecting Noun Compounds and Light Verb Constructions: a Contrastive Study</td></tr>
<tr><td>           </td><td><em>Veronika Vincze, Istv&aacute;n Nagy T. and G&aacute;bor Berend</em></li></td></tr>
<tr><td>           </td><td><strong>Demo Papers</strong></td></tr>
<tr><td>           </td><td><li>jMWE: A Java Toolkit for Detecting Multi-Word Expressions</td></tr>
<tr><td>           </td><td><em>Nidhi Kulkarni and Mark Finlayson</em></li></td></tr>
<tr><td>           </td><td><li>On-line Visualisation of Collocations Extracted from Multilingual Corpora</td></tr>
<tr><td>           </td><td><em>Violeta Seretan and Eric Wehrli</em></li></td></tr>
<tr><td>           </td><td><li>StringNet Lexico-Grammatical Knowledgebase and its Applications</td></tr>
<tr><td>           </td><td><em>David Wible and Nai-Lung Tsao</em></li></td></tr>
<tr><td>           </td><td><li>The Ngram Statistics Package (Text::NSP) : A Flexible Tool for Identifying Ngrams, 
<br/>Collocations, and Word Associations</td></tr>
<tr><td>           </td><td><em>Ted Pedersen, Satanjeev Banerjee, Bridget McInnes, Saiyam Kohli, Mahesh Joshi and Ying Liu</em></li></td></tr>
<tr><td>           </td><td><li>Fast and Flexible MWE Candidate Generation with the mwetoolkit</td></tr>
<tr><td>           </td><td><em>Vitor De Araujo, Carlos Ramisch and Aline Villavicencio</em></li></td></tr>
<tr><td>15:30-16:00</td><td>AFTERNOON BREAK</td></tr>
<tr><td>16:00-17:00</td><td>How Many Multiword Expressions do People Know?</td></tr>
<tr><td>           </td><td><em>Invited talk by Ken Church</em></td></tr>
<tr><td>17:00-18:00</td><td>Panel: Toward a Special Interest Group for MWEs</td></tr>
<tr><td>           </td><td> Moderator: Valia Kordoni, DFKI GmbH &amp; Saarland University, Germany</td></tr>
<tr><td>           </td><td> <li><em>Mark Johnson, Macquarie University, Australia </em></li> </td></tr>
<tr><td>           </td><td> <li><em>Preslav Nakov, National University of Singapore, Singapore </em></li> </td></tr>
<tr><td>           </td><td> <li><em>Jason Eisner, Johns Hopkins University, MD, USA</em></li> </td></tr>
</table>

<h1>List of accepted papers </h1>

<h2>Oral presentations</h2>

<ul>
    <li>Long papers</li>
    <ul>
        <li>MWU-aware Part-of-Speech Tagging with a CRF model and lexical resources</br>
        <em>Matthieu Constant and Anthony Sigogne</em></li>
        <li>Tree-rewriting Models of Multi-word Expressions</br>
        <em>William Schuler</em></li>
        <li>The Web is not a PERSON, Berners-Lee is not an ORGANIZATION, and African-Americans are not LOCATIONS: An Analysis of the Performance of Named-Entity Recognition</br> 
        <em>Robert Krovetz, Paul Deane and Nitin Madnani</em></li>
        <li>A machine learning approach to relational noun mining in German</br>
        <em>Berthold Crysmann</em></li>
        <li>Learning English Light Verb Constructions: Contextual or Statistical</br>
        <em>Yuancheng Tu and Dan Roth</em></li>
        <li>Two Types of Korean Light Verb Constructions in a Typed Feature Structure Grammar</br>
        <em>Juwon Lee</em></li>
    </ul>
    <li>Short papers</li>
    <ul>
        <li>Detecting Multi-Word Expressions improves Word Sense Disambiguation</br>
        <em>Mark Finlayson and Nidhi Kulkarni</em></li>
        <li>Semantic Clustering: an Attempt to Identify Multiword Expressions in Bengali</br>
        <em>Tanmoy Chakraborty, Dipankar Das and Sivaji Bandyopadhyay</em></li>
        <li>Automatic extraction of NV expressions in Basque: basic issues on cooccurrence techniques</br>
        <em>Antton Gurrutxaga and I&ntilde;aki Alegria</em></li>
        <li>Decreasing lexical data sparsity in syntactic parsing - experiments with named entities</br>
        <em>Deirdre Hogan, Jennifer Foster and Josef van Genabith</em></li>
    </ul>
</ul>

<h2>Poster presentations</h2>

<ul>
    <li>Long papers</li>
    <ul>
        <li>Identifying and Analyzing Brazilian Portuguese Complex Predicates</br>
        <em>Magali Sanches Duran, Carlos Ramisch, Sandra Maria Alu&iacute;sio and Aline Villavicencio</em></li>
        <li>N-gram frequency database reference to handle MWE extraction in NLP applications</br>
        <em>Patrick Watrin and Thomas Fran&ccedil;ois</em></li>
        <li>Extracting Transfer Rules for Multiword Expressions from Parallel Corpora</br>
        <em>Petter Haugereid and Francis Bond</em></li>
        <li>Identification and Treatment of Multiword Expressions applied to Information Retrieval</br>
        <em>Otavio Acosta, Aline Villavicencio and Viviane Moreira</em></li>
    </ul>
    <li>Short papers</li>
    <ul>
        <li>Stepwise Mining of Multi-Word Expressions in Hindi</br>
        <em>Rai Mahesh Sinha</em></li>
        <li>Detecting noun compounds and light verb constructions: a contrastive study</br>
        <em>Veronika Vincze, Istv&aacute;n Nagy T. and G&aacute;bor Berend</em></li>
    </ul>
</ul>

<h2>Demo presentations</h2>
<ul>
    <li>jMWE: A Java Toolkit for Detecting Multi-Word Expressions</br>
    <em>Nidhi Kulkarni and Mark Finlayson</em></li>
    <li>On-line Visualisation of Collocations Extracted from Multilingual Corpora</br>
    <em>Violeta Seretan and Eric Wehrli</em></li>
    <li>StringNet Lexico-Grammatical Knowledgebase and its Applications</br>
    <em>David Wible and Nai-Lung Tsao</em></li>
    <li>The Ngram Statistics Package (Text::NSP) : A Flexible Tool for Identifying Ngrams, Collocations, and Word Associations</br>
    <em>Ted Pedersen</em></li>
    <li>Fast and Flexible MWE Candidate Generation</br>
    <em>V&iacute;tor De Ara&uacute;jo, Carlos Ramisch and Aline Villavicencio</em></li>
</ul>


