<h2>Multiword Expressions: from Parsing and Generation to the Real World (MWE 2011)</h2>

<h3>Workshop at ACL 2011 (Portland, Oregon, USA), June 23, 2011</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.clres.com/siglex.html" target="_blank">SIGLEX</a>)</p>

<p><small>Last updated: May 03, 2011</small></p>

<h1>Program Committee</h1>

<ul>
<li>I&ntilde;aki Alegria                <em>(University of the Basque Country, Spain)</em></li>
<li>Dimitra Anastasiou                  <em>(University of Bremen, Germany)</em></li>
<li>Timothy Baldwin                     <em>(University of Melbourne, Australia)</em></li>
<li>Srinivas Bangalore                  <em>(AT&T Labs-Research, USA)</em></li>
<!--<li>Colin Bannard                       <em>(University of Texas at Austin, USA)</em></li>-->
<li>Francis Bond                        <em>(Nanyang Technological University, Singapore)</em></li>
<li>Aoife Cahill                        <em>(IMS University of Stuttgart, Germany)</em></li>
<li>Paul Cook                           <em>(University of Toronto, Canada)</em></li>
<li>B&eacute;atrice Daille              <em>(Nantes University, France)</em></li>
<li>Mona Diab                           <em>(Columbia University, USA)</em></li>
<li>Ga&euml;l Dias                           <em>(Beira Interior University, Portugal)</em></li>
<li>Stefan Evert                        <em>(University of Osnabrueck, Germany)</em></li>
<li>Roxana Girju                        <em>(University of Illinois at Urbana-Champaign, USA)</em></li>
<li>Chikara Hashimoto                   <em>(National Institute of Information and Communications Technology, Japan)</em></li>
<li>Ulrich Heid                         <em>(Stuttgart University, Germany)</em></li>
<li>Kyo Kageura                         <em>(University of Tokyo, Japan)</em></li>
<li>Adam Kilgarriff                     <em>(Lexical Computing Ltd., UK)</em></li>
<!--<li>Anna Korhonen                       <em>(University of Cambridge, UK)</em></li>-->
<li>Ioannis Korkontzelos                <em>(University of Manchester, UK)</em></li>
<li>Zornitsa Kozareva                   <em>(University of Southern California, USA)</em></li>
<li>Brigitte Krenn                      <em>(Austrian Research Institute for Artificial Intelligence, Austria)</em></li>
<li>Takuya Matsuzaki                    <em>(University of Tokyo, Japan)</em></li>
<li>Diana McCarthy                      <em>(Lexical Computing Ltd., UK)</em></li>
<li>Yusuke Miyao                        <em>(National Institute of Informatics, Japan)</em></li>
<li>Rosamund Moon                       <em>(University of Birmingham, UK)</em></li>
<li>Diarmuid &Oacute; S&eacute;aghdha   <em>(University of Cambridge, UK)</em></li>
<li>Jan Odijk                           <em>(University of Utrecht, The Netherlands)</em></li>
<!--<li>Darren Pearce-Lazard                <em>(University of Sussex, UK)</em></li>-->
<li>Pavel Pecina                        <em>(Dublin City University, Ireland)</em></li>
<li>Scott Piao                          <em>(Lancaster University, UK)</em></li>
<lI>Thierry Poibeau                     <em>(CNRS and &Eacute;cole Normale Sup&eacute;rieure, France)</em></li>
<li>Elisabete Ranchhod                  <em>(University of Lisbon, Portugal)</em></li>
<li>Barbara Rosario                     <em>(Intel Labs, USA)</em></li>
<li>Agata Savary                        <em>(Universit&eacute; Fran&ccedil;ois Rabelais Tours, France)</em></li>
<li>Violeta Seretan                     <em>(University of Edinburgh, UK)</em></li>
<li>Ekaterina Shutova                   <em>(University of Cambridge, UK)</em></li>
<li>Suzanne Stevenson                   <em>(University of Toronto, Canada)</em></li>
<li>Sara Stymne                         <em>(Link&ouml;ping University, Sweden)</em></li>
<li>Stan Szpakowicz                     <em>(University of Ottawa, Canada)</em></li>
<li>Beata Trawinski                     <em>(University of Vienna, Austria)</em></li>
<li>Vivian Tsang                        <em>(Bloorview Research Institute, Canada)</em></li>
<li>Kyioko Uchiyama                     <em>(National Institute of Informatics, Japan)</em></li>
<li>Ruben Urizar                        <em>(University of the Basque Country, Spain)</em></li>
<li>Gertjan van Noord                   <em>(University of Groningen, The Netherlands)</em></li>
<li>Tony Veale                          <em>(University College Dublin, Ireland)</em></li>
<li>Bego&ntilde;a Villada Moir&oacute;n <em>(RightNow, The Netherlands)</em></li>
<li>Yi Zhang                            <em>(DFKI GmbH &amp; Saarland University, Germany)</em></li>
</ul>

<h1>Consulting Body</h1>

<ul>
<li><a href="http://sunamkim.me/" target="_blank">Su Nam Kim</a><em> (University of Melbourne, Australia)</em></li>
<li><a href="http://www.comp.nus.edu.sg/~nakov/" target="_blank">Preslav Nakov</a><em> (National University of Singapore, Singapore)</em></li>
</ul>

<h1>Workshop Organizers</h1>

<ul>
<li><a href="http://www.coli.uni-saarland.de/~kordoni/" target="_blank">Valia Kordoni</a><em> (DFKI GmbH &amp; Saarland University, Germany)</em></li>
<li><a href="http://www.inf.ufrgs.br/~ceramisch/" target="_blank">Carlos Ramisch</a><em> (University of Grenoble, France and Federal University of Rio Grande do Sul, Brazil)</em></li>
<li><a href="http://www.inf.ufrgs.br/~avillavicencio/" target="_blank">Aline Villavicencio</a><em> (Federal University of Rio Grande do Sul, Brazil)</em></li>
</ul>

<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwe2011 at gmail.com">mwe2011 at gmail.com</a></p>

