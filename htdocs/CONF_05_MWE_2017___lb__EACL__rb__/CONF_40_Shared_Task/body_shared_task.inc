<h2>Shared task on automatic identification of verbal multiword expressions</h2>

<h3>Organized as part of the MWE 2017 workshop co-located with <a href="http://eacl2017.org/" target="_blanc">EACL 2017</a> (Valencia, Spain), April 4, 2017</h3>

<p><small>Last updated: February 1, 2017</small></p>

<h1>Description</h1>

<p>The <a href="http://www.parseme.eu" target="_blank">PARSEME</a> shared task on automatic identification of verbal multiword expressions (VMWEs) aims at identifying verbal MWEs in running texts. Verbal MWEs include idioms (<em>let the cat out of the bag</em>), light verb constructions (<em>make a decision</em>), verb-particle constructions (<em>give up</em>), and inherently reflexive verbs (<em>se suicider</em> 'to suicide' in French). Their identification is a well-known challenge for NLP applications, due to their complex characteristics: discontinuity, non-compositionality, heterogeneity and syntactic variability.</p>

<p>The shared task is highly multilingual: we cover 18 languages from several language families. <a href="http://www.parseme.eu" target="_blank">PARSEME</a> members have elaborated annotation <a href="http://parsemefr.lif.univ-mrs.fr/guidelines-hypertext/" target="_blank">guidelines</a> based on annotation experiments in these languages. They take both universal and language-specific phenomena into account. We hope that this will boost the development of language-independent and cross-lingual VMWE identification systems.</p>

<!----------------------->
<h1><a name="participation"></a>Participation</h1>

<p style="color:red">The evaluation phase of the shared task is now over, but you can find useful information about the shared task on this page.</p>

<p>Participation was open and free worldwide. </p>

<!-- We asked potential participant teams to register using the expression of interest <a href="https://docs.google.com/forms/d/e/1FAIpQLSfnrAfr1B9IV3ftymUwxPGau91Zzc0ggt10U96tP7IpCkAG0g/viewform" target="_blank">form</a>.</p> -->

<p>Task updates and questions can still be posted to our public <a href="http://groups.google.com/group/verbalmwe" target="_blank">mailing list</a>.</p>

<p>For more details on the annotation of the corpora visit the <a href="http://typo.uni-konstanz.de/parseme/index.php/2-general/142-parseme-shared-task-on-automatic-detection-of-verbal-mwes" target="_blank">dedicated PARSEME page</a> and check the <a href="http://parsemefr.lif.univ-mrs.fr/guidelines-hypertext/">annotation guidelines</a> used in manual annotation of the training and test sets.</p>

<p>It should be noted that a large international community has been gathered (via the <a href="http://www.parseme.eu" target="_blank">PARSEME</a> network) around the effort of putting forward universal guidelines and performing corpus annotations. Our policy was to <b>allow the same national teams, which provided annotated corpora, to also submit VMWE identification systems</b> for the shared task. While this policy is non-standard and introduces a bias to system evaluation, we follow it for several reasons:</p>

<ul>
<li>For many languages there are only very few NLP teams, so adopting an exclusive approach (either you annotate or you present a system but not both) would actually exclude the whole language from participation.</li>
<li> We are interested more in cross-language discussions than in a real competition.</li>
<li>We admit that we can trust the teams to respect some <b>best practices</b>, including those:
	<ul>
	<li>The test data are never used for training, even if system authors have access to them in advance.</li>
	<li>If any resources were used to annotate the corpus, the same resources should not be used by the system (in the <a href="#tracks">open track</a>).</li>
	<li>If system authors notice other sources of bias between their annotating activity and system evaluation, they should describe them in the submitted papers (if any).</li>
	</ul>
</ul>

<!----------------------->
<h1><a name="data"></a>Provided data</h1>

<font color="red"><strong>NEW!</strong></font> The shared task corpus (version 1.0) has been published at the <a href="http://hdl.handle.net/11372/LRT-2282" target=_blanc">LINDAT/CLARIN</a> infrastructure.

<p>The shared task covers <strong>18 languages</strong>: Bulgarian (BG), Czech (CS), German (DE), Greek (EL), Spanish (ES), Farsi (FA), French (FR), Hebrew (HE), Hungarian (HU), Italian (IT), Lithuanian (LT), Maltese (MT), Polish (PL), Brazilian Portuguese (PT), Romanian (RO), Slovene (SL), Swedish (SV) and Turkish (TR). For all these languages, we provided two corpora to the participants:</p>
<ul>
<li>Manually built <b>training corpora</b> in which VMWEs are annotated according to the universal <a href="http://parsemefr.lif.univ-mrs.fr/guidelines-hypertext/" target="_blank">guidelines</a>. </i>
<li>Raw (unannotated) <b>test corpora</b> (<strike>to be released on 20 January</strike>) to be used as input to the systems. The VMWE annotations contained in this corpus, performed according to the same guidelines, <strike>will be kept secret</strike> are now available on the GitLab repository.</li>
</ul>

<p>The corpora are provided in the <a href="http://typo.uni-konstanz.de/parseme/index.php/2-general/184-parseme-shared-task-format-of-the-final-annotation" target="_blank">parsemetsv</a> format, inspired by the <a href="http://universaldependencies.org/format.html" target="_blank">CONLL-U</a> format. <!--Have a look at a <a href="http://typo.uni-konstanz.de/parseme/images/shared-task/format/parseme-tsv-sample-blind.parsemetsv.zip" target="_blanc">sample input file</a> to be annotated, and a <a href="http://typo.uni-konstanz.de/parseme/images/shared-task/format/parseme-tsv-sample-train.parsemetsv.zip" target="_blanc">sample training file</a> in English.</p>-->

<p>For most languages (all except BG, HE and LT), paired files in the CONLL-U format - not necessarily using UD tagsets - containing parts of speech, lemmas, morphological features and/or syntactic dependencies are also provided. Depending on the language, the information comes from treebanks (e.g., <a href="http://universaldependencies.org/" target="_blanc">Universal Dependencies</a>) or from automatic parsers trained on treebanks (e.g., <a href="http://ufal.mff.cuni.cz/udpipe" target="_blanc">UDPipe</a>).</p>

<p>The table below summarizes the sizes of the <b>training corpora</b> per language:</p>

<!--
<p>The training data are now available</strong> in our <a target="_blank" href="https://gitlab.com/parseme/sharedtask-data">public GitLab repository</a> for <strong>16 languages</strong>: Bulgarian (BG), Czech (CS), German (DE), Greek (EL), Farsi (FA), French (FR), Hebrew (HE), Hungarian (HU), Italian (IT), Lithuanian (LT), Maltese (MT), Polish (PL), Brazilian Portuguese (PT), Romanian (RO), Slovene (SL), Turkish (TR). For 2 languages, we intend to provide only test data, but the trial data is available for training: Spanish (ES), Swedish (SV). Follow the <a href="https://gitlab.com/parseme/sharedtask-data/tree/master" target="_nlanc">Repository</a> link to access folders for individual languages. You can also download an archive containing all the training data directly using <a target="_blank" href="https://gitlab.com/parseme/sharedtask-data/repository/archive.zip?ref=master">this shortcut link</a>.  The table below summarizes the sizes of the datasets per language:</p>
-->

<table style="text-align:right">
<tbody>
<tr><td style="font-weight:bold">Language</td><td style="font-weight:bold">Sentences</td><td style="font-weight:bold">Tokens</td><td style="font-weight:bold">VMWE</td><td style="font-weight:bold">ID</td><td style="font-weight:bold">IReflV</td><td style="font-weight:bold">LVC</td><td style="font-weight:bold">OTH</td><td style="font-weight:bold">VPC</td></tr>
<tr><td style="text-align:left">BG</td><td>6913</td><td>157647</td><td>1933</td><td>417</td><td>1079</td><td>435</td><td>2</td><td>0</td></tr>
<tr><td style="text-align:left">CS</td><td>43955</td><td>740530</td><td>12852</td><td>1419</td><td>8851</td><td>2580</td><td>2</td><td>0</td></tr>
<tr><td style="text-align:left">DE</td><td>6261</td><td>120840</td><td>2447</td><td>1005</td><td>111</td><td>178</td><td>10</td><td>1143</td></tr>
<tr><td style="text-align:left">EL</td><td>5244</td><td>142322</td><td>1518</td><td>515</td><td>0</td><td>955</td><td>16</td><td>32</td></tr>
<tr><td style="text-align:left">ES</td><td>2502</td><td>102090</td><td>748</td><td>196</td><td>336</td><td>214</td><td>2</td><td>0</td></tr>
<tr><td style="text-align:left">FA</td><td>2736</td><td>46530</td><td>2707</td><td>0</td><td>0</td><td>0</td><td>2707</td><td>0</td></tr>
<tr><td style="text-align:left">FR</td><td>17880</td><td>450221</td><td>4462</td><td>1786</td><td>1313</td><td>1362</td><td>1</td><td>0</td></tr>
<tr><td style="text-align:left">HE</td><td>4673</td><td>99790</td><td>1282</td><td>86</td><td>0</td><td>253</td><td>535</td><td>408</td></tr>
<tr><td style="text-align:left">HU</td><td>3569</td><td>87777</td><td>2999</td><td>0</td><td>0</td><td>584</td><td>0</td><td>2415</td></tr>
<tr><td style="text-align:left">IT</td><td>15728</td><td>387325</td><td>1954</td><td>913</td><td>580</td><td>395</td><td>4</td><td>62</td></tr>
<tr><td style="text-align:left">LT</td><td>12153</td><td>209636</td><td>402</td><td>229</td><td>0</td><td>173</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">MT</td><td>5965</td><td>141096</td><td>772</td><td>261</td><td>0</td><td>434</td><td>77</td><td>0</td></tr>
<tr><td style="text-align:left">PL</td><td>11578</td><td>191239</td><td>3149</td><td>317</td><td>1548</td><td>1284</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">PT</td><td>19640</td><td>359345</td><td>3447</td><td>820</td><td>515</td><td>2110</td><td>2</td><td>0</td></tr>
<tr><td style="text-align:left">RO</td><td>45469</td><td>778674</td><td>4040</td><td>524</td><td>2496</td><td>1019</td><td>1</td><td>0</td></tr>
<tr><td style="text-align:left">SL</td><td>8881</td><td>183285</td><td>1787</td><td>283</td><td>945</td><td>186</td><td>2</td><td>371</td></tr>
<tr><td style="text-align:left">SV</td><td>200</td><td>3376</td><td>56</td><td>9</td><td>3</td><td>13</td><td>0</td><td>31</td></tr>
<tr><td style="text-align:left">TR</td><td>16715</td><td>334880</td><td>6169</td><td>2911</td><td>0</td><td>2624</td><td>634</td><td>0</td></tr>
<tr><td style="text-align:left">Total</td><td>230062</td><td>4536603</td><td>52724</td><td>11691</td><td>17777</td><td>14799</td><td>3995</td><td>4462</td></tr>
</tbody>
</table>

<p>The table below summarizes the sizes of the <b>test corpora</b> per language:</p>

<table style="text-align:right">
<tbody>
<tr><td style="font-weight:bold">Language</td><td style="font-weight:bold">Sentences</td><td style="font-weight:bold">Tokens</td><td style="font-weight:bold">VMWE</td><td style="font-weight:bold">ID</td><td style="font-weight:bold">IReflV</td><td style="font-weight:bold">LVC</td><td style="font-weight:bold">OTH</td><td style="font-weight:bold">VPC</td></tr>
<tr><td style="text-align:left">BG</td><td>1947</td><td>42481</td><td>473</td><td>100</td><td>297</td><td>76</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">CS</td><td>5476</td><td>92663</td><td>1684</td><td>192</td><td>1149</td><td>343</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">DE</td><td>1239</td><td>24016</td><td>500</td><td>214</td><td>20</td><td>40</td><td>0</td><td>226</td></tr>
<tr><td style="text-align:left">EL</td><td>3567</td><td>83943</td><td>500</td><td>127</td><td>0</td><td>336</td><td>21</td><td>16</td></tr>
<tr><td style="text-align:left">ES</td><td>2132</td><td>57717</td><td>500</td><td>166</td><td>220</td><td>106</td><td>8</td><td>0</td></tr>
<tr><td style="text-align:left">FA</td><td>490</td><td>8677</td><td>500</td><td>0</td><td>0</td><td>0</td><td>500</td><td>0</td></tr>
<tr><td style="text-align:left">FR</td><td>1667</td><td>35784</td><td>500</td><td>119</td><td>105</td><td>271</td><td>5</td><td>0</td></tr>
<tr><td style="text-align:left">HE</td><td>2327</td><td>47571</td><td>500</td><td>30</td><td>0</td><td>127</td><td>158</td><td>185</td></tr>
<tr><td style="text-align:left">HU</td><td>742</td><td>20398</td><td>500</td><td>0</td><td>0</td><td>146</td><td>0</td><td>354</td></tr>
<tr><td style="text-align:left">IT</td><td>1272</td><td>40523</td><td>500</td><td>250</td><td>150</td><td>87</td><td>2</td><td>11</td></tr>
<tr><td style="text-align:left">LT</td><td>2710</td><td>46599</td><td>100</td><td>58</td><td>0</td><td>42</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">MT</td><td>4635</td><td>111189</td><td>500</td><td>185</td><td>0</td><td>259</td><td>56</td><td>0</td></tr>
<tr><td style="text-align:left">PL</td><td>2028</td><td>29695</td><td>500</td><td>66</td><td>265</td><td>169</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">PT</td><td>2600</td><td>54675</td><td>500</td><td>90</td><td>81</td><td>329</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">RO</td><td>6031</td><td>100753</td><td>500</td><td>75</td><td>290</td><td>135</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">SL</td><td>2530</td><td>52579</td><td>500</td><td>92</td><td>253</td><td>45</td><td>2</td><td>108</td></tr>
<tr><td style="text-align:left">SV</td><td>1600</td><td>26141</td><td>236</td><td>51</td><td>14</td><td>14</td><td>2</td><td>155</td></tr>
<tr><td style="text-align:left">TR</td><td>1321</td><td>27197</td><td>501</td><td>249</td><td>0</td><td>199</td><td>53</td><td>0</td></tr>
<tr><td style="text-align:left">Total</td><td>44314</td><td>902601</td><td>9494</td><td>2064</td><td>2844</td><td>2724</td><td>807</td><td>1055</td></tr>
</tbody>
</table>


<p>The training and test data are available in our <a target="_blank" href="https://gitlab.com/parseme/sharedtask-data">public GitLab repository</a>. Follow the <a href="https://gitlab.com/parseme/sharedtask-data/tree/master" target="_blanc">Repository</a> link to access folders for individual languages. You can also download an archive containing all the data directly using <a target="_blank" href="https://gitlab.com/parseme/sharedtask-data/repository/archive.zip?ref=master">this shortcut link</a>. The test files are provided in the following files:</p>
<ul>
<li>the <span style="text-indent: 50px; font-family: Courier New;">test.blind.parsemetsv</span> file contains the tokenized sentences in which systems had to identify VMWEs <strong>automatically</strong>,</li> 
<li>the <span style="text-indent: 50px; font-family: Courier New;">test.conllu</span> file in a <a href="http://universaldependencies.org/format.html" target="_blank">CONLL-U</a>-compatible format with morphosyntactic and/or syntactic information (not available for BG, HE and LT),
<li>the <span style="text-indent: 50px; font-family: Courier New;">test.parsemetsv</span> file contains the reference gold annotations against which system outputs were compared for evaluation; this file was made available on February 1, 2017, after the evaluation phase was over.</li> 
</ul>
<p>Both parsemetsv and conllu files could be used in the closed track.</p>

<p>All VMWE annotations are available under <strong>Creative Commons</strong> licenses (see README.md files for details).</p>

<!-- <p>Most annotations are available under <strong>open licenses</strong>, notably various flavors of the Creative Commons license (see README.md files for details).</p> -->

<p><Strong>A note for shared task participants</strong>: Small-size trial data were previously released via the same repository for most languages. We could fully ensure that no part of these data was included in the test data released on 20 January. Therefore, we asked participants <strong>not to use the trial.parsemetsv files</strong> for any language (except ES and SV) while training the final versions of their systems.</p>


<!-- <p>We are currently preparing corpora for the following languages: Bulgarian, Czech, German, Greek, <del>English</del>, Spanish, Farsi, French, <del>Hebrew</del>, <del>Croatian</del>, Hungarian, Italian, Lithuanian, Maltese, Polish, Brazilian Portuguese, Romanian, Swedish, Slovene, Turkish, <del>Yiddish</del>. The amount of the annotated data will depend on the language, and the list of covered languages may vary until the release of the training corpora.</p> -->



<!----------------------->
<h1><a name="tracks"></a>Tracks</h1>

<p>System results could be submitted in two tracks:</p>

<ul>
<li><b>Closed track</b>: Systems using only the provided training data - VMWE annotations + CONLL-U files (if any) - to learn VMWE identification models and/or rules.</li>
<li><b>Open track</b>: Systems using or not the provided training data, plus any additional resources deemed useful (MWE lexicons, symbolic grammars, wordnets, raw corpora, word embeddings, language models trained on external data, etc.). This track includes notably purely symbolic and rule-based systems.</li>
</ul>

<p>Teams submitting systems in the open track were requested to describe and provide references to all resources used at submission time. Teams were encouraged to favor freely available resources for better reproducibility of their results.</p>

<!----------------------->
<h1><a name="metrics"></a>Evaluation metrics</h1>

<p>Participants were to provide the output produced by their systems on the test corpus. This output was compared with the gold standard (ground truth).  Evaluation metrics are precision, recall and F1, both strict (per VMWE) and fuzzy (per toke, i.e. taking partial matches into account). The <a href="https://gitlab.com/parseme/sharedtask-data/tree/master/bin">evaluation script</a> is available in our public data repository. It can be used as follows:
	<ul style="list-style: none;">
        <li><span style="text-indent: 50px; font-family: Courier New;"> ./evaluate.py gold.parsemetsv system.parsemetsv</span></li>
	</ul>
<p>The token-based F1 takes into account the fact that:</p>
<ul>
  <li>discontinuities allowed (<em><strong>take</strong> something <strong>into account</strong></em>)</li>
  <li>overlapping allowed (<em><strong>take</strong> a <strong>walk</strong> and then a long <strong>shower</strong></em>)</li>
  <li>embeddings allowed both at the syntactic level (<em><strong>take</strong> the fact that I didn't <strong><u>give up</u> into account</strong></em>) and at the level of lexicalized components (<em><strong><u>let</u> the cat <u>out</u> of the bag</strong></em>)</li>
  <li>multiword tokens lead to one-token MWEs (<em>ES <strong>suicidarse</strong></em>)</li>
</ul>

<p>Therefore, we measure the best F1 score from all possible matches between the set of MWE token ranks in the gold and system sentences. We perform this by looking at all possible ways of matching MWEs in both sets.</p>

<p>VMWE categories (e.g., LVC, ID, IReflV, VPC) are ignored by the evaluation metrics. Categories are only provided in the training data to guide system design. Systems focusing on selected VMWE categories only were also encouraged to participate - see the <a href="#faq">FAQ</a>.</p>

<!----------------------->
<h1>Tokenization issues</h1>

<p>Tokenization is closely related to MWE identification, and it has been shown that performing both tasks <b>jointly</b> may enhance the quality of their results. </p>

<p>Note, however, that the data provided by us consist of pre-tokenized sentences, which implies that we expect typical systems to perform tokenization <b>prior</b> to VMWE identification, and that we do not allow the tokenization to be modified with respect to the ground truth. This is necessary since the evaluation measures are token-based.   
This approach may disadvantage systems which expect untokenized raw text on input, and apply their own tokenization methods, whether jointly with VMWE identification or not.</p>

<p>We are aware of this bias, and we did encourage such systems to participate in the shared task. We believe that re-tokenization methods can be defined, so as to adapt a system output to the tokenization imposed by us. </p>

<!----------------------->
<h1>Publication and workshop</h1>

<!-- <p>Shared task participants are invited to submit a system description paper to a special track of the <a href="http://eacl2017.org/" target="_blank">EACL 2017</a> workshop on Multiword Expressions (<a href="http://multiword.sourceforge.net/mwe2017" target="_blank">MWE 2017</a>), to be held in Valencia, Spain on April 4. Submitted system description papers must follow the workshop <a href="http://multiword.sourceforge.net/PHITE.php?sitesig=CONF&page=CONF_05_MWE_2017___lb__EACL__rb__#submissions">submission instructions</a> and will go through double-blind peer reviewing by other participants and selected MWE 2017 program committee members. Their acceptance depends on the quality of the paper rather than on the results obtained in the shared task. Authors of the accepted papers will present their work as posters/demos in a dedicated session of the workshop. The submission of a system description paper is not mandatory.</p> -->

<p>Shared task participants are invited to submit input of two kinds to the SHARED TASK TRACK of the <a href="http://eacl2017.org/" target="_blank">EACL 2017</a> workshop on Multiword Expressions (<a href="http://multiword.sourceforge.net/mwe2017" target="_blank">MWE 2017</a>) via the dedicated <a href="https://www.softconf.com/eacl2017/mwe2017" target="_blank">START space</a>:</p>

<ul>
<li><strong>System results</strong> (by January 27) obtained on the blind data (released on 20 January). The results for all languages should be submitted in a single <span style="text-indent: 50px; font-family: Courier New;">your-system-name.zip</span> archive containing a single folder per language, named according to the <a href="https://en.wikipedia.org/wiki/ISO_639-1" target="_blank">ISO 639-1</a> code (e.g. FR for French, MT for Maltese, etc.). Each output file must be named test.system.parsemetsv and conform to the <a href="http://typo.uni-konstanz.de/parseme/index.php/2-general/184-parseme-shared-task-format-of-the-final-annotation" target="_blank">parsemetsv</a> format. The format of each file should be checked before submission by the validation script as follows:
	<ul style="list-style: none;">
        <li><span style="text-indent: 50px; font-family: Courier New;">./checkParsemeTsvFormat.py test.system.parsemetsv</span></li>
	</ul>
If one system participates both in the open and in the closed track, two independent submissions are required.</li>

<li>A <strong>system description paper</strong> (by February 5). These papers must follow the workshop submission instructions and will go through double-blind peer reviewing by other participants and selected MWE 2017 program committee members.  Their acceptance depends on the quality of the paper rather than on the results obtained in the shared task.  Authors of the accepted papers will present their work as posters/demos in a dedicated session of the workshop. The submission of a system description paper is not mandatory.</li>
</ul>

<!----------------------->
<h1>Results</h1>

<p><font color="red"><strong>NEW!</strong></font></strong> The <a href="PHITE.php?sitesig=CONF&page=CONF_05_MWE_2017___lb__EACL__rb__&subpage=CONF_50_Shared_Task_Results" target="_blank">results page</a> now contains system evaluation results per language (disregarding VMWE categories).</p>

<!----------------------->
<h1>Important dates</h1>

<ul>
<li><strike>Oct 14, 2016: first Call for Participation</strike></li>
<li><strike>Nov 18, 2016: second Call for Participation</strike></li>
<li><strike><strike>(previous deadline: Dec 13)</strike> Dec 22, 2016: trial data and evaluation script released</strike></li>
<li><strike>Jan 6, 2016: training data released</strike></li>
<li><strike>Jan 10, 2017: final Call for Participation</strike></li>
<li><strike>Jan 20, 2017: blind test data released</strike></li>
<li><strike>(previous deadline: Jan 30) Jan 31, 2017: announcement of results</strike></li>
<li><strike>(Feb 5, 2017: submission of shared task system description papers</strike>(</li>
<li><strike>(Feb 12, 2017: notification of acceptance</strike>(</li>
<li><strike>(Feb 20, 2017: camera-ready system description papers due</strike>(</li>
<li><strike>(April 4, 2017: shared task workshop colocated with <a href="http://multiword.sourceforge.net/mwe2017" target=_blanc">MWE 2017</a></strike></li>
</ul>

<!----------------------->
<h1>Organizing team</h1>
Marie Candito, Fabienne Cap, Silvio Cordeiro, Antoine Doucet, Voula Giouli, Behrang QasemiZadeh, Carlos Ramisch, Federico Sangati, Agata Savary, Ivelina Stoyanova, Veronika Vincze

<!----------------------->
<h1><a name="faq"></a>Frequently asked questions</h1>

<ol>

<li><i>My system can identify only one category of VMWEs (e.g. verb particle constructions). Can I still participate in the shared task?</i><br />
Organizing different tracks for different VMWE categories would be too complex. Therefore, we plan to publish the systems' results globally, i.e. without the distinction into particular VMWE categories. 
If a system can only recognize one category, its results with respect to this global picture will probably not be very high. In spite of that <b>we do encourage such systems to participate</b>, since we are interested more in cross-language discussions than in a real competition. Our evaluation script (with a proper choice of parameters) does allow restraining the evaluation to a particular VMWE category. It is, thus, possible for a system's authors to perform the evaluation on their own and describe their results in a <a href="http://multiword.sourceforge.net/PHITE.php?sitesig=CONF&page=CONF_05_MWE_2017___lb__EACL__rb__#submissions">system description paper</a> to be submitted to the MWE 2017 workshop.
</li>

</ol>








