<h2>The 13th Workshop on Multiword Expressions (MWE 2017)</h2>

<h3>Workshop at <a href="http://eacl2017.org/" target="_blanc">EACL 2017</a> (Valencia, Spain), April 4, 2017</h3>

<!-- <p>Multiword expressions are said to be a pain in the neck for NLP - let's relieve the pain together at MWE 2017!</p> -->

<p>Endorsed by the Special Interest Group on the Lexicon (<a href="http://www.siglex.org" target="_blank">SIGLEX</a>) of the Association for Computational Linguistics and <a href="http://www.parseme.eu" target="_blanc">PARSEME</a>, the European IC1207 COST Action</p>

<p><small>Last updated: November 10, 2018</small></p>

<!--<p><span style="font-size:110%; color:red;font-weight:bold;">Note:</span> <span style="font-size:110%; color:red;"> This is website of the proposal entitled <em>13th Workshop on MWEs</em> and is <u>not related</u> to the proposal entitled <em>First Conference on MWEs</em>.</span></p>-->

<ul>
<li><font color="red"><strong>NEW!</strong></font>&nbsp;&nbsp;[Nov 10, 2018]: Extended papers from the MWE 2017 workshop have been published as <a href="http://langsci-press.org/catalog/book/204" target="_blank">Multiword expressions at length and in depth: Extended papers from the MWE 2017 workshop</a>, volume 2 of the book series Phraseology and Multiword Expressions (<a href="http://langsci-press.org/catalog/series/pmwe" target="_blank">PMWE</a>), a book series at Language Science Press (<a href="http://langsci-press.org/" target="_blank">LSP</a>).</li>
<li>[Mar 2017] The <a href="https://www.aclweb.org/anthology/events/ws-2017/#w17-17" target="_blank">MWE 2017 proceedings</a> are available on the ACL Anthology.</li>
</ul>

<!------------------------------------->
<h1>Description</h1>

<p>In natural languages, there are many ways to express complex human thoughts and ideas. This can be achieved by exploiting compositionality, i.e. concatenating simplex elements of language and thus yielding a more complex meaning that can be computed from the meaning of the original parts and the way they are combined. However, non-compositional phrases are also very frequent in any human language. These complex phrases can often be decomposed into single meaningful units, but the meaning of the whole phrase cannot (or can only partially) be computed from the meaning of its parts. Such phrases are often called multiword expressions (MWEs) and display lexical, syntactic, semantic, pragmatic and/or statistical idiosyncrasies (<a href="http://people.eng.unimelb.edu.au/tbaldwin/pubs/handbook2009.pdf" target="_blank">Baldwin & Kim 2010</a>). In addition to idiomatic constructions, MWEs encompass closely related linguistic constructs such as light verb constructions, rhetorical figures and institutionalised phrases or collocations (<a href="http://lingo.stanford.edu/pubs/WP-2001-03.pdf" target="_blank">Sag et al. 2002</a>). MWEs pose problems for linguistic processing, especially in language learning and natural language processing (NLP), for instance, in machine translation, syntactic and semantic parsing, just to name a few applications.</p>

<p>Researchers from several disciplines such as computer science, linguistics and psychology have been jointly working on MWE modeling and processing. For instance, designing guidelines for the annotation of MWEs in corpora, and prominently in treebanks, has been undertaken in various languages and linguistic frameworks (<a href="http://www.lrec-conf.org/proceedings/lrec2016/pdf/1131_Paper.pdf" target=_blank">Rosén et al. 2015</a>). Lexical resources with MWEs in dozens of languages exist and are still being developed (<a href="http://www.lrec-conf.org/proceedings/lrec2016/pdf/718_Paper.pdf" target="_blank">Losnegaard et al. 2016</a>). Many papers describe methods to discover new MWEs in texts, applying a wide variety of tools and techniques such as association measures, distributional methods and machine learning. Interactions of MWE processing with deeper levels of linguistic analysis, notably parsing and semantic processing, are being increasingly investigated (e.g. in SEMEVAL 2016 task 10 - <a href="http://dimsum16.github.io/" target=_blank">DiMSUM</a>). Special issues on MWEs have been published by leading journals (<a href="http://www.sciencedirect.com/science/journal/08852308/19/4" target="_blank">CSL in 2005</a>, <a href="https://www.jstor.org/stable/i40028998" target="_blank">LR&E in 2010</a>, <a href="http://dl.acm.org/citation.cfm?id=2483691" target="_blank">ACM TSLP in 2013</a>). Several funded projects focusing on MWEs are indicative of the growing importance of the field within the NLP community. For instance, the EU H2020 program currently supports the COST Action <a href="http://www.parseme.eu" target="_blank">PARSEME</a> (2013-2017), that addresses the role of MWEs in parsing and gathers more than 200 researchers from 33 countries covering 30 languages. It also inspired several <a href="http://typo.uni-konstanz.de/parseme/index.php/related-links/spin-off-and-related-national-projects" target="_blank">national spin-off projects</a> on MWEs.</p>

<p>Many of these advances are described and published in the annual MWE workshop. It attracts the attention of an ever-growing community working on a variety of languages and linguistic phenomena. The workshop has been held since 2001 in conjunction with major computational linguistics conferences (ACL, COLING, LREC, EACL). It represents an important venue for the community to interact, share resources and tools, and collaborate on efforts for advancing the computational treatment of MWEs.</p>

<p>We call for papers on major challenges in MWE processing, both from the theoretical and the computational viewpoint, focusing on original research related (but not limited) to the following topics:</p>

<ul> 
<li>Manually and automatically constructed lexical resources</li>
<li>MWE representation in lexical resources</li>
<li>MWE annotation in corpora and treebanks</li>
<li>MWEs in non-standard language (e.g. tweets, forums, spontaneous speech)</li>
<li>Original MWE discovery methods (e.g. using word embeddings, parallel corpora)</li>
<li>Original MWE in-context identification methods (e.g. using deep learning, topic models)</li>
<li>MWE processing in syntactic frameworks (e.g. HPSG, LFG, TAG, universal dependencies)</li>
<li>MWE processing in semantic frameworks (e.g. WSD, semantic parsing)</li>
<li>MWE processing in end-user applications (e.g. summarization, machine translation)</li>
<li>Orchestration of MWE processing with respect to applications</li>
<li>Evaluation of MWE processing techniques</li>
<li>Models of first and second language acquisition of MWEs</li>
<li>Theoretical and psycholinguistic studies on MWEs</li>
<li>Crosslinguistic studies on MWEs</li>
</ul>

<p>This year, we propose to have an extension to the traditional workshop by including a special track for shared task papers. The <a href="http://multiword.sourceforge.net/sharedtask2017" target="_blank">shared task</a> will compare and evaluate systems for the automatic identification of verbal MWEs in sentences (see below). Participants will have the opportunity to submit shared task system description papers and present their approach and results at the workshop.</p>

<p>The organizers are planning the edition of selected papers from the workshop in a volume of the <i>Phraseology and Multiword Expressions</i> book series, whose creation has been accepted by <a href="http://langsci-press.org/" target="_blank">Language Science Press</a>.</p>

<!------------------------------------->
<h1>Special Track: PARSEME Shared Task on Automatic Verbal MWE Identification</h1>

<p>In addition to the main scientific program, we propose to have a special track dedicated to the <a href="http://multiword.sourceforge.net/sharedtask2017" target="_blank">shared task on automatic verbal MWE identification</a>. A separate session will be allocated for the special track within the workshop. 
<!-- Language teams will summarize results in oral presentations and a poster/demo session will be dedicated to participating systems. -->
Language teams will present their systems and summarize their results in a poster/demo session. 
Authors may submit papers either to the special track or to the regular workshop. They should follow common submission instructions, based on those of the main conference. The special track is endorsed and partly funded by <a href="http://www.parseme.eu" target="_blank">PARSEME</a>.</p>

<!------------------------------------->
<h1><a name="submissions"></a>Submission modalities</h1>

<p>The <strong>main track</strong> will feature long and short papers:</p>
<ul>
<li><strong>Long papers</strong> (8 content pages + references): Long papers should report on solid and finished research including new experimental results, resources and/or techniques.</li>
<li><strong>Short papers</strong> (4 content pages + references): Short papers should report on small experiments, focused contributions, ongoing research, negative results and/or philosophical discussion.</li>
</ul>

<p>The <strong>shared task track</strong> will feature system description papers:</p>
<ul>
<li><strong>System description papers</strong> (4 content pages + references): System description papers briefly describe the approach implemented to solve the problem. They may include references and links to more detailed descriptions in other documents.</li>
</ul>

<p>There is no limit on the number of reference pages. Authors will be granted an extra page for the final version of their papers.</p>

<p>For the main track, submission will be double-blind, the reported research should be substantially original and the papers will be presented orally or as posters. The decision as to which papers will be presented orally and which as posters will be made by the program committee based on the nature rather than on the quality of the work.</p>

<p>The shared task system description papers will go through a separate reviewing process. Like in SEMEVAL, submissions will be double-blind and will be reviewed by the shared task organizers and participants according to the schedule below. The selected papers will be presented as posters. Participants of the shared task are not required to submit system description papers, and their acceptance depends on the quality of the paper rather than on the results obtained in the shared task.</p>

<p>For all types of submission, the EACL 2017 LaTeX <a href="http://eacl2017.org/index.php/calls/call-for-papers">templates</a> should be used. In accordance to EACL 2017 submission policy, this is a condition for accepting the paper for the reviewing process. Final versions of accepted papers will be submitted both in PDF and source LaTeX formats.</p>

<p>All papers should be submitted via the <a href="https://www.softconf.com/eacl2017/mwe2017" target="_blanc">START space</a>. Please choose the appropriate submission type, according to the category of your paper.</p>

<!------------------------------------->
<h1><a name="grants"></a>PARSEME travel grants</h1>

PARSEME will fund travel and stay for over 30 participants from the Action's <a href="http://www.cost.eu/COST_Actions/ict/IC1207?parties" target="_blanc">member countries</a>. The modalities are available on a <a href="http://typo.uni-konstanz.de/parseme/index.php/2-general/198-mwe-workshop-at-eacl-2017" target="_blanc">dedicated page</a>.

<!------------------------------------->
<h1>Important dates</h1>

<table>
<tr><td><strike>Jan <strike>16</strike> 22, 2017</strike></td><td><strike>(Extended) submission <strong>deadline</strong> for the main track long &amp; short papers</strike> <!--at 23:59 Pacific time - GMT-8 --></td></tr>
<tr><td><strike>Feb 5, 2017</strike></td><td><strike>Submission <strong>deadline</strong> for <a href="http://multiword.sourceforge.net/sharedtask2017" target="_blank">shared task</a> system description papers <!--at 23:59 Pacific time - GMT-8 --><strike></td></tr>
<tr><td><strike>Feb 11, 2017</strike></td><td><strike>Notification of acceptance for the main track papers</strike></td></tr>
<tr><td><strike>Feb 12, 2017</strike></td><td><strike>Notification of acceptance for the shared task papers</strike></td></tr>
<tr><td><strike>Feb 20, 2017</strike></td><td><strike>Camera-ready papers due (main track and shared task)</strike></td></tr>
<tr><td><strike>April 4, 2017</strike></td><td><strike>MWE 2017 Workshop</strike></td></tr>
</table>
See also the important dates for the <a href="http://multiword.sourceforge.net/sharedtask2017" target="_blank">shared task</a> systems.

<!------------------------------------->
<h1>Workshop Organizers</h1>
<ul>
<li><a href="http://www.ilsp.gr/en/profile/staff?view=member&id=38&task=show" target="_blank">Stella Markantonatou</a>, Institute for Language and Speech Processing (Greece) </li>
<li><a href="http://pageperso.lif.univ-mrs.fr/~carlos.ramisch" target="_blank">Carlos Ramisch</a>, Aix Marseille University (France) </li>
<li><a href="http://www.info.univ-tours.fr/~savary/" target="_blank">Agata Savary</a>, Université François Rabelais Tours (France) </li>
<li><a href="http://www.inf.u-szeged.hu/~vinczev" target="_blank">Veronika Vincze</a>, Hungarian Academy of Sciences (Hungary) </li>
</ul>

<!------------------------------------->
<h1>Contact</h1>
<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwe2017workshop at gmail.com">mwe2017workshop at gmail.com</a></p>

<!------------------------------------->
<h1>Anti-harassment policy</h1>
<p>The MWE 2017 workshop, as an EACL-colocated event, supports the ACL <a href="https://www.aclweb.org/adminwiki/index.php?title=Anti-Harassment_Policy" target=_blanc">anti-harassment policy</a>.</p>

