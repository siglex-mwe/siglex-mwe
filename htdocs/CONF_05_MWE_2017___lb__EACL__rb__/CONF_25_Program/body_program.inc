<h2>The 13th Workshop on Multiword Expressions (MWE 2017)</h2>

<h3>Workshop at <a href="http://eacl2017.org/" target="_blanc">EACL 2017</a> (Valencia, Spain), April 4, 2017</h3>

<p>Endorsed by the Special Interest Group on the Lexicon (<a href="http://www.siglex.org" target="_blank">SIGLEX</a>) of the Association for Computational Linguistics and <a href="http://www.parseme.eu" target="_blanc">PARSEME</a>, the European IC1207 COST Action</p>

<p><small>Last updated: March 30, 2017</small></p>

<h2>Selected papers</h2>

<p>34 papers (22 short and 12 long) were submitted to the <strong>main track</strong> of the workshop. 7 papers (4 short and 3 long) were selected as oral presentations and 14 (11 short and 3 long) as posters. The overall <strong>selectivity rate is 62%</strong>.</p>

<p>6 system description papers and 1 shared task description paper were submitted to the <strong>shared task track</strong>. The former were all selected as posters and the latter as an oral presentation. The reviewing modalities were different in this track, therefore we do not count these papers in the workshop selectivity rate.</p>

<!--
<center>
<table cellspacing="0" cellpadding="0" border="0">
	<tr>
                <td colspan="3"><a href="index.html">ACL 2014 Proceedings Home</a> | <a href="http://acl2014.org">ACL 2014 WEBSITE</a> | <a href="http://www.aclweb.org/">ACL WEBSITE</a></td>
	</tr>
</table>
<br><br>
-->

<!---------------------------------->
<h2>Program</h2>
<table cellspacing="0" cellpadding="3" border="0"><tr><td colspan=2 style="padding-top: 0px;"><h4>Tuesday, April 4 2017 (9:20-14:50 in Room 3; 14:50-16:00 in Auditoriums 2 and 3; 16:30-18:30 in Room 3)</h4></td></tr>
<tr><td valign=top style="padding-top: 2px;"><b>09:15&#8211;09:20</b></td><td valign=top style="padding-top: 2px;"><b><em>Opening Remarks</em></b></td></tr>
<tr><td valign=top style="padding-top: 2px;"><b>09:20&#8211;09:30</b></td><td valign=top style="padding-top: 2px;"><b><em>COST &#8211; Creating spaces for people and ideas to grow <br> Ralph Stuebner</em></b></td></tr>
<tr><td valign=top style="padding-top: 14px;">&nbsp;</td><td valign=top style="padding-top: 14px;"><b>Oral Session - Long Papers</b> (chair: Marie Candito)</td></tr>
<tr><td valign=top width=100>09:30&#8211;10:00</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201701.pdf" target="_blank"><i>ParaDi: Dictionary of Paraphrases of Czech Complex Predicates with Light Verbs</i></a><br>
Petra Barancikova and V&aacute;clava Kettnerov&aacute;</td></tr>
<tr><td valign=top width=100>10:00&#8211;10:30</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201702.pdf" target="_blank"><i>Multi-word Entity Classification in a Highly Multilingual Environment</i></a><br>
Sophie Chesney, Guillaume Jacquet, Ralf Steinberger and Jakub Piskorski</td></tr>
<tr><td valign=top width=100>10:30&#8211;11:00</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201703.pdf" target="_blank"><i>Using bilingual word-embeddings for multilingual collocation extraction</i></a><br>
Marcos Garcia, Marcos Garc&iacute;a-Salido and Margarita Alonso-Ramos</td></tr>
<tr><td valign=top style="padding-top: 14px;"><b>11:00&#8211;11:30</b></td><td valign=top style="padding-top: 14px;"><b><em>COFFEE BREAK</em></b></td></tr>
<!-- <tr><td valign=top style="padding-top: 14px;">&nbsp;</td><td valign=top style="padding-top: 14px;"><b></b></td></tr target="_blank">-->
<tr><td valign=top style="padding-top: 14px;"><b>11:30&#8211;12:30</b></td><td valign=top style="padding-top: 14px;"><b><em>Invited Talk</em></b> (chair: Carlos Ramisch)<br />
<b><em><a href="mwe2017/slides/mwe2017-cook.pdf" target="_blank">Exploiting multilingual lexical resources to predict the compositionality of MWEs</em></a> <br />Paul Cook</em></b> </td></tr>
<tr><td valign=top style="padding-top: 14px;">&nbsp;</td><td valign=top style="padding-top: 14px;"><b>Shared Task Session</b> (chair: Carlos Ramisch)</td></tr>
<tr><td valign=top width=100>12:30&#8211;12:50</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201704.pdf" target="_blank"><i>The PARSEME Shared Task on Automatic Identification of Verbal Multiword Expressions</i></a><br>
Agata Savary, Carlos Ramisch, Silvio Cordeiro, Federico Sangati, Veronika Vincze, Behrang QasemiZadeh, Marie Candito, Fabienne Cap, Voula Giouli, Ivelina Stoyanova and Antoine Doucet</td></tr>
<tr><td valign=top style="padding-top: 14px;"><b>12:50&#8211;13:00</b></td><td valign=top style="padding-top: 14px;"><b><em>Shared Task Poster Boosters (1min per poster - ST1 to ST6)</em></b></td></tr>
<tr><td valign=top style="padding-top: 14px;"><b>13:00&#8211;14:30</b></td><td valign=top style="padding-top: 14px;"><b><em>LUNCH</em></b></td></tr>
<tr><td valign=top style="padding-top: 14px;"><b>14:30&#8211;14:50</b></td><td valign=top style="padding-top: 14px;"><b><em>Poster Session Boosters (1min per poster - A01 to A07, B08 to B14)</em></b></td></tr>
<tr><td valign=top style="padding-top: 14px;"><b>14:50&#8211;15:25</b></td><td valign=top style="padding-top: 14px;"><b><em>Poster Session A</em></b></td></tr>
<tr><td valign=top width=100>ST1</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201705.pdf" target="_blank"><i>USzeged: Identifying Verbal Multiword Expressions with POS Tagging and Parsing Techniques</i></a><br>
Katalin Ilona Simk&oacute;, Vikt&oacute;ria Kov&aacute;cs and Veronika Vincze</td></tr>
<tr><td valign=top width=100>ST2</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201706.pdf" target="_blank"><i>Parsing and MWE Detection: Fips at the PARSEME Shared Task</i></a><br>
Luka Nerima, Vasiliki Foufi and Eric Wehrli</td></tr>
<tr><td valign=top width=100>ST3</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201707.pdf" target="_blank"><i>Neural Networks for Multi-Word Expression Detection</i></a><br>
Natalia Klyueva, Antoine Doucet and Milan Straka</td></tr>
<tr><td valign=top width=100>A01</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201708.pdf" target="_blank"><i>Factoring Ambiguity out of the Prediction of Compositionality for German Multi-Word Expressions</i></a><br>
Stefan Bott and Sabine Schulte im Walde</td></tr>
<tr><td valign=top width=100>A02</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201709.pdf" target="_blank"><i>Multiword expressions and lexicalism: the view from LFG</i></a><br>
Jamie Y. Findlay</td></tr>
<tr><td valign=top width=100>A03</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201710.pdf" target="_blank"><i>Understanding Idiomatic Variation</i></a><br>
Kristina Geeraert, R. Harald Baayen and John Newman</td></tr>
<tr><td valign=top width=100>A04</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201711.pdf" target="_blank"><i>Discovering Light Verb Constructions and their Translations from Parallel Corpora without Word Alignment</i></a><br>
Natalie Vargas, Carlos Ramisch and Helena Caseli</td></tr>
<tr><td valign=top width=100>A05</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201712.pdf" target="_blank"><i>Identification of Multiword Expressions for Latvian and Lithuanian: Hybrid Approach</i></a><br>
Justina Mandravickaite and Tomas Krilavi&#269;ius</td></tr>
<tr><td valign=top width=100>A06</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201713.pdf" target="_blank"><i>Show Me Your Variance and I Tell You Who You Are - Deriving Compound Compositionality from Word Alignments</i></a><br>
Fabienne Cap</td></tr>
<tr><td valign=top width=100>A07</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201714.pdf" target="_blank"><i>Semantic annotation to characterize contextual variation in terminological noun compounds: a pilot study</i></a><br>
Melania Cabezas-Garc&iacute;a and Antonio San Mart&iacute;n</td></tr>
<tr><td valign=top style="padding-top: 14px;"><b>15:25&#8211;16:00</b></td><td valign=top style="padding-top: 14px;"><b><em>Poster Session B</em></b></td></tr>
<tr><td valign=top width=100>ST4</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201715.pdf" target="_blank"><i>Detection of Verbal Multi-Word Expressions via Conditional Random Fields with Syntactic Dependency Features and Semantic Re-Ranking</i></a><br>
Alfredo Maldonado, Lifeng Han, Erwan Moreau, Ashjan Alsulaimani, Koel Dutta Chowdhury, Carl Vogel and Qun Liu</td></tr>
<tr><td valign=top width=100>ST5</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201716.pdf" target="_blank"><i>A data-driven approach to verbal multiword expression detection. PARSEME Shared Task system description paper</i></a><br>
Tiberiu Boro&#351;, Sonia Pipa, Verginica Barbu Mititelu and Dan Tufi&#351;</td></tr>
<tr><td valign=top width=100>ST6</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201717.pdf" target="_blank"><i>The ATILF-LLF System for Parseme Shared Task: a Transition-based Verbal Multiword Expression Tagger</i></a><br>
Hazem Al Saied, Marie Candito and Matthieu Constant</td></tr>
<tr><td valign=top width=100>B08</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201718.pdf" target="_blank"><i>Investigating the Opacity of Verb-Noun Multiword Expression Usages in Context</i></a><br>
Shiva Taslimipoor, Omid Rohanian, Ruslan Mitkov and Afsaneh Fazly</td></tr>
<tr><td valign=top width=100>B09</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201719.pdf" target="_blank"><i>Compositionality in Verb-Particle Constructions</i></a><br>
Archna Bhatia, Choh Man Teng and James Allen</td></tr>
<tr><td valign=top width=100>B10</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201720.pdf" target="_blank"><i>Rule-Based Translation of Spanish Verb-Noun Combinations into Basque</i></a><br>
Uxoa I&ntilde;urrieta, Itziar Aduriz, Arantza Diaz de Ilarraza, Gorka Labaka and Kepa Sarasola</td></tr>
<tr><td valign=top width=100>B11</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201721.pdf" target="_blank"><i>Verb-Particle Constructions in Questions</i></a><br>
Veronika Vincze</td></tr>
<tr><td valign=top width=100>B12</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201722.pdf" target="_blank"><i>Simple Compound Splitting for German</i></a><br>
Marion Weller-Di Marco</td></tr>
<tr><td valign=top width=100>B13</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201723.pdf" target="_blank"><i>Identification of Ambiguous Multiword Expressions Using Sequence Models and Lexical Resources</i></a><br>
Manon Scholivet and Carlos Ramisch</td></tr>
<tr><td valign=top width=100>B14</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201724.pdf" target="_blank"><i>Comparing Recurring Lexico-Syntactic Trees (RLTs) and Ngram Techniques for Extended Phraseology Extraction</i></a><br>
Agn&egrave;s Tutin and Olivier Kraif</td></tr>
<tr><td valign=top style="padding-top: 14px;"><b>16:00&#8211;16:30</b></td><td valign=top style="padding-top: 14px;"><b><em>COFFEE BREAK</em></b></td></tr>
<tr><td valign=top style="padding-top: 14px;">&nbsp;</td><td valign=top style="padding-top: 14px;"><b>Oral Session - Short Papers</b> (chair: Shuly Wintner)</td></tr>
<tr><td valign=top width=100>16:30&#8211;16:50</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201725.pdf" target="_blank"><i>Benchmarking Joint Lexical and Syntactic Analysis on Multiword-Rich Data</i></a><br>
Matthieu Constant and H&eacute;ctor Mart&iacute;nez Alonso</td></tr>
<tr><td valign=top width=100>16:50&#8211;17:10</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201726.pdf" target="_blank"><i>Semi-Automated Resolution of Inconsistency for a Harmonized Multiword Expression and Dependency Parse Annotation</i></a><br>
King Chan, Julian Brooke and Timothy Baldwin</td></tr>
<tr><td valign=top width=100>17:10&#8211;17:30</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201727.pdf" target="_blank"><i>Combining Linguistic Features for the Detection of Croatian Multiword Expressions</i></a><br>
Maja Buljan and Jan &#352;najder</td></tr>
<tr><td valign=top width=100>17:30&#8211;17:50</td><td valign=top align=left><a href="mwe2017/proceedings/MWE201728.pdf" target="_blank"><i>Complex Verbs are Different: Exploring the Visual Modality in Multi-Modal Models to Predict Compositionality</i></a><br>
Maximilian K&ouml;per and Sabine Schulte im Walde</td></tr>
<tr><td valign=top style="padding-top: 14px;"><b>17:50&#8211;18:30</b></td><td valign=top style="padding-top: 14px;"><b><em><a href="mwe2017/slides/MWE-2017-panel-slides.pdf" target="_blank">Panel Discussion - Multiword Expressions: getting the taste of things to come</a></em></b> (chairs: Stella Markantonatou, Carlos Ramisch, Agata Savary, Veronika Vincze) <br />
<ul>
<li>News about MWE-related initiatives, projects and proposals</li>
<li>Reorganising the SIGLEX-MWE section</li>
<li>Shared task - second edition</li>
</ul>
</table>

<!-----------------------------------------
<h1>Papers selected in the main track</h1>

<ol>
<li>BENCHMARKING JOINT LEXICAL AND SYNTACTIC ANALYSIS ON MULTIWORD-RICH DATA<br />
Matthieu Constant and Héctor Martínez Alonso</li>

<li>COMBINING LINGUISTIC FEATURES FOR THE IDENTIFICATION OF CROATIAN MULTIWORD EXPRESSIONS<br />
Maja Buljan and Jan Šnajder</li>

<li>COMPARING RECURRING LEXICO-SYNTACTIC TREES (RLTS) AND NGRAM TECHNIQUES FOR EXTENDED PHRASEOLOGY EXTRACTION<br />
Agnès Tutin and Olivier Kraif</li>

<li>COMPLEX VERBS ARE DIFFERENT: EXPLORING THE VISUAL MODALITY IN MULTI-MODAL MODELS TO PREDICT COMPOSITIONALITY<br />
Maximilian Köper and Sabine Schulte im Walde</li>

<li>COMPOSITIONALITY IN VERB-PARTICLE CONSTRUCTIONS<br />
Archna Bhatia, Choh Man Teng and James Allen</li>

<li>DISCOVERING MULTIWORD EXPRESSIONS AND THEIR TRANSLATIONS FROM PARALLEL CORPORA WITHOUT WORD ALIGNMENT<br />
Natalie Vargas, Carlos Ramisch and Helena Caseli</li>

<li>FACTORING AMBIGUITY OUT OF THE PREDICTION OF COMPOSITIONALITY FOR GERMAN MULTI-WORD EXPRESSIONS<br />
Stefan Bott and Sabine Schulte im Walde</li>

<li>IDENTIFICATION OF AMBIGUOUS MULTIWORD EXPRESSIONS USING SEQUENCE MODELS AND LEXICAL RESOURCES<br />
Manon Scholivet and Carlos Ramisch</li>

<li>IDENTIFICATION OF MULTIWORD EXPRESSIONS FOR LATVIAN AND LITHUANIAN: HYBRID APPROACH<br />
Justina Mandravickaite and Tomas Krilavičius</li>

<li>INVESTIGATING THE OPACITY OF VERB-NOUN MULTIWORD EXPRESSION USAGES IN CONTEXT<br />
Shiva Taslimipoor, Omid Rohanian, Ruslan Mitkov and Afsaneh Fazly</li>

<li>MULTI-WORD ENTITY CLASSIFICATION IN A HIGHLY MULTILINGUAL ENVIRONMENT<br />
Sophie Chesney, Guillaume Jacquet, Ralf Steinberger and Jakub Piskorski</li>

<li>MULTIWORD EXPRESSIONS AND LEXICALISM: THE VIEW FROM LFG<br />
Jamie Y. Findlay</li>

<li>PARADI: DICTIONARY OF PARAPHRASES OF CZECH COMPLEX PREDICATES WITH LIGHT VERBS<br />
Petra Barancikova and Václava Kettnerová</li>

<li>RULE-BASED TRANSLATION OF SPANISH VERB-NOUN COMBINATIONS INTO BASQUE<br />
Uxoa Iñurrieta, Itziar Aduriz, Arantza Diaz de Ilarraza, Gorka Labaka and Kepa Sarasola</li>

<li>SEMANTIC ANNOTATION TO CHARACTERIZE CONTEXTUAL VARIATION IN TERMINOLOGICAL NOUN COMPOUNDS: A PILOT STUDY<br />
Melania Cabezas-García and Antonio San Martín</li>

<li>SEMI-AUTOMATED RESOLUTION OF INCONSISTENCY FOR A HARMONIZED MULTIWORD EXPRESSION AND DEPENDENCY PARSE ANNOTATION<br />
King Chan, Julian Brooke and Timothy Baldwin</li>

<li>SHOW ME YOUR VARIANCE AND I TELL YOU WHO YOU ARE - DERIVING COMPOUND COMPOSITIONALITY FROM WORD ALIGNMENTS<br />
Fabienne Cap</li>

<li>SIMPLE COMPOUND SPLITTING FOR GERMAN<br />
Marion Weller-Di Marco</li>

<li>UNDERSTANDING IDIOMATIC VARIATION<br />
Kristina Geeraert, R. Harald Baayen and John Newman</li>

<li>USING BILINGUAL WORD-EMBEDDINGS FOR MULTILINGUAL COLLOCATION EXTRACTION<br />
Marcos Garcia, Marcos García-Salido and Margarita Alonso-Ramos</li>

<li>VERB-PARTICLE CONSTRUCTIONS IN QUESTIONS<br />
Veronika Vincze</li>

</ol>

<h1>Papers selected in the shared task track</h1>

<ol>
<li>A TRANSITION-BASED VERBAL MULTIWORD EXPRESSION TAGGER DESCRIPTION PAPER<br />
Hazem al saied, Mathieu CONSTANT and Marie CANDITO</li>

<li>A DATA-DRIVEN APPROACH TO VERBAL MULTIWORD EXPRESSION DETECTION. PARSEME SHARED TASK SYSTEM DESCRIPTION PAPER<br />
Tiberiu Boroș, Sonia Pipa, Verginica Barbu Mititelu and Dan Tufiș</li>

<li>DETECTION OF VERBAL MULTI-WORD EXPRESSIONS VIA CONDITIONAL RANDOM FIELDS WITH SYNTACTIC DEPENDENCY FEATURES AND SEMANTIC RE-RANKING<br />
Alfredo Maldonado, Aaron Lifeng, Erwan Moreau, Ashjan Alsulaimani and
Koel Dutta Chowdhury</li>

<li>NEURAL NETWORKS FOR MULTI-WORD EXPRESSION DETECTION<br />
Natalia Klyueva, Antoine Doucet and Milan Straka</li>

<li>PARSING AND MWE DETECTION<br />
Luka Nerima, Vasiliki Foufi and Eric Wehrli</li>

<li>THE PARSEME SHARED TASK ON AUTOMATIC IDENTIFICATION OF VERBAL MULTIWORD EXPRESSIONS<br />
Agata Savary, Carlos Ramisch, Silvio Cordeiro, Federico Sangati,
Veronika Vincze, Behrang QasemiZadeh, Marie Candito, Fabienne Cap,
Voula Giouli, Ivelina Stoyanova and Antoine Doucet</li>

<li>USZEGED: IDENTIFYING VERBAL MULTIWORD EXPRESSIONS WITH POS TAGGING AND PARSING TECHNIQUES<br />
Katalin Ilona Simkó, Viktória Kovács and Veronika Vincze</li>

</ol>
------------------------------------------------>


