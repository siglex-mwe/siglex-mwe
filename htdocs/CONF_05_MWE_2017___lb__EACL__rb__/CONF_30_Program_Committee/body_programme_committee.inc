<h2>The 13th Workshop on Multiword Expressions (MWE 2017)</h2>

<h3>Workshop at <a href="http://eacl2017.org/" target="_blanc">EACL 2017</a> (Valencia, Spain), April 4, 2017</h3>

<p>Endorsed by the Special Interest Group on the Lexicon (<a href="http://www.siglex.org" target="_blank">SIGLEX</a>) of the Association for Computational Linguistics and <a href="http://www.parseme.eu" target="_blanc">PARSEME</a>, the European IC1207 COST Action</p>

<p><small>Last updated: January 25, 2017</small></p>

<h1>Program Committee</h1>

<ul>

<li>Iñaki Alegria, University of the Basque Country (Spain) </li>
<li>Anna Anastasiadi-Symeonidi, Aristoteleian University of Thessaloniki (Greece) </li>
<li>Dimitra Anastasiou, Luxemburg Institute of Science and Technology (Luxembourg) </li>
<li>Doug Arnold, University of Essex (UK) </li>
<li>Tim Baldwin, University of Melbourne (Australia) </li>
<li>Eduard Bejček, Charles University (Czech Republic) </li>
<li>Francis Bond, Nanyang Technological University (Singapore) </li>
<li>Antonio Branco, University of Lisbon  (Portugal) </li>
<li>Miriam Butt, Universität Konstanz (Germany) </li>
<li>Marie Candito, Paris Diderot University (France) </li>
<li>Fabienne Cap, Uppsala University (Sweden) </li>
<li>Marine Carpuat, University of Maryland (USA) </li>
<li>Helena Caseli, Federal University of Sao Carlos (Brazil) </li>
<li>Anastasia Christofidou, Academy of Athens (Greece) </li>
<li>Ken Church, IBM Research (USA) </li>
<li>Matthieu Constant, Université de Lorraine (France) </li>
<li>Silvio Cordeiro, Federal University of Rio Grande do Sul (Brazil) </li>
<li>Béatrice Daille, Nantes University (France) </li>
<li>Koenraad de Smedt, University of Bergen (Norway) </li>
<li>Mona Diab, Columbia University (USA) </li>
<li>Gaël Dias, University of Caen Basse-Normandie (France) </li>
<li>Gülşen Eryiğit , Istanbul Technical University (Turkey) </li>
<li>Stefan Evert, FAU Erlangen-Nürnberg (Germany) </li>
<li>Meghdad Farahmand, University of Geneva (Switzerland) </li>
<li>Joaquim Ferreira da Silva, New University of Lisbon (Portugal) </li>
<li>Dan Flickinger, Stanford University (USA) </li>
<li>Aggeliki Fotopoulou, ILSP/RC "Athena" (Greece) </li>
<li>Voula Giouli, Institute for Language and Speech Processing (Greece) </li>
<li>Antton Gurrutxaga, Elhuyar Foundation (Basque Country, Spain) </li>
<li>Chikara Hashimoto, Yahoo!Japan (Japan) </li>
<li>Kyo Kageura, University of Tokyo (Japan) </li>
<li>Philipp Koehn, University of Edinburgh (UK) </li>
<li>Dimitris Kokkinakis, University of Gothenburg (Sweden) </li>
<li>Ioannis Korkontzelos, Edge Hill University (UK) </li>
<li>Brigitte Krenn, Austrian Research Institute for Artificial Intelligence (Austria) </li>
<li>Cvetana Krstev, University of Belgrade (Serbia) </li>
<!-- <li>Tita Kyriakopoulou, University Paris-Est Marne-la-Vallee (France) </li> -->
<li>Eric Laporte, University Paris-Est Marne-la-Vallee (France) </li>
<li>Evita Linardaki, Hellenic Open University (Greece) </li>
<li>Ismail el Maarouf, Adarga Ltd (UK) </li>
<li>Hector Martínez Alonso, INRIA (France) </li>
<li>Diana McCarthy, University of Cambridge (UK) </li>
<li>Johanna Monti, "L'Orientale" University of Naples (Italy) </li>
<li>Preslav Nakov, Qatar Computing Research Institute, HBKU (Qatar) </li>
<li>Joakim Nivre, Uppsala University (Sweden) </li>
<li>Diarmuid Ó Séaghdha, University of Cambridge (UK) </li>
<li>Michael Oakes, University of Wolverhampton (UK) </li>
<li>Jan Odijk, University of Utrecht (The Netherlands) </li>
<li>Petya Osenova , Bulgarian Academy of Sciences (Bulgaria) </li>
<li>Haris Papageorgiou, Institute for Language and Speech Processing (Greece) </li>
<li>Yannick Parmentier, Université d'Orléans (France) </li>
<li>Carla Parra Escartín, Dublin City University (Ireland) </li>
<li>Agnieszka Patejuk, Institute of Computer Science, Polish Academy of Sciences (Poland) </li>
<li>Pavel Pecina, Charles University (Czech Republic) </li>
<li>Scott Piao, Lancaster University (UK) </li>
<li>Thierry Poibeau, CNRS and École Normale Supérieure (France) </li>
<li>Martin Riedl, University of Hamburg (Germany) </li>
<li>Mike Rosner, University of Malta (Malta) </li>
<li>Manfred Sailer, Goethe-Universität Frankfurt am Main (Germany) </li>
<li>Magali Sanches Duran, University of São Paulo (Brazil) </li>
<li>Federico Sangati, Independent researcher (Italy) </li>
<li>Nathan Schneider, Georgetown University (USA) </li>
<li>Sabine Schulte im Walde, University of Stuttgart (Germany) </li>
<li>Serge Sharoff, University of Leeds (UK) </li>
<li>Kiril Simov, Bulgarian Academy of Sciences (Bulgaria) </li>
<li>Sara Stymne, Uppsala University (Sweden) </li>
<li>Stan Szpakowicz, University of Ottawa (Canada) </li>
<li>Beata Trawinski, Institut für Deutsche Sprache Mannheim (Germany) </li>
<li>Yulia Tsvetkov, Carnegie Mellon University (USA) </li>
<li>Yuancheng Tu, Microsoft (USA) </li>
<li>Ruben Urizar, University of the Basque Country (Spain) </li>
<li>Lonneke van der Plas, University of Malta (Malta) </li>
<li>Gertjan van Noord, University of Groningen (The Netherlands) </li>
<li>Aline Villavicencio, Federal University of Rio Grande do Sul (Brazil) </li>
<li>Tom Wasow, Stanford University (USA) </li>
<!-- <li>Eric Wehrli, University of Geneva (Switzerland) </li> -->
<li>Marion Weller-Di Marco, University of Stuttgart (Germany) </li>
<li>Shuly Wintner, University of Haifa (Israel) </li>

</ul>


<h1>Workshop Organizers</h1>

<ul>
<li><a href="http://www.ilsp.gr/en/profile/staff?view=member&id=38&task=show" target="_blank">Stella Markantonatou</a>, Institute for Language and Speech Processing (Greece) </li>
<li><a href="http://pageperso.lif.univ-mrs.fr/~carlos.ramisch" target="_blank">Carlos Ramisch</a>, Aix Marseille University (France) </li>
<li><a href="http://www.info.univ-tours.fr/~savary/" target="_blank">Agata Savary</a>, Université François Rabelais Tours (France) </li>
<li><a href="http://www.inf.u-szeged.hu/~vinczev" target="_blank">Veronika Vincze</a>, Hungarian Academy of Sciences (Hungary) </li>
</ul>


<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwe2017workshop at gmail.com">mwe2017workshop at gmail.com</a></p>
