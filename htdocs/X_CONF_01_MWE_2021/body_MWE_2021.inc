<h2>17th Workshop on Multiword Expressions (MWE 2021)</h2>

<h3>Please access our new website <a href="https://multiword.org/mwe2021">https://multiword.org/mwe2021</a></h3>

<!--<h3>Workshop proposal</h3> <!--at <a href="http://coling2020.org/" target="_blank">COLING 2020</a> (<del>Barcelona, Spain</del> online), <del>September 14</del> December 13, 2020.</h3>-->

<!--<p>Organized and sponsored by: <br/>
Special Interest Group on the Lexicon (<a href="http://www.siglex.org" target="_blank">SIGLEX</a>) of the Association for Computational Linguistics (<a href="https://www.aclweb.org/portal/" target="_blank">ACL</a>)
</p>

<p><small>Last updated: Oct 8, 2020</small></p>-->

<!--<ul>
<li><strong><font color="red">NEW!</font> [Aug 31, 2020]</strong> The MWE-LEX workshop paper submission deadline has been extended to <strong>September 9, 23:59</strong> (anywhere in the world).</li>
</ul>-->

<!------------------------------------------------------------------------------>

<!--<h1>Description</h1>

<p>Multiword expressions (MWEs) are word combinations which exhibit lexical, syntactic, semantic, pragmatic and/or statistical idiosyncrasies (Baldwin & Kim 2010), such as by and large, hot dog, pay a visit and pull one's leg. The notion encompasses closely related phenomena: idioms, compounds, light-verb constructions, rhetorical figures, institutionalised phrases, collocations, etc. The behaviour of MWEs is often unpredictable, in particular their meanings are not regularly composed of the meanings of their parts. Thus, MWEs pose problems in linguistic modelling (e.g. treebanking), computational modelling (e.g. parsing), and end-user NLP applications such as natural language understanding, machine translation, and social media mining (Constant et al. 2017). 

<p>Modelling and processing MWEs for NLP has been the topic of the MWE workshop organised by the MWE section of SIGLEX in conjunction with major NLP conferences since 2003. Although much progress has been made in the field, MWE processing in end-user NLP tasks is currently under-explored, and most studies still introduce MWEs as future work. Nonetheless, there are recent studies in which MWEs gained particular attention in end-user applications, including machine translation (Zaninello & Birch 2020), text simplification (Kochmar et al. 2020, Liu & Hwa 2016) —SemEval 2021 task 1 has a subtask on predicting MWE complexity—, language learning and assessment (Paquot et al. 2019, Christiansen & Arnon 2017), social media mining (Maisto et al. 2017), and abusive language detection (Zampieri et al. 2020, Caselli et al. 2020).

<p>For this 17th edition of the workshop, we propose a special focus on MWE processing in end-user applications such as those listed above. On the one hand, the PARSEME shared tasks (Ramisch et al., 2018, Savary et al., 2017), among others, fostered significant progress in MWE identification, providing datasets, evaluation measures and tools that now allow fully integrating MWE identification into end-user applications. On the other hand, NLP seems to be shifting towards end-to-end neural models capable of solving complex end-user tasks with little or no intermediary linguistic symbols, questioning the extent to which MWEs should be implicitly or explicitly modelled. Therefore, we would like to bring together and encourage researchers in various NLP subfields to submit MWE-related research, so that approaches that deal with MWEs in various applications could benefit from each other.

<p>We also intend to consolidate the converging effects of previous joint workshops LAW-MWE-CxG 2018, MWE-WN 2019 and MWE-LEX 2020, extending our scope to MWEs in e-lexicons and WordNets, MWE annotation, as well as grammatical constructions (Losnegaard et al. 2016, Lichte et al. 2019). We will call for papers on research related (but not limited) to:

<p><strong>Traditional MWE topics:</strong>
<ul>
<p><li>Computationally-applicable theoretical work on MWEs and constructions in psycholinguistics and corpus linguistics</li>
<p><li>MWE and construction annotation and representation in resources such as corpora, treebanks, e-lexicons and WordNets</li>
<p><li>Processing of MWEs and constructions in syntactic and semantic frameworks (e.g. CCG, CxG, HPSG, LFG, TAG, UD, etc.)</li>
<p><li>Discovery and identification methods for MWEs and constructions</li>
<p><li>MWEs and constructions in language acquisition, language learning, and non-standard language (e.g. tweets, speech)</li>
<p><li>Evaluation of annotation and processing techniques for MWEs and constructions</li>
<p><li>Retrospective comparative analyses from the PARSEME shared tasks on automatic identification of MWEs</li>
</ul>

<p><strong>Topics on MWEs and end-user applications:</strong>
<ul>
<p><li>Processing of MWEs and constructions in end-user applications (e.g. MT, NLU, summarisation, social media mining, computer assisted language learning)</li>
<p><li>Implicit and explicit representation of MWEs and constructions in end-user applications</li>
<p><li>Evaluation of end-user applications concerning MWEs and constructions</li>
<p><li>Resources and tools for MWEs and constructions (e.g. lexicons, identifiers) in end-user applications</li>
</ul>

<h1>JOINT SESSION WITH WOAH WORKSHOP</h1>

The MWE Section has initiated a synergy with the community of the Workshop on Online  Abuse and Harm (WOAH). We intend to organise a joint session during our workshops, which would require locating them with the same venue (see preferred venues below). The session would last about 1h30, and its format is currently under discussion. We believe that MWEs are important in online abuse detection, and that the latter can provide an interesting testbed for MWE processing technology. The main goal is to pave the way towards the creation of data for a shared task involving both communities.-->

<!------------------------------------------------------------------------------>

<!--
SUBMISSION FORMATS: the workshop will call for submissions that must be substantially original. Submissions should follow the ACL Policies for Submission, Review and Citation; they can be long papers (8 content pages + references) or short papers (4 content pages + references). One extra page will be granted for the final versions. The decisions as to oral or poster presentations will be taken by the PC chairs, with no distinction in the proceedings. Submission will be double-blind.

<h1 id="modalities"><a name="submissions"></a>Submission modalities</h1>

<p><strong>Regular research track</strong>:</p>
<ul>
<li>Long papers (9 content pages + references): They should report on solid and finished research including new experimental results, resources and/or techniques.</li>
<li>Short papers (4 content pages + references): They should report on small experiments, focused contributions, ongoing research, negative results and/or philosophical discussion.</li>
</ul>

<p>In regular research papers, the reported research should be substantially original. Papers available as preprints can also be submitted provided that they fulfil the conditions defined by the <a href="https://www.aclweb.org/portal/content/new-policies-submission-review-and-citation" target="_blank">ACL Policies for Submission, Review and Citation</a>. </p>

<p><strong>Shared task track</strong>:</p>

<ul>
<li>System description papers (4 content pages + references): These papers should briefly describe the approach implemented to solve the problem. They may include references and links to more detailed descriptions in other documents.</li>
</ul>

<p>Shared task system description papers will go through a separate reviewing process. Submissions will be reviewed by the shared task organizers and participants. Participants of the shared task are not required to submit system description papers, and their acceptance depends on the quality of the paper rather than on the results obtained in the shared task.</p>

<p><strong>Instructions for authors</strong>:</p>

<p>For all 3 types of papers, the submission is <strong>double-blind</strong> as per the <a href="https://coling2020.org/pages/call_for_papers">COLING 2020 guidelines</a>. For all types of submission, the COLING 2020 <a href="https://coling2020.org/coling2020.zip">templates</a> must be used. There is no limit on the number of reference pages.</p>  Authors will be granted an extra page for the final version of their papers.</p>

<p>The decisions as to oral or poster presentations of the selected papers will be taken by the PC chairs, depending on the available infrastructure for virtual participation. No distinction between papers presented orally and as posters is made in the workshop proceedings.</p>

<p>All papers should be submitted via the following  <a href="https://www.softconf.com/coling2020/MWE-LEX/">START space</a>. Please choose the appropriate track (research/shared task) and submission modality (long/short). </p>
-->
<!------------------------------------------------------------------------------>
<!--
<h1>Important dates</h1>

<p>All deadlines are at 23:59 UTC-12 (anywhere in the world).</p>

<table>
<tr><td><del>May 20</del> <strong><del>Sep 2</del> Sep 9, 2020</strong>: <strong>extended</strong> paper submission deadline</td></tr>
<tr><td><del>Jun 24</del> Oct 16, 2020: notification of acceptance</td></tr>
<tr><td><del>Jul 11</del> Nov 1, 2020: camera-ready papers due</td></tr>
<tr><td><del>Sep 14</del> Dec 13: MWE-LEX workshop dates</td></tr>
</table>

<p>See also the important dates for the <a href="http://multiword.sourceforge.net/sharedtask2020" target="_blank">shared task</a> systems.</p>
-->
<!------------------------------------------------------------------------------>
<!--
<h1>Organizers</h1>
<ul>
<li>Program chairs:
   <ul>   
   <li><a href="http://cs.unb.ca/~ccook1/">Paul Cook</a>, University of New Brunswick (Canada)</li>
   <li><a href="http://jelena.mitrovic.rs">Jelena Mitrović</a>, University of Passau (Germany)</li>
   <li><a href="https://sites.google.com/site/carlaparraescartin/">Carla Parra Escartín</a>, Iconic Translation (Ireland)</li>
   <li><a href="http://web.iitd.ernet.in/~avaidya/">Ashwini Vaidya</a>, Indian Institute of Technology in Delhi (India)</li>   	
   </ul>
</li>
<li>Publication chairs:
	<ul>
	<li><a href="http://bultreebank.org/en/our-team/petya-osenova/" target="_blank">Petya Osenova</a>, University of Sofia and Bulgarian Academy of Sciences (Bulgaria) </li>
	<li><a href="https://shivaat.github.io/" >Shiva Taslimipoor</a>, University of Cambridge (UK) </li>
	</li>
	</ul>
<li>Communication chair:
   <ul>
   <li><a href="http://pageperso.lis-lab.fr/~carlos.ramisch/" target="_blank">Carlos Ramisch</a>, Aix Marseille University (France)</li>
   </ul>
</li>
</ul>
-->
<!------------------------------------------------------------------------------>


<!------------------------------------------------------------------------------>

<!--
<h1>Contact</h1>
<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwelex2020@gmail.com">mwelex2020@gmail.com</a></p>
-->

<!------------------------------------------------------------------------------>

<!--<h1>References</h1>
<p>Timothy Baldwin and Su Nam Kim. 2010. Multiword Expressions. Handbook of Natural Language Processing.
<p>Tommaso Caselli, Valerio Basile, Jelena Mitrovic, Inga Kartoziya, and Michael Granitzer. 2020. I feel offended, don’t be abusive! implicit/explicit messages in offensive and abusive language. In LREC 2020.
<p>Morten H Christiansen, Inbal Arnon. 2017. More Than Words: The Role of Multiword Sequences in Language Learning and Use. In TOPICS in Cognitive Science, Journal of the Cognitive Science Society, Volume 9, Issue 3. Pages 542-551.
<p>Matthieu Constant, Gülsen Eryigit, Johanna Monti, Lonneke van der Plas, Carlos Ramisch, Michael Rosner, and Amalia Todirascu. 2017. Multiword expression processing: A survey. Computational Linguistics, 43(4):837–892.
<p>Ekaterina Kochmar, Sian Gooding, Matthew Shardlow. 2020. Detecting multiword expression type helps lexical complexity assessment. In LREC 2020.
<p>Timm Lichte, Simon Petitjean, Agata Savary, Jakub Waszczuk. 2019. Lexical encoding formats for multi-word expressions: The challenge of “irregular” regularities. In Representation and Parsing of MWEs, p. 41–72. Language Science Press, Berlin, Germany.
<p>Changsheng Liu and Rebecca Hwa. 2016. Phrasal substitution of idiomatic expressions. In NAACL-HLT 2016, San Diego, USA.
<p>Gyri Smørdal Losnegaard, Federico Sangati, Carla Parra Escartín, Agata Savary, Sascha Bargmann, and Johanna Monti. 2016. PARSEME survey on MWE resources. In LREC 2016, Portoroz, Slovenia.
<p>Alessandro Maisto, Serena Pelosi, Simonetta Vietri, Pierluigi Vitale. 2017. Mining Offensive Language on Social Media. In CLiC-it 2017.
<p>Magali Paquot, Hubert Naets, Stefan Gries. 2019. Tracing phraseological complexity development in a longitudinal learner corpus with verb+ object co-occurrences American Association of Applied Linguistics (AAAL) Conference.
<p>Carlos Ramisch et al. 2018. Edition 1.1 of the PARSEME Shared Task on automatic identification of verbal multiword expressions. In LAW-MWE-CxG-2018, Santa Fe, USA.
<p>Agata Savary et al. 2017. The PARSEME Shared Task on automatic identification of verbal multiword expressions. In MWE 2017, Valencia, Spain.
<p>Marcos Zampieri, Preslav Nakov, Sara Rosenthal, Pepa Atanasova, Georgi Karadzhov, Hamdy Mubarak, LeonDerczynski, Zeses Pitenis, and Cagrı C̈oltekin. 2020. SemEval-2020 Task 12: Multilingual Offensive Language Identification in Social Media (OffensEval 2020). In SemEval 2020.
<p>Andrea Zaninello and Alexandra Birch. 2020. Multiword Expression aware Neural Machine Translation. In LREC 2020.

<h1>Anti-harassment policy</h1>
<p>The workshop supports the ACL <a href="https://www.aclweb.org/adminwiki/index.php?title=Anti-Harassment_Policy" target=_blanc">anti-harassment policy</a>.</p>-->
