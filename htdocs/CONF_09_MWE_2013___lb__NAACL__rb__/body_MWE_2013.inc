<h2>The 9th Workshop on Multiword Expressions (MWE 2013)</h2>

<h3>Workshop at NAACL 2013 (Atlanta, Georgia, USA), June 13/14, 2013</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.siglex.org" target="_blank">SIGLEX</a>)</p>

<!--<p><strong>EXTENDED Submission deadline:<br/> 
 <span style="text-decoration:line-through">Long &amp; short papers - Mar 01, 2013 at 23:59 PDT (GMT-12)</span><br/>
Long &amp; short papers - Mar 15, 2013 at 23:59 PDT (GMT-12)</strong></p>-->

<p><small>Last updated: Apr 11, 2013</small></p>

<h1>News</h1>

<ul>
<li><em>Apr 11, 2013:</em> Registration is open through <a href="http://naacl2013.naacl.org/Registration.aspx">NAACL website</a>. The fees for a regular early registration are US$ 180.</li>
<li><em>Apr 10, 2013:</em> Notifications are out (a bit earlier than expected). List of accepted papers online soon!</li>
<li><em>Apr 08, 2013:</em> <a href="?sitesig=CONF&page=CONF_09_MWE_2013___lb__NAACL__rb__&subpage=CONF_20_Keynote_Speakers">Invited speakers</a> announced!</li>
<li><em>Apr 02, 2013:</em> The important dates have been shifted because of the deadline extension. The notifications will be sent out on April 12.</li>
</ul>

<h1>Call For Participation</h1>

<p>Under the denomination "multiword expression", one assumes a wide range of linguistic constructions such as idioms (<em>storm in a teacup, sweep under the rug</em>), fixed phrases (<em>in vitro, by and large, rock'n roll</em>), noun compounds (<em>olive oil, laser printer</em>), compound verbs (<em>take a nap, bring about</em>), etc. While easily mastered by native speakers, their interpretation poses a major challenge for computational systems, due to their flexible and heterogeneous nature.</p>

<p> For starters, MWEs are not nearly as frequent in NLP resources as they are in real-word text, and this problem of coverage may impact the performance of many NLP tasks. Moreover, treating MWEs also involves problems like determining their semantics, which is not always compositional (<em>to kick the bucket</em> meaning <em>to die</em>). In sum, MWEs are a key issue and a current weakness for natural language parsing and generation, as well as real-life applications depending on language technology, such as machine translation, just to name a prominent one among many.</p>

<p>Thanks to the joint efforts of researchers from several fields working on MWEs, significant progress has been made in recent years, especially concerning the construction of large-scale language resources. For instance, there is a large number of recent papers that focus
on acquisition of MWEs from corpora, and others that describe a variety of techniques to find paraphrases for MWEs. Current methods use a plethora of tools such as association measures, machine learning, syntactic patterns, web queries, etc. A considerable body of techniques, resources and tools to perform these tasks are now available, and are indicative of the growing importance of the field within the NLP community.</p>

<p>Many of these advances are described as part of the annual workshop on MWEs, that attracts the attention of an ever-growing community working on a variety of languages and MWE types. The workshop has been held since 2001 in conjunction with major computational linguistics conferences (ACL, COLING, LREC and EACL), providing an important venue for the community to interact, share resources and tools and collaborate on efforts for advancing the computational treatment of MWEs. Additionally, special issues on MWEs have been published by leading journals in computational linguistics. The latest such effort is the special issue on “Multiword Expressions: from Theory to Practice and Use”, currently under publication from the <a href="http://multiword.sourceforge.net/tslp2011si">ACM Transactions on Speech and Language Processing</a>.</p>

<p>MWE 2013 will be the 9th event in the series. We will be interested in major challenges in the overall process of MWE treatment, both from the theoretical and the computational viewpoint, focusing on original research related (but not limited) to the following topics:</p>

<ul>
<li> Manually and automatically constructed resources </li>
<li> Representation of MWEs in dictionaries and ontologies </li>
<li> MWEs in linguistic theories like HPSG, LFG and minimalism </li>
<li> MWEs and user interaction </li>
<li> Multilingual acquisition </li>
<li> Multilingualism and MWE processing </li>
<li> Models of first and second language acquisition of MWEs </li>
<li> Crosslinguistic studies on MWEs </li>
<li> The role of MWEs in the domain adaptation of parsers </li>
<li> Integration of MWEs into NLP applications </li>
<li> Evaluation of MWE treatment techniques </li>
<li> Lexical, syntactic or semantic aspects of MWEs </li>
</ul>

<p>The workshop is endorsed by ACL SIGLEX, the Special Interest Group on the Lexicon of the Association for Computational Linguistics.</p>

<h1>Important dates</h1>

<table>
<tr><td><span style="text-decoration:line-through">Mar 1, 2013</span></td><td><span style="text-decoration:line-through">Long &amp; short paper submission deadline 23:59 PDT (GMT-12)</span></td></tr>
<tr><td><span style="text-decoration:line-through">Mar 15, 2013</span></td><td><span style="text-decoration:line-through">Long &amp; short paper submission deadline 23:59 PDT (GMT-12)</span></td></tr>
<tr><td>Apr 12, 2013</td><td>Notification of acceptance</td></tr>
<tr><td>Apr 26, 2013</td><td>Camera-ready deadline</td></tr>
<tr><td>Jun 13/14, 2013</td><td>MWE 2013 Workshop</td></tr>
</table>

<h1>Workshop Organizers</h1>

<ul>
<li><a href="http://www.coli.uni-saarland.de/~kordoni/" target="_blank">Valia Kordoni</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="http://www.inf.ufrgs.br/~ceramisch/" target="_blank">Carlos Ramisch</a><em> (Joseph Fourier University, France)</em></li>
<li><a href="http://www.inf.ufrgs.br/~avillavicencio/" target="_blank">Aline Villavicencio</a><em> (Federal University of Rio Grande do Sul, Brazil)</em></li>
</ul>

<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwe2013workshop at gmail.com">mwe2013workshop at gmail.com</a></p>

