<h2>The 9th Workshop on Multiword Expressions (MWE 2013)</h2>

<h3>Workshop at NAACL 2013 (Atlanta, Georgia, USA), June 13/14, 2013</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.siglex.org" target="_blank">SIGLEX</a>)</p>

<p><small>Last updated: May 02, 2013</small></p>

<h1>Workshop Program<img width="24" alt="  New: " src="images/new.gif"/></h1>

<h3>Thursday, June 13, 2013</h3>

<table>
<tr><td width="87">09:00-09:15</td><td>Opening Remarks</td></tr>
<tr></tr>
<tr><td width="87"></td><td><strong>Oral Session 1: Resources and Applications</strong></td></tr>
<tr><td width="87"></td><td>Chair: Jan Hajic</td></tr>
<tr><td width="87">09:15-09:40</td><td><em>Managing Multiword Expressions in a Lexicon-Based Sentiment Analysis System for Spanish</em></td></tr>
<tr><td width="87"></td><td>Antonio Moreno-Ortiz, Chantal Perez-Hernandez and Maria Del-Olmo</td></tr>
<tr><td width="87">09:40-10:05</td><td><em>Introducing PersPred, a Syntactic and Semantic Database for Persian Complex Predicates</em></td></tr>
<tr><td width="87"></td><td>Pollet Samvelian and Pegah Faghiri</td></tr>
<tr><td width="87">10:05-10:30</td><td><em><span style="color:red">CANCELLED: </span>Improving Word Translation Disambiguation by Capturing Multiword Expressions with Dictionaries</em></td></tr>
<tr><td width="87"></td><td>Lars Bungum, Bj&#246;rn Gamb&#228;ck, Andr&#233; Lynum and Erwin Marsi</td></tr>
<tr></tr>
<tr><td width="87">10:30-11:00</td><td><strong>COFFEE BREAK</strong></td></tr>
<tr></tr>
<tr><td width="87">11:00-12:00</td><td><strong>Invited Talk 1</strong></td></tr>
<tr><td width="87"></td><td><em>Complex Predicates are Multi-Word Expressions</em></td></tr>
<tr><td width="87"></td><td>Martha Palmer</td></tr>
<tr></tr>
<tr><td width="87"></td><td><strong>Oral Session 2: Compositionality</strong></td></tr>
<tr><td width="87"></td><td>Chair: Malvina Nissim</td></tr>
<tr><td width="87">12:00-12:25</td><td><em>The (Un)expected Effects of Applying Standard Cleansing Models to Human Ratings on Compositionality</em></td></tr>
<tr><td width="87"></td><td>Stephen Roller, Sabine Schulte im Walde and Silke Scheible</td></tr>
<tr></tr>
<tr><td width="87">12:30-14:00</td><td><strong>LUNCH BREAK</strong></td></tr>
<tr></tr>
<tr><td width="87"></td><td><strong>Oral Session 2: Compositionality (contd.)</strong></td></tr>
<tr><td width="87">14:05-14:30</td><td><em>Determining Compositionality of Word Expressions Using Word Space Models</em></td></tr>
<tr><td width="87"></td><td>Lubom&#237;r Krcm&#225;ř, Karel Jezek and Pavel Pecina</td></tr>
<tr></tr>
<tr><td width="87">14:30-15:30</td><td><strong>Invited Talk 2</strong></td></tr>
<tr><td width="87"></td><td><em>Modelling the Internal Variability of MWEs</em></td></tr>
<tr><td width="87"></td><td>Malvina Nissim</td></tr>
<tr></tr>
<tr><td width="87">15:30-16:00</td>
<td><strong>COFFEE BREAK</strong></td></tr>
<tr><td width="87"></td>
<td><strong>Oral Session 3: Short Papers</strong></td></tr>
<tr><td width="87"></td><td>Chair: Yuji Matsumoto</td></tr>
<tr><td width="87">16:00-16:15</td>
<td><em>Automatically Assessing Whether a Text Is Cliched, with Applications to Literary Analysis</em></td></tr>
<tr><td width="87"></td>
<td>Paul Cook and Graeme Hirst</td></tr>
<tr><td width="87">16:15-16:30</td>
<td><em>An Analysis of Annotation of Verb-Noun Idiomatic Combinations in a Parallel Dependency Corpus</em></td></tr>
<tr><td width="87"></td>
<td>Zdenka Uresova, Jan Hajic, Eva Fucikova and Jana Sindlerova</td></tr>
<tr></tr>
<tr><td width="87">16:30-17:40</td><td><strong>Poster Session</strong></td></tr>
<!--<tr><td width="87"></td><td>Chair: I&ntilde;aki Alegria</td></tr>-->
<tr><td width="87"></td><td>Chair: Sara Berlanda</td></tr>
<tr><td width="87">16:30-16:40</td><td>Poster Boosters</td></tr>
<tr><td width="87"></td><td><em>Automatic Identification of Bengali Noun-Noun Compounds Using Random Forest</em></td></tr>
<tr><td width="87"></td><td>Vivekananda Gayen and Kamal Sarkar</td></tr>
<tr><td width="87"></td><td><em>Automatic Detection of Stable Grammatical Features in N-Grams</em></td></tr>
<tr><td width="87"></td><td>Mikhail Kopotev, Lidia Pivovarova, Natalia Kochetkova and Roman Yangarber</td></tr>
<tr><td width="87"></td><td><em>Exploring MWEs for Knowledge Acquisition from Corporate Technical Documents</em></td></tr>
<tr><td width="87"></td><td>Bell Manrique-Losada, Carlos M. Zapata-Jaramillo and Diego A. Burgos</td></tr>
<tr><td width="87"></td><td><em><span style="color:red">CANCELLED: </span>MWE in Portuguese: Proposal for a Typology  for Annotation in Running Text</em></td></tr>
<tr><td width="87"></td><td>Sandra Antunes and Am&#225;lia Mendes</td></tr>
<tr><td width="87"></td><td><em>Identifying Pronominal Verbs: Towards Automatic Disambiguation of the Clitic 'se' in Portuguese</em></td></tr>
<tr><td width="87"></td><td>Magali Sanches Duran, Carolina Evaristo Scarton, Sandra Maria Alu&#237;sio and Carlos Ramisch</td></tr>
<tr><td width="87"></td><td><em>A Repository of Variation Patterns for Multiword Expressions</em></td></tr>
<tr><td width="87"></td><td>Malvina Nissim and Andrea Zaninello</td></tr>
</table>

<!----------------------------------------------------------------------------->
<h3>Friday, June 14, 2013</h3>

<table>
<tr><td width="87"></td><td><strong>Oral Session 4: Identification and Classification</strong></td></tr>
<tr><td width="87"></td><td>Chair: Thomas Lynch</td></tr>
<tr><td width="87">09:10-09:35</td><td><em>Syntactic Identification of Occurrences of Multiword Expressions in Text using a Lexicon with Dependency Structures</em></td></tr>
<tr><td width="87"></td><td>Eduard Bejček, Pavel Straňák and Pavel Pecina</td></tr>
<tr><td width="87">09:35-10:00</td><td><em><span style="color:red">CANCELLED: </span>Combining Different Features of Idiomaticity for the Automatic Classification of Noun+Verb Expressions in Basque</em></td></tr>
<tr><td width="87"></td><td>Antton Gurrutxaga and I&#241;aki Alegria</td></tr>
<tr></tr>

<tr><td width="87"></td><td><strong>Oral Session 5: Short Papers</strong></td></tr>
<tr><td width="87"></td><td>Chair: Antonio Moreno Ortiz</td></tr>
<tr><td width="87">10:00-10:15</td><td><em>Semantic Roles for Nominal Predicates: Building a Lexical Resource</em></td></tr>
<tr><td width="87"></td><td>Ashwini Vaidya, Martha Palmer and Bhuvana Narasimhan</td></tr>
<tr><td width="87">10:15-10:30</td><td><em>Constructional Intensifying Adjectives in Italian</em></td></tr>
<tr><td width="87"></td><td>Sara Berlanda</td></tr>
<tr></tr>
<tr><td width="87">10:30-11:00</td><td><strong>COFFEE BREAK</strong></td></tr>
<tr></tr>
<tr><td width="87">11:00-12:00</td><td><strong>Invited Talk 3</strong></td></tr>
<tr><td width="87"></td><td><em>The Far Reach of Multiword Expressions in Educational Technology</em></td></tr>
<tr><td width="87"></td><td>Jill Burstein</td></tr>
<tr></tr>
<tr><td width="87"></td><td><strong>Oral Session 5: Short Papers (contd.)</strong></td></tr>
<tr><td width="87">12:00-12:15</td><td><em>Construction of English MWE Dictionary and its Application to POS Tagging</em></td></tr>
<tr><td width="87"></td><td>Yutaro Shigeto, Ai Azuma, Sorami Hisamoto, Shuhei Kondo, Tomoya Kouse, Keisuke Sakaguchi, Akifumi Yoshimoto, Frances Yung and Yuji Matsumoto</td></tr>
<tr></tr>
<tr><td width="87">12:15-12:30</td><td>Closing Remarks</td></tr>
</table>

