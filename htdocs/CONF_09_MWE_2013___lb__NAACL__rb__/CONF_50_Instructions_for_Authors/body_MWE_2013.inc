<h2>The 9th Workshop on Multiword Expressions (MWE 2013)</h2>

<h3>Workshop at NAACL 2013 (Atlanta, Georgia, USA), June 13/14, 2013</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.siglex.org" target="_blank">SIGLEX</a>)</p>

<p><small>Last updated: May 17, 2013</small></p>

<h2>Instructions for oral presentations</h2>

<p>According to the type of paper, the slots for oral presentation are as follows:</p>

<ul>
<li> The short-paper sessiona are divided into 15 minute slots split in the following way:
<ul>
    <li>10 mins for the presentation</li>
    <li>5 mins for questions/discussion</li>
</ul>    
</li>
<li> The long-paper sessions are divided into 25 minute slots split in the following way:
<ul>
    <li>20 mins for the presentation</li>
    <li>5 mins for questions/discussion</li>
</ul>    
</li>    
</ul>

<p>Please check the <a href="http://multiword.sourceforge.net/PHITE.php?sitesig=CONF&page=CONF_09_MWE_2013___lb__NAACL__rb__&subpage=CONF_10_Program">full schedule</a>.</p>

<p>The following equipment will be available for the presentations:</p>
<ul>
<li> a table</li>
<li> a data projector</li>
<li> a microphone</li>
</ul>

<p>Each presentation room has a switcher to allow for laptops to be connected before the session starts. We will provide a laptop with a PDF Reader in case you do not bring your own laptop. Please make sure the laptop can be connected to the switch (bring your Mac/HDMI adapter if needed). Volunteers and technicians will be on the site to help with any problems.</p>

<p>Please make sure that your presentation works flawlessly. If you need any other software or hardware (e.g. speakers), please contact us immediately. If you do not bring your own laptop, each presentation in PDF format should be transferred onto the laptop in the presentation room before the session begins. The session chair and the helper will be available 30 minutes before the session.</p>

<p>Presenters must introduce themselves to the session chair and set up their presentations before the session starts. You should not expect to have time to do this at the beginning of your talk.</p>

<h2>Instructions for poster presentations</h2>

<p>The poster session will take place on June 13, from 16:30 to 17:40. Presenters must set up their posters before the session starts, and you should not expect to have time to do this at the start of the session. Therefore, you should put up your poster during the coffee break preceding the poster session, that is, from 15:30 to 16:00. Please notice that there is a short paper session before the poster session, so you won't be able to put up your poster during this session. Please check the <a href="http://multiword.sourceforge.net/PHITE.php?sitesig=CONF&page=CONF_09_MWE_2013___lb__NAACL__rb__&subpage=CONF_10_Program">full schedule</a>.</p>

<p>Posters should be no larger than 48 inches (122cm) wide and 36 inches high (91cm). This roughly corresponds to landscape A0 format. There is no further restriction on the format, but we recommend you to use large fonts and high resolution images, as well as clearly indicating the paper title and avoiding to overload your poster with text, as this might attract more people to see your poster.</p>

<p>We will provide self-standing posterboards (which sit on 18 inches-deep tables), pushpins, clips, double-sided tape, Blu-tack etc. for mounting. For each poster board, there will be one table and one chair. Please make sure that your poster has the correct dimensions. If you want to give a demo along with your poster or if you have more than two presenters for your poster, please contact us so that we assure there is enough space in the room for your poster presentation.</p>

