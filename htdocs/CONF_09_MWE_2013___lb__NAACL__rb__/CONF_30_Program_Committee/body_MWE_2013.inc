<h2>The 9th Workshop on Multiword Expressions (MWE 2013)</h2>

<h3>Workshop at NAACL 2013 (Atlanta, Georgia, USA), June 13/14, 2013</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.siglex.org" target="_blank">SIGLEX</a>)</p>

<!--<p><strong>EXTENDED Submission deadline:<br/> 
 <span style="text-decoration:line-through">Long &amp; short papers - Mar 01, 2013 at 23:59 PDT (GMT-12)</span><br/>
Long &amp; short papers - Mar 15, 2013 at 23:59 PDT (GMT-12)</strong></p>-->

<p><small>Last updated: May 03, 2013</small></p>

<h1>Program Committee</h1>

<ul>
<li>Iñaki Alegria, University of the Basque Country (Spain) </li>
<li>Dimitra Anastasiou, University of Bremen (Germany) </li>
<li>Doug Arnold, University of Essex (UK) </li>
<li>Giuseppe Attardi, Università di Pisa (Italy) </li>
<li>Eleftherios Avramidis, DFKI GmbH (Germany) </li>
<li>Tim Baldwin, The University of Melbourne (Australia) </li>
<li>Chris Biemann, Technische Universität Darmstadt (Germany)</li>
<li>Francis Bond, Nanyang Technological University (Singapore) </li>
<li>Antonio Branco, University of Lisbon  (Portugal) </li>
<li>Aoife Cahill, Educational Testing Service (USA) </li>
<li>Helena Caseli, Federal University of São Carlos (Brazil) </li>
<li>Ken Church, IBM Research (USA) </li>
<li>Matthieu Constant, Université Paris-Est Marne-la-Vallée (France) </li>
<li>Paul Cook, The University of Melbourne (Australia) </li>
<li>Béatrice Daille, Nantes University (France) </li>
<li>Koenraad de Smedt, University of Bergen (Norway) </li>
<!--<li>Mona Diab, Columbia University (USA) </li>-->
<!--<li>Gaël Dias, University of Caen Basse-Normandie (France) </li>-->
<li>Markus Egg, Humboldt-Universität zu Berlin (Germany) </li>
<li>Stefan Evert, Friedrich-Alexander Universität Erlangen-Nürnberg (Germany) </li>
<li>Afsaneh Fazly, University of Toronto (Canada) </li>
<li>Joaquim Ferreira da Silva, New University of Lisbon (Portugal) </li>
<!--<li>Roxana Girju, University of Illinois at Urbana-Champaign (USA) </li>-->
<li>Chikara Hashimoto, National Institute of Information and Communications Technology (Japan) </li>
<li>Kyo Kageura, University of Tokyo (Japan) </li>
<li>Su Nam Kim, Monash University (Australia) </li>
<!--<li>Philipp Koehn, University of Edinburgh (UK) </li>-->
<li>Ioannis Korkontzelos, University of Manchester (UK) </li>
<li>Brigitte Krenn, Austrian Research Institute for Artificial Intelligence (Austria) </li>
<li>Evita Linardaki, Hellenic Open University (Greece) </li>
<li>Takuya Matsuzaki, National Institute of Informatics (Japan) </li>
<li>Yusuke Miyao, National Institute of Informatics (Japan) </li>
<li>Preslav Nakov, Qatar Computing Research Institute - Qatar Foundation (Qatar) </li>
<li>Joakim Nivre, Uppsala University (Sweden) </li>
<li>Diarmuid Ó Séaghdha, University of Cambridge (UK) </li>
<li>Jan Odijk, University of Utrecht (The Netherlands) </li>
<li>Yannick Parmentier, Université d'Orléans (France) </li>
<li>Pavel Pecina, Charles University Prague (Czech Republic) </li>
<li>Scott Piao, Lancaster University (UK) </li>
<li>Adam Przepiórkowski, Polish Academy of Sciences (Poland) </li>
<li>Magali Sanches Duran, University of São Paulo (Brazil) </li>
<li>Agata Savary, Université François Rabelais Tours (France) </li>
<li>Ekaterina Shutova, University of California at Berkeley (USA) </li>
<!--<li>Lucia Specia, University of Wolverhampton (UK) </li>-->
<li>Mark Steedman, University of Edinburgh (UK) </li>
<li>Sara Stymne,  Uppsala University (Sweden) </li>
<li>Stan Szpakowicz, University of Ottawa (Canada) </li>
<li>Beata Trawinski, University of Vienna (Austria) </li>
<li>Yulia Tsvetkov, Carnegie Mellon University (USA) </li>
<li>Yuancheng Tu, Microsoft (USA) </li>
<li>Kyioko Uchiyama, National Institute of Informatics (Japan) </li>
<li>Ruben Urizar, University of the Basque Country (Spain) </li>
<!--<li>Gertjan van Noord, University of Groningen (The Netherlands) </li>-->
<li>Tony Veale, University College Dublin (Ireland) </li>
<li>David Vilar, DFKI GmbH (Germany) </li>
<li>Veronika Vincze, Hungarian Academy of Sciences (Hungary) </li>
<li>Tom Wasow, Stanford University (USA) </li>
<li>Eric Wehrli, University of Geneva (Switzerland) </li>
</ul>

<h1>Workshop Organizers</h1>

<ul>
<li><a href="http://www.coli.uni-saarland.de/~kordoni/" target="_blank">Valia Kordoni</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="http://www.inf.ufrgs.br/~ceramisch/" target="_blank">Carlos Ramisch</a><em> (Joseph Fourier University, France)</em></li>
<li><a href="http://www.inf.ufrgs.br/~avillavicencio/" target="_blank">Aline Villavicencio</a><em> (Federal University of Rio Grande do Sul, Brazil)</em></li>
</ul>

<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwe2013workshop at gmail.com">mwe2013workshop at gmail.com</a></p>

