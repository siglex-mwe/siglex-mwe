<h2><a href="http://langsci-press.org/catalog/book/204" target="_blank">Multiword expressions at length and in depth</a></h2>
<h3>Volume 2 of the <a href="http://langsci-press.org/catalog/series/pmwe" target="_blank">Phraseology and Multiword Expressions</a> book series at <a href="http://langsci-press.org" target="_blank">Language Science Press</a>, containing extended revised papers of the <a href="http://multiword.sourceforge.net/mwe2017">13th Workshop on Multiword Expressions</a></h3>

<!-- <h2>Multiword Expressions: from Theory to Practice and Use</h2> -->

<!--
<p><strong>Deadlines (extended):</strong><br/>June 9, 2017 (expression of interest)<br/>
August 28 <strike>July 30</strike> (submission)</p>
-->

<!--<p>Electronic submission: <a href="" target="_blank">https://www.softconf.com/acl2011/mwe/</a></p>-->

<p><small>Last updated: November 10, 2018</small></p>

<h1>Volume page</h1>
The volume was published on 25 Oct 2018 at a <a href="http://langsci-press.org/catalog/book/204" target="_blank">dedicated page</a> at the Language Science Press (LSP) website. The page contains:
<ul>
<li>pdf files of the whole volume and of separate chapters</li>
<li>latex sources of all chapters</li>
<li>proposal of <a href="http://langsci-press.org/catalog/view/204/1661/1317-1" target="_blank">conventions</a> for citing, glossing and translation multilingual examples of MWEs</li>
</ul>

<h1>Call for Papers</h1>

<p>The <a href="http://multiword.sourceforge.net/mwe2017">13th Workshop on Multiword Expressions</a> (MWE 2017) that was co-located with the <a href="http://eacl2017.org/" target="_blank">EACL 2017</a> conference in Valencia, on 4 April 2017, featured an invited talk and 21 peer-reviewed papers. It was also the culminating event of the PARSEME shared task on automatic identification of verbal MWEs and hosted several presentations related to this initiative. </p>

<p>We invite all authors of the papers published at MWE 2017, including shared task system description papers, to submit <strong>extended</strong> versions of their contributions to a volume of the <a href="http://langsci-press.org/catalog/series/pmwe" target="_blank">Phraseology and Multiword Expressions</a> (PMWE) book series of <a href="http://langsci-press.org/" target="_blank">Language Science Press</a> (LangSci). All contributions will be subject to a <strong>new reviewing process</strong>, i.e. the selection of a paper for MWE 2017 does not necessarily imply the selection of its extended version for the volume.

<h1>Instructions for authors</h1>

<p>Please, send your <strong>expression of interest</strong> on June 9, 2017 at latest, together with the list of co-authors, the intended title and the abstract of the extended version of your paper to <a href="mailto:mweguesteditor at gmail.com">mwe2017workshop at gmail.com</a>.</p>

<p>At least <strong>30% of the contents</strong> of the PMWE volume submissions should be original, notably with respect to the MWE 2017 papers. Extended versions of <strong>short and long papers</strong> should have approximately <strong>12 and 20 pages</strong> of contents, respectively, plus any number of pages for bibliographic references. The submission is <strong>single-blind</strong>: the papers should contain author names and affiliations, but the identity of the reviewers will not be disclosed. All papers should follow the LangSci <a href="http://langsci-press.org/forAuthors" target="_blank">guidelines</a> and use the provided <a href="http://langsci-press.org/templatesAndTools" target="_blank">LaTeX templates</a>, and should be submitted via the <a href="https://easychair.org/conferences/?conf=pmwe17" target="_blank">EasyChair space</a> by <strike>July 30</strike> August 28 23:59 GMT at the latest. Please, choose the appropriate submission type, according to the category of your paper. Final versions of accepted papers will be submitted both in PDF and source LaTeX formats.</p>

<h1>Important dates</h1>

<table>
<tr><td><strike>June 9, 2017</td><td>expression of interest</td></tr>
<tr><td><strike>July 30</strike> 2017</td><td>paper submission - EXTENDED!</td></tr>
<tr><td><strike>October 27</strike>, 2017</td><td>notification</td></tr>
<tr><td><strike>November 18</strike> 2017</td><td>revised versions of papers</td></tr>
<tr><td><strike>January 7</strike>, 2018</td><td>editorial comments and the common book template sent to the authors</td></tr>
<tr><td><strike>January 24,</strike> 2018</td><td> submission of final versions (including LaTeX sources)</td></tr>
<tr><td><strike>March-June, 2018</strike></td><td> volume compilation, index creation</td></tr>
<tr><td><strike>July-August 2018</strike></td><td> proofreading and typesetting</td></tr>
<tr><td><strike>October 2018</strike></td><td> publication</td></tr>
</table>


<h1>Frequently asked questions (FAQ)</h1>

<ul>
<li><strong> Is it possible to change the title and the list of authors of the extended submission with respect to the MWE 2017 workshop paper? </strong></li>
</ul>

<p>
Yes, no problem. We even <em>recommend</em> changing the title to reflect the addition of original content. As for authors, you may add new collaborators and/or remove authors who are not available to work on the extended version. As long as the paper can still be identified as an extended version of a workshop paper, it's fine.
</p>

<h1>Reviewers</h1>

<ul>
<li>Dimitra Anastasiou (Luxembourg Institute of Science and Technology, Luxembourg)</li>
<li>Doug Arnold (University of Essex, UK)</li>
<li>Timothy Baldwin (University of Melbourne, Australia)</li>
<li>Eduard Bejček (Charles University in Prague, Czech Republic)</li>
<li>António Branco (University of Lisbon, Portugal)</li>
<li>Marie Candito (Paris Diderot University, France)</li>
<li>Fabienne Cap (Uppsala University, Sweden)</li>
<li>Matthieu Constant (Université de Lorraine, France)</li>
<li>Paul Cook (University of New Brunswick, Canada)</li>
<li>Lucia Donatelli (Georgetown University, USA)</li>
<li>Silvio Ricardo Cordeiro (Aix-Marseille University, France)</li>
<li>Béatrice Daille (University of Nantes, France)</li>
<li>Gaël Dias (University of Caen Basse-Normandie, France)</li>
<li>Voula Giouli (Institute for Language and Speech Processing/Athena RIC, Greece)</li>
<li>Tracy Holloway King (eBay, USA)</li>
<li>Philipp Koehn (Johns Hopkins University, USA)</li>
<li>Dimitrios Kokkinakis (University of Gothenburg, Sweden)</li>
<li>Yannis Korkontzelos (Edge Hill University, UK)</li>
<li>Eric Laporte (Université Paris-Est Marne-la-Vallee, France)</li>
<li>Timm Lichte (University of Düsseldorf, Germany)</li>
<li>Gyri S. Losnegaard (University of Bergen, Norway)</li>
<li>Héctor Martínez Alonso (Thomson Reuters Labs, Canada)</li>
<li>Verginica Mititelu (Romanian Academy Research Institute for Artificial Intelligence, Romania)</li>
<li>Preslav Nakov (Qatar Computing Research Institute, HBKU, Qatar)</li>
<li>Joakim Nivre (Uppsala University, Sweden)</li>
<li>Jan Odijk (University of Utrecht, Netherlands)</li>
<li>Petya Osenova (Bulgarian Academy of Sciences, Bulgaria)</li>
<li>Harris Papageorgiou (Institute for Language and Speech Processing/Athena RIC, Greece)</li>
<li>Yannick Parmentier (Université de Lorraine, France)</li>
<li>Carla Parra Escartín (Dublin City University, ADAPT Centre, Ireland)</li>
<li>Agnieszka Patejuk (Institute of Computer Science, Polish Academy of Sciences, Poland)</li>
<li>Pavel Pecina (Charles University in Prague, Czech Republic)</li>
<li>Scott Piao (Lancaster University, UK)</li>
<li>Martin Riedl (University of Stuttgart, Germany)</li>
<li>Manfred Sailer (Goethe-Universität Frankfurt am Main, Germany)</li>
<li>Nathan Schneider (Georgetown University, USA)</li>
<li>Sabine Schulte Im Walde (University of Stuttgart, Germany)</li>
<li>Ruben Urizar (University of the Basque Country, Spain)</li>
<li>Aline Villavicencio (Federal University of Rio Grande do Sul, Brazil)</li>
<li>Jakub Waszczuk (University of Tours, France)</li>
<li>Shuly Wintner (University of Haifa, Israel)</li>
</ul>

<h1>Volume Editors</h1>

<ul>
<li><a href="http://www.ilsp.gr/en/profile/staff?view=member&id=38&task=show" target="_blank">Stella Markantonatou</a>, Institute for Language and Speech Processing (Greece) </li>
<li><a href="http://pageperso.lif.univ-mrs.fr/~carlos.ramisch" target="_blank">Carlos Ramisch</a>, Aix Marseille University (France) </li>
<li><a href="http://www.info.univ-tours.fr/~savary/" target="_blank">Agata Savary</a>, Université François Rabelais Tours (France) </li>
<li><a href="http://www.inf.u-szeged.hu/~vinczev" target="_blank">Veronika Vincze</a>, Hungarian Academy of Sciences (Hungary) </li>
</ul>


<h1>Contact</h1>

<p>For any inquiries regarding the special issue, please send an email to <a href="mailto:mwe2017workshop at gmail.com">mwe2017workshop at gmail.com</a></p>
