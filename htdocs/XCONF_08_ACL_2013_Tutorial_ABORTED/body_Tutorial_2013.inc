<h2>The ACL 2013 Tutorial on Multiword Expressions</h2>

<h3>Robust Automated Natural Language Processing with Multiword Expressions and Collocations</h3>

<p> by Valia Kordoni, Carlos Ramisch and Aline Villavicencio</p>

<p><small>Last updated: June 04, 2013</small></p>

<h1>Description</h1>

<p>Multiword expressions (MWEs) like <em>break down</em>, <em>bus stop</em>  and <em>make ends meet</em>, are expressions consisting of two or more lexical units that correspond to some conventional way of saying things. They range over linguistic constructions such as fixed phrases (<em>per se, by and large</em>), noun compounds (<em>telephone booth, cable car</em>), compound verbs (<em>give a presentation</em>), idioms (<em>a frog in the throat, kill some time</em>), etc. They are also widely known as collocations, for the frequent co-occurrence of their components.</p>

<p>From a natural language processing perspective, the interest in MWEs comes from the very important role they play forming a large part of human language, which involves the use of linguistic routines or prefabricated sequences in any kind of text or speech, from the terminology of a specific domain (<em>parietal cortex, substantia nigra, splice up</em>) to the more colloquial vocabulary (<em>freak out, make out, mess up</em>) and the language of the social media (<em>hash tag, fail whale, blackbird pie</em>). New MWEs are constantly being introduced in the language (<em>cloud services, social networking site, security apps</em>), and knowing how they are used reflects the ability to successfully understand and generate language. </p>

<p>While easily mastered by native speakers, their treatment and interpretation involves considerable effort for computational systems (and non-native speakers), due to their idiosyncratic, flexible and heterogeneous nature. First of all, there is the task of identifying whether a given sequence of words is an MWE or not (e.g. <em>bus stop</em> in <em>Does the bus stop here?</em> vs <em>The bus stop is here</em>). </p>

<p>For a given MWE, there is also the problem of determining whether it forms a compositional (<em>take away the dishes</em>), semi-idiomatic (<em>boil up the beans</em>) or idiomatic combination (<em>roll up your sleeves</em>).  Furthermore, MWEs may also be polysemous: <em>bring up</em> as carrying (<em>bring up the bags</em>), raising (<em>bring up the children</em>) and mentioning (<em>bring up the subject</em>). Unfortunately, solutions that are successfully employed for treating similar problems in the context of simplex works may not be adequate for MWEs, given the complex interactions between their component words (e.g. the idiomatic use of <em>spill</em> in <em>spilling beans</em> as revealing secrets vs its literal usage in <em>spilling lentils</em>).</p>

<h1>Overview</h1>

<ol>
<li> <strong>PART I - General overview: </strong></li>
<ol>
<li> Introduction </li>
<li> Types and examples of MWEs and collocations </li>
<li> Linguistic dimensions of MWEs: idiomaticity, syntactic and semantic fixedness, specificity, etc. </li>
<li> Statistical dimensions of MWEs: variability, recurrence, association, etc. </li>
<li> Linguistic and psycholinguistic theories of MWEs </li>
</ol>
<li> <strong>PART II - Computational methods (symbolic and statistical) </strong></li>
<ol>
<li> Recognizing the elements of MWEs </li>
<li> Recognising how elements are combined </li>
<li> Type and token evaluation of MWE identification </li>
<li> Robust automated natural language processing with MWEs </li>
</ol>
<li> <strong>PART III - Resources and applications:</strong> </li>
<ol>
<li> MWEs in resources: corpora, lexicons and ontologies (e.g., WordNet and Genia), parsers and tools (e.g., NSP, mwetoolkit, UCS, and jMWE), and MWE website </li>
<li> Pipelines for MWE treatment: creation and annotation of resources, identification of MWEs in text, evaluation of results</li>
<li> Applications to language technology: parsing, information retrieval, information extraction, machine translation</li>
</ol>
<li> <strong>PART IV - Future challenges and open problems</strong></li>
</ol>

<h1>Presenters</h1>

<p><a href="http://www.coli.uni-saarland.de/~kordoni/" target="_blank">Valia Kordoni</a><em> (Humboldt Universität zu Berlin, Germany)</em></p>
<table><tr><td width="130">
<img src="images/kordoni.jpg" height="100" />
</td><td>
<p>
Valia Kordoni is a senior researcher at the Language Technology Lab of the German Research Centre for Artificial Intelligence (DFKI GmbH) and an assistant professor at the Department of Computational Linguistics of Saarland University. Her main areas of interest are syntax, semantics, pragmatics and discourse. She works on the theoretical development of these areas as well as on their implementation in NLP systems.
</p>
</td></tr></table>

<p><a href="http://www.inf.ufrgs.br/~ceramisch/" target="_blank">Carlos Ramisch</a><em> (Joseph Fourier University, France)</em></p>
<table><tr><td width="130">
<img src="images/ramisch.jpg" height="100" />
</td><td>
<p>
Carlos Ramisch's research in computational linguistics includes statistical machine translation and automatic language- and type-independent identification and classification of MWEs in corpora. His current research focuses on empirical methods for multilingual lexical acquisition of MWEs and their integration in multilingual applications such as machine translation.
</p>
</td></tr></table>

<p><a href="http://www.inf.ufrgs.br/~avillavicencio/" target="_blank">Aline Villavicencio</a><em> (Federal University of Rio Grande do Sul, Brazil)</em></p>
<table><tr><td width="130">
<img src="images/villavicencio.jpg" height="100" />
</td><td>
<p>
Aline Villavicencio is an associate professor at the Federal University of Rio Grande do Sul (Brazil) and a visiting scholar at MIT (USA). She has developed research on computational language acquisition and grammar engineering for languages such as English and Portuguese, using a variety of formalisms (HPSG, CG, LFG). For these, MWEs provide a rich research topic, with challenges like their identification from domain specific corpora and for resource poor languages.
</p>
</td></tr></table>

<h1>Materials (soon...)</h1>

<ul>
<li> Slides</li>
<li> List of references</li>
<li> Toy experiment package</li>
</ul>

<h1>A toy experiment</h1>

<p>To be completed</p>


