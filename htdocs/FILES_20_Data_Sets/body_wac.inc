<h2>Legacy page</h2>

<p>Please go to our new website <a href="https://multiword.org/">https://multiword.org/</a> for up-to-date information</p>

<h2>Data sets</h2>

<p>The following resources are included in the MWE_resources package.  The complete package can be downloaded from the <a href="http://sourceforge.net/projects/multiword/files/MWE_resources/20110627/" target="_blank">Multiword project site</a>.  

<!--Please make sure that you download the <a href="https://sourceforge.net/projects/multiword/files/MWE_shared_task/20080221/MWE_shared_task_080221.zip/download" target="_blank">latest release</a></p>-->

<table width="100%" border="0" cellpadding="2">
  <tr>
    <td valign="top" align="left"><strong>File name</strong></td>
    <td valign="top" align="left"><strong>Description</strong></td>
    <td valign="top" align="left"><strong>Contributor</strong></td>
    <td valign="top" align="left"><strong>Link</strong></td>
    <td valign="top" align="left"><strong>Updated</strong></td>
  </tr>
    <td valign="top" align="left">Chinese_IR_Xu</td>
    <td valign="top" align="left">Chinese queries and relevant document annotation for IR</td>
    <td valign="top" align="left"><a href="mailto:yx2[[--aT--]]cs.ualberta.ca">Ying Xu</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/Chinese_IR_Xu" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Sep 2010</td>
  </tr>

  </tr>
    <td valign="top" align="left">Czech_Bigram_Pecina</td>
    <td valign="top" align="left">Czech Dependency Bigrams from the Prague Dependency Treebank</td>
    <td valign="top" align="left"><a href="mailto:pecina[[--aT--]]ufal.mff.cuni.cz">Pavel Pecina</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/Czech_Bigram_Pecina" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Feb 2008</td>
  </tr>
   <tr>
    <td valign="top" align="left">English_CE_Trawinski</td>
    <td valign="top" align="left">CWen: 77 cranberry words of English<br>
            CCen: corresponding cranberry collocations</td>
    <td valign="top" align="left"><a href="mailto:trawinski[[--aT--]]sfs.uni-tuebingen.de">Beata Trawinski</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/English_CE_Trawinski" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Feb 2008</td>    
  </tr>
  <tr>
    <td valign="top" align="left">English_CN_Baldwin</td>
    <td valign="top" align="left">A 1000 sentences corpus incl. 345 English compound nouns</td>
    <td valign="top" align="left"><a href="mailto:tim[[--aT--]]csse.unimelb.edu.au">Timothy Baldwin</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/English_CN_Baldwin" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Feb 2008</td>    
  </tr>
  <tr>
    <td valign="top" align="left">English_LVC_Tu</td>
    <td valign="top" align="left">Dataset submitted with paper 'Learning English Light Verb Constructions: Contextual or Statistical'</td>
    <td valign="top" align="left"><a href="mailto:ytu[[--aT--]]illinois.edu">Yuancheng Tu</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/English_LVC_Tu" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Jun 2011</td>    
  </tr>
  <tr>
    <td valign="top" align="left">English_MWE_Vincze</td>
    <td valign="top" align="left">Dataset submitted with paper 'Detecting noun compounds and light verb constructions: a contrastive study'</td>
    <td valign="top" align="left"><a href="mailto:vinczev[(--aT--)]inf.u-szeged.hu">Veronika Vincze</a></td>
    <td valign="top" align="left"><a href="http://www.inf.u-szeged.hu/rgai/mwe" target="_blank">Website</a></td>
    <td valign="top" align="left">Jun 2011</td>    
  </tr>  
  <tr>
    <td valign="top" align="left">English_NC_Kim</td>
    <td valign="top" align="left">Semantic relations for Noun Compounds</td>
    <td valign="top" align="left"><a href="mailto:sunamkim[[--aT--]]gmail.com">Su Nam Kim</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/English_NC_Kim" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Feb 2008</td>    
  </tr>  
   <tr>
    <td valign="top" align="left">English_NC_Nakov</td>
    <td valign="top" align="left">English Noun Compound interpretation</td>
    <td valign="top" align="left"><a href="mailto:nakov[[--aT--]]cs.berkeley.edu">Preslav Nakov</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/English_NC_Nakov" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Feb 2008</td>    
  </tr>  
  <tr>
    <td valign="top" align="left">English_VNC_Cook</td>
    <td valign="top" align="left">2984 annotated idiomatic English verb-noun tokens</td>
    <td valign="top" align="left"><a href="mailto:pcook[[--aT--]]cs.toronto.edu">Paul Cook</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/English_VNC_Cook" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Feb 2008</td>    
  </tr>
    <tr>
    <td valign="top" align="left">English_VPC_Baldwin</td>
    <td valign="top" align="left">English Verb-Preposition combinations</td>
    <td valign="top" align="left"><a href="mailto:tim[[--aT--]]csse.unimelb.edu.au">Timothy Baldwin</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/English_VPC_Baldwin" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Feb 2008</td>    
  </tr>
  <tr>
    <td valign="top" align="left">Estonian_MWV_corpus_Kaalep</td>
    <td valign="top" align="left">manually tagged corpus with Estonian Multiword Verbs (300,000 tokens)</td>
    <td valign="top" align="left"><a href="mailto:heiki-jaan.kaalep[[--aT--]]ut.ee">Heiki-Jaan Kaalep</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/Estonian_MWV_corpus_Kaalep" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Feb 2008</td>    
  </tr>
   <tr>
    <td valign="top" align="left">Estonian_MWV_db_Kaalep</td>
    <td valign="top" align="left">13,000 Estonian Multiword Verbs</td>
    <td valign="top" align="left"><a href="mailto:heiki-jaan.kaalep[[--aT--]]ut.ee">Heiki-Jaan Kaalep</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/Estonian_MWV_db_Kaalep" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Feb 2008</td>    
  </tr>
    <tr>
    <td valign="top" align="left">French_MWA_Voyatzi</td>
    <td valign="top" align="left">Dictionary of 6,763 French Multiword adverbs</td>
    <td valign="top" align="left"><a href="mailto:voyatzi[[--aT--]]univ-mlv.fr">Stavroula Voyatzi</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/French_MWA_Voyatzi" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Feb 2008</td>    
  </tr>
     <tr>
    <td valign="top" align="left">French_MWN_Laporte</td>
    <td valign="top" align="left">French corpus annotated for Multiword Nouns (5,057 occurrences)</td>
    <td valign="top" align="left"><a href="mailto:eric.laporte[[--aT--]]univ-paris-est.fr">Eric Laporte</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/French_MWN_Laporte" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Feb 2008</td>    
  </tr>
  <tr>
    <td valign="top" align="left">Ge_En_MWE_Anastasiou</td>
    <td valign="top" align="left">Bilingual German-English lexicon of 871 MWEs</td>
    <td valign="top" align="left"><a href="mailto:dimitraa[[--aT--]]coli.uni-sb.de">Dimitra Anastasiou</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/Ge_En_MWE_Anastasiou" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Feb 2008</td>    
  </tr>
  <tr>
    <td valign="top" align="left">German_AN_La11t</td>
    <td valign="top" align="left">Langenscheidt German Adj-N Collocations</td>
    <td valign="top" align="left"><a href="mailto:stefan.evert[[--aT--]]uos.de">Stefan Evert</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/German_AN_La11t" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Feb 2008</td>    
  </tr>
   <tr>
     <td valign="top" align="left">German_CE_Trawinski</td>
    <td valign="top" align="left">CWde: 444 cranberry words of German<br>
            CCde: corresponding cranberry collocations</td>
    <td valign="top" align="left"><a href="mailto:trawinski[[--aT--]]sfs.uni-tuebingen.de">Beata Trawinski</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/German_CE_Trawinski" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Feb 2008</td>    
  </tr>
  <tr>
    <td valign="top" align="left">German_MWE_Anastasiou</td>
    <td valign="top" align="left">Corpus with 536 German annotated sentences</td>
    <td valign="top" align="left"><a href="mailto:dimitraa[[--aT--]]coli.uni-sb.de">Dimitra Anastasiou</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/German_MWE_Anastasiou" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Feb 2008</td>    
  </tr>  
  <tr>
    <td valign="top" align="left">German_PNV_Fritzinger</td>
    <td valign="top" align="left">Contextual annotation of idiomatic/literal German PNV</td>
    <td valign="top" align="left"><a href="mailto:fritzife[[--aT--]]ims.uni-stuttgart.de">Fabienne Fritzinger</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/German_PNV_Fritzinger" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Sep 2010</td>    
  </tr>    
  <tr>
    <td valign="top" align="left">German_PNV_Krenn</td>
    <td valign="top" align="left">Brigitte Krenn's German PP-Verb Collocations</td>
    <td valign="top" align="left"><a href="mailto:brigitte.krenn[[--aT--]]researchstudio.at">Brigitte Krenn</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/German_PNV_Krenn" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Feb 2008</td>    
  </tr>
  <tr>
    <td valign="top" align="left">Greek_MWE_Linardaki</td>
    <td valign="top" align="left">815 Greek nominal MWE candidates annotated by three judges</td>
    <td valign="top" align="left"><a href="mailto:elinardaki[[--aT--]]gmail.com">Evita Linardaki</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/Greek_MWE_Linardaki" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Sep 2010</td>    
  </tr>
  <tr>
    <td valign="top" align="left">Portuguese_CP_Duran</td>
    <td valign="top" align="left">Dataset submitted with paper 'Identifying and Analyzing Brazilian Portuguese Complex Predicates'</td>
    <td valign="top" align="left"><a href="mailto:magali.duran[[--aT--]]uol.com.br">Magali Sanchez Duran</a></td>
    <td valign="top" align="left"><a href="http://multiword.wiki.sourceforge.net/Portuguese_CP_Duran" target="_blank">Wiki</a></td>
    <td valign="top" align="left">Jun 2011</td>    
  </tr>
<tr><td></td><td><h3>External links</h3></td></tr>  
    <tr>
    <td valign="top" align="left"><a href="http://dspace.mit.edu/handle/1721.1/62792" target="_blank">English_WSD_Finlayson</a></td>
    <td valign="top" align="left">Resources submitted with paper 'Detecting Multi-Word Expressions Improves Word Sense Disambiguation'</td>
    <td valign="top" align="left"><a href="mailto:markaf{{(==AT==)}}mit.edu">Mark Finlayson</a></td>
    <td valign="top" align="left"></td>
    <td valign="top" align="left">Jun 2011</td>    
  </tr>
  
  <tr>
    <td valign="top" align="left"><a href="" target="_blank">Japanese-English_MT_Haugereid</a></td>
    <td valign="top" align="left">Complimentary link for the paper 'Extracting Transfer Rules for Multiword Expressions from Parallel Corpora'</td>
    <td valign="top" align="left"><a href="mailto:petterha{{(==AT==)}}ntu.edu.sg">Peter Haugereid</a></td>
    <td valign="top" align="left"></td>
    <td valign="top" align="left">Jun 2011</td>    
  </tr>

  
  
</table>


<p><a target="_blank" rel="license" href="http://creativecommons.org/licenses/by/3.0/"><img alt="Creative Commons Licence" style="border-width:0" src="http://i.creativecommons.org/l/by/3.0/88x31.png" /></a></p>



