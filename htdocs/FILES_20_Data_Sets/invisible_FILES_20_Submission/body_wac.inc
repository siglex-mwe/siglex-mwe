<h2>Submission Information</h2>

<p>Please send your resource package to <a href="mailto:Nicole.Gregoire@let.uu.nl">Nicole Grégoire</a>. Include the following information:</p>

<ul>
<li>Documentation including extraction information and annotation guidelines.</li>
<li>A Readme file including: package name, contact details of the contributor(s), submission date, source, description, file names.</li>
</ul> 