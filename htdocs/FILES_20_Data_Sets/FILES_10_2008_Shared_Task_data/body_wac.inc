<h2>Legacy page</h2>

<p>Please go to our new website <a href="https://multiword.org/">https://multiword.org/</a> for up-to-date information</p>

<h2>Corpus frequency data</h2>

<p>
  In order to ensure fully comparable conditions for evaluation studies, we also provide standard corpus frequency data for some of the manually annotated MWE resources.  Because of their size, these data sets are hosted on external servers and can be downloaded by clicking on the data set names in the table below.
</p>

<table width="100%" border="0" cellpadding="2">
  <tr>
    <td valign="top" align="left"><strong>File name</strong></td>
    <td valign="top" align="left"><strong>Description</strong></td>
    <td valign="top" align="left"><strong>Size</strong></td>
    <td valign="top" align="left"><strong>MWE data</strong></td>
    <td valign="top" align="left"><strong>Contributor</strong></td>
  </tr>
  <tr>
    <td valign="top" align="left"><a href="http://www.collocations.de/data/German_PNV_Data_FR.zip">German_PNV_FR</a></td>
    <td valign="top" align="left">German PP-verb combinations from the <em>Frankfurter Rundschau</em> corpus</td>
    <td valign="top" align="left">56&nbsp;MiB</td>
    <td valign="top" align="left">German_PNV_Krenn<br> Shared task 2008</td>
    <td valign="top" align="left"><a href="mailto:stefan.evert@uos.de">Stefan Evert</a></td>
  </tr>
  <tr>
    <td valign="top" align="left"><a href="http://www.collocations.de/data/German_AN_Data_FR.zip">German_AN_FR</a></td>
    <td valign="top" align="left">German adjective-noun combinations from the <em>Frankfurter Rundschau</em> corpus</td>
    <td valign="top" align="left">32&nbsp;MiB</td>
    <td valign="top" align="left">German_AN_La11t<br> Shared task 2008</td>
    <td valign="top" align="left"><a href="mailto:stefan.evert@uos.de">Stefan Evert</a></td>
  </tr>
  <tr>
    <td valign="top" align="left"><a href="http://www.collocations.de/data/Czech_Bigram_PDT.zip">Czech_Bigram_PDT</a></td>
    <td valign="top" align="left">Czech dependency bigrams from the Prague Dependency Treebank</td>
    <td valign="top" align="left">870&nbsp;KiB</td>
    <td valign="top" align="left">Czech_Bigram_Pecina<br> Shared task 2008</td>
    <td valign="top" align="left"><a href="mailto:pecina@ufal.mff.cuni.cz">Pavel Pecina</a></td>
  </tr>
</table>

<h2>Shared task evaluation packages</h2>

<p>
  Evaluation packages from previous shared tasks can be downloaded here:
</p>

<ul>
  <li><a href="http://www.collocations.de/data/MWE2008_Evaluation_Package.zip">MWE 2008 Shared Task</a> (R scripts for precision-recall evaluation of candidate ranking)</li>
</ul>
