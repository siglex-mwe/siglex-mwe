<h2>The 12th Workshop on Multiword Expressions (MWE 2016)</h2>

<h3>Workshop at ACL 2016 (Berlin, Germany), August 11 or 12, 2016</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.siglex.org/" target="_blank">SIGLEX</a>) and SIGLEX's Multiword Expressions Section (<a href="http://multiword.sourceforge.net/PHITE.php?sitesig=MWE" target="_blank">SIGLEX-MWE</a>).
</p>


<p><small>Last updated: July 5, 2016</small></p>


<h1>Program Committee</h1>

<ul>
<li> Dimitra Anastasiou, University of Bremen (Germany) </li>
<li> Tim Baldwin, University of Melbourne (Australia)</li>
<li> Núria Bel, Pompeu Fabra University (Spain)</li>
<li> Lars Borin, University of Gothenburg (Sweden) </li>
<li> Jill Burstein, ETS (USA)</li>
<li> Aoife Cahill, ETS (USA)</li>
<li> Paul Cook, University of New Brunswick (Canada)</li>
<li> Béatrice Daille, Nantes University (France)</li>
<li> Joaquim Ferreira da Silva, New University of Lisbon (Portugal)</li>
<li> Voula Gotsoulia, Institute for Language and Speech Processing/R.C. “Athena” (Greece)</li>
<li> Chikara Hashimoto, National Institute of Information and Communications Technology (Japan)</li>
<li> Kyo Kageura, University of Tokyo (Japan)</li>
<li> Dimitrios Kokkinakis, University of Gothenburg (Sweden)</li>
<li> Ioannis Korkontzelos, University of Manchester (UK)</li>
<li> Takuya Matsuzaki, Nagoya University (Japan)</li>
<li> Yusuke Miyao, National Institute of Informatics (Japan)</li>
<li> Joakim Nivre, University of Uppsala (Sweden)</li>
<li> Diarmuid Ó Séaghdha, University of Cambridge and VocalIQ (UK)</li>
<li> Haris Papageorgiou, Institute for Language and Speech Processing/R.C. “Athena” (Greece)</li>
<li> Yannick Parmentier, Universite d’Orleans (France)</li>
<li> Pavel Pecina, Charles University Prague (Czech Republic)</li>
<li> Scott Piao, Lancaster University (UK)</li>
<li> Barbara Plank, University of Groningen (The Netherlands)</li>
<li> Maja Popović, Humboldt University Berlin (Germany)</li>
<li>Prokopidis Prokopis, Institute for Language and Speech Processing/R.C. “Athena” (Greece)</li>
<li> Carlos Ramisch, Aix-Marseille University (France)</li>
<li> Martin Riedl, University of Darmstadt (Germany)</li>
<li> Will Roberts, Humboldt University Berlin (Germany)</li>
<li> Agata Savary, Université François Rabelais Tours (France)</li>
<li> Aline Villavicencio, Federal University of Rio Grande do Sul (Brazil)</li>
<li> Veronika Vincze, Hungarian Academy of Sciences (Hungary)</li>
</ul>


<h1>Workshop Organizers</h1>

<ul>
<li><a href="https://www.angl.hu-berlin.de/staff/kordoni" target="_blank">Valia Kordoni</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="https://www.angl.hu-berlin.de/staff/1687478" target="_blank">Kostadin Cholakov</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="https://www.angl.hu-berlin.de/staff/markus_egg" target="_blank">Markus Egg</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="http://www.ilsp.gr/en/profile/staff?view=member&id=38&task=show" target="_blank">Stella Markantonatou</a><em> (Institute for Language and Speech Processing (ILSP) - Athena Research Center, Greece)</em></li>
<li><a href="http://qcri.org.qa/page?a=117&pid=35&lang=en-CA" target="_blank">Preslav Nakov</a><em> (Qatar Computing Research Institute, Qatar)</em></li>
</ul>


<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwe2016.acl@gmail.com">mwe2016.acl@gmail.com</a></p>