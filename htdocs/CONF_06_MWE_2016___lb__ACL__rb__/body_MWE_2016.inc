<h2>The 12th Workshop on Multiword Expressions (MWE 2016)</h2>

<h3>Workshop at ACL 2016 (Berlin, Germany), August 11, 2016</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.siglex.org/" target="_blank">SIGLEX</a>) and SIGLEX's Multiword Expressions Section (<a href="http://multiword.sourceforge.net/PHITE.php?sitesig=MWE" target="_blank">SIGLEX-MWE</a>).
</p>


<p><small>Last updated: Jul 5, 2016</small></p>

<h1>News</h1>

<ul>
  <li><em>Jul 5, 2016:</em> The workshop program is now online!</li>
</ul>

<h1>Important Dates</h1>

<table>
<tr><td><del>May 8, 2016</del></td><td>Submission <strong>deadline</strong> for long &amp; short papers at 23:59 Pacific time - GMT-8</td></tr>
<tr><td><del>Jun 5, 2016</del></td><td>Notification of acceptance</td></tr>
<tr><td><del>Jun 22, 2016</del></td><td>Camera-ready papers due</td></tr>
<tr><td>Aug 11, 2016</td><td>MWE 2015 Workshop</td></tr>
</table>

<p>Submission and reviewing will be electronic, managed by the START system:</p>
<p align="center"><a href="https://www.softconf.com/acl2016/mwe" target="_blank">https://www.softconf.com/acl2016/mwe</a></p>

<h1>Call For Papers</h1>

<p>Under the denomination "multiword expression", one assumes a wide range of linguistic constructions such as idioms (“storm in a teacup”, “sweep under the rug”), fixed phrases (“in vitro”, “by and large”, “rock'n roll”), noun compounds (“olive oil”, “laser printer”), compound verbs (“take a nap”, “bring  about”), etc. While easily mastered by native speakers, their interpretation poses a major challenge for computational systems, due to their flexible and heterogeneous nature.</p>

<p>For a start, MWEs are not nearly as frequent in NLP resources as they are in real-world
text, and this problem of coverage may impact the performance of many NLP tasks. Moreover, treating MWEs also involves problems like determining their semantics, which is not always compositional (“to kick the bucket” meaning “to die”). In sum, MWEs are a key issue and a current weakness for natural language parsing and generation, as well as real-life applications depending on language technology, such as machine translation, just to name a prominent one among many. Thanks to the joint efforts of researchers from several fields working on MWEs, significant progress has been made in recent years, especially concerning the construction of large-scale language resources. For instance, there is a large number of recent papers which focus on acquisition of MWEs from corpora, and others that describe a variety of techniques to find paraphrases for MWEs. Current methods use a plethora of tools such as association measures, machine learning, syntactic patterns, web queries, etc. A considerable body of techniques, resources and tools to perform these tasks are now available, and are indicative of the growing importance of the field within the NLP community.</p>

<p>Many of these advances are described as part of the annual workshop on MWEs, which attracts the attention of an ever-growing community working on a variety of languages and MWE types. The workshop has been held since 2001 in conjunction with major computational linguistics conferences (ACL, EACL, NAACL, COLING, LREC), providing an important venue for the community to interact, share resources and tools and collaborate on efforts for advancing the computational treatment of MWEs. Currently, EU supports the IC1207 COST Action PARSEME (2013-2017) that focuses on the role of MWEs in parsing. Additionally, special issues on MWEs have been published by leading journals in computational linguistics. The latest such effort is the special issue in 2013 on “Multiword Expressions: from Theory to Practice and Use”, which has recently been published by the <a href="http://multiword.sourceforge.net/tslp2011si" target="_blank">ACM Transactions on Speech and Language Processing</a>. Also in 2013, there was a special issue of Natural Language Engineering on the <a href="http://journals.cambridge.org/action/displayIssue?jid=NLE&volumeId=19&seriesId=0&issueId=03" target="_blank">“Semantics of Noun Compounds”</a>.</p>

<p>MWE 2016 will be the 12th event in the series. We will be interested in major challenges in the overall process of MWE treatment, both from the theoretical and the computational viewpoint, focusing on original research related (but not limited) to the following topics:</p>

<ul>
<li> Lexicon-grammar interface for MWEs </li>
<li> Parsing techniques for MWEs </li>
<li> Hybrid parsing of MWEs </li>
<li> Annotating MWEs in treebanks </li>
<li> Manually and automatically constructed resources </li>
<li> Representation of MWEs in dictionaries and ontologies </li>
<li> MWEs and user interaction </li>
<li> Multilingual acquisition </li>
<li> Multilingualism and MWE processing </li>
<li> Models of first and second language acquisition of MWEs </li>
<li> Crosslinguistic studies on MWEs </li>
<li> The role of MWEs in the domain adaptation of parsers </li>
<li> Integration of MWEs into NLP applications </li>
<li> Evaluation of MWE treatment techniques </li>
<li> Lexical, syntactic or semantic aspects of MWEs </li>
</ul>

<h1>Submission modalities</h1>

<p>For MWE 2016, we accept the following two types of submissions:</p>
<ul>
<li>Regular long papers (8 content pages + 2 page for references): Long papers should report on solid and finished research including new experimental results, resources and/or techniques.</li> 
<li>Regular short papers (4 content pages + 2 page for references): Short papers should report on small experiments, focused contributions, ongoing research, negative results and/or philosophical discussion.</li>
</ul>

<p>The reported research should be substantially original. The papers will be presented orally or as posters. The decision as to which papers will be presented orally and which as posters will be made by the program committee based on the nature rather than on the quality of the work. All submissions must be in PDF format and must follow the ACL 2016 formatting requirements (style files available <a href="http://www.acl2016.org/files/acl2016.zip" target="_blank">here</a>). We strongly advise the use of the provided Word or LaTeX template files.</p>

<p>Reviewing will be double-blind, and thus no author information should be included in the papers; self-reference should be avoided as well. </p>

<p>Resources submitted with the papers should be anonymized for submission. Papers and/or resources that do not conform to these requirements will be rejected without review. Accepted papers will appear in the workshop proceedings, where no distinction will be made between papers presented orally or as posters. </p>

<p>Submission and reviewing will be electronic, managed by the START system:</p>
<p align="center"><a href="https://www.softconf.com/acl2016/mwe" target="_blank">https://www.softconf.com/acl2016/mwe</a></p>
<p>Submissions must be uploaded onto the START system by the submission deadline: <b>May 8, 2016</b> (11:59pm, GMT-8).
Please choose the appropriate submission type from the starting submission page, according to the category of your paper.</p>


<h1>Workshop Organizers</h1>

<ul>
<li><a href="https://www.angl.hu-berlin.de/staff/kordoni" target="_blank">Valia Kordoni</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="https://www.angl.hu-berlin.de/staff/1687478" target="_blank">Kostadin Cholakov</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="https://www.angl.hu-berlin.de/staff/markus_egg" target="_blank">Markus Egg</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="http://www.ilsp.gr/en/profile/staff?view=member&id=38&task=show" target="_blank">Stella Markantonatou</a><em> (Institute for Language and Speech Processing (ILSP) - Athena Research Center, Greece)</em></li>
<li><a href="http://qcri.org.qa/page?a=117&pid=35&lang=en-CA" target="_blank">Preslav Nakov</a><em> (Qatar Computing Research Institute, Qatar)</em></li>
</ul>


<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwe2016.acl@gmail.com">mwe2016.acl@gmail.com</a></p>
