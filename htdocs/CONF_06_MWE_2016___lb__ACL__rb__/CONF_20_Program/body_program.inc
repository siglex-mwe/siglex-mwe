<h2>The 12th Workshop on Multiword Expressions (MWE 2016)</h2>

<h3>Workshop at ACL 2016 (Berlin, Germany), August 11 or 12, 2016</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.siglex.org/" target="_blank">SIGLEX</a>) and SIGLEX's Multiword Expressions Section (<a href="http://multiword.sourceforge.net/PHITE.php?sitesig=MWE" target="_blank">SIGLEX-MWE</a>).
</p>


<p><small>Last updated: July 5, 2016</small></p>

<h1>Workshop Program</h1>

<h3>Thursday, 11 August 2016</h3>

<table>
  <tr><td valign="top">08:50–09:00</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Opening remarks</span>
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td></td><td><strong>Oral Session 1</strong></td></tr>
  <tr></tr>
  <tr><td valign="top">09:00–09:30</td><td valign="top">
    <span style="font-style: italic; color: #2255AA"> Learning Paraphrasing for Multiword Expressions</span><br/>
    Seid Muhie Yimam, Héctor Martínez Alonso, Martin Riedl and Chris Biemann
  </td></tr>
  <tr><td valign="top">09:30-10:00</td><td valign="top">
    <span style="font-style: italic; color: #2255AA"> Exploring Long-Term Temporal Trends in the Use of Multiword Expressions</span><br/>
    Tal Daniel and Mark Last
  </td></tr>
  <tr><td valign="top">10:00–10:30</td><td valign="top">
    <span style="font-style: italic; color: #2255AA"> Lexical Variability and Compositionality: Investigating Idiomaticity with Distributional Semantic Models</span><br/>
    Marco Silvio Giuseppe Senaldi, Gianluca E. Lebani and Alessandro Lenci
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td valign="top">10:30–11:00</td><td valign="top">
    Coffee Break
  </td></tr>
<tr><td></td><td>&nbsp;</td></tr>
  <tr><td></td><td><strong>Oral Session 2</strong></td></tr>
  <tr></tr>
  <tr><td valign="top">11:00–11:20</td><td valign="top">
  <span style="font-style: italic; color: #2255AA"> Filtering and Measuring the Intrinsic Quality of Human Compositionality Judgments </span><br/>
    Carlos Ramisch, Silvio Cordeiro and Aline Villavicencio
  </td></tr>
  <tr><td valign="top">11:20–11:40</td><td valign="top">
    <span style="font-style: italic; color: #2255AA"> Graph-based Clustering of Synonym Senses for German Particle Verbs</span><br/>
    Moritz Wittmann, Marion Weller-Di Marco and Sabine Schulte im Walde
  </td></tr>
<tr><td valign="top">11:40–12:00</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Accounting ngrams and multi-word terms can improve topic models</span><br/>
    Michael Nokel and Natalia Loukachevitch
  </td></tr>
<tr><td></td><td>&nbsp;</td></tr>
<tr><td valign="top">12:00–13:00</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Invited Talk: Multiword Expressions in Dependency Parsing</span><br/>
  Joakim Nivre
  </td></tr>
<tr><td></td><td>&nbsp;</td></tr>
  <tr><td valign="top">13:00–14:00</td><td valign="top">
    Lunch
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td>14:00–14:40</td><td>
    <strong>Poster Booster Session</strong> (5 minutes per poster)
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Top a Splitter: Using Distributional Semantics for Improving Compound Splitting</span><br/>
    Patrick Ziering, Stefan Müller and Lonneke van der Plas
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Using Word Embeddings for Improving Statistical Machine Translation of PhrasalVerbs</span><br/>
    Kostadin Cholakov and Valia Kordoni
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Modeling the Non-Substitutability of Multiword Expressions with Distributional Semantics and a Log-Linear Model</span><br/>
    Meghdad Farahmand and James Henderson
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Phrase Representations for Multiword Expressions</span><br/>
    Joël Legrand and Ronan Collobert
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Representing Support Verbs in  FrameNet</span><br/>
   Miriam R L Petruck and Michael Ellsworth
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Inherently Pronominal Verbs in Czech: Description and Conversion Based on Treebank Annotation</span><br/>
    Zdenka Uresova, Eduard Bejcek and Jan Hajic
  </td></tr>
<tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Using collocational features to improve automated scoring of EFL texts</span><br/>
   Yves Bestgen
  </td></tr>
<tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">A study on the production of collocations by European Portuguese learners</span><br/>
   Angela Costa, Luísa Coheur and Teresa Lino
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td>14:40–15:30</td><td><strong>Poster Session</strong></td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td valign="top">15:30–16:00</td><td valign="top">
    Coffee Break
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
<tr><td></td><td><strong>Oral Session 3</strong></td></tr>
  <tr></tr>
  <tr><td valign="top">16:00–16:30</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Extraction and Recognition of Polish Multiword Expressions using Wikipedia and Finite-State Automata</span><br/>
    Paweł Chrzaszcz
  </td>
  <tr><td valign="top">16:30–16:50</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Impact of MWE Resources on Multiword Recognition</span><br/>
    Martin Riedl and Chris Biemann
  </td></tr>
<tr><td valign="top">16:50–17:10</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">A Word Embedding Approach to Identifying Verb-Noun Idiomatic Combinations</span><br/>
    Waseem Gharbieh, Virendra Bhavsar and Paul Cook
  </td></tr>
</table>

<!--
<table>
  <tr><td style="width: 10em;"></td><td>&nbsp;</td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td></td><td><strong>Session:</strong></td></tr>
  <tr><td valign="top">00:00–00:00</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">TITLE</span><br/>
    AUTHORS
  </td></tr>
</table>
-->
