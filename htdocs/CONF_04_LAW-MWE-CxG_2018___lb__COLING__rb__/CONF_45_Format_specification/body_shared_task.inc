<h2>CUPT format specification</h2>

<p>The <a href="https://gitlab.com/parseme/corpora/wikis/home">PARSEME corpora</a> and PARSEME shared task systems (starting at edition 1.1) use a format called <tt>cupt</tt>, standing for <em>CoNLL-U + parseme-TSV</em>. It corresponds to a single-file merge of CoNLL-U and <a href="https://typo.uni-konstanz.de/parseme/index.php/2-general/184-parseme-shared-task-format-of-the-final-annotation">parsemetsv</a> (used in edition 1.0). The <a href="https://gitlab.com/parseme/sharedtask-data/tree/master/1.2/trial">PARSEME shared task 1.2 trial corpus</a> contains examples of the <tt>cupt</tt> format.</p>

<p><p>In cooperation with <a href="http://universaldependencies.org">Universal Dependencies (UD)</a>, we specify below how one can extend UD's <a href="http://universaldependencies.org/format.html">CoNLL-U format</a>. Then, we specify the <tt>cupt</tt> format and in particular the syntax and semantics of the extra <tt>PARSEME:MWE</tt> column. The cupt format is at the origin of the <a href="http://universaldependencies.org/ext-format.html">CoNLL-U plus format</a>, with which <tt>cupt</tt> is mostly compatible (differences listed below).</p>

<h1>Extended CoNLL-U format (CoNLL-U Plus Format)</h1>

<p>We define a way of extending the CoNLL-U format to include information from other initiatives such as PARSEME. Notice that these specifications do not only cover UD and PARSEME, but any text encoded in CoNLL-U reused by any initiative, since other initiatives may use the CoNLL-U format to represent non-UD data (e.g. automatically parsed corpora, treebanks that respect the CoNLL-U column semantics but not the UD tagset, etc). The generalized version of the extended CoNLL-U format is described on UD's page of  <a href="http://universaldependencies.org/ext-format.html">CoNLL-U plus format</a>.</p>

<p>An extended CoNLL-U file follows the same rules as CoNLL-U files (text in UTF-8, LF as line break, tab-separated columns, 1 blank line after each sentence including the last, #-headed comments before each sentence, etc.). It can contain any number of columns from a CoNLL-U file in any order, merged with initiative-specific columns. Each initiative standardizes:</p>
<ol>
<li>Its acronym, used as prefix of all initiative-specific column names, for instance, <tt>PARSEME</tt></li>
<li>The names of columns, for instance, the MWE-dedicated column in PARSEME is called <tt>PARSEME:MWE</tt></li>
<li>The syntax and semantics of initiative-specific columns, with the following constraints:
<ul>
  <li>The underscore <tt>'_'</tt>, when it occurs alone in a field, is reserved for underspecified annotations. It can be used in incomplete annotations or in blind versions of the annotated files.</li>
  <li>The star <tt>'*'</tt>, when it occurs alone in a field, is reserved for empty annotations, which are different from underspecified. This concerns sporadic annotations (where not necessarily all words receive an annotation, as opposed to e.g. part-of-speech tags in UPOS). </li>
  <li>The use of underscore <tt>'_'</tt> and star <tt>'*'</tt> is unconstrained when they occur with other characters (e.g. in names of features or values as in <tt>spec_char=*</tt>).</li>
</ul></li>
</ol>      
<p>The first line, that is, the first comment of the first sentence, specifies the names and order of the corresponding CoNLL-U columns (note that UPOSTAG was renamed to UPOS and XPOSTAG to XPOS) and of the initiative-specific columns, for example:</p>

<code># global.columns = ID FORM PARSEME:MWE</code>

<p>Two mandatory metadata fields are included before each sentence, in the form of comments:</p>

<ol>
  <li><tt>text</tt> contains the original text and conforms to the <a href="http://universaldependencies.org/format.html#sentence-boundaries-and-comments">CoNLL-U <tt>text</tt> comment</a></li>
  <li>CoNLL-U's <tt>sent_id</tt> is replaced with <tt>source_sent_id</tt>, containing three parts separated by spaces:<br/>
  <code># source_sent_id = prefix-uri file-path-under-root sentence-id</code>
  <ol>
    <li><tt>prefix-uri</tt>: a URI permanently referring to a source containing the same version of the corpus, in <a href="http://universaldependencies.org/format.html">CoNLL-U format</a>.
    <ul>
      <li>For sentences coming from a UD treebank, the URI of the corresponding UD release, for example:
      <ul> 
        <li><tt>http://hdl.handle.net/11234/1-1983</tt> for UD 2.0.</li>
        <li><tt>http://hdl.handle.net/11234/1-2515</tt> for UD 2.1.</li>
      </ul></li>    
      <li>For sentences from other resources, a permanent/immutable URL to an official website from which the original resource can be downloaded, for example:
      <ul>      
        <li><tt>http://hdl.handle.net/11372/LRT-2282</tt> for the PARSEME shared task 1.0 corpora.</li>
        <li><tt>http://hdl.handle.net/11372/LRT-2842</tt> for the PARSEME shared task 1.1 corpora.</li>        
        <li><tt>https://gitlab.com/parseme/sharedtask-data/tree/a762bcde22b08740f006a4ae0272b63fd6ff5074</tt> is also a valid <tt>prefix-uri</tt> for the PARSEME shared task 1.0 corpora, but we recommend the <tt>handle.net</tt> version above instead.</li>
        <li><del><tt>https://gitlab.com/parseme/sharedtask-data</tt></del> should not be used, as a git repository by itself is not permanent/immutable.</li>
      <!--https://github.com/UniversalDependencies/UD_English/tree/fa482c98e46784662d920cde424b40dd3e219358  (for UD_English v2.1 -- 2017-11-15) CR I prefer disallowing this, since the UD release above should be preferred-->
      </ul></li>
      <li>For sentences from a local corpus (e.g. stored within the extended CoNLL-U file, or in a local CoNLL-U file) or if there is no source treebank (there are only initiative-specific columns and no UD-related information), the <tt>prefix-uri</tt> is a single period <tt>'.'</tt>.</li>
    </ul></li>
    <li><tt>file-path-under-root</tt>: the relative path inside the release folder pointed by <tt>prefix-uri</tt>, that is, zero or more directory names followed by a filename, all separated by <tt>'/'</tt>.
    <ul>
      <li>For sentences coming from a UD treebank, the filename in the corresponding release, for example:
      <ul>
        <li><tt>UD_German/de-ud-train.conllu</tt> points to the training file of the German UD 2.1 treebank.</li>
        <li><tt>UD_Portuguese-GSD/pt_br-ud-dev.conllu</tt> points to the dev file of the Portuguese-GSD UD 2.5 treebank.</li>
      </ul>
      </li>
      <li>For sentences from other resources, the actual path to the corpus file under a given directory pointed by <tt>prefix-uri</tt>.</li>
      <li>If <tt>prefix-uri</tt> uniquely identifies exactly one file, the <tt>file-path-under-root</tt> must be a single period <tt>'.'</tt>.</li>
    </ul>
    </li>
    <li><tt>sentence-id</tt>: a unique sentence identifier in the whole corpus.
    <ul>
      <li>For sentences coming from a UD treebank, the same <tt>sent_id</tt> as in the corresponding CoNLL-U file, for example:
      <ul>
        <li><tt>fr-ud-train_10542</tt> is a sentence in the French training corpus in UD 2.1.</li>
        <li><tt>sv-ud-test-143</tt> is a sentence in the Swedish test corpus in UD 2.1.</li>
      </ul>
      </li>
      <li>For sentences from other resources, a new unique identifier not containing any whitespace or slash <tt>'/'</tt>.</li>
    </ul>
    </li>
  </ol>
  </li>
</ol>

<p>The extended CoNLL-U file may contain free comments, preceded by a hash <tt>#</tt>. If the extended CoNLL-U file has the same metadata and comments before each sentence, and sentences in the same order as in the original CoNLL-U file, then both files can be easily aligned. This is, however, not a requirement, as sentences do not need to appear in the same order and can have other comments in addition to the mandatory <tt>text</tt> and <tt>source_sent_id</tt> metadata comments.</p>

<p>There are two differences between <tt>cupt</tt> and CoNLL-U Plus files. First, the metadata field <tt>source_sent_id</tt> in <tt>cupt</tt> is mandatory and has three parts, whereas the CoNLL-U Plus <tt>source_sent_id</tt> is not mandatory and has four parts, with the first one corresponding to the format of the source file. Second, <tt>cupt</tt> files do not contain necessarily contain a <tt>sent_id</tt> field whereas this is mandatory in CoNLL-U Plus.</p>

<!--------------------------------------------------------------------------------->
<h1>Syntax of the <tt>PARSEME:MWE</tt> column</h1>

<p>A <tt>cupt</tt> file is an instantiation of the extended CoNLL-U file format for the PARSEME Shared Task. It includes all 10 columns of a CoNLL-U file in the same order, plus an 11th column called <tt>PARSEME:MWE</tt>.</p>

<p>The <tt>PARSEME:MWE</tt> column encodes information about verbal multiword expressions (VMWEs) present in a sentence. It is very similar to the fourth column of edition 1.0's <a href="https://typo.uni-konstanz.de/parseme/index.php/2-general/184-parseme-shared-task-format-of-the-final-annotation">parsemetsv format</a>:</p>
<ul>
  <li>It contains a star <tt>'*'</tt> if the word in the current line is not part of a VMWE, or if the current line describes a multiword tokens (e.g. <tt>2-3  don't</tt>).</li>
  <li>It contains an underscore <tt>'_'</tt> if this information is underspecified (e.g. in the blind test corpus).</li>
  <li>It contains a list of semicolon-separated <em>VMWE codes</em> if the current word is part of one or more VMWEs. VMWE codes are only assigned to the lexicalized components of a VMWE (see <a href="https://parsemefr.lis-lab.fr/parseme-st-guidelines/1.2/?page=lexicalized"><em>Lexicalized components and open slots</em></a> in the annotation guidelines).
  <ul>
    <li>If the current line contains the <u>first</u> lexicalized component of the VMWE in the sentence, the VMWE code consists of a <em>VMWE identifier</em> followed by a colon <tt>':'</tt> and a <em>VMWE category label</em>, for example: <tt>1:VID</tt>
    <ul> 
       <li><em>VMWE identifiers</em> are integers starting from 1 for each new sentence, and increased by 1 for each new VMWE.</li>
       <li><em>VMWE category labels</em> are strings corresponding to the category of the VMWE (see <a href="https://parsemefr.lis-lab.fr/parseme-st-guidelines/1.2/?page=categ"><em>VMWE categories</em></a> in the annotation guidelines). The following VMWE category labels are allowed in shared task 1.1:
       <ul>
        <li><tt>LVC.full</tt> -- light-verb constructions, the verb only adds meaning expressed by its morphology, e.g. <em>to <b>give</b> a <b>lecture</b></em> (edition 1.0: <tt>LVC</tt>).</li>
        <li><tt>LVC.cause</tt> -- light-verb constructions, the verb adds a causative meaning to the noun, e.g. <em>to <b>grant rights</b></em> </li>
        <li><tt>VID</tt> -- verbal idioms, e.g. <em>to <b>go bananas</b></em> (edition 1.0: <tt>ID</tt>).</li>
        <li><tt>IRV</tt> -- inherently reflexive verbs, e.g. <em>to <b>help oneself</b> to the cookies</em> (edition 1.0: <tt>IReflV</tt>).</li>
        <li><tt>VPC.full</tt> -- fully non-compositional verb-particle constructions, the particle totally changes the meaning of the verb, e.g. <em>to <b>do in</b></em> (edition 1.0: <tt>VPC</tt>).</li>
        <li><tt>VPC.semi</tt> -- semi-compositional verb-particle constructions, the particle adds a partly predictable but non-spatial meaning to the verb, e.g. <em>to <b>eat up</b></em>.</li>
        <li><tt>MVC</tt> -- multi-verb constructions, e.g. <em>to <b>make do</b></em>.</li>
        <li><tt>IAV</tt> -- inherently adpositional verbs, e.g. <em>to <b>come across</b></em>.</li>
        <li><tt>LS.ICV</tt> -- inherently clitic verbs: this language-specific category is used only in Italian.</li>
       </ul>
       </li>
    </ul>
    <li>If the current line contains a lexicalized component of the VMWE which is <u>not the first</u> one in the sentence, the VMWE code contains the VMWE identifier only, as described above, and no VMWE category label</li>
  </ul>
  </li>
</ul>

<p>Examples of <tt>cupt</tt> files can be found in the <a href="https://gitlab.com/parseme/sharedtask-data/tree/master/1.2/trial">shared task 1.2 trial corpus</a>.

<!--------------------------------------------------------------------------------->
<h1>Tools</h1>

<p>To check that a file conforms to the <tt>cupt</tt> format, use the <a href="https://gitlab.com/parseme/sharedtask-data/blob/master/1.2/bin/validate_cupt.py">validation script</a> as follows:
	<ul style="list-style: none;">
        <li><span style="text-indent: 50px; font-family: Courier New;">./validate_cupt.py --input your-file.cupt</span></li>
	</ul>
</p>

<p>To facilitate the upgrade of tools dealing with the old CoNLL-U+parsemetsv pair of files, we provide a script that converts a <tt>parsemetsv</tt> file into a <tt>cupt</tt> file: <a href="https://gitlab.com/parseme/sharedtask-data/blob/master/1.1/bin/parsemetsv2cupt.py"><tt>parsemetsv2cupt.py</tt></a>. Notice that these scripts rely on libraries present in the gitlab repository: one must donwload the whole repository to be able to use the scripts.</p>


<!--<p>Note that, while the manually annotated training corpora will contain category labels for all VMWEs, system outputs do not need to provide category labels, as they will be ignored by the evaluation metrics and script. VMWE codes in system outputs can contain identifiers only (e.g., 1, 2), and no LVC.full, VID, IRV etc. category labels.</p>-->








