<h2>Joint Workshop on Linguistic Annotation, Multiword Expressions and Constructions (LAW-MWE-CxG-2018)</h2>

<!-- <h3>Workshop proposal for <a href="http://coling2018.org/" target="_blank">COLING 2018</a> (Santa Fé, USA) or <a href="http://acl2018.org/" target="_blank">ACL 2018</a> (Melbourne, Australia)</h3> -->

<h3>Workshop at <a href="http://coling2018.org/" target="_blank">COLING 2018</a> (Santa Fe, USA), August 25-26, 2018</h3>

<p>Organized and sponsored by the Special Interest Group for Annotation (<a href="https://www.cs.vassar.edu/sigann/" target="_blank">SIGANN</a>) and the Special Interest Group on the Lexicon (<a href="http://www.siglex.org" target="_blank">SIGLEX</a>) of the Association for Computational Linguistics (<a href="https://www.aclweb.org/portal/" target="_blank">ACL</a>). Endorsed also by the Special Interest Group on Computational Semantics (<a href="http://www.sigsem.org" target="_blank">SIGSEM</a>).</p>

<p>This joint event brings together:</p>
<ul style="margin-top:-1em;">
<li><strong>The 12th Linguistic Annotation Workshop (<a href="https://www.cs.vassar.edu/sigann/" target="_blank">LAW XII</a>)</strong>, and</li>
<li><strong>The 14th Workshop on Multiword Expressions (<a href="?sitesig=CONF" target="_blank">MWE</a>)</strong>.</li>
</ul>

<p><small>Last updated: Aug 13, 2018</small></p>

<ul>
<li><font color="red"><strong>NEW!</strong></font>&nbsp;&nbsp;[Aug 13]: The <a href="https://aclanthology.coli.uni-saarland.de/events/ws-2018/#W18-49" target="_blank">LAW-MWE-CxG proceedings</a> are 	available on the ACL Anthology.</li>

<li><font color="red"><strong>NEW!</strong></font>&nbsp;&nbsp;[May 24]: COLING 2018 has changed its registration policy. Now <strong>a registration to a workshop only is possible</strong>. See the <a href="https://coling2018.org/" target="_blank">COLING home page</a> for details.</li>

<li><font color="red"><strong>NEW!</strong></font>&nbsp;&nbsp;[May 23]: The submission deadline has been extended until Sunday, <strong>May, 27</strong>. We have also extended the page limits for all papers by <strong>1 page</strong>, to be consistent with the COLING policy.</li>

<li><font color="red"><strong>NEW!</strong></font>&nbsp;&nbsp;[May 22]: Remote presentations of papers will be possible in case of <strong>visa denial</strong>. In order to qualify for this service, the author will need to prove that he or she was denied a visa, i.e. provide the following: (i) the embassy location where the application was made, (ii) the DS160 case number (this is given when the applicant has filed and paid for a formal application). US State Department website will be used to check whether the visa was actually refused.</li>

<li><font color="red"><strong>NEW!</strong></font>&nbsp;&nbsp;<strike>[May 18]: May COLING 2018 has published its <a href="https://www.softconf.com/coling2018/COLING18-REGISTRATION/cgi-bin/scmd.cgi?scmd=people&pageid=0" target="_blank">registration fees</a>. Note that the registration to the main conference is mandatory - <strong>it is not possible to register only to the workshop</strong>. This policy is now being discussed among the conference and workshop organizers. More details soon.</strike></li>

</ul>

<!------------------------------------->
<h1>Description</h1>

<p>This workshop addresses, within a joint event, three domains - linguistic annotation, multiword expressions and grammatical constructions - with partly overlapping communities and research interests, but relatively divergent practices and terminologies.</p>

<p>Linguistic annotation of natural language corpora is the backbone of supervised methods for statistical natural language processing. It also provides valuable data for evaluation of both rule-based and supervised systems and can help formalize and study linguistic phenomena. Challenges posed by creation/evaluation of annotation schemes, automatic and manual annotation, use and evaluation of annotation software and frameworks, or representation of linguistic data and annotations, have been addressed for the last decade within the Linguistic Annotation Workshop (LAW) organised yearly by the <a href="https://www.cs.vassar.edu/sigann/" target="_blank">SIGANN</a>.</p>

<p>The domain of multiword expressions (MWEs) is orthogonal to linguistic annotation since it addresses one particular linguistic phenomenon across various NLP modelling and processing layers or practices (including annotation). MWEs are word combinations, such as all of a sudden, a hot dog, to pay a visit or to pull one's leg, which exhibit lexical, syntactic, semantic, pragmatic and/or statistical idiosyncracies. They encompass closely related linguistic objects such as idioms, compounds, light verb constructions, rhetorical figures, institutionalised phrases or collocations. Modelling and computational aspects of MWEs have been covered by the Multiword Expression Workshop, organised over the past years by the MWE section of <a href="http://www.siglex.org" target="_blank">SIGLEX</a>. Due to their unpredictable behavior, and most prominently their non-compositional semantics, MWEs pose special problems in linguistic modelling (e.g. treebank annotation and grammar engineering), in NLP pipelines (e.g. when their orchestration with parsing is concerned), and in end-use applications (e.g. information extraction or machine translation).</p>

<p>These challenges are magnified when larger classes of idiosyncratic units are considered, namely grammatical constructions, i.e. conventional associations of lexical, syntactic, and pragmatic information, such as the-Adj-more-Adj (the more the merrier, the higher the better, etc.). In the framework of Construction Grammar (CxG), linguistic knowledge is captured in an inventory of form-meaning pairings of varying degrees of internal complexity and lexical fixedness. Thus, MWEs can be seen as special types of constructions: those in which constraints of a lexical nature are particularly strong. The potential new insights to be gained from bringing MWE and construction studies together are mutual. On the one hand, computational approaches to MWEs usually take binary decisions about units of language (MWE vs. non-MWE), i.e. the fact that MWEs occupy a "continuum of compositionality" is neglected. Construction-oriented modelling, conversely, paves the way towards a more nuanced representation of MWE idiosyncrasies. On the other hand, most grammatical constructions display considerable flexibility, therefore their discovery and description is a highly complex and labor-intensive process. This process might be largely facilitated if recent computational achievements for MWEs could be extended to constructions.</p>

<p>Annotation of grammatical constructions in training data could improve machine translation and information extraction, especially cross-lingually, as meanings that are similar across languages (like comparison) can be expressed in drastically different forms. However, annotation of constructions poses significant challenges: because constructions are form-meaning pairs that can be more or less fluid in form, determining the annotation units for a construction is not straightforward. As a result, strategies for choosing annotation units may vary greatly among annotators and projects depending on a range of factors, from practical concerns (intended use, processing constraints) to concerns imposed by an underlying theory. Annotation of grammatical constructions is therefore an area that offers rich opportunities for identifying principled annotation strategies, accommodating  different perspectives on a given phenomenon, and finding ways to allow for harmonization of annotations not only from different sources, but also at different linguistic levels.</p>

<p>For the above reasons, grammatical constructions were elected as a joint focus of interest both by the MWE and the LAW community. We call for papers focusing on research related (but not limited) to the following topics.</p>

<h3>Joint topics on constructions, annotation, and MWEs</h3>
<ul> 
<li>MWE and construction annotation in corpora and treebanks</li>
<li>MWE and construction representation in manually and automatically constructed lexical resources</li>
<li>Extending MWE discovery and identification methods to constructions</li>
<li>MWEs and constructions (and their annotations) in language acquisition and in non-standard language (e.g. tweets, forums, spontaneous speech)</li>
<li>Evaluation of MWE and construction annotation and processing techniques</li>
<li>Computationally-applicable theoretical studies on MWEs and constructions in psycholinguistics, corpus linguistics and grammar formalisms, and/or how such studies can impact annotation of constructions</li>
</ul>

<h3>Annotation-specific topics</h3>
<ul> 
<li>Annotation procedures, whether manual or automatic, including machine learning and knowledge-based methods</li>
<li>Maintenance and interactive exploration of annotation structures and annotated data</li>
<li>Qualitative and quantitative annotation evaluation</li>
<li>Linguistic considerations, representation formats and exploration tools for merged annotations of different phenomena</li>
<li>Standards, best practices, documentation, interoperability, and comparison of annotation schemes</li>
<li>Development, evaluation and innovative use of annotation software frameworks</li>
</ul>

<h3>MWE-specific topics</h3>
<ul> 
<li>Original MWE discovery and identification methods</li>
<li>MWE processing in syntactic and semantic frameworks (e.g. HPSG, LFG, TAG, universal dependencies, WSD, semantic parsing), and in end-user applications (e.g. summarization, machine translation)</li>
</ul>

<!------------------------------------->
<h1>Special Track: PARSEME Shared Task on Automatic Verbal MWE Identification</h1>

<p>The LAW-MWE-CxG-2018 workshop hosts edition 1.1 of the PARSEME <a href="http://multiword.sourceforge.net/sharedtask2018" target=_blank">shared task</a> on automatic verbal MWE identification (see below). This initiative is a follow-up of <a href="http://multiword.sourceforge.net/sharedtask2017" target="_blank">edition 1.0</a> in 2017, which attracted 7 systems working on 18 languages in total. In 2018, we extend the scope to new languages. A separate session will be allocated for the shared task track within the workshop, featuring presentations of the participating systems.</p>

<!------------------------------------->
<h1 id="modalities"><a name="submissions"></a>Submission modalities</h1>

<p>Note that we have extended the page limits for all papers by <strong>1 page</strong>, to be consistent with the COLING policy.</p>

<p><strong>Regular research track</strong>:</p>
<ul>
<li>Long papers (9 content pages + references): They should report on solid and finished research including new experimental results, resources and/or techniques.</li>
<li>Short papers (5 content pages + references): They should report on small experiments, focused contributions, ongoing research, negative results and/or philosophical discussion.</li>
<!-- <li>Demos with posters (4 content pages + references): Demonstrations of software for any aspect of linguistic annotation, automatic identification of MWEs and other phenomena, etc., accompanied by a poster.</li> -->
</ul>

<p>In regular research papers, the reported research should be substantially original. Papers available as preprints can also be submitted provided that they fulfil the conditions defined by the <a href="https://www.aclweb.org/portal/content/new-policies-submission-review-and-citation" target="_blank">ACL Policies for Submission, Review and Citation</a>.</p>

<p><strong>Shared task track</strong>:</p>
<ul>
<li>System description papers (5 content pages + references): These papers should briefly describe the approach implemented to solve the problem. They may include references and links to more detailed descriptions in other documents.</li>
</ul>

<p>Shared task system description papers will go through a separate reviewing process. Submissions will be reviewed by the shared task organizers and participants. Participants of the shared task are not required to submit system description papers, and their acceptance depends on the quality of the paper rather than on the results obtained in the shared task.</p>

<p><strong>Instructions for authors</strong>:</p>

<p>For all 3 types of papers, the submission is <strong>double-blind</strong> as per the <a href="https://coling2018.org/final-call-for-papers/" target="_blank">COLING guidelines</a>. There is no limit on the number of reference pages. Authors will be granted an extra page for the final version of their papers.</p>

<p>All papers will be presented orally or as posters, as determined by the Program Committee chairs. No distinction between papers presented orally or as posters is made in the workshop proceedings.</p>

<p>For all types of submission, the COLING 2018 LaTeX <a href="http://coling2018.org/wp-content/uploads/2018/01/coling2018.zip" target="_blank">templates</a> should be used. All papers should be submitted via the START space: <br/>
<a href="https://www.softconf.com/coling2018/ws-LAW-MWE-CxG-2018/" target="_blanc">https://www.softconf.com/coling2018/ws-LAW-MWE-CxG-2018/</a> <br/>
Please choose the appropriate track (research/shared task) and submission modality (long/short).</p>






<!-- In accordance to EACL 2017 submission policy, this is a condition for accepting the paper for the reviewing process. Final versions of accepted papers will be submitted both in PDF and source LaTeX formats.</p> 

<p>All papers should be submitted via the <a href="https://www.softconf.com/eacl2017/mwe2017" target="_blanc">START space</a>. Please choose the appropriate submission type, according to the category of your paper.</p> 
-->

<!------------------------------------->
 
<h1>Important dates</h1>

<p>All deadlines are at 23:59 UTC-12 (anywhere in the world).</p>

<table>
<tr><td><del>May <strike>25</strike> 27, 2018</del></td><td><del>(<strong>extended</strong>) submission <strong>deadline</strong></del></td></tr>
<tr><td><del>June 20, 2018</del></td><td><del>Notification of acceptance</del></td></tr>
<tr><td><del>June 30, 2018</del></td><td><del>Camera-ready papers due</del></td></tr>
<tr><td><del>August 25-26, 2018</del></td><td><del>LAW-MWE-CxG 2018 Workshop</del></td></tr>
</table>

<!--
See also the important dates for the <a href="http://multiword.sourceforge.net/sharedtask2017" target="_blank">shared task</a> systems.
-->

<!------------------------------------->
<h1>Workshop Organizers</h1>
<ul>
<li><a href="https://www.cs.vassar.edu/~ide/" target="_blank">Nancy Ide</a>, Vassar College (USA)</li>
<li><a href="http://nlp.cs.nyu.edu/people/meyers.html" target="_blank">Adam Meyers</a>, New York University (USA) </li>
<li><a href="http://pageperso.lif.univ-mrs.fr/~carlos.ramisch/" target="_blank">Carlos Ramisch</a>, Aix Marseille University (France) </li>
<li><a href="http://www.info.univ-tours.fr/~savary/" target="_blank">Agata Savary</a>, Université François Rabelais Tours (France) </li>
</ul>

<!------------------------------------->

<h1>Program Committee Chairs</h1>
<ul>
<li><a href="https://www.ihmc.us/groups/jena-hwang/" target="_blank">Jena Hwang</a>, Institute for Human and Machine Cognition (USA)</li>
<li><a href="https://www.icsi.berkeley.edu/icsi/people/miriamp" target="_blank">Miriam R L Petruck</a>, ICSI (USA)</li>
<li><a href="http://cemantix.org/" target="_blank">Sameer Pradhan</a>, cemantix.org and Vassar College, New York (USA)</li>
<li><a href="http://pageperso.lis-lab.fr/~carlos.ramisch/" target="_blank">Carlos Ramisch</a>, Aix Marseille University (France)</li>
<li><a href="http://www.info.univ-tours.fr/~savary/" target="_blank">Agata Savary</a>, Université François Rabelais Tours (France)</li>
<li><a href="http://people.cs.georgetown.edu/nschneid/" target="_blank">Nathan Schneider</a>, Georgetown University (USA)</li>
</ul>

<!------------------------------------->
<h1>Publication Chairs</h1>
<ul>
<li><a href="https://www.slm.uni-hamburg.de/germanistik/personen/andresen.html" target="_blank">Melanie Andresen</a>, Hamburg University (Germany) </li>
<li><a href="http://www.info.univ-tours.fr/~savary/" target="_blank">Agata Savary</a>, Université François Rabelais Tours (France) </li>
</ul>


<!------------------------------------->
<h1>Publicity Chairs</h1>
<ul>
<li><a href="http://nlp.cs.nyu.edu/people/meyers.html" target="_blank">Adam Meyers</a>, New York University (USA) </li>
<li><a href="http://www.info.univ-tours.fr/~savary/" target="_blank">Agata Savary</a>, Université François Rabelais Tours (France) </li>
</ul>

<!------------------------------------->

<h1>Contact</h1>
<p>For any inquiries regarding the workshop please send an email to <a href="mailto:lawmwecxg2018@gmail.com">lawmwecxg2018@gmail.com</a></p>


<!------------------------------------->
<h1>Anti-harassment policy</h1>
<p>The workshop supports the ACL <a href="https://www.aclweb.org/adminwiki/index.php?title=Anti-Harassment_Policy" target=_blanc">anti-harassment policy</a>.</p>

