<h2>Joint Workshop on Linguistic Annotation, Multiword Expressions and Constructions (LAW-MWE-CxG-2018)</h2>

<!-- <h3>Workshop proposal for <a href="http://coling2018.org/" target="_blank">COLING 2018</a> (Santa Fé, USA) or <a href="http://acl2018.org/" target="_blank">ACL 2018</a> (Melbourne, Australia)</h3> -->

<h3>Workshop at <a href="http://coling2018.org/" target="_blank">COLING 2018</a> (Santa Fe, USA), August 25-26, 2018</h3>

<p>Organized and funded by the Special Interest Group for Annotation (<a href="https://www.cs.vassar.edu/sigann/" target="_blank">SIGANN</a>), the Special Interest Group on the Lexicon (<a href="http://www.siglex.org" target="_blank">SIGLEX</a>), and the Special Interest Group on Computational Semantics (<a href="http://www.sigsem.org" target="_blank">SIGSEM</a>) of the Association for Computational Linguistics (<a href="https://www.aclweb.org/portal/" target="_blank">ACL</a>)</p>

<p><small>Last updated: July 16, 2018</small></p>

<!-- ------------------------------------------------------------------------- -->

<h1>Keynote speakers</h1>

<h1><a name="keynote-lori"></a><a href="lawmwecxg2018/slides/Levin.pdf" target="_blank">Annotation Schemes for Surface Construction Labeling</a></h1>
<p><strong><a href="http://www.cs.cmu.edu/~lsl/" target="_blank">Lori Levin</a></strong> – Language Technologies Institute, Carnegie Mellon University, Pittsburgh, PA, USA</p>

<table><tr>
  <td width="160" valign="top"><img src="images/lori-levin.jpg" width="100" /></td>
  <td valign="top">
     Lori Levin received a B.A. in linguistics from the University of Pennsylvania in 1979 and a Ph.D. in linguistics from MIT in 1986.   She is a Research Professor at the Language Technologies Institute at Carnegie Mellon University, specializing in language technologies for low-resource languages.    She is also co-Chair of the North American Computational Linguistics Olympiad.
  </td>
</tr></table>

<p><strong>Abstract</strong></p>
<p>
  In this talk I will describe the interaction of linguistics and language technologies in Surface Construction Labeling (SCL) from the perspective of corpus annotation tasks such as definiteness, modality, and causality.     Linguistically, following Construction Grammar,  SCL recognizes that meaning may be carried by morphemes, words, or arbitrary constellations of morpho-lexical elements.     SCL is like Shallow Semantic Parsing in that it does not attempt a full compositional analysis of meaning, but rather identifies only the main elements of a semantic frame, where the frames may be invoked by constructions as well as lexical items.  Computationally, SCL is different from tasks such as information extraction in that it deals only with meanings that are expressed in a conventional, grammaticalized way and does not address inferred meanings.    I review the work of Dunietz (2018) on the labeling of causal frames  including causal connectives and cause and effect arguments.    I will  describe how to design an annotation scheme for SCL, including isolating basic units of form and meaning and building a "constructicon".       I will conclude with remarks about the nature of universal categories and universal meaning representations in language technologies.   This talk describes joint work with Jaime Carbonell, Jesse Dunietz, Nathan Schneider, and Miriam Petruck.
</p>
<p>
Reference:</br>
Dunietz, Jesse (2018) <i>Annotating and Automatically Tagging Constructions of Causal Language</i>. Ph.D. thesis, Carnegie Mellon University, Pittsburgh, PA.
</p>

<!-- ------------------------------------------------------------------------- -->

<h1><a name="keynote-adam"></a><a href="lawmwecxg2018/slides/Przepiorkowski.pdf" target="_blank">From Lexical Functional Grammar to Enhanced Universal Dependencies</a></h1>
<p><strong><a href="http://zil.ipipan.waw.pl/AdamPrzepiorkowski/" target="_blank">Adam Przepiórkowski</a></strong> – Institute of Philosophy, University of Warsaw, and Institute of Computer Science, Polish Academy of Sciences, Warsaw, Poland</br >
(joint work with <a href="http://zil.ipipan.waw.pl/AgnieszkaPatejuk" target="_blank">Agnieszka Patejuk</a>	)</p>


<table><tr>
  <td width="110" valign="top"><img src="images/adam-przepiorkowski.jpg" width="80" /></td>
  <td valign="top">
<!--    <br/>
    <br/>-->
    As a computational and corpus linguist, Adam Przepiórkowski has led NLP projects resulting in the development of various tools and resources for Polish, including the National Corpus of Polish and tools for its manual and automatic annotation, and has worked on topics ranging from deep and shallow syntactic parsing to corpus search engines and valency dictionaries.  As a theoretical linguist, he has worked on case assignment (mainly within Head-driven Phrase Structure Grammar) and the morphosyntax of Polish, and, more recently, on topics including the syntax and semantics of the argument/adjunct dichotomy, distributivity and coordination (within Lexical Functional Grammar and Glue Semantics).
    </p>
  </td>
</tr></table>

<p><strong>Abstract</strong></p>
<p>
  The aim of this talk is twofold.  First, I will present the process and the result of an automatic conversion of a corpus of Polish annotated with constituent and functional LFG structures to the Universal Dependencies standard, making heavy use of enhanced dependencies.  Second, this conversion exercise will provide the basis for a more general discussion of the strengths and limitations of the current version of UD.  (This is joint work with Agnieszka Patejuk.)
</p>
<p>
Reference:</br>
Agnieszka Patejuk, Adam Przepiórkowski (2018) <a href="http://nlp.ipipan.waw.pl/Bib/pat:prz:18:book.pdf" target="_blank"><i>From Lexical Functional Grammar to Enhanced Universal Dependencies. Linguistically informed treebanks of Polish</i></a>. Institute of Computer Science Polish Academy of Sciences, Warszawa.
</p>


<!-- ------------------------------------------------------------------------- -->

<h1><a name="keynote-nathan"></a><a href="lawmwecxg2018/slides/Schneider.pdf" target="_blank">Leaving no token behind: comprehensive (and delicious) annotation of MWEs and supersenses
</a></h1>
<p><strong><a href="http://people.cs.georgetown.edu/nschneid/" target="_blank">Nathan Schneider</a></strong> – Georgetown University, USA</p>

<table><tr>
  <td width="110" valign="top"><img src="images/nathan.jpg" width="80" /></td>
  <td valign="top">
  <p>
    Nathan Schneider is an annotation schemer and computational modeler for natural language. As Assistant Professor of Linguistics and Computer Science at Georgetown University, he looks for synergies between practical language technologies and the scientific study of language. He specializes in broad-coverage semantic analysis: designing linguistic meaning representations, annotating them in corpora, and automating them with statistical natural language processing techniques. A central focus in this research is the nexus between grammar and lexicon as manifested in multiword expressions and adpositions/case markers. He has inhabited UC Berkeley (BA in Computer Science and Linguistics), Carnegie Mellon University (Ph.D. in Language Technologies), and the University of Edinburgh (postdoc). Now a Hoya and leader of NERT, he continues to play with data and algorithms for linguistic meaning.
  </p>
  </td>
</tr></table>

<p><strong>Abstract</strong></p>
<p>
I will describe an unorthodox approach to lexical semantic annotation that prioritizes corpus coverage, democratizing analysis of a wide range of expression types. I argue that a lexicon-free lexical semantics—defined in terms of units and supersense tags—is an appetizing direction for NLP, as it is robust, cost-effective, easily understood, not too language-specific, and can serve as a foundation for richer semantic structure. Linguistic delicacies from the STREUSLE and DiMSUM corpora, which have been multiword- and supersense-annotated, attest to the veritable smörgåsbord of noncanonical constructions in English, including various flavors of prepositions, MWEs, and other curiosities.
</p>

<!-- ------------------------------------------------------------------------- -->


<!--
<h3>TITLE</h3>
<p>by <strong><a href="http://" target="_blank">NAME</a></strong> – AFFILIATION</p>

<table><tr>
  <td width="160" valign="top"><img src="images/.jpg" width="120" /></td>
  <td valign="top">
    SHORT BIO
  </td>
</tr></table>

<p>
  ABSTRACT
</p>
<p><strong>References</strong></p>
<p>
  REFS
</p>
</p>
-->

<!-- ------------------------------------------------------------------------- -->


