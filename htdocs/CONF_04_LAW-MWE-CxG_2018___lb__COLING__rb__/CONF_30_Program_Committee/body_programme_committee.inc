<h2>Joint Workshop on Linguistic Annotation, Multiword Expressions and Constructions (LAW-MWE-CxG-2018)</h2>

<!-- <h3>Workshop proposal for <a href="http://coling2018.org/" target="_blank">COLING 2018</a> (Santa Fé, USA) or <a href="http://acl2018.org/" target="_blank">ACL 2018</a> (Melbourne, Australia)</h3> -->

<h3>Workshop at <a href="http://coling2018.org/" target="_blank">COLING 2018</a> (Santa Fe, USA), August 25-26, 2018</h3>

<p>Organized and funded by the Special Interest Group for Annotation (<a href="https://www.cs.vassar.edu/sigann/" target="_blank">SIGANN</a>), the Special Interest Group on the Lexicon (<a href="http://www.siglex.org" target="_blank">SIGLEX</a>), and the Special Interest Group on Computational Semantics (<a href="http://www.sigsem.org" target="_blank">SIGSEM</a>) of the Association for Computational Linguistics (<a href="https://www.aclweb.org/portal/" target="_blank">ACL</a>)</p>

<p>This joint event brings together:</p>
<ul style="margin-top:-1em;">
<li><strong>The Twelfth Linguistic Annotation Workshop (<a href="https://www.cs.vassar.edu/sigann/" target="_blank">LAW XII</a>)</strong>, and</li>
<li><strong>The 14th Workshop on Multiword Expressions (<a href="?sitesig=CONF" target="_blank">MWE</a>)</strong>.</li>
</ul>

<p><small>Last updated: July 11, 2018</small></p>

<h1>Tracks and PC-chairs</h1>
<ul>
<li><strong>LAW-MWE-CxG track</strong> - <a href="https://www.icsi.berkeley.edu/icsi/people/miriamp" target="_blank">Miriam R L Petruck</a>, ICSI (USA) and <a href="http://people.cs.georgetown.edu/nschneid/" target="_blank">Nathan Schneider</a>, Georgetown University (USA)</li>
<li><strong>LAW-specific track</strong> - <a href="http://cemantix.org/" target="_blank">Sameer Pradhan</a>, cemantix.org and Vassar College, New York (USA), <a href="https://www.ihmc.us/groups/jena-hwang/" target="_blank">Jena Hwang</a>, Institute for Human and Machine Cognition (USA)</li>
<li><strong>MWE-specific track</strong> - <a href="http://www.info.univ-tours.fr/~savary/" target="_blank">Agata Savary</a>, Université François Rabelais Tours (France), <a href="http://pageperso.lif.univ-mrs.fr/~carlos.ramisch/" target="_blank">Carlos Ramisch</a>, Aix-Marseille University (France)</li>
<li><strong>MWE shared task track</strong> - <a href="http://pageperso.lif.univ-mrs.fr/~carlos.ramisch/" target="_blank">Carlos Ramisch</a>, Aix-Marseille University (France), <a href="http://www.info.univ-tours.fr/~savary/" target="_blank">Agata Savary</a>, Université François Rabelais Tours (France)</li>
</ul>

<h1>Program Committee</h1>

<ul>
<li>Omri Abend, The Hebrew University of Jerusalem (Israel) </li>
<li>Ron Artstein, Institute for Creative Technologies, USC (USA) </li>
<li>Timothy Baldwin, University of Melbourne (Australia) </li>
<li>Libby Barak, University of Toronto (Canada) </li>
<li>Riyaz A. Bhat, University of Colorado Boulder (USA) </li>
<li>Archna Bhatia, Carnegie Mellon University (USA) </li>
<li>Ann Bies, Linguistic Data Consortium, University of Pennsylvania (USA) </li>
<li>Francis Bond, Nanyang Technological University (Singapore) </li>
<li>Claire Bonial, Air-Force Research Lab (USA) </li>
<li>Antonio Branco, University of Lisbon  (Portugal) </li>
<li>Miriam Butt, Universität Konstanz (Germany) </li>
<li>Aoife Cahill, Educational Testing Service (USA) </li>
<li>Nicoletta Calzolari, National Research Council of Italy (Italy) </li>
<li>Marie Candito, Paris Diderot University (France) </li>
<li>Özlem Çetinoğlu, University of Stuttgart (Germany) </li>
<li>Christian Chiarcos, University of Frankfurt (Germany) </li>
<li>Anastasia Christofidou, Academy of Athens (Greece) </li>
<li>Kathryn Conger, University of Colorado Boulder (USA) </li>
<li>Matthieu Constant, Université de Lorraine (France) </li>
<li>Paul Cook, University of New Brunswick (Canada) </li>
<li>Silvio Ricardo Cordeiro, Federal University of Rio Grande do Sul (Brazil) </li>
<li>Béatrice Daille, Nantes University (France) </li>
<li>Gaël Dias, University of Caen Basse-Normandie (France) </li>
<li>Stefanie Dipper, RUHR University at Bochum (Germany) </li>
<li>Ellen Dodge, ICSI, University of California at Berkeley (USA) </li>
<li>Jonathan Dunn, Illinois Institute of Technology (USA) </li>
<li>Kilian Evang, Institute for Language and Information Science (Germany) </li>
<li>Federico Fancellu, University of Edinburgh (UK) </li>
<li>Pablo Faria, University of Campinas (Brazil) </li>
<li>Joaquim Ferreira da Silva, New University of Lisbon (Portugal) </li>
<li>Aggeliki Fotopoulou, ILSP/RC "Athena" (Greece) </li>
<li>Mirjam Fried, Charles University (Czech Republic) </li>
<li>Andrew Gargett, University of Birmingham (UK) </li>
<li>Kim Gerdes, University of Paris (France) </li>
<li>Voula Giouli, Institute for Language and Speech Processing (Greece) </li>
<li>Roxana Girju, University of Illinois at Urbana-Champaign (USA) </li>
<li>Udo Hahn, Jena University (Germany) </li>
<li>Chikara Hashimoto, Yahoo!Japan (Japan) </li>
<li>Kyoko Hirose Ohara, Keio University (Japan) </li>
<li>Nancy Ide, Vassar College (USA) </li>
<li>Kyo Kageura, University of Tokyo (Japan) </li>
<li>Dimitrios Kokkinakis, University of Gothenburg (Sweden) </li>
<li>Valia Kordoni, Humboldt University Berlin (Germany) </li>
<li>Ioannis Korkontzelos, Edge Hill University (UK) </li>
<li>Brigitte Krenn, Austrian Research Institute for Artificial Intelligence (Austria) </li>
<li>Cvetana Krstev, University of Belgrade (Serbia) </li>
<li>Tita Kyriakopoulou, University Paris-Est Marne-la-Vallee (France) </li>
<li>Vicky Lai, University of Arizona (USA) </li>
<li>Eric Laporte, University Paris-Est Marne-la-Vallee (France) </li>
<li>John S. Y. Lee, City University of Hong Kong (Hong Kong) </li>
<li>Els Lefever, Ghent University (Belgium) </li>
<li>Lori Levin, Carnegie Mellon University (USA) </li>
<li>Ben Lyngfelt, University of Gothenburg (Sweden) </li>
<li>Stella Markantonatou, Institute for Language and Speech Processing (Greece) </li>
<li>Hector Martínez Alonso, INRIA (France) </li>
<li>Amália Mendes, University of Lisbon (Portugal) </li>
<li>Adam Meyers, New York University (USA) </li>
<li>Laura A. Michaelis, University of Colorado Boulder (USA) </li>
<li>Johanna Monti, "L'Orientale" University of Naples (Italy) </li>
<li>Éva Mújdricza-Maydt, University of Heidelberg (Germany) </li>
<li>Stefan Müller, Humboldt University (Germany)</li>
<li>Preslav Nakov, Qatar Computing Research Institute, HBKU (Qatar) </li>
<li>Anna Nedoluzhko, Charles University (Czech Republic) </li>
<li>Kiki Nikiforidou, School of Philosophy (Greece) </li>
<li>Joakim Nivre, Uppsala University (Sweden) </li>
<li>Michael Oakes, University of Wolverhampton (UK) </li>
<li>Jan Odijk, University of Utrecht (The Netherlands) </li>
<li>Kemal Oflazer, Carnegie Mellon University (USA) </li>
<li>Gertjan van Noord, University of Groningen (The Netherlands) </li>
<li>Petya Osenova, Bulgarian Academy of Sciences (Bulgaria) </li>
<li>Simon Ostermann, Saarland University (Germany) </li>
<li>Lilja Øvrelid, University of Oslo (Norway) </li>
<li>Alexis Palmer, University of North Texas (USA) </li>
<li>Haris Papageorgiou, Institute for Language and Speech Processing (Greece) </li>
<li>Antonio Pareja-Lora, Universidad Complutense de Madrid (UCM), DMEG (UdG) and ATLAS (UNED)</li>
<li>Yannick Parmentier, Université d'Orléans (France) </li>
<li>Carla Parra Escartín, Dublin City University (Ireland) </li>
<li>Agnieszka Patejuk, Institute of Computer Science, Polish Academy of Sciences (Poland) </li>
<li>Pavel Pecina, Charles University (Czech Republic) </li>
<li>Scott Piao, Lancaster University (UK) </li>
<li>Thierry Poibeau, CNRS and École Normale Supérieure (France) </li>
<li>James Pustejovsky, Brandeis University (USA) </li>
<li>Ines Rehbein, University of Heidelberg (Germany) </li>
<li>Arndt Riester, University of Stuttgart (Germany) </li>
<li>Josef Ruppenhofer,  Heidelberg University (Germany) </li>
<li>Manfred Sailer, Goethe-Universität Frankfurt am Main (Germany) </li>
<li>Sabine Schulte im Walde, University of Stuttgart (Germany) </li>
<li>Djamé Seddah, Paris-Sorbonne University (France) </li>
<li>Michael Spranger, Sony Labs (Japan) </li>
<li>Manfred Stede, University of Potsdam (Germany) </li>
<li>Sara Stymne, Uppsala University (Sweden) </li>
<li>Stan Szpakowicz, University of Ottawa (Canada) </li>
<li>Tiago Torrent, Federal University of Juiz de Fora (Brazil) </li>
<li>Beata Trawinski, Institut für Deutsche Sprache Mannheim (Germany) </li>
<li>Yuancheng Tu, Microsoft (USA) </li>
<li>Ruben Urizar, University of the Basque Country (Spain) </li>
<li>Aline Villavicencio, Federal University of Rio Grande do Sul (Brazil) </li>
<li>Veronika Vincze, Hungarian Academy of Sciences (Hungary) </li>
<li>Michael Wiegand, Saarland University (Germany) </li>
<li>Susan Windisch Brown, University of Colorado Boulder (USA) </li>
<li>Shuly Wintner, University of Haifa (Israel) </li>
<li>Andreas Witt, Institut fur Deutsche Sprache (Germany) </li>
<li>Amir Zeldes, Georgetown University (USA) </li>
<li>Heike Zinsmeister, University of Hamburg (Germany) </li>
</ul>


<h1>Workshop Organizers</h1>
<ul>
<li><a href="https://www.cs.vassar.edu/~ide/" target="_blank">Nancy Ide</a>, Vassar College (USA)</li>
<li><a href="http://nlp.cs.nyu.edu/people/meyers.html" target="_blank">Adam Meyers</a>, New York University (USA) </li>
<li><a href="http://pageperso.lif.univ-mrs.fr/~carlos.ramisch/" target="_blank">Carlos Ramisch</a>, Aix Marseille University (France) </li>
<li><a href="http://www.info.univ-tours.fr/~savary/" target="_blank">Agata Savary</a>, Université François Rabelais Tours (France) </li>
</ul>


<!--
<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwe2017workshop at gmail.com">mwe2017workshop at gmail.com</a></p>
-->
