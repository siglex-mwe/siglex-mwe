<h2>Shared task on automatic identification of verbal multiword expressions - edition 1.1</h2>

<h3>Organized as part of the <a href="http://multiword.sourceforge.net/lawmwecxg2018" target="_blank">LAW-MWE-CxG 2018</a> workshop co-located with <a href="http://coling2018.org/" target="_blanc">COLING 2018</a> (Santa Fe, USA), August 25-26, 2018</h3>

<p><small>Last updated: August 16, 2018</small></p>

<ul>
<li><font color="red"><strong>NEW!</strong></font>&nbsp;&nbsp;The PARSEME corpus edition 1.1 (used in this shared task) is now available via the <a href="http://hdl.handle.net/11372/LRT-2842" target="_blank">CLARIN/LINDAT</a> infrastructure.</li>
<li><font color="red"><strong>NEW!</strong></font>&nbsp;&nbsp;<a href="sharedtaskresults2018">System results</a> are now available. Congratulations to all participants! (May 11)</li>
<li><font color="red"><strong>NEW!</strong></font>&nbsp;&nbsp;The <a href="https://gitlab.com/parseme/sharedtask-data/tree/master/1.1">gold test data</a> for all <strong>20 languages</strong> is now available (May 11)</li>
<li>Good news: <strong>DEADLINE EXTENDED UNTIL MAY 08</strong> for the submission of system results!  (May 3)</li>
<li>The <a href="https://gitlab.com/parseme/sharedtask-data/tree/master/1.1">blind test data</a> for all <strong>20 languages</strong> is now available (April 30)</li>
<li>We have released new version of the <a href="?sitesig=CONF&page=CONF_04_LAW-MWE-CxG_2018___lb__COLING__rb__&subpage=CONF_50_Evaluation_metrics#eval-script">evaluation script</a>
 and a new script to calculate the <a href="https://gitlab.com/parseme/sharedtask-data/tree/master/1.1/bin/average_of_evaluations.py">macro-averages</a> across languages. (April 23)</li>
<li>An extra corpus for Arabic is now available through LDC, see the <a href="https://gitlab.com/parseme/sharedtask-data/tree/master/1.1/AR/README.md">instructions</a> (April 22)</li>
<li>The full <a href="https://gitlab.com/parseme/sharedtask-data/tree/master/1.1">training/development data</a> for all <strong>19 languages</strong> (including Basque and Hebrew) is now available (April 12)</li>
<li>The <a href="https://gitlab.com/parseme/sharedtask-data/tree/master/1.1">training/development data</a> for <strong>17 languages</strong> is now available (April 5).</li>
<li><a href="https://gitlab.com/parseme/sharedtask-data/blob/master/1.1/trial">Trial data</a>, the definition of the <a href="?sitesig=CONF&page=CONF_04_LAW-MWE-CxG_2018___lb__COLING__rb__&subpage=CONF_45_Format_specification">.cupt</a> format, the description of <a href="?sitesig=CONF&page=CONF_04_LAW-MWE-CxG_2018___lb__COLING__rb__&subpage=CONF_50_Evaluation_metrics">evaluation measures</a> and the <a href="?sitesig=CONF&page=CONF_04_LAW-MWE-CxG_2018___lb__COLING__rb__&subpage=CONF_50_Evaluation_metrics#eval-script">evaluation script</a> are now available.</li>
</ul>

<h1>Description</h1>

<p>The second edition of the <a href="http://www.parseme.eu" target="_blank">PARSEME</a> shared task on automatic identification of verbal multiword expressions (VMWEs) aims at identifying verbal MWEs in running texts.  Verbal MWEs include, among others, idioms (<em>to let the cat out of the bag</em>), light verb constructions (<em>to make a decision</em>), verb-particle constructions (<em>to give up</em>), multi-verb constructions (<em>to make do</em>) and inherently reflexive verbs (<em>se suicider</em> 'to suicide' in French).  Their identification is a well-known challenge for NLP applications, due to their complex characteristics including discontinuity, non-compositionality, heterogeneity and syntactic variability.</p>

<p>The shared task is highly multilingual: <a href="http://www.parseme.eu" target="_blank">PARSEME</a> members have elaborated <a href="http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1/" target="_blank">annotation guidelines</a> based on annotation experiments in about 20 languages from several language families.  These guidelines take both universal and language-specific phenomena into account.  We hope that this will boost the development of language-independent and cross-lingual VMWE identification systems.</p>

<!----------------------->
<h1><a id="participation"></a>Participation policies</h1>

<p style="color:red">The evaluation phase of the shared task is now over, but you can find useful information about the shared task on this page.</p>

<p>Participation is open and free worldwide. We ask potential participant teams to register using the expression of interest <a href="https://docs.google.com/forms/d/e/1FAIpQLSd6L8IntkNKXbMp8QVLLvCYzzhoH-_8ovSW0DL3BtYGNnsFhA/viewform?c=0&w=1" target="_blank">form</a>. Task updates and questions will be posted to our public <a href="http://groups.google.com/group/verbalmwe" target="_blank">mailing list</a>. More details on the annotated corpora can be found in a <a href="https://typo.uni-konstanz.de/parseme/index.php/2-general/202-parseme-shared-task-on-automatic-identification-of-verbal-mwes-edition-1-1" target="_blank">dedicated PARSEME page</a>. See also the <a href="http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1" target="_blank">annotation guidelines</a> used in manual annotation of the training/development and test sets. See also the description of of <a href="?sitesig=CONF&page=CONF_04_LAW-MWE-CxG_2018___lb__COLING__rb__&subpage=CONF_45_Format_specification">evaluation measures</a> and the <a href="?sitesig=CONF&page=CONF_04_LAW-MWE-CxG_2018___lb__COLING__rb__&subpage=CONF_45_Format_specification#eval-script">evaluation script</a>.</p>

<p>It should be noted that a large international community has been gathered (via the <a href="http://www.parseme.eu" target="_blank">PARSEME</a> network) around the effort of putting forward universal guidelines and performing corpus annotations. Our policy was to <b>allow the same national teams, which provided annotated corpora, to also submit VMWE identification systems</b> for the shared task. While this policy is non-standard and introduces a bias to system evaluation, we follow it for several reasons:</p>

<ul>
<li>For many languages there are only very few NLP teams, so adopting an exclusive approach (either you annotate or you present a system but not both) would actually exclude the whole language from participation.</li>
<li> We are interested more in cross-language discussions than in a real competition.</li>
<li>We admit that we can trust the teams to respect some <b>best practices</b>, including those:
	<ul>
	<li>The test data are never used for training/development, even if system authors have access to them in advance.</li>
	<li>If any resources were used to annotate the corpus, the same resources should not be used by the system (in the <a href="#tracks">open track</a>).</li>
	<li>If system authors notice other sources of bias between their annotating activity and system evaluation, they should describe them in the submitted papers (if any).</li>
	</ul>
</ul>

<!----------------------->
<h1><a id="publication"></a>Submission of results and system description paper</h1>

<!--
<p>Shared task participants will be invited to submit a system description paper to a special track of the Joint Workshop on Linguistic Annotation, Multiword Expressions and Constructions (<a href="http://multiword.sourceforge.net/lawmwecxg2018" target="_blank">LAW-MWE-CxG-2018</a>) at <a href="http://coling2018.org/" target="_blanc">COLING 2018</a>, to be held on August 25-26, 2018, in Santa Fe, New Mexico, USA. 
Submitted system description papers must follow the workshop submission instructions and will go through double-blind peer reviewing by other participants and selected LAW-MWE-CxG-2018 program committee members.  Their acceptance depends on the quality of the paper rather than on the results obtained in the shared task.  Authors of the accepted papers will present their work as posters/demos in a dedicated session of the workshop, collocated with COLING 2018. The submission of a system description paper is not mandatory.</p>
-->

<p>Shared task participants are invited to submit input of two kinds to the SHARED TASK TRACK of the Joint Workshop on Linguistic Annotation, Multiword Expressions and Constructions (<a href="http://multiword.sourceforge.net/lawmwecxg2018/" target="_blank">LAW-MWE-CxG 2018</a>):</p>

<ol>
<li><strong>System results</strong> (by <del>May 4</del> May 8) obtained on the blind data (released on 30 April). The results for all languages should be submitted in a single <tt>.zip</tt> archive containing a single folder per language, named according to the <a href="https://en.wikipedia.org/wiki/ISO_639-1" target="_blank">ISO 639-1</a> code (e.g. FR/ for French, SL/ for Slovene, etc.). Each output file must be named <tt></tt>test.system.cupt</tt> and conform to the <a href="http://multiword.sourceforge.net/cupt-format" target="_blank">cupt</a> format. The format of each file should be checked before submission by the <a href="https://gitlab.com/parseme/sharedtask-data/blob/master/1.1/bin/validate_cupt.py" target="_blank">validation script</a> as follows:
	<ul style="list-style: none;">
        <li><span style="text-indent: 50px; font-family: Courier New;">./validate_cupt.py --input test.system.cupt</span></li>
	</ul>		
If one system participates both in the open and in the closed track, two independent submissions are required. The number of submissions per system is limited to 2 per track, i.e. a team can have at most 4 submissions (with at most one result per language in each submission). Please, use an <em>anonymous nickname</em> for your system i.e. one that reveals neither the authors nor their affiliation.</li>

<li>A <strong>system description paper</strong> (by May 25). These papers must follow the <a href="?sitesig=CONF&page=CONF_04_LAW-MWE-CxG_2018___lb__COLING__rb__#modalities">LAW-MWE-CxG workshop submission instructions</a> and will go through double-blind peer reviewing by other participants and selected LAW-MWE-CxG 2018 Program Committee members.  Their acceptance depends on the quality of the paper rather than on the results obtained in the shared task.  Authors of the accepted papers will present their work as posters/demos in a dedicated session of the workshop. <b>The submission of a system description paper is not mandatory.</b></li>
</ol>


<p>The submission of system results and of a system description paper should be made via the dedicated START space:</p>
<p><a href="https://www.softconf.com/coling2018/ws-LAW-MWE-CxG-2018" target="_blank">https://www.softconf.com/coling2018/ws-LAW-MWE-CxG-2018</a></p>	
	


<!----------------------->
<h1><a id="data"></a>Provided data</h1>

<p>The PARSEME corpus edition 1.1 (used in this shared task) is available via the <a href="http://hdl.handle.net/11372/LRT-2842" target="_blank">CLARIN/LINDAT</a> infrastructure.</p>

<p>The shared task covers 20 languages: Arabic (AR), Bulgarian (BG), German (DE), Greek (EL), English (EN), Spanish (ES), Basque (EU), Farsi (FA), French (FR), Hindi (HI), Hebrew (HE), Croatian (HR), Hungarian (HU), Italian (IT), Lithuanian (LT), Polish (PL), Brazilian Portuguese (PT), Romanian (RO), Slovenian (SL), Turkish (TR).</p>

<p>For each language, we provide corpora (in the <a href="?sitesig=CONF&page=CONF_04_LAW-MWE-CxG_2018___lb__COLING__rb__&subpage=CONF_45_Format_specification">.cupt</a> format) in which VMWEs are annotated according to universal <a href="http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1/" target="_blank">guidelines</a>:</p>

<ul>
<li>Manually annotated <strong>training corpora</strong> made available to the participants in advance, in order to allow them to train their systems.</li>
<li>Manually annotated <strong>development corpora</strong> also made available in advance so as to tune/optimize the systems' parameters.</li>
<li>Raw (unannotated) <strong>test corpora</strong> to be used as input to the systems during the evaluation phase. The VMWE annotations in this corpus will be kept secret.</li>
</ul>


<p>For most languages, morphosyntactic data  (parts of speech, lemmas, morphological features and/or syntactic dependencies) are also be provided. Depending on the language, the information comes from treebanks (e.g., <a href="http://universaldependencies.org/" target="_blank">Universal Dependencies</a>) or from automatic parsers trained on treebanks (e.g., <a href="http://ufal.mff.cuni.cz/udpipe" target="_blank">UDPipe</a>).</p>

<p>Our <a target="_blank" href="https://gitlab.com/parseme/sharedtask-data/tree/master/1.1">public GitLab repository</a> contains:
<ul>
<li><strong>trial data</strong> in <a href="?sitesig=CONF&page=CONF_04_LAW-MWE-CxG_2018___lb__COLING__rb__&subpage=CONF_45_Format_specification">.cupt</a> format in English</li>
<li><strong>training/development data</strong> in <a href="?sitesig=CONF&page=CONF_04_LAW-MWE-CxG_2018___lb__COLING__rb__&subpage=CONF_45_Format_specification">.cupt</a> format in all participating languages</li>
<li>the <strong>evaluation script</strong> to calculate per-language <a href="?sitesig=CONF&page=CONF_04_LAW-MWE-CxG_2018___lb__COLING__rb__&subpage=CONF_50_Evaluation_metrics">evaluation scores</a> (its extension to marco-average scores for all languages will be published shortly)</strong>
</ul>


<p>The table below summarizes the sizes of the <b>training/development/test corpora</b> per language:</p>

<table style="text-align:right">
<tbody>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="font-weight:bold">Lang-split</td><td style="font-weight:bold">Sentences</td><td style="font-weight:bold">Tokens</td><td style="font-weight:bold">Avg. length</td><td style="font-weight:bold">VMWE</td><td style="font-weight:bold">VID</td><td style="font-weight:bold">IRV</td><td style="font-weight:bold">LVC.full</td><td style="font-weight:bold">LVC.cause</td><td style="font-weight:bold">VPC.full</td><td style="font-weight:bold">VPC.semi</td><td style="font-weight:bold">IAV</td><td style="font-weight:bold">MVC</td><td style="font-weight:bold">LS.ICV</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">AR-train  </td><td>2370</td><td>231030</td><td>97.4</td><td>3219</td><td>1272</td><td>17</td><td>940</td><td>0</td><td>957</td><td>0</td><td>0</td><td>33</td><td>0</td></tr>
<tr><td style="text-align:left">AR-dev  </td><td>387</td><td>16252</td><td>41.9</td><td>500</td><td>17</td><td>0</td><td>419</td><td>0</td><td>64</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">AR-test  </td><td>380</td><td>17962</td><td>47.2</td><td>500</td><td>31</td><td>0</td><td>410</td><td>0</td><td>59</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">AR-Total</td><td>3137</td><td>265244</td><td>84.5</td><td>4219</td><td>1320</td><td>17</td><td>1769</td><td></td><td>1080</td><td></td><td></td><td>33</td><td></td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">BG-train  </td><td>17813</td><td>399173</td><td>22.4</td><td>5364</td><td>1005</td><td>2729</td><td>1421</td><td>135</td><td>0</td><td>0</td><td>74</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">BG-dev  </td><td>1954</td><td>42020</td><td>21.5</td><td>670</td><td>173</td><td>240</td><td>214</td><td>35</td><td>0</td><td>0</td><td>8</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">BG-test  </td><td>1832</td><td>39220</td><td>21.4</td><td>670</td><td>82</td><td>254</td><td>274</td><td>52</td><td>0</td><td>0</td><td>8</td><td>0</td><td>0</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">BG-Total</td><td>21599</td><td>480413</td><td>22.2</td><td>6704</td><td>1260</td><td>3223</td><td>1909</td><td>222</td><td>0</td><td>0</td><td>90</td><td>0</td><td>0</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">DE-train  </td><td>6734</td><td>130588</td><td>19.3</td><td>2820</td><td>977</td><td>220</td><td>218</td><td>28</td><td>1264</td><td>113</td><td>0</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">DE-dev  </td><td>1184</td><td>22146</td><td>18.7</td><td>503</td><td>181</td><td>48</td><td>34</td><td>2</td><td>221</td><td>17</td><td>0</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">DE-test  </td><td>1078</td><td>20559</td><td>19</td><td>500</td><td>183</td><td>40</td><td>42</td><td>2</td><td>210</td><td>23</td><td>0</td><td>0</td><td>0</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">DE-Total</td><td>8996</td><td>173293</td><td>19.2</td><td>3823</td><td>1341</td><td>308</td><td>294</td><td>32</td><td>1695</td><td>153</td><td>0</td><td>0</td><td>0</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">EL-train  </td><td>4427</td><td>122458</td><td>27.6</td><td>1404</td><td>395</td><td>0</td><td>938</td><td>44</td><td>19</td><td>0</td><td>0</td><td>8</td><td>0</td></tr>
<tr><td style="text-align:left">EL-dev  </td><td>2562</td><td>66431</td><td>25.9</td><td>500</td><td>81</td><td>0</td><td>376</td><td>34</td><td>8</td><td>0</td><td>0</td><td>1</td><td>0</td></tr>
<tr><td style="text-align:left">EL-test  </td><td>1261</td><td>35873</td><td>28.4</td><td>501</td><td>169</td><td>0</td><td>308</td><td>11</td><td>11</td><td>0</td><td>0</td><td>2</td><td>0</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">EL-Total</td><td>8250</td><td>224762</td><td>27.2</td><td>2405</td><td>645</td><td>0</td><td>1622</td><td>89</td><td>38</td><td>0</td><td>0</td><td>11</td><td>0</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">EN-train  </td><td>3471</td><td>53201</td><td>15.3</td><td>331</td><td>60</td><td>0</td><td>78</td><td>7</td><td>151</td><td>19</td><td>16</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">EN-test  </td><td>3965</td><td>71002</td><td>17.9</td><td>501</td><td>79</td><td>0</td><td>166</td><td>36</td><td>146</td><td>26</td><td>44</td><td>4</td><td>0</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">EN-Total</td><td>7436</td><td>124203</td><td>16.7</td><td>832</td><td>139</td><td>0</td><td>244</td><td>43</td><td>297</td><td>45</td><td>60</td><td>4</td><td>0</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">ES-train  </td><td>2771</td><td>96521</td><td>34.8</td><td>1739</td><td>167</td><td>479</td><td>223</td><td>36</td><td>0</td><td>0</td><td>360</td><td>474</td><td>0</td></tr>
<tr><td style="text-align:left">ES-dev  </td><td>698</td><td>26220</td><td>37.5</td><td>500</td><td>65</td><td>114</td><td>84</td><td>17</td><td>0</td><td>0</td><td>87</td><td>133</td><td>0</td></tr>
<tr><td style="text-align:left">ES-test  </td><td>2046</td><td>59623</td><td>29.1</td><td>500</td><td>95</td><td>121</td><td>85</td><td>28</td><td>1</td><td>0</td><td>64</td><td>106</td><td>0</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">ES-Total</td><td>5515</td><td>182364</td><td>33</td><td>2739</td><td>327</td><td>714</td><td>392</td><td>81</td><td>1</td><td>0</td><td>511</td><td>713</td><td>0</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">EU-train  </td><td>8254</td><td>117165</td><td>14.1</td><td>2823</td><td>597</td><td>0</td><td>2074</td><td>152</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">EU-dev  </td><td>1500</td><td>21604</td><td>14.4</td><td>500</td><td>104</td><td>0</td><td>382</td><td>14</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">EU-test  </td><td>1404</td><td>19038</td><td>13.5</td><td>500</td><td>73</td><td>0</td><td>410</td><td>17</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">EU-Total</td><td>11158</td><td>157807</td><td>14.1</td><td>3823</td><td>774</td><td>0</td><td>2866</td><td>183</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">FA-train  </td><td>2784</td><td>45153</td><td>16.2</td><td>2451</td><td>17</td><td>1</td><td>2433</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">FA-dev  </td><td>474</td><td>8923</td><td>18.8</td><td>501</td><td>0</td><td>0</td><td>501</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">FA-test  </td><td>359</td><td>7492</td><td>20.8</td><td>501</td><td>0</td><td>0</td><td>501</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">FA-Total</td><td>3617</td><td>61568</td><td>17</td><td>3453</td><td>17</td><td>1</td><td>3435</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">FR-train  </td><td>17225</td><td>432389</td><td>25.1</td><td>4550</td><td>1746</td><td>1247</td><td>1470</td><td>68</td><td>0</td><td>0</td><td>0</td><td>19</td><td>0</td></tr>
<tr><td style="text-align:left">FR-dev  </td><td>2236</td><td>56254</td><td>25.1</td><td>629</td><td>207</td><td>154</td><td>252</td><td>15</td><td>0</td><td>0</td><td>0</td><td>1</td><td>0</td></tr>
<tr><td style="text-align:left">FR-test  </td><td>1606</td><td>39489</td><td>24.5</td><td>498</td><td>212</td><td>108</td><td>160</td><td>14</td><td>0</td><td>0</td><td>0</td><td>4</td><td>0</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">FR-Total</td><td>21067</td><td>528132</td><td>25</td><td>5677</td><td>2165</td><td>1509</td><td>1882</td><td>97</td><td>0</td><td>0</td><td>0</td><td>24</td><td>0</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">HE-train  </td><td>12106</td><td>237472</td><td>19.6</td><td>1236</td><td>519</td><td>0</td><td>545</td><td>113</td><td>59</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">HE-dev  </td><td>3385</td><td>65843</td><td>19.4</td><td>501</td><td>258</td><td>0</td><td>148</td><td>61</td><td>34</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">HE-test  </td><td>3209</td><td>65698</td><td>20.4</td><td>502</td><td>182</td><td>0</td><td>211</td><td>49</td><td>60</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">HE-Total</td><td>18700</td><td>369013</td><td>19.7</td><td>2239</td><td>959</td><td>0</td><td>904</td><td>223</td><td>153</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">HI-train  </td><td>856</td><td>17850</td><td>20.8</td><td>534</td><td>23</td><td>0</td><td>321</td><td>14</td><td>0</td><td>0</td><td>0</td><td>176</td><td>0</td></tr>
<tr><td style="text-align:left">HI-test  </td><td>828</td><td>17580</td><td>21.2</td><td>500</td><td>38</td><td>0</td><td>320</td><td>12</td><td>0</td><td>0</td><td>0</td><td>130</td><td>0</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">HI-Total</td><td>1684</td><td>35430</td><td>21</td><td>1034</td><td>61</td><td>0</td><td>641</td><td>26</td><td>0</td><td>0</td><td>0</td><td>306</td><td>0</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">HR-train  </td><td>2295</td><td>53486</td><td>23.3</td><td>1450</td><td>113</td><td>468</td><td>303</td><td>45</td><td>0</td><td>0</td><td>521</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">HR-dev  </td><td>834</td><td>19621</td><td>23.5</td><td>500</td><td>34</td><td>139</td><td>143</td><td>26</td><td>1</td><td>0</td><td>157</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">HR-test  </td><td>708</td><td>16429</td><td>23.2</td><td>501</td><td>33</td><td>118</td><td>131</td><td>31</td><td>0</td><td>0</td><td>188</td><td>0</td><td>0</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">HR-Total</td><td>3837</td><td>89536</td><td>23.3</td><td>2451</td><td>180</td><td>725</td><td>577</td><td>102</td><td>1</td><td>0</td><td>866</td><td>0</td><td>0</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">HU-train  </td><td>4803</td><td>120013</td><td>24.9</td><td>6205</td><td>84</td><td>0</td><td>892</td><td>363</td><td>4131</td><td>735</td><td>0</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">HU-dev  </td><td>601</td><td>15564</td><td>25.8</td><td>779</td><td>10</td><td>0</td><td>85</td><td>10</td><td>539</td><td>135</td><td>0</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">HU-test  </td><td>755</td><td>20759</td><td>27.4</td><td>776</td><td>10</td><td>0</td><td>166</td><td>28</td><td>486</td><td>86</td><td>0</td><td>0</td><td>0</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">HU-Total</td><td>6159</td><td>156336</td><td>25.3</td><td>7760</td><td>104</td><td>0</td><td>1143</td><td>401</td><td>5156</td><td>956</td><td>0</td><td>0</td><td>0</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">IT-train  </td><td>13555</td><td>360883</td><td>26.6</td><td>3254</td><td>1098</td><td>942</td><td>544</td><td>147</td><td>66</td><td>0</td><td>414</td><td>23</td><td>20</td></tr>
<tr><td style="text-align:left">IT-dev  </td><td>917</td><td>32613</td><td>35.5</td><td>500</td><td>197</td><td>106</td><td>100</td><td>19</td><td>17</td><td>2</td><td>44</td><td>6</td><td>9</td></tr>
<tr><td style="text-align:left">IT-test  </td><td>1256</td><td>37293</td><td>29.6</td><td>503</td><td>201</td><td>96</td><td>104</td><td>25</td><td>23</td><td>0</td><td>41</td><td>5</td><td>8</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">IT-Total</td><td>15728</td><td>430789</td><td>27.3</td><td>4257</td><td>1496</td><td>1144</td><td>748</td><td>191</td><td>106</td><td>2</td><td>499</td><td>34</td><td>37</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">LT-train  </td><td>4895</td><td>90110</td><td>18.4</td><td>312</td><td>106</td><td>0</td><td>195</td><td>11</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">LT-test  </td><td>6209</td><td>118402</td><td>19</td><td>500</td><td>202</td><td>0</td><td>284</td><td>14</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">LT-Total</td><td>11104</td><td>208512</td><td>18.7</td><td>812</td><td>308</td><td>0</td><td>479</td><td>25</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">PL-train  </td><td>13058</td><td>220465</td><td>16.8</td><td>4122</td><td>373</td><td>1785</td><td>1531</td><td>180</td><td>0</td><td>0</td><td>253</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">PL-dev  </td><td>1763</td><td>26030</td><td>14.7</td><td>515</td><td>57</td><td>245</td><td>153</td><td>33</td><td>0</td><td>0</td><td>27</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">PL-test  </td><td>1300</td><td>27823</td><td>21.4</td><td>515</td><td>73</td><td>249</td><td>149</td><td>15</td><td>0</td><td>0</td><td>29</td><td>0</td><td>0</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">PL-Total</td><td>16121</td><td>274318</td><td>17</td><td>5152</td><td>503</td><td>2279</td><td>1833</td><td>228</td><td>0</td><td>0</td><td>309</td><td>0</td><td>0</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">PT-train  </td><td>22017</td><td>506773</td><td>23</td><td>4430</td><td>882</td><td>689</td><td>2775</td><td>84</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">PT-dev  </td><td>3117</td><td>68581</td><td>22</td><td>553</td><td>130</td><td>83</td><td>337</td><td>3</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">PT-test  </td><td>2770</td><td>62648</td><td>22.6</td><td>553</td><td>118</td><td>91</td><td>337</td><td>7</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">PT-Total</td><td>27904</td><td>638002</td><td>22.8</td><td>5536</td><td>1130</td><td>863</td><td>3449</td><td>94</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">RO-train  </td><td>42704</td><td>781968</td><td>18.3</td><td>4713</td><td>1269</td><td>3048</td><td>250</td><td>146</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">RO-dev  </td><td>7065</td><td>118658</td><td>16.7</td><td>589</td><td>169</td><td>373</td><td>29</td><td>18</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">RO-test  </td><td>6934</td><td>114997</td><td>16.5</td><td>589</td><td>173</td><td>363</td><td>34</td><td>19</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">RO-Total</td><td>56703</td><td>1015623</td><td>17.9</td><td>5891</td><td>1611</td><td>3784</td><td>313</td><td>183</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">SL-train  </td><td>9567</td><td>201853</td><td>21</td><td>2378</td><td>500</td><td>1162</td><td>176</td><td>40</td><td>0</td><td>0</td><td>500</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">SL-dev  </td><td>1950</td><td>38146</td><td>19.5</td><td>500</td><td>121</td><td>224</td><td>30</td><td>12</td><td>0</td><td>0</td><td>113</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">SL-test  </td><td>1994</td><td>40523</td><td>20.3</td><td>500</td><td>106</td><td>245</td><td>35</td><td>13</td><td>0</td><td>0</td><td>101</td><td>0</td><td>0</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">SL-Total</td><td>13511</td><td>280522</td><td>20.7</td><td>3378</td><td>727</td><td>1631</td><td>241</td><td>65</td><td>0</td><td>0</td><td>714</td><td>0</td><td>0</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">TR-train  </td><td>16715</td><td>334880</td><td>20</td><td>6125</td><td>3172</td><td>0</td><td>2952</td><td>0</td><td>0</td><td>0</td><td>0</td><td>1</td><td>0</td></tr>
<tr><td style="text-align:left">TR-dev  </td><td>1320</td><td>27196</td><td>20.6</td><td>510</td><td>285</td><td>0</td><td>225</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
<tr><td style="text-align:left">TR-test  </td><td>577</td><td>14388</td><td>24.9</td><td>506</td><td>233</td><td>0</td><td>272</td><td>0</td><td>0</td><td>0</td><td>0</td><td>1</td><td>0</td></tr>
<tr style="height:1px; background-color: #d5d5d5;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">TR-Total</td><td>18612</td><td>376464</td><td>20.2</td><td>7141</td><td>3690</td><td>0</td><td>3449</td><td>0</td><td>0</td><td>0</td><td>0</td><td>2</td><td>0</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
<tr><td style="text-align:left">Total    </td><td>280838</td><td>6072331</td><td>21.6</td><td>79326</td><td>18757</td><td>16198</td><td>28190</td><td>2285</td><td>8527</td><td>1156</td><td>3049</td><td>1127</td><td>37</td></tr>
<tr style="height:2px; background-color: #222222;"><td colspan="14"></td></tr>
</tbody>
</table>


<!--
<p>The table below summarizes the sizes of the <b>test corpora</b> per language:</p>
<table style="text-align:right">
<tbody>
[ADD TEST DATA STATS HERE]
</tbody>
</table>
-->
<p>The training, development and test data are available in our <a target="_blank" href="https://gitlab.com/parseme/sharedtask-data/tree/master/1.1">public GitLab repository</a>. Follow the <a href="https://gitlab.com/parseme/sharedtask-data/tree/master/1.1" target="_blanc">Repository</a> link to access folders for individual languages.</p>

<p>All VMWE annotations (except Arabic) are available under <strong>Creative Commons</strong> licenses (see README.md files for details).</p>

<p>The Arabic corpus does not have an open licence. Participants are required to fill in an <a href="" target="_blank">agreement</a> and obtain the corpus through LDC.
Given that this is a late addition, Arabic will be considered as optional this year. This means that we will publish generic and per-category rankings for teams who address Arabic, but it will not be included in the macro-average rankings across languages.</p>

<p><strong>A note for shared task participants</strong>: We cannot ensure that the test data of the current edition of the shared task do not overlap with the data published in <a href="http://multiword.sourceforge.net/sharedtask2017">edition 1.0</a>. Therefore, we kindly ask participants <strong>not to use the .parsemetsv files from edition 1.0</strong> for any language during the training or testing phase.</p>

<!----------------------->
<h1><a id="tracks"></a>Tracks</h1>

<p>System results can be submitted in two tracks:</p>
<ul>
<li><strong>Closed track</strong>: Systems using only the provided training/development data in the <em>cupt</em> files - VMWE annotations + morpho-syntactic data (if any) - to learn VMWE identification models and/or rules.</li>
<li><strong>Open track</strong>: Systems using or not the provided training/development data, plus any additional resources deemed useful (MWE lexicons, symbolic grammars, wordnets, raw corpora, word embeddings, language models trained on external data, etc.). This track includes notably purely symbolic and rule-based systems.</li>
</ul>

<p>Teams submitting systems in the open track will be requested to describe and provide references to all resources used at submission time. Teams are encouraged to favor freely available resources for better reproducibility of their results.</p>


<!----------------------->

<!---
<h1>Results</h1>

<p><font color="red"><strong>NEW!</strong></font></strong> The <a href="PHITE.php?sitesig=CONF&page=CONF_05_MWE_2017___lb__EACL__rb__&subpage=CONF_50_Shared_Task_Results" target="_blank">results page</a> now contains system evaluation results per language (disregarding VMWE categories).</p>

-->

<!----------------------->
<h1>Important dates</h1>

<p>All deadlines are at 23:59 UTC-12 (anywhere in the world).</p>

<ul>
<li><del>March 21, 2018: shared task trial data and evaluation script released</del></li>
<li><del>April 4, 2018: shared task training and development data released</del></li>
<li><del>April 30, 2018: shared task blind test data released</del></li>
<li><del>May 4, 2018</del> May 8, 2018: submission of system results (EXTENDED!)</del> </li>
<li><del>May 11, 2018: announcement of results</del></li>
<li><del>May 25, 2018: submission of system description papers</del></li>
<li><del>June 20, 2018: notification</del></li>
<li><del>June 30, 2018: camera-ready papers</del></li>
<li><del>August 25-26, 2018: shared task workshop colocated with <a href="http://multiword.sourceforge.net/lawmwecxg2018" target="_blank">LAW-MWE-CxG-2018</a></del></li>
</ul>

<!----------------------->
<h1>Organizing team</h1>
Silvio Ricardo Cordeiro, Carlos Ramisch, Agata Savary, Veronika Vincze

<!----------------------->
<!----
<h1><a id="faq"></a>Frequently asked questions</h1>

<ol>

<li><i>My system can identify only one category of VMWEs (e.g. verb particle constructions). Can I still participate in the shared task?</i><br />
Organizing different tracks for different VMWE categories would be too complex. Therefore, we plan to publish the systems' results globally, i.e. without the distinction into particular VMWE categories. 
If a system can only recognize one category, its results with respect to this global picture will probably not be very high. In spite of that <b>we do encourage such systems to participate</b>, since we are interested more in cross-language discussions than in a real competition. Our evaluation script (with a proper choice of parameters) does allow restraining the evaluation to a particular VMWE category. It is, thus, possible for a system's authors to perform the evaluation on their own and describe their results in a <a href="?sitesig=CONF&page=CONF_05_MWE_2017___lb__EACL__rb__#submissions">system description paper</a> to be submitted to the MWE 2017 workshop.
</li>

</ol>
--->

<!----------------------->
<h1>Contact</h1>

For any inquiries regarding the shared task please send an email to parseme-st-core@nlp.ipipan.waw.pl







