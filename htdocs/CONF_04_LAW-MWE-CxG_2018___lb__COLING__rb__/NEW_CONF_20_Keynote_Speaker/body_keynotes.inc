<h2>The 13th Workshop on Multiword Expressions (MWE 2017)</h2>

<h3>Workshop at <a href="http://eacl2017.org/" target="_blanc">EACL 2017</a> (Valencia, Spain), April 4, 2017</h3>

<p>Endorsed by the Special Interest Group on the Lexicon (<a href="http://www.siglex.org" target="_blank">SIGLEX</a>) of the Association for Computational Linguistics and <a href="http://www.parseme.eu" target="_blanc">PARSEME</a>, the European IC1207 COST Action</p>

<p><small>Last updated: January 31, 2017</small></p>

<!-- ------------------------------------------------------------------------- -->

<h1><a href="mwe2017/slides/mwe2017-cook.pdf" target="_blank">Exploiting multilingual lexical resources to predict the compositionality of MWEs</a></h1>
<p>by <strong><a href="http://cs.unb.ca/~ccook1/" target="_blank">Paul Cook</a></strong> – University of New Brunswick</p>

<table><tr>
  <td width="160" valign="top"><img src="images/cook.png" width="130" /></td>
  <td valign="top">
    Paul Cook is an Assistant Professor at the University of New Brunswick in
Canada. He received his PhD in Computer Science from the University of
Toronto in 2010. Prior to joining the University of New Brunswick he was a
McKenzie Postdoctoral Fellow at the University of Melbourne from
2011-2014. Paul's research interests include lexical semantics, multiword
expressions (MWEs), social media text processing, and web corpus
construction and analysis. His research in MWEs has focused primarily on
compositionality prediction and token-level MWE identification.
  </td>
</tr></table>

<p><strong>Abstract</strong></p>

<p>
Semantic idiomaticity is the extent to which the meaning of a
multiword expression (MWE) cannot be predicted from the meanings of
its component words. Much work in natural language processing on
semantic idiomaticity has focused on compositionality prediction,
wherein a binary or continuous-valued compositionality score is
predicted for an MWE as a whole, or its individual component
words. One source of information for making compositionality
predictions is the translation of an MWE into other languages. In this
talk we will consider methods for predicting compositionality that
exploit translation information provided by multilingual lexical
resources, and that are applicable to many kinds of MWEs in a wide
range of languages. These methods will make use of distributional
similarity of an MWE and its component words under translation into
many languages, as well as string similarity measures applied to
definitions of translations of an MWE and its component
words. Experimental results over English verb-particle constructions,
and English and German noun compounds, will highlight the importance
of token-level identification of MWEs in type-level compositionality
prediction. We will conclude by considering limitations of
compositionality scores. In particular, such measures on their own do
not indicate which of the possible meanings of a component word is
contributed in the case that an MWE is at least partially
compositional. We will discuss this issue through a case study on
English verb-particle constructions.
</p>

<p><strong>Photos from the invited talk</strong></p>
<table><tr>
  <td height="110" valign="top"><a href="mwe2017/mwe2017InvitedTalk/DSC_0047.JPG" target="_self"><img src="mwe2017/mwe2017InvitedTalk/DSC_0047-small.JPG" height="90" /></a></td>
  <td height="110" valign="top"><a href="mwe2017/mwe2017InvitedTalk/DSC_0048.JPG" target="_self"><img src="mwe2017/mwe2017InvitedTalk/DSC_0048-small.JPG" height="90" /></a></td>
  <td height="110" valign="top"><a href="mwe2017/mwe2017InvitedTalk/DSC_0049.JPG" target="_self"><img src="mwe2017/mwe2017InvitedTalk/DSC_0049-small.JPG" height="90" /></a></td>
  <td height="110" valign="top"><a href="mwe2017/mwe2017InvitedTalk/DSC_0050.JPG" target="_self"><img src="mwe2017/mwe2017InvitedTalk/DSC_0050-small.JPG" height="90" /></a></td>
  <td height="110" valign="top"><a href="mwe2017/mwe2017InvitedTalk/DSC_0051.JPG" target="_self"><img src="mwe2017/mwe2017InvitedTalk/DSC_0051-small.JPG" height="90" /></a></td>
  <td height="110" valign="top"><a href="mwe2017/mwe2017InvitedTalk/DSC_0052.JPG" target="_self"><img src="mwe2017/mwe2017InvitedTalk/DSC_0052-small.JPG" height="90" /></a></td>
  <td height="110" valign="top"><a href="mwe2017/mwe2017InvitedTalk/DSC_0053.JPG" target="_self"><img src="mwe2017/mwe2017InvitedTalk/DSC_0053-small.JPG" height="90" /></a></td>
</tr></table>

<!-- ------------------------------------------------------------------------- -->


<!--
<h3>TITLE</h3>
<p>by <strong><a href="http://" target="_blank">NAME</a></strong> – AFFILIATION</p>

<table><tr>
  <td width="160" valign="top"><img src="images/.jpg" width="120" /></td>
  <td valign="top">
    SHORT BIO
  </td>
</tr></table>

<p>
  ABSTRACT
</p>
<p><strong>References</strong></p>
<p>
  REFS
</p>
</p>
-->

<!-- ------------------------------------------------------------------------- -->


