﻿<h2>Multiword Expressions: Identification, Interpretation, Disambiguation
and Applications (MWE 2009)</h2>

<h3>Workshop at the ACL/IJCNLP 2009 Conference (Singapore), 06 August 2009</h3>

<p>
	Endorsed by the ACL Special Interest Group on the Lexicon (SIGLEX)
</p>
<p>
<h1> Papers and slides </h1>
</p>

Full proceedings:  <a href="proceedings_MWE09.pdf"> PDF</a> <br>
Presentation slides:  <a href="Presentations.zip">ZIP</a>
<ul>


</p>
<p>


</ul></ul>
</p>


Statistically-Driven Alignment-Based Multiword Expression
Identification for Technical Domains<br>
<i>Helena de Medeiros Caseli, Aline Villavicencio, André Machado, Maria José Finatto</i><br><br>
 Re-examining Automatic Keyphrase Extraction Approaches in Scientific Articles<br>
<i>Su Nam Kim and Min-Yen Kan</i><br><br>
Verb Noun Construction MWE Token Classification<br>
<i>Mona Diab and Pravin Bhutada</i><br><br>





 Identification, Interpretation, and Disambiguation</b><br>
Exploiting Translational Correspondences for Pattern-Independent MWE Identification<br>
<i>Sina Zarrieß and Jonas Kuhn</i><br><br>
 A re-examination of lexical association measures <br>
<i>Hung Huu Hoang, Su Nam Kim and Min-Yen Kan</i><br><br>
 Mining Complex Predicates In Hindi Using A Parallel Hindi-English Corpus<br>
<i>R. Mahesh K. Sinha </i><br><br>




  Improving Statistical Machine Translation Using Domain Bilingual Multiword Expressions<br>
<i>Zhixiang Ren, Yajuan Lù, Jie Cao, Qun Liu and Yun Huang</i><br><br>
  Bottom-up Named Entity Recognition using Two-stage Machine Learning Method<br>
  Abbreviation Generation for Japanese Multi-Word Expressions<br>
<i>Hiromi Wakaki, Hiroko Fujii, Masaru Suzuki, Mika Fukui and Kazuo Sumita</i><br><br>



</table> 
</ul></p>






</ul>

<h1>Programme committee</h1>
<ul>	
	<li>Inaki Alegria, <i>University of the Basque Country (Spain)</i></li>
	<li>Timothy Baldwin, <i>Stanford University (USA); University of Melbourne (Australia)</i></li>
	<li>Colin Bannard, <i>Max Planck Institute (Germany)</i></li>
	<li>Francis Bond, <i>National Institute of Information and Communications Technology (Japan)</i></li>
	<li>Gael Dias, <i>Beira Interior University (Portugal)</i></li>
	<li>Ulrich Heid, <i>Stuttgart University (Germany)</i></li>
	<li>Stefan Evert, <i>University of Osnabrueck (Germany)</i></li>
	<li>Afsaneh Fazly,<i>University of Toronto (Canada)</i></li>
	<li>Nicole Gregoire,<i>University of Utrecht (The Netherlands)</i></li>
	<li>Roxana Girju,<i>University of Illinois at Urbana-Champaign (USA)</i></li>
	<li>Kyo Kageura, <i>University of Tokyo (Japan)</i></li>
	<li>Brigitte Krenn, <i>Austrian Research Institute for Artificial Intelligence (Austria)</i></li>
	<li>Eric Laporte, <i> University of Marne-la-Vall?e (France)</i></li>
	<li>Rosamund Moon, <i>University of Birmingham (UK)</i></li>
	<li>Diana McCarthy, <i>University of Sussex (UK)</i></li>
	<li>Jan Odijk, <i>University of Utrecht (The Netherlands)</i></li>	
	<li>Stephan Oepen, <i>University of Oslo (Norway)</i></li>
	<li>Darren Pearce, <i>London Knowledge Lab (UK)</i></li>
	<li>Pavel Pecina, <i>Charles University (Czech Republic)</i></li>
	<li>Scott Piao, <i>University of Manchester (UK)</i></li>
	<li>Violeta Seretan, <i>University of Geneva (Switzerland)</i></li>
	<li>Stan Szpakowicz, <i>University of Ottawa (Canada)</i></li>
	<li>Beata Trawinski, <i>University of Tuebingen (Germany)</i></li>
	<li>Peter Turney, <i>National Research Council of Canada (Canada)</i></li>
	<li>Kiyoko Uchiyama, <i>Keio University (Japan)</i></li>
	<li>Begona Villada Moiron, <i>University of Groningen (The Netherlands)</i></li>
	<li>Aline Villavicencio, <i>Federal University of Rio Grande do Sul (Brazil)</i></li>
</ul>


<h1>Workshop chairs</h1>

<ul>
	<li><a href=  "http://ai.cs.uni-sb.de/~stahl/d-anastasiou/index.html">Dimitra Anastasiou</a>, <i>Localisation Research Centre, Limerick University, Ireland</a></i></li>
	<li><a href=  "http://www2.nict.go.jp/x/x161/member/hashimoto_chikara/index.html">Chikara Hashimoto</a>, <i>National Institute of Information and Communications
  Technology, Japan </i></li>
	<li><a href=" http://lml.bas.bg/~nakov/">Preslav Nakov</a>, <i> National University of Singapore, Singapore</i></li>
	<li><a href="http://www.cs.mu.oz.au/~snkim/index.html">Su Nam Kim</a>, <i>University of Melbourne, Australia</i></li>
</ul>

