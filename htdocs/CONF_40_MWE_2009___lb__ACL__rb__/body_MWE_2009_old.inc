﻿<h2>Multiword Expressions: Identification, Interpretation, Disambiguation
and Applications (MWE 2009)</h2>

<h3>Workshop at the ACL/IJCNLP 2009 Conference (Singapore), 06 August 2009</h3>

<p>
	Endorsed by the ACL Special Interest Group on the Lexicon (SIGLEX)
</p>
<b>Here are the <a href="proceedings_MWE09.pdf"> proceedings</a> of the workshop MWE09.</b>
<p>
<h1> Description </h1>
</p>

<ul>
The ACL 2009 Workshop on Multiword Expressions: Identification, Interpretation, Disambiguation and
Applications (MWE’09) will take place on August 6, 2009 in Singapore, immediately following the annual
meeting of the Association for Computational Linguistics (ACL). This is the fifth time this workshop has
been held in conjunction with ACL, following the meetings in 2003, 2004, 2006, and 2007.<br><br>

The workshop focused on Multi-Word Expressions (MWEs), which represent an indispensable part of
natural languages and appear steadily on a daily basis, both novel and already existing but paraphrased,
which makes them important for many natural language applications. Unfortunately, while easily
mastered by native speakers, MWEs are often non-compositional, which poses a major challenge for
both foreign language learners and automatic analysis.<br><br>

The growing interest in MWEs in the NLP community has led to many specialized workshops held
every year since 2001 in conjunction with ACL, EACL and LREC; there have been also two recent
special issues on MWEs published by leading journals: the International Journal of Language Resources
and Evaluation, and the Journal of Computer Speech and Language.<br><br>

As a result of the overall progress in the field, the time has come to move from basic preliminary research
to actual applications in real-world NLP tasks. Thus, in MWE’09, we were interested in the overall
process of dealing with MWEs, asking for original research on the following four fundamental topics:
<br><br>

<ul>(1) <b>Identification.</b>

	Identifying MWEs in free text is a very challenging problem. Due to the variability of
expression, it does not suffice to collect and use a static list of known MWEs; complex rules and
machine learning are typically needed as well. 
</p>
<p>
(2) <b>Interpretation.</b>
Semantically interpreting MWEs is a central issue. For some kinds of MWEs, e.g.,
noun compounds, it could mean specifying their semantics using a static inventory of semantic
relations, e.g., WordNet-derived. In other cases, MWE’s semantics could be expressible by a
suitable paraphrase.
	
</p>
<p>
(3) <b>Disambiguation.</b>
Most MWEs are ambiguous in various ways. A typical disambiguation task is to
determine whether an MWE is used non-compositionally (i.e., figuratively) or compositionally
(i.e., literally) in a particular context.	

</p>
<p>
(4) <b>Applications. </b>
Identifying MWEs in context and understanding their syntax and semantics is important
for many natural language applications, including but not limited to question answering, machine
translation, information retrieval, information extraction, and textual entailment. Still, despite the
growing research interest, there are not enough successful applications in real NLP problems,
which we believe is the key for the advancement of the field.
	
<br>
</ul>Of course, the above topics largely overlap. For example, identification can require disambiguating
between literal and idiomatic uses since MWEs are typically required to be non-compositional by
definition. Similarly, interpreting three-word noun compounds like morning flight ticket and plastic
water bottle requires disambiguation between a left and a right syntactic structure, while interpreting
two-word compounds like English teacher requires disambiguating between (a) ‘teacher who teaches
English’ and (b) ‘teacher coming from England (who could teach any subject, e.g., math)’.<br>
We received 18 submissions, and, given our limited capacity as a one-day workshop, we were only able
to accept 9 full papers for oral presentation, an acceptance rate of 50%.<br>
We would like to thank the members of the Program Committee for their timely reviews. We would also
like to thank the authors for their valuable contributions.
</p>
<p>


</ul></ul>
</p>


<h1>Programme of Workshop </h1>
<ul>
<br>
08:30-08:45   Welcome and Introduction to the Workshop  
<p>
<b>Session 1 (08:45–10:00): MWE Identification and Disambiguation </b><br>
<br>
08:45-09:10  Statistically-Driven Alignment-Based Multiword Expression
Identification for Technical Domains<br>
<i>Helena de Medeiros Caseli, Aline Villavicencio, André Machado, Maria José Finatto</i><br><br>
09:10-09:35  Re-examining Automatic Keyphrase Extraction Approaches in Scientific Articles<br>
<i>Su Nam Kim and Min-Yen Kan</i><br><br>
09:35-10:00  Verb Noun Construction MWE Token Classification<br>
<i>Mona Diab and Pravin Bhutada</i><br><br>



10:00-10:30   <u>BREAK </u><p>
 


<b>Session 2 (10:30–12:10): Identification, Interpretation, and Disambiguation</b><br>
<br>10:30-10:55  Exploiting Translational Correspondences for Pattern-Independent MWE Identification<br>
<i>Sina Zarrieß and Jonas Kuhn</i><br><br>
10:55-11:20  A re-examination of lexical association measures <br>
<i>Hung Huu Hoang, Su Nam Kim and Min-Yen Kan</i><br><br>
11:20-11:45  Mining Complex Predicates In Hindi Using A Parallel Hindi-English Corpus<br>
<i>R. Mahesh K. Sinha </i><br><br>
11:45-13:50   <u>LUNCH </u></p>



<b>Session 3 (13:50–15:30): Applications</b><br>
<br>13:50-14:15  Improving Statistical Machine Translation Using Domain Bilingual Multiword Expressions<br>
<i>Zhixiang Ren, Yajuan Lù, Jie Cao, Qun Liu and Yun Huang</i><br><br>
14:15-14:40  Bottom-up Named Entity Recognition using Two-stage Machine Learning Method<br>
<i>Hirotaka Funayama, Tomohide Shibata and Sadao Kurohashi</i><br><br>
14:50-15:15  Abbreviation Generation for Japanese Multi-Word Expressions<br>
<i>Hiromi Wakaki, Hiroko Fujii, Masaru Suzuki, Mika Fukui and Kazuo Sumita</i><br><br>
14:40-15:05 Discussion of Theme 3 <p>

15:05-15:30 Discussion of Sessions 1, 2, 3 (Creating an Agenda for the general discussion)  </p>
<p>
15:30-16:00  <u>BREAK  </u><br><br>
16:00-17:00   <u>General Discussion </u><br><br>
17:00-17:15   <u>Closing Remarks </u><br>
</table> 
</ul></p>






</ul>

<h1>Programme committee</h1>
<ul>	
	<li>Inaki Alegria, <i>University of the Basque Country (Spain)</i></li>
	<li>Timothy Baldwin, <i>Stanford University (USA); University of Melbourne (Australia)</i></li>
	<li>Colin Bannard, <i>Max Planck Institute (Germany)</i></li>
	<li>Francis Bond, <i>National Institute of Information and Communications Technology (Japan)</i></li>
	<li>Gael Dias, <i>Beira Interior University (Portugal)</i></li>
	<li>Ulrich Heid, <i>Stuttgart University (Germany)</i></li>
	<li>Stefan Evert, <i>University of Osnabrueck (Germany)</i></li>
	<li>Afsaneh Fazly,<i>University of Toronto (Canada)</i></li>
	<li>Nicole Gregoire,<i>University of Utrecht (The Netherlands)</i></li>
	<li>Roxana Girju,<i>University of Illinois at Urbana-Champaign (USA)</i></li>
	<li>Kyo Kageura, <i>University of Tokyo (Japan)</i></li>
	<li>Brigitte Krenn, <i>Austrian Research Institute for Artificial Intelligence (Austria)</i></li>
	<li>Eric Laporte, <i> University of Marne-la-Vall?e (France)</i></li>
	<li>Rosamund Moon, <i>University of Birmingham (UK)</i></li>
	<li>Diana McCarthy, <i>University of Sussex (UK)</i></li>
	<li>Jan Odijk, <i>University of Utrecht (The Netherlands)</i></li>	
	<li>Stephan Oepen, <i>University of Oslo (Norway)</i></li>
	<li>Darren Pearce, <i>London Knowledge Lab (UK)</i></li>
	<li>Pavel Pecina, <i>Charles University (Czech Republic)</i></li>
	<li>Scott Piao, <i>University of Manchester (UK)</i></li>
	<li>Violeta Seretan, <i>University of Geneva (Switzerland)</i></li>
	<li>Stan Szpakowicz, <i>University of Ottawa (Canada)</i></li>
	<li>Beata Trawinski, <i>University of Tuebingen (Germany)</i></li>
	<li>Peter Turney, <i>National Research Council of Canada (Canada)</i></li>
	<li>Kiyoko Uchiyama, <i>Keio University (Japan)</i></li>
	<li>Begona Villada Moiron, <i>University of Groningen (The Netherlands)</i></li>
	<li>Aline Villavicencio, <i>Federal University of Rio Grande do Sul (Brazil)</i></li>
</ul>


<h1>Workshop chairs</h1>

<ul>
	<li><a href=  "http://ai.cs.uni-sb.de/~stahl/d-anastasiou/index.html">Dimitra Anastasiou</a>, <i>Localisation Research Centre, Limerick University, Ireland</a></i></li>
	<li><a href=  "http://www2.nict.go.jp/x/x161/member/hashimoto_chikara/index.html">Chikara Hashimoto</a>, <i>National Institute of Information and Communications
  Technology, Japan </i></li>
	<li><a href=" http://lml.bas.bg/~nakov/">Preslav Nakov</a>, <i> National University of Singapore, Singapore</i></li>
	<li><a href="http://www.cs.mu.oz.au/~snkim/index.html">Su Nam Kim</a>, <i>University of Melbourne, Australia</i></li>
</ul>

<h1>Contact</h1>
<ul>For any inquiries regarding the workshop please contact <a href=  "mailto:dimitra@d-anastasiou.com">Dimitra Anastasiou</a></li>.</ul>
