<h2>Joint Workshop on Linguistic Annotation, Multiword Expressions and Constructions (LAW-MWE-CxG-2018)</h2>

<!-- <h3>Workshop proposal for <a href="http://coling2018.org/" target="_blank">COLING 2018</a> (Santa Fé, USA) or <a href="http://acl2018.org/" target="_blank">ACL 2018</a> (Melbourne, Australia)</h3> -->

<h3>Workshop at <a href="http://coling2018.org/" target="_blank">COLING 2018</a> (Santa Fe, USA), August 25-26, 2018</h3>

<p>Organized and funded by the Special Interest Group for Annotation (<a href="https://www.cs.vassar.edu/sigann/" target="_blank">SIGANN</a>), the Special Interest Group on the Lexicon (<a href="http://www.siglex.org" target="_blank">SIGLEX</a>), and the Special Interest Group on Computational Semantics (<a href="http://www.sigsem.org" target="_blank">SIGSEM</a>) of the Association for Computational Linguistics (<a href="https://www.aclweb.org/portal/" target="_blank">ACL</a>)</p>

<p><small>Last updated: Aug 7, 2018</small></p>

<h1>Order of poster boosters</h1>


<!-----------------------------------------
<h2>Session 1: Poster boosters in the research track</h2>

Annotation

MWEs


<ol>

<!-- Treebanks and lexicons -->
<li>THE RST SPANISH-CHINESE TREEBANK<br />
Shuyuan Cao, Iria da Cunha and Mikel Iruskieta

<li>A TREEBANK FOR THE HEALTHCARE DOMAIN<br />
Nganthoibi Oinam, Diwakar Mishra, Pinal Patel, Narayan Choudhary and Hitesh Desai

<li>TOWARDS A COMPUTATIONAL LEXICON FOR MOROCCAN DARIJA: WORDS, IDIOMS, AND CONSTRUCTIONS<br />
Jamal Laoudi, Claire Bonial, Lucia Donatelli, Stephen Tratz and Clare Voss


<!-- Annotation procedures -->

<li>A SYNTAX-BASED SCHEME FOR THE ANNOTATION AND SEGMENTATION OF GERMAN SPOKEN LANGUAGE INTERACTIONS<br />
Swantje Westpfahl and Jan Gorisch

<li>ALL ROADS LEAD TO UD: CONVERTING STANFORD AND PENN PARSES TO ENGLISH UNIVERSAL DEPENDENCIES WITH MULTILAYER ANNOTATIONS<br />
Siyao Peng and Amir Zeldes

<li>COOPERATING TOOLS FOR MWE LEXICON MANAGEMENT AND CORPUS ANNOTATION<br />
Yuji Matsumoto, Akihiko Kato, Hiroyuki Shindo and Toshio Morita

<li>DEVELOPING AND EVALUATING ANNOTATION PROCEDURES FOR TWITTER DATA DURING HAZARD EVENTS<br />
Kevin Stowe, Martha Palmer, Leysia Palen, Kenneth M. Anderson, Jennings Anderson, Marina Kogan, Rebecca Morss, Julie Demuth and Heather Lazrus

<li>"FINGERS IN THE NOSE": EVALUATING THE SPEAKERS’ INTUITION IN IDENTIFYING MWES USING A SLIGHTLY GAMIFIED CROWDSOURCING PLATFORM<br />
Karën Fort, Bruno Guillaume, Matthieu Constant, Nicolas Lefèbvre and Yann-Alan Pilatte


<!-- MWE annotation -->
<li>VERBAL MULTIWORD EXPRESSIONS IN BASQUE CORPORA<br />
Uxoa Iñurrieta, Itziar Aduriz, Ainara Estarrona, Itziar Gonzalez-Dios, Antton Gurrutxaga and Ruben Urizar

<li>CONSTRUCTING AN ANNOTATED CORPUS OF VERBAL MWES FOR ENGLISH<br />
Abigail Walsh and Claire Bonial

<!-- MWE-hood -->
<li>THE OTHER SIDE OF THE COIN: UNSUPERVISED DISAMBIGUATION OF POTENTIALLY IDIOMATIC EXPRESSIONS BY CONTRASTING SENSES<br />
Hessel Haagsma, Malvina Nissim and Johan Bos

<li>DO CHARACTER-LEVEL NEURAL NETWORK LANGUAGE MODELS CAPTURE KNOWLEDGE OF MULTIWORD EXPRESSION COMPOSITIONALITY?<br />
Ali Hakimi Parizi and Paul Cook

</ol>

<!-----------------------------------------
<h2>Session 2: Poster boosters in the shared task track</h2>

<ol>
<li>DEEP-BGT AT PARSEME SHARED TASK 2018: BIDIRECTIONAL LSTM-CRF MODEL FOR VERBAL MULTIWORD EXPRESSION IDENTIFICATION<br />
Gözde Berk, Berna Erden and Tunga Gungor

<li>MULTI-WORD EXPRESSION DETECTION USING BIDIRECTIONAL LONG-SHORT-TERM MEMORY NETWORKS AND GRAPH-BASED DECODING.<br />
Tiberiu Boroș and Ruxandra Burtica

<li>MUMPITZ AT PARSEME SHARED TASK 2018: A BIDIRECTIONAL LSTM FOR THE IDENTIFICATION OF VERBAL MULTIWORD EXPRESSIONS<br />
Rafael Ehren, Timm Lichte and Younes Samih

<li>DETECTING VERBAL MULTIWORD EXPRESSIONS WITH CONDITIONAL RANDOM FIELDS: A ROBUST SEQUENTIAL APPROACH AND A DEPENDENCY-BASED APPROACH<br />
Erwan Moreau, Ashjan Alsulaimani, Alfredo Maldonado and Carl Vogel

<li>VARIDE AT PARSEME SHARED TASK 2018: ARE VARIANTS REALLY AS ALIKE AS TWO PEAS IN A POD?<br />
Caroline Pasquer, Agata Savary, Carlos Ramisch and Jean-Yves Antoine

<li>TRAPACC AND TRAPACCS AT PARSEME SHARED TASK 2018: A NEURAL TRANSITION SYSTEM FOR VMWE IDENTIFICATION<br />
Regina Stodden, Behrang QasemiZadeh and Laura Kallmeyer

<li>TRAVERSAL AT PARSEME SHARED TASK 2018: IDENTIFICATION OF MULTIWORD EXPRESSION USING A DISCRIMINATIVE TREE-STRUCTURED MODEL<br />
Jakub Waszczuk

<li>VEYN AT PARSEME SHARED TASK 2018: RECURRENT NEURAL NETWORKS FOR VMWE IDENTIFICATION<br />
Nicolas Zampieri, Manon Scholivet, Carlos Ramisch and Benoit Favre

</ol>
------------------------------------------------>



