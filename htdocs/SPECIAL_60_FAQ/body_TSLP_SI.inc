<h3><a href="http://tslp.acm.org/specialissues.html">ACM Transactions on Speech and Language Processing</a> Special Issue on</h3>

<h2>Multiword Expressions: from Theory to Practice and Use</h2>

<p><small>Last updated: May 09, 2012</small></p>

<h1>Frequently asked questions</h1>

<ul>
<li><h2> What is the page limit for submissions? </h2></li>
</ul>

<p>There is no page limit except the one imposed by ACM TSLP <a href="http://tslp.acm.org/authgl.html" target="_blank">author instructions</a>:</p>

<p><em>"3. TSLP will discourage excessively long papers (longer than 50 double-spaced pages including figures, references, etc.), and unnecessary digressions even in shorter papers. This is to motivate the authors to bring out the essence of their papers more clearly, to make it easier for the reviewers and readers, and to allow TSLP to publish more papers in any given issue."</em></p>

<p>We recommend submissions with reasonable length, around 20-30 pages, but of course shorter papers are welcome as well.</p>

<ul>
<li><h2> Are submissions anonymous? </h2></li>
</ul>

<p>No, the special issue follows the same submission procedure as regular ACM TSLP papers. Thus, authors should include their names and affiliations on the first page of their submission and they are not required to anonymise self-quotations.</p>

<h1>Contact</h1>

<p>If you didn't find the answer to your question here, please send an email to <a href="mailto:mweguesteditor at gmail.com">mweguesteditor at gmail.com</a></p>
