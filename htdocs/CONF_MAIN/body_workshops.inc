<h2>Please go to our new website <a href="https://multiword.org/events/previousevents">https://multiword.org/events/previousevents</a></h2>


<!--
<h2>Overview of the workshops dedicated to MWEs (organised by the SIGLEX-MWE section or endorsed by SIGLEX)</h2>

<h3>2020</h3>

<ul>
<li>COLING <a href="mwelex2020"><em>Joint Workshop on Multiword Expressions and Electronic Lexicons (MWE-LEX 2020) </em></a> – <del>September 14</del> December 13, 2020 – <del>Barcelona, Spain</del> Online.</li>
</ul>

<h3>2019</h3>

<ul>
<li>ACL <a href="mwewn2019"><em>Joint Workshop on Multiword Expressions and WordNet (MWE-WN 2019)</em></a> – August 2nd, 2019 – Florence, Italy.</li>
<li>International Conference <a href="http://www.lexytrad.es/europhras2019" target="_blank"><em>Europhras 2019</em></a> — 25-27 September 2019 — Málaga, Spain.</li>
<li>EUROPHRAS <em><a href="http://www.lexytrad.es/europhras2019/mumttt-2019-2/" target="_blank">4th Workshop on Multi-word Units in Machine Translation and Translation Technology (MUMTTT 2019)</a></em>
— 27 September 2019 — Málaga, Spain.</li>
</ul>

<h3>2018</h3>

<ul>
<li>COLING <a href="lawmwecxg2018"><em>Joint Workshop on Linguistic Annotation, Multiword Expressions and Constructions (LAW-MWE-CxG-2018)</em></a> – August 25-26, 2018 – Santa Fe, USA.</li>
</ul>

<h3>2017</h3>

<ul>
<li>EACL <a href="mwe2017"><em>13th Workshop on Multiword Expressions (MWE 2017)</em></a> – April 4, 2017 – Valencia, Spain.</li>
<li>EUROPHRAS <a href="http://rgcl.wlv.ac.uk/europhras2017/mumttt-2017/" target ="_blank"><em>3rd Workshop on Multi-word Units in Machine Translation and Translation Technology (MUMTTT 2017)</a> – November 14, 2017 – London, UK.</li>
</ul>



<h3>2016</h3>

<ul>
<li>ACL <a href="mwe2016"><em>12th Workshop on Multiword Expressions (MWE 2016)</em></a> – August 11, 2016 – Berlin, Germany.</li>
</ul>

<h3>2015</h3>

<ul>

<li>NAACL <a href="mwe2015"><em>11th Workshop on Multiword Expressions (MWE 2015)</em></a> – June 4, 2015 – Denver, Colorado, USA.</li>
<li>Europhras 2015 <a href="http://www.europhras2015.eu/presentation" target="_blank"><em>Workshop on Multi-word Units in Machine Translation and Translation Technologies</em></a> — July 1-2, 2015 — Málaga, Spain.</li>
</ul>

<h3>2014</h3>

<ul>
<li>EACL <a href="PHITE.php?sitesig=CONF&page=CONF_08_MWE_2014___lb__EACL__rb__"><em>10th Workshop on Multiword Expressions (MWE 2014)</em></a> – April 26-27, 2014 – Gothenburg, Sweden.</li>
</ul>

<h3>2013</h3>

<ul>
<li>NAACL <a href="PHITE.php?sitesig=CONF&page=CONF_09_MWE_2013___lb__NAACL__rb__"><em>9th Workshop on Multiword Expressions (MWE 2013)</em></a> – June 13-14, 2013 – Atlanta, Georgia, USA.</li>
<li>MT SUMMIT <a href="http://www.mtsummit2013.info/workshop4.asp" target ="_blank"><em>Workshop on Multi-word Units in Machine Translation and Translation Technology</em></a> – September 3, 2013 – Nice, France.</li>
</ul>

<h3>2012</h3>

<ul>
<li><a href="http://ixa2.si.ehu.es/starsem/"><em>*SEM – First Joint Conference on Lexical and Computational Semantics</em></a> – June 7-8, 2012 – Montréal, Canada<br/>Read this <a href="PHITE.php?sitesig=CONF&page=CONF_10_STARSEM_2012">ANNOUNCEMENT</a>.</li>
</ul>

<h3>2011</h3>

<ul>
<li>ACL workshop on <a href="PHITE.php?sitesig=CONF&page=CONF_20_MWE_2011___lb__ACL__rb__"><em>Multiword Expressions: from Parsing and Generation to the real world</em></a> – June 23, 2011 – Portland, Oregon, USA.</li>
</ul>


<h3>2010</h3>
<ul>
<li>COLING workshop on <a href="PHITE.php?sitesig=CONF&page=CONF_30_MWE_2010___lb__COLING__rb__"><em>Multiword Expressions: from Theory to Applications</em></a> – August 28, 2010 – Beijing, China.</li>

<li>SLTC 2010 Workshop on <a href="http://www.ida.liu.se/~sarst/compound-ws/" target ="_blank"><em>Compounds and Multiword Expressions</em></a> – October 29, 2010 – Linköping, Sweden.</li>


</ul>

<h3>2009</h3>
<ul>
<li>ACL workshop on <a href="PHITE.php?sitesig=CONF&page=CONF_40_MWE_2009___lb__ACL__rb__"><em>Multiword Expressions: Identification, Interpretation, Disambiguation and Applications</em></a> – 6 August 2009 – Singapore.</li>
</ul>

<h3>2008</h3>
<ul>
<li>LREC workshop <a href="PHITE.php?sitesig=CONF&page=CONF_50_MWE_2008___lb__LREC__rb__"><em>Towards a Shared Task for Multiword Expressions</em></a> – Sunday, 1 June 2008 – Marrakech, Morocco.</li>
</ul>

<h3>2007</h3>
<ul>
<li>ACL workshop <a href="PHITE.php?sitesig=CONF&page=CONF_70_MWE_2007___lb__ACL__rb__"><em>A Broader Perspective on Multiword Expressions</em></a> – Prague, Czech Republic.</li>
</ul>

<h3>2006</h3>
<ul>
<li>ACL workshop <a href="http://www.inf.ufrgs.br/~avillavicencio/mwe-acl06.html" target ="_blank"><em>Multiword Expressions: Identifying and Exploiting Underlying Properties</em></a> – Sydney, Australia. [<a href="http://acl.ldc.upenn.edu/W/W06/#W06-1200" target="_blank">Proceedings</a>]</li>
<li>EACL workshop <a href="http://ucrel.lancs.ac.uk/EACL06MWEmc/" target ="_blank"><em>Multi-word-expressions in a multilingual context</em></a> – Trento, Italy.</li>
<li><a href="http://kollokationen.bbaw.de/htm/collconf2_en.html" target ="_blank"><em>Collocations and idioms 2006: linguistic, computational, and psycholinguistic perspectives</em></a> – Berlin, Germany.</li>
</ul>

<h3>2004</h3>
<ul>
<li>ACL workshop <a href="http://www.cl.cam.ac.uk/~alk23/mwe04/mwe.html" target ="_blank"><em>Multiword Expressions: Integrating Processing</em></a> – Barcelona, Spain.</li>
</ul>

<h3>2003</h3>
<ul>
<li>ACL workshop <a href="http://www.cl.cam.ac.uk/~alk23/mwe/mwe.html" target ="_blank"><em>Multiword Expressions: Analysis, Acquisition and Treatment</em></a> – Sapporo, Japan.</li>
</ul>-->



