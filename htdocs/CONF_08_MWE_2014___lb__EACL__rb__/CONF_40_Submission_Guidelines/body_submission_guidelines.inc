<h2>The 10th Workshop on Multiword Expressions (MWE 2014)</h2>

<h3>Workshop at EACL 2014 (Gothenburg, Sweden), April 26-27, 2014</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.siglex.org/" target="_blank">SIGLEX</a>), SIGLEX's Multiword Expressions Section (SIGLEX-MWE), and 
<a href="http://www.parseme.eu/" target="_blank">PARSEME</a>, European IC1207 COST Action.
</p>

<p><small>Last updated: Jan 22, 2014</small></p>


<h1>Submission modalities</h1>

<p>For MWE 2014, we accept the following two types of submissions:</p>
<ul>
<li>Regular long papers (8 content pages + 1 page for references): Long papers should report on solid and finished research including new experimental results, resources and/or techniques.</li> 
<li>Regular short papers (4 content pages + 1 page for references): Short papers should report on small experiments, focused contributions, ongoing research, negative results and/or philosophical discussion.</li>
</ul>

<p>The reported research should be substantially original. The papers will be presented orally or as posters. The decision as to which papers will be presented orally and which as posters will be made by the program committee based on the nature rather than on the quality of the work. All submissions must be in PDF format and must follow the EACL 2014 formatting requirements (<a href="http://www.eacl2014.org/files/eacl-2014-styles.zip">download ZIP archive</a>). We strongly advise the use of the provided Word or LaTeX template files.</p>

<p>Reviewing will be double-blind, and thus no author information should be included in the papers; self-reference should be avoided as well. </p>

<p>Resources submitted with the papers should be anonymized for submission. Papers and/or resources that do not conform to these requirements will be rejected without review. Accepted papers will appear in the workshop proceedings, where no distinction will be made between papers presented orally or as posters. </p>

<p>Submission and reviewing will be electronic, managed by the START system:</p>
<p align="center"><a href="https://www.softconf.com/eacl2014/MWE/" target="_blank">https://www.softconf.com/eacl2014/MWE/</a></p>
<p>Submissions must be uploaded onto the START system by the submission deadline: <b>January 30th, 2014</b> (11:59pm, GMT-12).
Please choose the appropriate submission type from the starting submission page, according to the category of your paper.</p>

<h1>Workshop Organizers</h1>

<ul>
<li><a href="https://www.angl.hu-berlin.de/staff/kordoni" target="_blank">Valia Kordoni</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="https://www.angl.hu-berlin.de/staff/markus_egg" target="_blank">Markus Egg</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="http://www.info.univ-tours.fr/~savary/" target="_blank">Agata Savary</a><em>, special track organizer (Université François Rabelais Tours, France)</em></li>
<li><a href="http://www.latl.unige.ch/" target="_blank">Eric Wehrli</a><em>, special track organizer (Université de Genève, Switzerland)</em></li>
<li><a href="http://www.stefan-evert.de/" target="_blank">Stefan Evert</a><em> (Friedrich-Alexander-Universität Erlangen-Nürnberg, Germany)</em></li>
</ul>


<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mweworkshop.eacl2014@gmail.com">mweworkshop.eacl2014@gmail.com</a></p>

