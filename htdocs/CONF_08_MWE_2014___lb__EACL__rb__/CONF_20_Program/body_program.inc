<h2>The 10th Workshop on Multiword Expressions (MWE 2014)</h2>

<h3>Workshop at EACL 2014 (Gothenburg, Sweden), April 26-27, 2014 <span style="color:#2244AA">| room HB2</span></h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.siglex.org/" target="_blank">SIGLEX</a>), SIGLEX's Multiword Expressions Section (SIGLEX-MWE), and 
<a href="http://www.parseme.eu/" target="_blank">PARSEME</a>, European IC1207 COST Action.
</p>

<p><small>Last updated: Apr 25, 2014</small></p>

<h1>Workshop program</h1>

<h3>Saturday, 26 April 2014</h3>

<table>
  <tr><td valign="top">08:45–09:00</td><td valign="top">
      Opening Remarks
    </td></tr>
  <tr><td style="width: 10em;"></td><td>&nbsp;</td></tr>
  <tr><td></td><td><strong>Oral Session 1: Detection and Extraction of MWEs</strong></td></tr>
  <tr></tr>
  <tr><td valign="top">09:00–09:30</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Breaking Bad: Extraction of Verb-Particle Constructions from a Parallel Subtitles Corpus</span><br/>
    Aaron Smith
  </td>
  <tr><td valign="top">09:30–10:00</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">A Supervised Model for Extraction of Multiword Expressions, Based on Statistical Context Features</span><br/>
    Meghdad Farahmand and Ronaldo Martins
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td></td><td><strong>Oral Session 2: PARSEME I — Parsing MWEs</strong></td></tr>
  <tr><td valign="top">10:00–10:30</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">VPCTagger: Detecting Verb-Particle Constructions With Syntax-Based Methods</span><br/>
    István Nagy T. and Veronika Vincze
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td valign="top">10:30–11:00</td><td valign="top">
    Coffee Break
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td valign="top">11:00–12:00</td><td valign="top">
    Invited Talk 1: <span style="font-style: italic; color: #2255AA">The Web as an Implicit Training Set: Application to Noun Compounds Syntax and Semantics</span><br/>
    Preslav Nakov
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td></td><td><strong>Oral Session 2: PARSEME I — Parsing MWEs</strong> (continued)</td></tr>
  <tr><td valign="top">12:00–12:30</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">The Relevance of Collocations for Parsing</span><br/>
    Eric Wehrli
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td valign="top">12:30–14:00</td><td valign="top">
    Lunch
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td></td><td><strong>Oral Session 3: PARSEME II — Short Papers</strong></td></tr>
  <tr><td valign="top">14:00–14:20</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Parsing Modern Greek verb MWEs with LFG/XLE grammars</span><br/>
    Niki Samaridi and Stella Markantonatou
  </td></tr>
  <tr><td valign="top">14:20–14:40</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Evaluation of a Substitution Method for Idiom Transformation in Statistical Machine Translation</span><br/>
    Giancarlo Salton, Robert Ross and John Kelleher
  </td></tr>
  <tr><td valign="top">14:40–15:00</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Encoding MWEs in a conceptual lexicon</span><br/>
    Aggeliki Fotopoulou, Stella Markantonatou and Voula Giouli
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td>15:00–15:30</td><td>
    <strong>Poster Booster</strong> (4 minutes per poster)
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">German Compounds and Statistical Machine Translation. Can they get along?</span><br/>
    Carla Parra Escartín, Stephan Peitz and Hermann Ney
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Extracting MWEs from Italian corpora: A case study for refining the POS-pattern methodology</span><br/>
    Sara Castagnoli, Malvina Nissim and Francesca Masini
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Mickey Mouse is not a Phrase: Improving Relevance in E-Commerce with Multiword Expressions</span><br/>
    Prathyusha Senthil Kumar, Vamsi Salaka, Tracy Holloway King and Brian Johnson
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Encoding of Compounds in Swedish FrameNet</span><br/>
    Karin Friberg Heppin and Miriam R L Petruck
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Extraction of Nominal Multiword Expressions in French</span><br/>
    Marie Dubremetz and Joakim Nivre
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Towards an Empirical Subcategorization of Multiword Expressions</span><br/>
    Luigi Squillante
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Contexts, Patterns, Interrelations – New Ways of Presenting Multiword Expressions</span><br/>
    Kathrin Steyer and Annelen Brunner
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Detecting change and emergence for multiword expressions</span><br/>
    Martin Emms and Arun Jayapal
  </td></tr>
  <tr><td valign="top"></td><td valign="top">
    <span style="font-style: italic; color: #2255AA">An Approach to Take Multi-Word Expressions</span><br/>
    Claire Bonial, Meredith Green, Jenette Preciado and Martha Palmer
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td valign="top">15:30–16:00</td><td valign="top">
    Coffee Break
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td>16:00–17:30</td><td><strong>Poster Session</strong></td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
</table>

<h3>Sunday, 27 April 2014</h3>

<table>
  <tr><td valign="top">09:30–10:30</td><td valign="top">
    Panel: <span style="font-style: italic; color: #2255AA">Current European and other research initiatives on Multiword Expressions</span><br/>
    Valia Kordoni, Agata Savary, Markus Egg, Eric Wehrli, Stefan Evert
  </td></tr>
  <tr><td style="width: 10em;"></td><td>&nbsp;</td></tr>
  <tr><td valign="top">10:30–11:00</td><td valign="top">
    Coffee Break
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td></td><td><strong>Oral Session 4: MWEs in Multilingual Applications — Short Papers</strong></td></tr>
  <tr><td valign="top">11:00–11:20</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Paraphrasing Swedish Compound Nouns in Machine Translation</span><br/>
    Edvin Ullman and Joakim Nivre
  </td></tr>
  <tr><td valign="top">11:20–11:40</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Feature Norms of German Noun Compounds</span><br/>
    Stephen Roller and Sabine Schulte im Walde
  </td></tr>
  <tr><td valign="top">11:40–12:00</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Identifying collocations using cross-lingual association measures</span><br/>
    Lis Pereira, Elga Strafella, Kevin Duh and Yuji Matsumoto
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td></td><td><strong>Oral Session 5: Issues in Lexicon Construction and Machine Translation</strong></td></tr>
  <tr><td valign="top">12:00–12:30</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Unsupervised Construction of a Lexicon and a Repository of Variation Patterns for Arabic Modal Multiword Expressions</span><br/>
    Rania Al-Sabbagh, Roxana Girju and Jana Diesner
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td valign="top">12:30–14:00</td><td valign="top">
    Lunch
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td></td><td><strong>Oral Session 5: Issues in Lexicon Construction and Machine Translation</strong> (continued)</td></tr>
  <tr><td valign="top">14:00–14:30</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">Issues in Translating Verb-Particle Constructions from German to English</span><br/>
    Nina Schottmüller and Joakim Nivre
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td valign="top">14:30–15:30</td><td valign="top">
    Invited Talk 2: <span style="font-style: italic; color: #2255AA">Statistical Modelling of Metaphor</span><br/>
    Ekaterina Shutova
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td valign="top">15:30–15:45</td><td valign="top">
    Closing Remarks
  </td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
</table>

<!--
<table>
  <tr><td style="width: 10em;"></td><td>&nbsp;</td></tr>
  <tr><td></td><td>&nbsp;</td></tr>
  <tr><td></td><td><strong>Session:</strong></td></tr>
  <tr><td valign="top">00:00–00:00</td><td valign="top">
    <span style="font-style: italic; color: #2255AA">TITLE</span><br/>
    AUTHORS
  </td></tr>
</table>
-->
