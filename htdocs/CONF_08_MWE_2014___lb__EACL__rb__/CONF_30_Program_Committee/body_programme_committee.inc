<h2>The 10th Workshop on Multiword Expressions (MWE 2014)</h2>

<h3>Workshop at EACL 2014 (Gothenburg, Sweden), April 26-27, 2014</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.siglex.org/" target="_blank">SIGLEX</a>), SIGLEX's Multiword Expressions Section (SIGLEX-MWE), and 
<a href="http://www.parseme.eu/" target="_blank">PARSEME</a>, European IC1207 COST Action.
</p>

<p><small>Last updated: Feb 07, 2014</small></p>


<h1>Program Committee</h1>

<ul>
<li> Iñaki Alegria, University of the Basque Country (Spain) </li>
<li> Dimitra Anastasiou, University of Bremen (Germany)</li>
<li> Doug Arnold, University of Essex (UK)</li>
<li> Eleftherios Avramidis, DFKI GmbH (Germany)</li>
<li> Tim Baldwin, University of Melbourne (Australia)</li>
<li> Núria Bel, Universitat Pompeu Fabra (Spain)</li>
<li> Chris Biemann, Technische Universität Darmstadt (Germany)</li>
<li> Francis Bond, Nanyang Technological University (Singapore)</li>
<li> Lars Borin, University of Gothenburg (Sweden)</li>
<li> António Branco, University of Lisbon (Portugal)</li>
<li> Miriam Butt, Universität Konstanz (Germany)</li>
<li> Aoife Cahill, ETS (USA)</li>
<li> Ken Church, IBM Research (USA)</li>
<li> Matthieu Constant, Université Paris-Est Marne-la-Vallée (France)</li>
<li> Paul Cook, University of Melbourne (Australia)</li>
<li> Béatrice Daille, Nantes University (France)</li>
<li> Koenraad De Smedt, University of Bergen (Norway)</li>
<li> Gaël Dias, University of Caen Basse-Normandie (France)</li>
<li> Gülşen Eryiğit, Istanbul Technical University (Turkey)</li>
<li> Tomaž Erjavec, Jožef Stefan Institute (Slovenia)</li>
<li> Joaquim Ferreira da Silva, New University of Lisbon (Portugal)</li>
<li> Roxana Girju, University of Illinois at Urbana-Champaign (USA)</li>
<li> Chikara Hashimoto, National Institute of Information and Communications Technology (Japan)</li>
<li> Ulrich Heid, Universität Hildesheim (Germany)</li>
<li> Kyo Kageura, University of Tokyo (Japan)</li>
<!--
<li> Su Nam Kim, Monash University (Australia)</li>
-->
<li> Ioannis Korkontzelos, University of Manchester (UK)</li>
<li> Brigitte Krenn, Austrian Research Institute for Artificial Intelligence (Austria)</li>
<li> Cvetana Krstev, University of Belgrade (Serbia)</li>
<li> Marie-Catherine de Marneffe, The Ohio State University (USA)</li>
<li> Takuya Matsuzaki, National Institute of Informatics (Japan)</li>
<li> Preslav Nakov, Qatar Computing Research Institute (Qatar)</li>
<li> Malvina Nissim, University of Bologna (Italy)</li>
<li> Joakim Nivre, Uppsala University (Sweden)</li>
<li> Diarmuid Ó Séaghdha, University of Cambridge (UK)</li>
<li> Jan Odijk, University of Utrecht (The Netherlands)</li>
<li> Yannick Parmentier, Université d'Orléans (France)</li>
<li> Pavel Pecina, Charles University in Prague (Czech Republic)</li>
<li> Scott Piao, Lancaster University (UK)</li>
<li> Adam Przepiórkowski, Institute of Computer Science, Polish Academy of Sciences (Poland)</li>
<li> Victoria Rosén, University of Bergen (Norway)</li>
<li> Carlos Ramisch, Aix-Marseille University (France)</li>
<li> Manfred Sailer, Goethe University Frankfurt am Main (Germany)</li>
<li> Magali Sanches Duran, University of São Paulo (Brazil)</li>
<li> Violeta Seretan, University of Geneva (Switzerland)</li>
<li> Ekaterina Shutova, University of California, Berkeley (USA)</li>
<li> Jan Šnajder, University of Zagreb (Croatia)</li>
<li> Pavel Straňák, Charles University in Prague (Czech Republic)</li>
<li> Sara Stymne, Uppsala University (Sweden)</li>
<li> Stan Szpakowicz, University of Ottawa (Canada)</li>
<li> Beata Trawinski, Institut für Deutsche Sprache (IDS), Mannheim (Germany)</li>
<li> Yulia Tsvetkov, Carnegie Mellon University (USA)</li>
<li> Yuancheng Tu, Microsoft (USA)</li>
<li> Ruben Urizar, University of the Basque Country (Spain)</li>
<li> Gertjan van Noord, University of Groningen (The Netherlands)</li>
<li> Aline Villavicencio, Federal University of Rio Grande do Sul (Brazil)</li>
<li> Veronika Vincze, Hungarian Academy of Sciences (Hungary)</li>
<li> Martin Volk, University of Zurich (Switzerland)</li>
<li> Tom Wasow, Stanford University (USA)</li>
<li> Shuly Wintner, University of Haifa (Israel)</li>
<li> Dekai Wu, The Hong Kong University of Science &amp; Technology (Hong Kong)</li>
</ul>


<h1>Workshop Organizers</h1>

<ul>
<li><a href="https://www.angl.hu-berlin.de/staff/kordoni" target="_blank">Valia Kordoni</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="https://www.angl.hu-berlin.de/staff/markus_egg" target="_blank">Markus Egg</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="http://www.info.univ-tours.fr/~savary/" target="_blank">Agata Savary</a><em>, special track organizer (Université François Rabelais Tours, France)</em></li>
<li><a href="http://www.latl.unige.ch/" target="_blank">Eric Wehrli</a><em>, special track organizer (Université de Genève, Switzerland)</em></li>
<li><a href="http://www.stefan-evert.de/" target="_blank">Stefan Evert</a><em> (Friedrich-Alexander-Universität Erlangen-Nürnberg, Germany)</em></li>
</ul>


<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mweworkshop.eacl2014@gmail.com">mweworkshop.eacl2014@gmail.com</a></p>

