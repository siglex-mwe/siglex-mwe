<h2>The 10th Workshop on Multiword Expressions (MWE 2014)</h2>

<h3>Workshop at EACL 2014 (Gothenburg, Sweden), April 26-27, 2014 <span style="color:#2244AA">| room HB2</span></h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.siglex.org/" target="_blank">SIGLEX</a>), SIGLEX's Multiword Expressions Section (SIGLEX-MWE), and 
<a href="http://www.parseme.eu/" target="_blank">PARSEME</a>, European IC1207 COST Action.
</p>



<!--<p><strong>EXTENDED Submission deadline:<br/> 
 <span style="text-decoration:line-through">Long &amp; short papers - Mar 01, 2013 at 23:59 PDT (GMT-12)</span><br/>
Long &amp; short papers - Mar 15, 2013 at 23:59 PDT (GMT-12)</strong></p>-->

<p><small>Last updated: Apr 25, 2014</small></p>

<h1>News</h1>

<ul>
  <li><em>Apr 25, 2014:</em> Final <a href="http://multiword.sourceforge.net/PHITE.php?sitesig=CONF&amp;page=CONF_08_MWE_2014___lb__EACL__rb__&amp;subpage=CONF_20_Program">workshop programme</a> and room announcement. We're looking forward to seeing you tomorrow at the workshop!</li>
  <!--
  <li><em>Mar 24, 2014:</em> Updated <a href="http://multiword.sourceforge.net/PHITE.php?sitesig=CONF&amp;page=CONF_08_MWE_2014___lb__EACL__rb__&amp;subpage=CONF_20_Program">workshop programme</a> and information about invited talks</li>
  <li><em>Feb 20, 2014:</em> <a href="#parseme_grants">Application details</a> for <span style="color:red;">PARSEME travel grants</span> available (deadline: Monday, March 3rd)</li>
  <li><em>Feb 20, 2014:</em> Notifications will be sent out around Tuesday, Feb 25th</li>  
  <li><em>Jan 22, 2014:</em> Travel funding available for ca. 20 participants in the Special Track on Parsing and MWEs; more information on the grant application will be provided at a later stage</li>  
  <li><em>Jan 22, 2014:</em> <span style="color:red;">Submission deadline extended by one week to January 30th</span></li>
  <li><em>Dec 12, 2014:</em> Second call for papers, submission details available</li>
  <li><em>Nov 11, 2014:</em> First call for papers</li>
-->
</ul>

<h1>Important Dates</h1>

<table>
<!-- 
<tr><td><span style="text-decoration:line-through">Mar 1, 2013</span></td><td><span style="text-decoration:line-through">Long &amp; short paper submission deadline 23:59 PDT (GMT-12)</span></td></tr>
-->
<tr><td><del>Jan 30, 2014</del></td><td><del>Submission <strong>deadline</strong> for long &amp; short papers at 23:59, GMT-12</del></td></tr>
<tr><td><del>Feb 25, 2014</del></td><td><del>Notification of acceptance</del></td></tr>
<tr><td><del>Mar 03, 2014</del></td><td><del>Camera-ready papers due | Deadline for travel grant applications</del></td></tr>
<tr><td>Apr 26-27, 2014</td><td>MWE 2014 Workshop</td></tr>
</table>


<h1>Call For Papers</h1>

<p>Under the denomination "multiword expression", one assumes a wide range of linguistic constructions such as idioms (<em>storm in a teacup, sweep under the rug</em>), fixed phrases (<em>in vitro, by and large, rock'n roll</em>), noun compounds (<em>olive oil, laser printer</em>), compound verbs (<em>take a nap, bring about</em>), etc. While easily mastered by native speakers, their interpretation poses a major challenge for computational systems, due to their flexible and heterogeneous nature.</p>

<p>For a start, MWEs are not nearly as frequent in NLP resources as they are in real-word text, and this problem of coverage may impact the performance of many NLP tasks. Moreover, treating MWEs also involves problems like determining their semantics, which is not always compositional (<em>to kick the bucket</em> meaning <em>to die</em>). In sum, MWEs are a key issue and a current weakness for natural language parsing and generation, as well as real-life applications depending on language technology, such as machine translation, just to name a prominent one among many.</p>

<p>Thanks to the joint efforts of researchers from several fields working on MWEs, significant progress has been made in recent years, especially concerning the construction of large-scale language resources. For instance, there is a large number of recent papers that focus on acquisition of MWEs from corpora, and others that describe a variety of techniques to find paraphrases for MWEs. Current methods use a plethora of tools such as association measures, machine learning, syntactic patterns, web queries, etc. A considerable body of techniques, resources and tools to perform these tasks are now available, and are indicative of the growing importance of the field within the NLP community.</p>

<p>Many of these advances are described as part of the annual workshop on MWEs, which attracts the attention of an ever-growing community working on a variety of languages and MWE types. The workshop has been held since 2001 in conjunction with major computational linguistics conferences (ACL, EACL, NAACL, COLING, LREC), providing an important venue for the community to interact, share resources and tools and collaborate on efforts for advancing the computational treatment of MWEs. Additionally, special issues on MWEs have been published by leading journals in computational linguistics. The latest such effort is the special issue on “Multiword Expressions: from Theory to Practice and Use”, which has recently been published by the <a href="http://multiword.sourceforge.net/tslp2011si">ACM Transactions on Speech and Language Processing</a>.</p>

<p>MWE 2014 will be the 10th event in the series. We will be interested in major challenges in the overall process of MWE treatment, both from the theoretical and the computational viewpoint, focusing on original research related (but not limited) to the following topics:</p>

<ul>
<li> Manually and automatically constructed resources </li>
<li> Representation of MWEs in dictionaries and ontologies </li>
<li> MWEs and user interaction </li>
<li> Multilingual acquisition </li>
<li> Multilingualism and MWE processing </li>
<li> Models of first and second language acquisition of MWEs </li>
<li> Crosslinguistic studies on MWEs </li>
<li> The role of MWEs in the domain adaptation of parsers </li>
<li> Integration of MWEs into NLP applications </li>
<li> Evaluation of MWE treatment techniques </li>
<li> Lexical, syntactic or semantic aspects of MWEs </li>
</ul>

<p>The workshop will feature a <strong>Special Track on Parsing and MWEs</strong> dedicated to “deep” parsing of MWEs, inviting submissions on the following and related challenges:

<ul>
<li> Lexicon-grammar interface: representing, at the lexical level, phenomena such as agreement, discontinuity and free word order; construction of MWE lexicons which would be easily convertible and maximally reusable in different parsing frameworks.</li>
<li> “Deep” parsing techniques for MWEs: optimal representation of MWEs within linguistic frameworks, such CCG, HPSG, LFG, TAG, minimalism, etc; processing MWEs before, during or after parsing; representing the semantics of MWEs.</li>
<li> Hybrid parsing of MWEs: combining data-driven and knowledge-based approaches for efficient and linguistically precise parsers; using unannotated data for improving models based on annotated data.</li>
<li> Annotating MWEs in treebanks: MWE-aware methodologies of treebank construction, and their increased usability in parsing.</li>
</ul>

<p>This special track is endorsed by <a href="http://www.parseme.eu/">PARSEME</a>, European IC1207 COST Action, dedicated to parsing and MWEs. A separate time slot will be allocated to the special track within the workshop. Authors may submit papers either to the special track or to the regular workshop. They should follow common submission instructions.</p>

<h1><a name="parseme_grants"></a>PARSEME travel grants</h1>

<p>PARSEME will fund travel and stay for at least 20 participants from the Action's member countries.  The deadline for grant applications is <strong>3 March 2014</strong>. <a href="http://typo.uni-konstanz.de/parseme/index.php/2-general/57-mwe-workshop-at-eacl-2014">Click here</a> for more information on the application procedure.</p>

<h1>Submission modalities</h1>

<p>For MWE 2014, we accept the following two types of submissions:</p>
<ul>
<li>Regular long papers (8 content pages + 1 page for references): Long papers should report on solid and finished research including new experimental results, resources and/or techniques.</li> 
<li>Regular short papers (4 content pages + 1 page for references): Short papers should report on small experiments, focused contributions, ongoing research, negative results and/or philosophical discussion.</li>
</ul>

<p>The reported research should be substantially original. The papers will be presented orally or as posters. The decision as to which papers will be presented orally and which as posters will be made by the program committee based on the nature rather than on the quality of the work. All submissions must be in PDF format and must follow the EACL 2014 formatting requirements (<a href="http://www.eacl2014.org/files/eacl-2014-styles.zip">download ZIP archive</a>). We strongly advise the use of the provided Word or LaTeX template files.</p>

<p>Reviewing will be double-blind, and thus no author information should be included in the papers; self-reference should be avoided as well. </p>

<p>Resources submitted with the papers should be anonymized for submission. Papers and/or resources that do not conform to these requirements will be rejected without review. Accepted papers will appear in the workshop proceedings, where no distinction will be made between papers presented orally or as posters. </p>

<p>Submission and reviewing will be electronic, managed by the START system:</p>
<p align="center"><a href="https://www.softconf.com/eacl2014/MWE/" target="_blank">https://www.softconf.com/eacl2014/MWE/</a></p>
<p>Submissions must be uploaded onto the START system by the submission deadline: <b>January 30th, 2014</b> (11:59pm, GMT-12).
Please choose the appropriate submission type from the starting submission page, according to the category of your paper.</p>


<h1>Workshop Organizers</h1>

<ul>
<li><a href="https://www.angl.hu-berlin.de/staff/kordoni" target="_blank">Valia Kordoni</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="https://www.angl.hu-berlin.de/staff/markus_egg" target="_blank">Markus Egg</a><em> (Humboldt Universität zu Berlin, Germany)</em></li>
<li><a href="http://www.info.univ-tours.fr/~savary/" target="_blank">Agata Savary</a><em>, special track organizer (Université François Rabelais Tours, France)</em></li>
<li><a href="http://www.latl.unige.ch/" target="_blank">Eric Wehrli</a><em>, special track organizer (Université de Genève, Switzerland)</em></li>
<li><a href="http://www.stefan-evert.de/" target="_blank">Stefan Evert</a><em> (Friedrich-Alexander-Universität Erlangen-Nürnberg, Germany)</em></li>
</ul>


<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mweworkshop.eacl2014@gmail.com">mweworkshop.eacl2014@gmail.com</a></p>

