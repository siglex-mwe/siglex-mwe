<h3><a href="http://tslp.acm.org/specialissues.html">ACM Transactions on Speech and Language Processing</a> Special Issue on</h3>

<h2>Multiword Expressions: from Theory to Practice and Use</h2>

<p><strong>Submission deadline EXTENDED:</strong><br/> <span style="text-decoration:line-through">May 15, 2012</span> <br/> <span style="text-decoration:line-through">June 12, 2012</span> <br/> SUBMISSIONS CLOSED</p>

<!--<p>Electronic submission: <a href="" target="_blank">https://www.softconf.com/acl2011/mwe/</a></p>-->

<p><small>Last updated: Aug 12, 2012</small></p>

<h1>Publication</h1>

<p>The special issue was published in two volumes and can be accessed at the ACM Digital Library:</p>

<ul>
<li><a href="http://dl.acm.org/citation.cfm?id=2483691"> Special issue on multiword expressions: From theory to practice and use, part 1. ACM Trans. Speech Lang. Process. 10, 2 (June 2013) </a></li>
<li><a href="http://dl.acm.org/citation.cfm?id=2483969"> Special issue on multiword expressions: From theory to practice and use, part 2. ACM Trans. Speech Lang. Process. 10, 3 (July 2013) </a></li>
</ul>

<h1>Call for Papers</h1>

<p>Are you submitting an article? Check the <a href="?sitesig=SPECIAL&page=SPECIAL_60_FAQ">Frequently Asked Questions</a> page</p>

<p>Multiword expressions (MWEs) range over linguistic constructions like idioms (<em>a frog in the throat, kill some time</em>), fixed phrases (<em>per se, by and large, rock'n roll</em>), noun compounds (<em>traffic light, cable car</em>), compound verbs (<em>draw a conclusion, go by [a name]</em>), etc. While easily mastered by native speakers, their interpretation poses a major challenge for computational systems, due to their flexible and heterogeneous nature. Surprisingly enough, MWEs are not nearly as frequent in NLP resources (dictionaries, grammars) as they are in real-word text, where they have been reported to account for half of the entries in the lexicon of a speaker and over 70% of the terms in a domain. Thus, MWEs are a key issue and a current weakness for tasks like natural language parsing and generation, as well as real-life applications such as machine translation.</p>

<p>In spite of several proposals for MWE representation ranging along the continuum from words-with-spaces to compositional approaches connecting lexicon and grammar, to date, it remains unclear how MWEs should be represented in electronic dictionaries, thesauri and grammars. New methodologies that take into account the type of MWE and its properties are needed for efficiently handling manually and/or automatically acquired expressions in NLP systems. Moreover, we also need strategies to represent deep attributes and semantic properties for these multiword entries. While there is no unique definition or classification of MWEs, most researchers agree on some major classes such as named entities, collocations, multiword terminology and verbal expressions. These, though, are very heterogeneous in terms of syntactic and semantic properties, and should thus be treated differently by applications. Type-dependent analyses could shed some light on the best methodologies to integrate MWE knowledge in our analysis and generation systems.</p>

<p>Evaluation is also a crucial aspect for MWE research. Various evaluation techniques have been proposed, from manual inspection of top-n candidates to classic precision/recall measures. The use of tools and datasets freely available on the <a href="multiword.sf.net/PHITE.php?sitesig=FILES">MWE community website</a> is encouraged when evaluating MWE treatment. However, application-oriented techniques are needed to give a clear indication of whether the acquired MWEs are really useful. Research on the impact of MWE handling in applications such as parsing, generation, information extraction, machine translation, summarization can help to answer these questions.</p>

<p>We call for papers that present research on theoretical and practical aspects of the computational treatment of MWEs, specifically focusing on MWEs in applications such as machine translation, information retrieval and question answering. We also strongly encourage submissions on processing MWEs in the language of social media and micro-blogs. The focus of the special issue, thus, includes, but is not limited to the following topics:</p>

<ul>
<li>MWE treatment in applications such as the ones mentioned above;</li>
<li>Lexical representation of MWEs in dictionaries and grammars;</li>
<li>Corpus-based identification and extraction of MWEs;</li>
<li>Application-oriented evaluation of MWE treatment;</li>
<li>Type-dependent analysis of MWEs;</li>
<li>Multilingual applications (e.g. machine translation, bilingual dictionaries);</li>
<li>Parsing and generation of MWEs, especially, processing of MWEs in the language of social media and micro-blogs;</li>
<li>MWEs and user interaction;</li>
<li>MWEs in linguistic theories like HPSG, LFG and minimalism and their contribution to applications;</li>
<li>Relevance of research on first and second language acquisition of MWEs for applications;</li>
<li>Crosslinguistic studies on MWEs.</li>
</ul>

<h1>Submission Procedure</h1>

<p>Authors should follow the ACM TSLP manuscript preparation guidelines described on the <a href="http://tslp.acm.org">journal web site</a> and submit an electronic copy of their complete manuscript through the <a href="http://mc.manuscriptcentral.com/acm/tslp">journal manuscript submission site</a>. Authors are required to specify that their submission is intended for this special issue by setting the submission type to &quot;Special Issue on Multiword Expressions&quot; on the first step of submission via the online system. Additionally, we recommend the inclusion of the note &quot;Submitted for the special issue on Multiword Expressions&quot; on the first page of the manuscript and in the field &quot;Author's Cover Letter&quot;.

</p>

<h1>Schedule</h1>

<table>
<tr><td>Submission deadline: EXTENDED </td><td> <span style="text-decoration:line-through"> May, 15, 2012</span> -&gt; <span style="text-decoration:line-through">June 12, 2012</span> SUBMISSIONS CLOSED </td></tr>
<tr><td>Notification of acceptance</td><td> <span style="text-decoration:line-through"> September, 15, 2012 -&gt; October 13, 2012</span>-&gt; <span style="text-decoration:line-through">October 20, 2012</span> NOTIFICATIONS SENT </td></tr>
<tr><td>Revised manuscript due</td><td> <span style="text-decoration:line-through">November, 30, 2012</span> -&gt; November 16, 2012 (minor revision), November 30 (major revision)</td></tr>
</table>

<h1>Program Committee</h1>

<ul>
<li>Iñaki Alegria, University of the Basque Country (Spain) </li>
<li>Dimitra Anastasiou, University of Bremen (Germany) </li>
<li>Eleftherios Avramidis, DFKI GmbH (Germany) </li>
<li>Tim Baldwin, University of Melbourne (Australia) </li>
<li>Chris Biemann, Technische Universität Darmstadt (Germany) </li>
<li>Francis Bond, Nanyang Technological University (Singapore) </li>
<li>Aoife Cahill, ETS (USA) </li>
<li>Helena Caseli, Federal University of Sao Carlos (Brazil) </li>
<li>Yu Tracy Chen, DFKI GmbH (Germany) </li>
<li>Paul Cook, University of Melbourne (Australia) </li>
<li>Ann Copestake, University of Cambridge (UK) </li>
<li>Béatrice Daille, Nantes University (France) </li>
<li>Gaël Dias, University of Caen Basse-Normandie (France) </li>
<li>Markus Egg, Humboldt-Universität zu Berlin (Germany) </li>
<li>Stefan Evert, University of Darmstadt (Germany) </li>
<li>Afsaneh Fazly, University of Toronto (Canada) </li>
<li>Joaquim Ferreira da Silva, New University of Lisbon (Portugal) </li>
<li>Dan Flickinger, Stanford University (USA) </li>
<li>Roxana Girju, University of Illinois at Urbana-Champaign (USA) </li>
<li>Chikara Hashimoto, National Institute of Information and Communications Technology (Japan) </li>
<li>Kyo Kageura, University of Tokyo (Japan) </li>
<li>Martin Kay, Stanford University and Saarland University (USA and Germany) </li>
<li>Su Nam Kim, University of Melbourne (Australia) </li>
<li>Philipp Koehn, University of Edinburgh (UK) </li>
<li>Ioannis Korkontzelos, University of Manchester (UK) </li>
<li>Brigitte Krenn, Austrian Research Institute for Artificial Intelligence (Austria) </li>
<li>Evita Linardaki, Hellenic Open University (Greece) </li>
<li>Takuya Matsuzaki, Tsujii Lab, University of Tokyo (Japan) </li>
<li>Yusuke Miyao, National Institute of Informatics (Japan) </li>
<li>Preslav Nakov, Qatar Computing Research Institute - Qatar Foundation (Qatar) </li>
<li>Diarmuid Ó Séaghdha, University of Cambridge (UK) </li>
<li>Jan Odijk, University of Utrecht (The Netherlands) </li>
<li>Pavel Pecina, Charles University (Czech Republic) </li>
<li>Scott Piao, Lancaster University (UK) </li>
<li>Thierry Poibeau, CNRS and École Normale Supérieure (France) </li>
<li>Magali Sanches Duran, University of São Paulo (Brazil) </li>
<li>Agata Savary, Université François Rabelais Tours (France) </li>
<li>Violeta Seretan, University of Geneva (Switzerland) </li>
<li>Ekaterina Shutova, University of Cambridge (UK) </li>
<li>Lucia Specia, University of Sheffield (UK) </li>
<li>Sara Stymne, Linköping University (Sweden) </li>
<li>Stan Szpakowicz, University of Ottawa (Canada) </li>
<li>Beata Trawinski, University of Vienna (Austria) </li>
<li>Yulia Tsvetkov, University of Haifa (Israel) </li>
<li>Kyioko Uchiyama, National Institute of Informatics (Japan) </li>
<li>Ruben Urizar, University of the Basque Country (Spain) </li>
<li>Gertjan van Noord, University of Groningen (The Netherlands) </li>
<li>Tony Veale, University College Dublin (Ireland) </li>
<li>David Vilar, DFKI GmbH (Germany) </li>
<li>Begoña Villada Moirón, Oracle (The Netherlands) </li>
<li>Tom Wasow, Stanford University (USA) </li>
<li>Shuly Wintner, University of Haifa (Israel) </li>
<li>Yi Zhang, DFKI GmbH and Saarland University (Germany) </li>
</ul>

<h1>Guest Editors</h1>

<ul>
<li>Valia Kordoni, Humboldt-Universität zu Berlin (Germany)</li>
<li>Carlos Ramisch, Joseph Fourier University (France) </li>
<li>Aline Villavicencio, Federal University of Rio Grande do Sul (Brazil) and Massachussets Institute of Technology (USA)</li>
</ul>


<h1>Contact</h1>

<p>For any inquiries regarding the special issue, please send an email to <a href="mailto:mweguesteditor at gmail.com">mweguesteditor at gmail.com</a></p>
