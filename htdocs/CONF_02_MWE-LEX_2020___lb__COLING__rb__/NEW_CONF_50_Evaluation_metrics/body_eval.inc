<h2>Evaluation metrics for the PARSEME shared task (edition 1.1)</h2>

<p>Participants are to provide the output produced by their systems on the (blind) test corpus. This output is compared with the gold standard (ground truth). Evaluation metrics are <strong>precision</strong> (P), <strong>recall</strong> (R) and <strong>F1</strong> (F) of two types:</p>
<ul>
<li>general metrics</li>
<li>metrics dedicated to specialized phenomena</li>
</ul>

<!--
<p>Cross-language systems, i.e. systems running for all languages in the release, are strongly encouraged. We will provide both <strong>per-language</strong> and <strong>cross-language</strong> rankings.</p>
<p>Systems focusing only on one or a few languages, or on one VMWE category, are also encouraged to participate.</p>

<p>We illustrate the evaluation measures on examples of 3 files: <a href="">sample-train.cupt</a> (gold training file), <a href="">sample-test-gold.cupt</a> (gold test file) and  <a href="">sample-test-pred.cupt</a> (test file with system predictions).</p>
-->

<!----------------------->
<h1><a name="general-metrics"></a>General metrics</h1>

The general metrics are defined along two dimensions, like in <a href="http://multiword.sourceforge.net/PHITE.php?sitesig=CONF&page=CONF_05_MWE_2017___lb__EACL__rb__&subpage=CONF_50_Shared_Task_Results">edition 1.0</a> of the PARSEME shared task:
<ul>
<li><u>strict</u> score (per-VMWE) vs. <u>fuzzy</u> score (per-token, i.e. taking partial matches into account), for more details, see the <a href="http://aclweb.org/anthology/W/W17/W17-1704.pdf" target="_blank">description paper of the PARSEME shared task edition 1.0</a>, section 6</li>
<!-- <li> score for all categories vs. per-category scores</li> -->
<li>score for <u>identification</u> (disregarding VMWE categories) vs. <u>per-category</u> scores</li>
</ul>
<p>These scores are calculated both per language and for all participating languages (as a <a href="#macro-average">macro-average</a>).</p>
<!--
Note that, contrary to edition 1.1 of the PARSEME shared task, systems are now expected to perform both the <strong>identification</strong> and the <strong>categorization</strong> of VMWEs. They are ranked according to their performances in these two tasks jointly. In other words, a VMWE which is correctly identified but wrongly categorized counts as an error.
-->
<!----------------------->
<h1><a name="dedicated-metrics"></a>Metrics dedicated to specialized phenomena</h1>

<p>For a better account of the challenges that VMWE identifiers have to face, we introduce metrics specialized in the following phenomena:</p>
<ul>
<li><strong>Continuity</strong> - we provide P/R/F scores separately for those VMWEs whose lexicalized components are <u>adjacent</u> (<em><strong>set up</strong> a meeting</em>) and <u>non-adjacent</u> (<em><strong>set</strong> me <strong>up</strong></em>) in the test corpus</li>
<li><strong>Length</strong> - we provide P/R/F scores separately for <u>multi-token</u> VMWEs (<em>ES: <strong>me abstengo</strong></em>; DE: <strong>macht</strong> es <strong>auf</strong>) and for  <u>single-token</u> ones (<em>ES: <strong>abstenerse</strong></em>; DE: <strong>aufmachen</strong>)</li>
<li><strong>Novelty</strong> - we provide P/R/F scores separately for <u>seen</u> and <u>unseen</u> VMWEs. A VMWE from the test corpus is considered seen if a VMWE with the same (multi-)set of lemmas is annotated at least once in the training corpus. For instance, given the occurrence of <em><strong>has</strong> a new <strong>look</strong></em> in the training corpus, the following VMWEs from the test corpus would be considered:
	<ul>
	<li>seen: <em><strong>has</strong> a new <strong>look</strong></em>, <em><strong>had</strong> an appealing <strong>look</strong></em>, <em><strong>has</strong> a <strong>look</strong> of innocence</em>, <em>the <strong>look</strong> that he <strong>had</strong></em></li>
	<li>unseen: <em><strong>has a look</strong> at this report</em>, <em><strong>gave</strong> a <strong>look</strong> to the book</em>, <em><strong>walk</strong> that he <strong>had</strong></em>, etc.
	</ul>
<li><strong>Variability</strong> - we additionally provide P/R/F scores for <u>variants</u>, i.e. those <u>seen</u> VMWEs which are <u>not identical</u> to VMWE occurrences from the training corpus. VMWE occurrences are considered identical if the strings between their first and last lexicalized components, including non-lexicalized elements in between, are identical. All the seen examples above are considered variants except <em><strong>has</strong> a new <strong>look</strong></em>.
</ul> 

<p>These metrics are not calculated per language but for all participating languages (as a <a href="#macro-average">macro-average</a>). They are per-VMWE only and do not distinguish VMWE categories. Per-token scores are not provided due to their complex interplay with the above phenomena. Per-language and per-category scores are not provided due to data scarcity.</p>

<!----------------------->
<h1><a name="macro-average"></a>Macro-average scores</h1>

Additionally to per-language scores for general metrics, we provide both <a href="#general-metrics">general</a> and <a href="#dedicated-metrics">phenomenon-dedicated</a> scores for all participating languages. They are calculated, as macro-averages, in the following way:
<ul>
<li>F-scores obtained by a system for each participating language are (arithmetically) averaged.</li>
<li>If a system provides no results for a given language, its F-score for this language is considered as equal to 0.</li>
<li>If a language has no VMWE corresponding to a given phenomenon (e.g. no single-token VMWEs), we do not include this language it the ranking.</li>
<!--consider that for each system the scores for this language are R=1, P=0 and F=0.</li>-->
</ul>

<!----------------------->
<h1><a name="rankings"></a>Rankings</h1>
We plan to publish rankings of the participating systems according to all F-scores defined above. These rankings should, however, be interpreted with care and should not be considered primary outcomes of the shared task. We are interested in promoting cross-language discussions more than a real competition.

<!----------------------->
<h1><a name="eval-script"></a>Evaluation scripts</h1>

<p>The evaluation script "evaluate.py" and the libraries required to run it are available in our <a href="https://gitlab.com/parseme/sharedtask-data/tree/master/1.1/bin/">public data repository</a>. It takes as input two variants of the same test file - the gold standard and the prediction - in the <a href="http://multiword.sourceforge.net/PHITE.php?sitesig=CONF&page=CONF_04_LAW-MWE-CxG_2018___lb__COLING__rb__&subpage=CONF_45_Format_specification">.cupt format</a>. If you also indicate the training corpus, the script can the calculcate novelty- and variability-dedicated metrics described above.</p>

<p>Only columns 1, 2 and 11 are relevant for the global metrics, while columns 1, 2, 3 (lemma) and 11 are relevant for the dedicated metrics. All other columns are ignored by the script. The script can be used as follows (<tt>--train</tt> is optional):</p>

<code> ./evaluate.py --gold gold.cupt --pred system.cupt --train train.cupt</code>
	
<p>For details of the evaluation, run the script with the <span style="text-indent: 50px; font-family: Courier New;">--debug</span> option, preferably in conjunction with <tt>less</tt>:</p>

<code> ./evaluate.py --gold gold.cupt --pred system.cupt  --train train.cupt --debug | less -RS</code>

<p>In case of errors, the evaluation script will indicate the line of the input file in which the error occurred. Please notice that the error may actually be located in the previous or next sentence, because the script reads sentences one by one before processing them.</p>

<p>In addition to the individual evaluation script for a single language, we also provide a script to calculate macro-averages called <a href="https://gitlab.com/parseme/sharedtask-data/tree/master/1.1/bin/average_of_evaluations.py">average_of_evaluations.py</a>. This script takes as input several files generated by the evaluation script (you can redirect the output) and generates averages for all metrics. For instance, you can run the following commands to calculate the macro-averaged scores between Bulgarian (BG) and German (DE):</p>

<code>./evaluate.py --gold BG/dev.cupt --pred BG/system.cupt  --train BG/train.cupt > BG/eval.txt <br/>
./evaluate.py --gold DE/dev.cupt --pred DE/system.cupt  --train DE/train.cupt > DE/eval.txt <br/>
./average_of_evaluations.py BG/eval.txt DE/eval.txt</code>

<p>The rankings using macro-averaged scores over all languages will be calculated using this script.</p>





