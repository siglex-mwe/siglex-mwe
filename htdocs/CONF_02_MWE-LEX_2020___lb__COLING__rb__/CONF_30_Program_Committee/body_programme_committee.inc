<h2>Joint Workshop on Multiword Expressions and Electronic Lexicons (MWE-LEX 2020)</h2>

<h3>Workshop at <a href="http://coling2020.org/" target="_blank">COLING 2020</a> (<del>Barcelona, Spain</del> online), <del>September 14</del> December 13, 2020.</h3>

<p>Organized and sponsored by: <br/>
Special Interest Group on the Lexicon (<a href="http://www.siglex.org">SIGLEX</a>) of the Association for Computational Linguistics (<a href="https://www.aclweb.org/portal/">ACL</a>) <br/>
<a href="https://elex.is/">ELEXIS</a> - European Lexicographic Infrastructure. <br/><br/>
This joint event is the 16th edition of the <strong>Workshop on Multiword Expressions (<a href="?sitesig=CONF">MWE</a>)</strong>.
<br/>
<a href="https://twitter.com/mwe_workshop?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-count="false">Follow @mwe_workshop</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>

<p><small>Last updated: Nov 15, 2020</small></p>


<!------------------------------------------------------------------------------>

<h1>Tracks and PC-chairs</h1>
<ul>
<li><strong>Research track, MWE-LEX topics</strong>: <a href="http://john.mccr.ae/" target="_blank">John McCrae</a>, National University of Ireland Galway (Ireland); Carole Tiberius, Dutch Language Institute in Leiden (Netherlands)</li>
<li><strong>Research track, MWE-specific topics</strong>: <a href="http://www.ilsp.gr/en/profile/staff?view=member&task=show&id=38" target="_blank">Stella Markantonatou</a>, Institute for Language and Speech Processing, R.C. "Athena" (Greece); <a href="https://bit.ly/2NhL2lz" target="_blank">Jelena Mitrović</a>, University of Passau (Germany)</li>
<li><strong>Shared task track</strong>: <a href="http://pageperso.lif.univ-mrs.fr/~carlos.ramisch/" target="_blank">Carlos Ramisch</a>, Aix-Marseille University (France); <a href="http://web.iitd.ernet.in/~avaidya/" target="_blank">Ashwini Vaidya</a>, Indian Institute of Technology in Delhi (India)</li>
</ul>

<!------------------------------------------------------------------------------>

<h1>Publication Chairs</h1>
<ul>
<li><a href="http://bultreebank.org/en/our-team/petya-osenova/" target="_blank">Petya Osenova</a>, University of Sofia and Bulgarian Academy of Sciences (Bulgaria) </li>
<li><a href="http://www.info.univ-tours.fr/~savary/" target="_blank">Agata Savary</a>, Université of Tours (France) </li>
</ul>

<!------------------------------------------------------------------------------>


<h1>Program Committee</h1>

<ul>
<li>Tim Baldwin, University of Melbourne (Australia) </li>
<li>Verginica Barbu Mititelu, Romanian Academy (Romania) </li>
<li>Archna Bhatia, Florida Institute for Human and Machine Cognition (USA) </li>
<li>Francis Bond, Nanyang Technological University (Singapore) </li>
<li>Tiberiu Boroș, Adobe (Romania) </li>
<li>Marie Candito, Paris Diderot University (France) </li>
<!-- <li>Fabienne Cap, Uppsala University (Sweden) </li> -->
<li>Helena Caseli, Federal University of Sao Carlos (Brazil) </li>
<li>Anastasia Christofidou, Academy of Athens (Greece) </li>
<li>Ken Church, IBM Research (USA) </li>
<li>Matthieu Constant, Université de Lorraine (France) </li>
<li>Paul Cook, University of New Brunswick (Canada) </li>
<li>Monika Czerepowicka, University of Warmia and Mazury (Poland) </li>
<li>Béatrice Daille, Nantes University (France) </li>
<li>Gerard de Melo, Rutgers University (USA) </li>
<li>Thierry Declerck, DFKI (Germany) </li>
<li>Gaël Dias, University of Caen Basse-Normandie (France) </li>
<!--<li>Gülşen Eryiğit, Istanbul Technical University (Turkey) </li>-->
<li>Meghdad Farahmand, University of Geneva (Switzerland) </li>
<li>Christiane Fellbaum, Princeton University (USA) </li>
<li>Joaquim Ferreira da Silva, New University of Lisbon (Portugal) </li>
<li>Aggeliki Fotopoulou, ILSP/RC "Athena" (Greece) </li>
<li>Francesca Frontini, Université Paul-Valéry Montpellier (France) </li>
<li>Marcos Garcia, CITIC (Spain) </li>
<li>Voula Giouli, Institute for Language and Speech Processing (Greece) </li>
<li>Chikara Hashimoto, Yahoo!Japan (Japan) </li>
<li>Kyo Kageura, University of Tokyo (Japan) </li>
<li>Diptesh Kanojia, IITB-Monash Research Academy (India)</li>
<li>Dimitris Kokkinakis, University of Gothenburg (Sweden) </li>
<li>Ioannis Korkontzelos, Edge Hill University (UK) </li>
<li>Iztok Kosem, Jožef Stefan” Institute (Slovenia) </li>
<li>Cvetana Krstev, University of Belgrade (Serbia) </li>
<li>Malhar Kulkarni, Indian Institute of Technology, Bombay (India) </li>
<li>Eric Laporte, University Paris-Est Marne-la-Vallee (France) </li>
<li>Timm Lichte, University of Duesseldorf (Germany) </li>
<li>Irina Lobzhanidze, Ilia State University (Georgia) </li>
<li>Ismail el Maarouf, Adarga Ltd (UK) </li>
<li>Yuji Matsumoto, Nara Institute of Science and Technology (Japan) </li>
<li>Nurit Melnik, The Open University of Israel (Israel) </li>
<li>Elena Montiel-Ponsoda, Universidad Politecnica de Madrid (Spain) </li>
<!--- <li>Preslav Nakov, Qatar Computing Research Institute, HBKU (Qatar) </li> --->
<li>Sanni Nimb, Det Denske Sprog- og Litteraturselskab (Denmark) </li>
<li>Haris Papageorgiou, Institute for Language and Speech Processing (Greece) </li>
<li>Carla Parra Escartín, Unbabel (Portugal) </li>
<!--<li>Agnieszka Patejuk, University of Oxford and Institute of Computer Science, Polish Academy of Sciences (UK and Poland) </li>-->
<li>Marie-Sophie Pausé, independent researcher (France) </li>
<li>Pavel Pecina, Charles University (Czech Republic) </li>
<li>Scott Piao, Lancaster University (UK) </li>
<li>Alain Polguère, Université de Lorraine (France) </li>
<li>Alexandre Rademaker, IBM Research Brazil and EMAp/FGV (Brazil) </li>
<li>Laurent Romary, INRIA & HUB-ISDL (France) </li>
<li>Mike Rosner, University of Malta (Malta) </li>
<li>Manfred Sailer, Goethe-Universität Frankfurt am Main (Germany) </li>
<li>Magali Sanches Duran, University of São Paulo (Brazil) </li>
<!--- <li>Federico Sangati, Independent researcher (Italy) </li> --->
<li>Nathan Schneider, Georgetown University (USA) </li>
<li>Sabine Schulte im Walde, University of Stuttgart (Germany) </li>
<li>Kiril Simov, Bulgarian Academy of Sciences (Bulgaria)</li>
<li>Ranka Stanković, University of Belgrade (Serbia) </li>
<li>Ivelina Stoyanova, Bulgarian Academy of Sciences (Bulgaria) </li>
<li>Stan Szpakowicz, University of Ottawa (Canada) </li>
<li>Shiva Taslimipoor, University of Wolverhampton (UK) </li>
<li>Arvi Tavast, Qlaara, Tallinn (Estonia) </li>
<li>Beata Trawinski, Institut für Deutsche Sprache Mannheim (Germany) </li>
<li>Zdeňka Urešová, Charles University (Czech Republic) </li>
<li>Ruben Urizar, University of the Basque Country (Spain) </li>
<li>Lonneke van der Plas, University of Malta (Malta) </li>
<li>Veronika Vincze, Hungarian Academy of Sciences (Hungary) </li>
<li>Jakub Waszczuk, University of Duesseldorf (Germany) </li>
<li>Eric Wehrli, University of Geneva (Switzerland) </li>
<li>Seid Muhie Yimam, Universität Hamburg (Germany) </li>
</ul>


<!--
<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwe2017workshop at gmail.com">mwe2017workshop at gmail.com</a></p>
-->
