<h2>Joint Workshop on Multiword Expressions and Electronic Lexicons (MWE-LEX 2020)</h2>

<h3>Workshop at <a href="http://coling2020.org/" target="_blank">COLING 2020</a> (<del>Barcelona, Spain</del> online), <del>September 14</del> December 13, 2020.</h3>

<p>Organized and sponsored by: <br/>
Special Interest Group on the Lexicon (<a href="http://www.siglex.org">SIGLEX</a>) of the Association for Computational Linguistics (<a href="https://www.aclweb.org/portal/">ACL</a>) <br/>
<a href="https://elex.is/">ELEXIS</a> - European Lexicographic Infrastructure. <br/><br/>
This joint event is the 16th edition of the <strong>Workshop on Multiword Expressions (<a href="?sitesig=CONF">MWE</a>)</strong>.
<br/>
<a href="https://twitter.com/mwe_workshop?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-count="false">Follow @mwe_workshop</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>

<p><small>Last updated: Dec 14, 2020</small></p>


<table>
  <!--<tr>
  <td height="120" valign="top"><a href="mwelex2020/photos/mwelex1.png"><img src="mwelex2020/photos/mwelex1.png" height="120" /></a></td>
  <td height="120" valign="top"><a href="mwelex2020/photos/mwelex2.png"><img src="mwelex2020/photos/mwelex2.png" height="120" /></a></td>
  <td height="120" valign="top"><a href="mwelex2020/photos/mwelex3.png"><img src="mwelex2020/photos/mwelex3.png" height="120" /></a></td>
  <td height="120" valign="top"><a href="mwelex2020/photos/mwelex4.png"><img src="mwelex2020/photos/mwelex4.png" height="120" /></a></td>
  <td height="120" valign="top"><a href="mwelex2020/photos/mwelex5.png"><img src="mwelex2020/photos/mwelex5.png" height="120" /></a></td>
  <td height="120" valign="top"><a href="mwelex2020/photos/mwelex6.png"><img src="mwelex2020/photos/mwelex6.png" height="120" /></a></td>
  </tr>-->
  <tr>
  <td height="300" colspan="6" valign="top" align="center"><a href="mwelex2020/photos/mwelex2020group.png"><img src="mwelex2020/photos/mwelex2020group.png" height="300" /></a></td>
</tr>
</table>

<!------------------------------------------------------------------------------>

<ul>
<li><strong> [Dec 14, 2020]</strong> Thanks to all those contributing to make MWE-LEX 2020 a success! See you next year in Bangkok (fingers crossed)!</li>
<li><strong> [Dec 02, 2020]</strong> <a href="https://www.aclweb.org/anthology/volumes/2020.mwe-1/">MWE-LEX proceedings</a> are now available on the ACL Anthology! Many thanks to our publication chairs Petya Osenova and Agata Savary, and to all authors and reviewers.</li>
<li><strong> [Dec 02, 2020]</strong> Videos for all MWE-LEX talks are now available on Underline for registered <a href="https://coling2020.org/">COLING2020</a> participants: <a href="https://underline.io/events/54/">https://underline.io/events/54/</a></li>
<li><strong> [Nov 15, 2020]</strong> The <a href="?sitesig=CONF&page=CONF_02_MWE-LEX_2020___lb__COLING__rb__&subpage=CONF_20_Program">program</a> of the workshop has arrived. .</li>
<li><strong> [Nov 15, 2020]</strong> We're happy to announce that Roberto Navigli will be our invited speaker. See the <a href="?sitesig=CONF&page=CONF_02_MWE-LEX_2020___lb__COLING__rb__&subpage=CONF_10_Keynote_Speaker">abstract of his talk</a>.</li>
<li><strong> [Oct 06, 2020]</strong> We invite authors of accepted papers at <a href="https://2020.emnlp.org/blog/2020-04-19-findings-of-emnlp">"Findings of EMNLP"</a> to present their work at our workshop (details below).</li>
<li><strong> [Aug 31, 2020]</strong> The MWE-LEX workshop paper submission deadline has been extended to <strong>September 9, 23:59</strong> (anywhere in the world).</li>
<li><strong> [Aug 24, 2020]</strong> <a href="https://coling2020.org/2020/08/14/virtual-conference.html" target="_blank">COLING 2020</a> is going entirely virtual. So is the MWE-LEX workshop! More details will follow.</li>
<li><strong> [Aug 07, 2020]</strong> We're on <a href="https://twitter.com/mwe_workshop">Twitter</a> :-)</li>

<li><strong>[Apr 06, 2020]</strong> <a href="https://coling2020.org/2020/03/30/covid-19.html">COLING 2020 has been postponed</a>, the workshop will take place on December 13, 2020 in Barcelona.</li>
<li><strong>[Feb 12, 2020]</strong> The <a href="https://www.softconf.com/coling2020/MWE-LEX/">START space</a> for submitting a paper is now open.</li>
</ul>

<!------------------------------------------------------------------------------>

<h1>Description</h1>

<p>The joint MWE-LEX workshop addresses two domains – multiword expressions and (electronic) lexicons – with partly overlapping communities and research interests, but divergent practices and terminologies.</p>

<p>Multiword expressions (MWEs) are word combinations, such as <i>by and large</i>, <i>hot dog</i>, <i>pay a visit</i> or <i>pull one's leg</i>, which exhibit lexical, syntactic, semantic, pragmatic or statistical idiosyncrasies. MWEs encompass closely related linguistic objects: idioms, compounds, light-verb constructions, rhetorical figures, institutionalised phrases and collocations. Because of their unpredictable behavior, notably their non-compositional semantics, MWEs pose problems in linguistic modelling (e.g. treebank annotation, grammar engineering), NLP pipelines (notably when orchestrated with parsing), and end-user applications (e.g. information extraction). Modelling and processing of MWEs has been the topic of the <a href="?sitesig=CONF" target="_blank">MWE workshop</a>, organised over the past years by the <a href="http://multiword.sourceforge.net/PHITE.php?sitesig=MWE" target="_blank">MWE section</a> of <a href="http://www.siglex.org" target="_blank">SIGLEX</a>.</p>

<p>Because MWE-hood is a largely lexical phenomenon, appropriately built electronic MWE lexicons turn out to be quite important for NLP. Their conception opens up, among others, the issues of lemmatization and of standardised representation of morphological, syntactic and semantic properties of MWEs. Large standardised multilingual, possibly interconnected, NLP-oriented MWE lexicons prove indispensable for NLP tasks such as MWE identification, due to its critical sensitivity to unseen data. But the development of such lexicons is challenging and calls for tools which would leverage, on the one hand, MWEs encoded in pre-existing NLP-unaware lexicons and, on the other hand, automatic MWE discovery in large non-annotated corpora.</p>

<p>In order to pave the way towards a better understanding of these issues, and to foster convergence and scientific innovation, the MWE and ELEXIS (European Union's Horizon 2020 research grant 731015) communities put forward a joint event. We call for papers on research related (but not limited) to:</p>

<h3>Joint topics on MWEs and e-lexicons</h3>
<ul>
<li>Extracting and enriching MWE lists from traditional human-readable lexicons for NLP use</li>
<li>Formats for NLP-applicable MWE lexicons</li>
<li>Interlinking MWE lexicons with other language resources</li>
<li>Using MWE lexicons in NLP tasks (identification, parsing, translation, ...)</li>
<li>MWE discovery in the service of lexicography</li>
<li>Multiword terms in specialized lexicons</li>
<li>Representing semantic properties of MWEs in lexicons</li>
<li>Paving the way towards encoding lexical idiosyncrasies in constructions</li>
</ul>

<h3>MWE-specific topics</h3>
<ul>
<li>Computationally-applicable theoretical work on MWEs and constructions in psycholinguistics, corpus linguistics and formal grammars</li>
<li>MWE and construction annotation in corpora and treebanks</li>
<li>Processing of MWEs and constructions in syntactic and semantic frameworks (e.g. CCG, CxG, HPSG, LFG, TAG, UD, etc.), and in end-user applications (e.g. information extraction, machine translation and summarization)</li>
<li>Original discovery and identification methods for MWEs and constructions</li>
<li>MWEs and constructions in language acquisition and in non-standard language (e.g. tweets, forums, spontaneous speech)</li>
<li>Evaluation of annotation and processing techniques for MWEs and constructions</li>
<li>Retrospective comparative analyses from the PARSEME shared tasks on automatic identification of MWEs</li>
</ul>

<p>Our intention is to also perpetuate previous converging effects with the Construction Grammar and WordNet community (see the <a href="http://multiword.sourceforge.net/lawmwecxg2018">LAW-MWE-CxG 2018</a> and <a href="http://multiword.sourceforge.net/mwewn2019">MWE-WN 2019 workshops</a>). Therefore, and we extend the traditional MWE scope to grammatical constructions and we include WordNets in the scope of e-lexicons.</p>

<h1>Findings of EMNLP 2020</h1>

<p>The MWE-LEX 2020 workshop is inviting authors of accepted papers at <a href="https://2020.emnlp.org/blog/2020-04-19-findings-of-emnlp">"Findings of EMNLP"</a> to present their work at our workshop.
To submit your paper for a presentation slot, please send an email to <a href="mailto:mwelex2020@gmail.com">mwelex2020@gmail.com</a> by Friday October 9, 2020 with:</p>

  <ul>
  <li>Your paper</li>
  <li>One or two sentences explaining why it would be a good fit for the scope of MWE-LEX 2020</li>
  </ul>

<!------------------------------------->
<h1>Special Track: PARSEME Shared Task on Semi-Supervised verbal MWE Identification</h1>

<p>MWE-LEX 2020 will host <a href="http://multiword.sourceforge.net/sharedtask2020">edition 1.2 of the PARSEME shared task</a> on semi-supervised identification of verbal MWEs. This is a follow-up of editions <a href="http://multiword.sourceforge.net/sharedtask2017">1.0</a> (2017), and <a href="http://multiword.sourceforge.net/sharedtask2017">1.1</a> (2018). Edition 1.2 will feature (a) improved and extended corpora annotated with MWEs, (b) complementary unannotated corpora for unsupervised MWE discovery, and (c) a new evaluation methodology focusing on unseen MWEs. Following the synergy with Elexis, our aim is to foster the development of unsupervised methods for MWE lexicon induction, which in turn can be used for identification. Authors may submit system description papers to a special track. Details are available on the <a href="http://multiword.sf.net/sharedtask2020">shared task 1.2 page</a>.</p>

<!------------------------------------------------------------------------------>

<h1 id="modalities"><a name="submissions"></a>Submission modalities</h1>

<p><strong>Regular research track</strong>:</p>
<ul>
<li>Long papers (9 content pages + references): They should report on solid and finished research including new experimental results, resources and/or techniques.</li>
<li>Short papers (4 content pages + references): They should report on small experiments, focused contributions, ongoing research, negative results and/or philosophical discussion.</li>
</ul>

<p>In regular research papers, the reported research should be substantially original. Papers available as preprints can also be submitted provided that they fulfil the conditions defined by the <a href="https://www.aclweb.org/portal/content/new-policies-submission-review-and-citation" target="_blank">ACL Policies for Submission, Review and Citation</a>. </p>

<p><strong>Shared task track</strong>:</p>

<ul>
<li>System description papers (4 content pages + references): These papers should briefly describe the approach implemented to solve the problem. They may include references and links to more detailed descriptions in other documents.</li>
</ul>

<p>Shared task system description papers will go through a separate reviewing process. Submissions will be reviewed by the shared task organizers and participants. Participants of the shared task are not required to submit system description papers, and their acceptance depends on the quality of the paper rather than on the results obtained in the shared task.</p>

<p><strong>Instructions for authors</strong>:</p>

<p>For all 3 types of papers, the submission is <strong>double-blind</strong> as per the <a href="https://coling2020.org/pages/call_for_papers">COLING 2020 guidelines</a>. For all types of submission, the COLING 2020 <a href="https://coling2020.org/coling2020.zip">templates</a> must be used. There is no limit on the number of reference pages.</p> <!-- Authors will be granted an extra page for the final version of their papers.</p>-->

<p>The decisions as to oral or poster presentations of the selected papers will be taken by the PC chairs, depending on the available infrastructure for virtual participation. No distinction between papers presented orally and as posters is made in the workshop proceedings.</p>

<p>All papers should be submitted via the following  <a href="https://www.softconf.com/coling2020/MWE-LEX/">START space</a>. Please choose the appropriate track (research/shared task) and submission modality (long/short). </p>

<!------------------------------------------------------------------------------>

<h1>Important dates</h1>

<p>All deadlines are at 23:59 UTC-12 (anywhere in the world).</p>

<table>
<tr><td><del>May 20</del> <strong><del>Sep 2</del> Sep 9, 2020</strong>: <strong>extended</strong> paper submission deadline</td></tr>
<tr><td><del>Jun 24</del> Oct 16, 2020: notification of acceptance</td></tr>
<tr><td><del>Jul 11</del> Nov 1, 2020: camera-ready papers due</td></tr>
<tr><td><del>Sep 14</del> Dec 13: MWE-LEX workshop dates</td></tr>
</table>

<p>See also the important dates for the <a href="http://multiword.sourceforge.net/sharedtask2020" target="_blank">shared task</a> systems.</p>

<!------------------------------------------------------------------------------>

<h1>Program Committee Chairs</h1>
<ul>
<li>Research track, MWE-specific topics:
   <ul>
   <li><a href="http://www.ilsp.gr/en/profile/staff?view=member&task=show&id=38" target="_blank">Stella Markantonatou</a>, Institute for Language and Speech Processing, R.C. "Athena" (Greece)</li>
   <li><a href="http://jelena.mitrovic.rs" target="_blank">Jelena Mitrović</a>, University of Passau (Germany)</li>
   </ul>
</li>
<li>Research track, MWE-LEX topics:
   <ul>
   <li><a href="http://john.mccr.ae/" target="_blank">John McCrae</a>, National University of Ireland Galway (Ireland)</li>
   <li>Carole Tiberius, Dutch Language Institute in Leiden (Netherlands)</li>
   </ul>
</li>
<li>Shared task track:
   <ul>
   <li><a href="http://pageperso.lis-lab.fr/~carlos.ramisch/" target="_blank">Carlos Ramisch</a>, Aix Marseille University (France)</li>
   <li><a href="http://web.iitd.ernet.in/~avaidya/" target="_blank">Ashwini Vaidya</a>, Indian Institute of Technology in Delhi (India)</li>
   </ul>
</li>
</ul>

<!------------------------------------------------------------------------------>

<h1>Publication Chairs</h1>
<ul>
<li><a href="http://bultreebank.org/en/our-team/petya-osenova/" target="_blank">Petya Osenova</a>, University of Sofia and Bulgarian Academy of Sciences (Bulgaria) </li>
<li><a href="http://www.info.univ-tours.fr/~savary/" target="_blank">Agata Savary</a>, Université of Tours (France) </li>
</ul>

<!------------------------------------------------------------------------------>

<h1>Contact</h1>
<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwelex2020@gmail.com">mwelex2020@gmail.com</a></p>

<!------------------------------------------------------------------------------>

<h1>Anti-harassment policy</h1>
<p>The workshop supports the ACL <a href="https://www.aclweb.org/adminwiki/index.php?title=Anti-Harassment_Policy" target=_blanc">anti-harassment policy</a>.</p>
