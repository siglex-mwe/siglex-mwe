<h2>Joint Workshop on Linguistic Annotation, Multiword Expressions and Constructions (LAW-MWE-CxG-2018)</h2>

<!-- <h3>Workshop proposal for <a href="http://coling2018.org/" target="_blank">COLING 2018</a> (Santa Fé, USA) or <a href="http://acl2018.org/" target="_blank">ACL 2018</a> (Melbourne, Australia)</h3> -->

<h3>Workshop at <a href="http://coling2018.org/" target="_blank">COLING 2018</a> (Santa Fe, USA), August 25-26, 2018</h3>

<p>Organized and funded by the Special Interest Group for Annotation (<a href="https://www.cs.vassar.edu/sigann/" target="_blank">SIGANN</a>), the Special Interest Group on the Lexicon (<a href="http://www.siglex.org" target="_blank">SIGLEX</a>), and the Special Interest Group on Computational Semantics (<a href="http://www.sigsem.org" target="_blank">SIGSEM</a>) of the Association for Computational Linguistics (<a href="https://www.aclweb.org/portal/" target="_blank">ACL</a>)</p>

<p>This joint event brings together:</p>
<ul style="margin-top:-1em;">
<li><strong>The Twelfth Linguistic Annotation Workshop (<a href="https://www.cs.vassar.edu/sigann/" target="_blank">LAW XII</a>)</strong>, and</li>
<li><strong>The 14th Workshop on Multiword Expressions (<a href="?sitesig=CONF" target="_blank">MWE</a>)</strong>.</li>
</ul>

<p><small>Last updated: Aug 13, 2018</small></p>

<h1>Proceedings</h1>
The <a href="https://aclanthology.coli.uni-saarland.de/events/ws-2018/#W18-49" target="_blank">LAW-MWE-CxG proceedings</a> are available on the ACL Anthology.

<h1>Selected papers</h1>

<p>34 papers (22 long and 12 short) were submitted to the <strong>research track</strong> of the workshop. 
16 long papers and 6 short ones were selected, and 1 paper was withdrawn. 
<!-- 7 papers (4 short and 3 long) were selected as oral presentations and 14 (11 short and 3 long) as posters.--> 
The overall <strong>selectivity rate is 65%</strong>.</br >
8 system description papers were submitted to the <strong>shared task track</strong>. All were selected. The reviewing modalities were different in this track, therefore we do not count these papers in the workshop selectivity rate.</p>

<!--
<center>
<table cellspacing="0" cellpadding="0" border="0">
	<tr>
                <td colspan="3"><a href="index.html">ACL 2014 Proceedings Home</a> | <a href="http://acl2014.org">ACL 2014 WEBSITE</a> | <a href="http://www.aclweb.org/">ACL WEBSITE</a></td>
	</tr>
</table>
<br><br>
-->

<!---------------------------------->
<h1>Program</h1>
<table cellspacing="0" cellpadding="3" border="0"><tr>
<td colspan=2 style="padding-top: 0px;"><h3>Saturday, August 25, 2018 (Peralta room)</h3></td></tr>

<!------------------------------------------------>
<tr><td valign=top style="padding-top: 2px;">8:55&#8211;09:00</td><td valign=top style="padding-top: 2px;"><em>Opening</em></td></tr>

<tr><td valign=top style="padding-top: 14px;">&nbsp;</td><td valign=top style="padding-top: 14px;"><b>Session 1: Multiword expressions</b> (chair: Carlos Ramisch)</td></tr>
<tr><td valign=top width=100>09:00&#8211;10:00</td>
<td valign=top align=left><strong>Invited talk: <i><a href="PHITE.php?sitesig=CONF&page=CONF_04_LAW-MWE-CxG_2018___lb__COLING__rb__&subpage=CONF_25_Keynote_Speakers#keynote-nathan">Leaving no token behind: comprehensive (and delicious) annotation of MWEs and supersenses</a></i></strong><br>
<strong>Nathan Schneider</strong></td></tr>
<tr><td valign=top width=100>10:00&#8211;10:30</td>
<td valign=top align=left><i><a href="lawmwecxg2018/slides/boosters-research.pdf" target="_blank">Poster boosters of research papers</a></i> (2 min. per poster)</tr>

<!------------------------------------------------>
<tr><td valign=top style="padding-top: 14px;">10:30&#8211;11:00</td><td valign=top style="padding-top: 14px;"><b><em>COFFEE BREAK</em></b></td></tr>
<!------------------------------------------------>

<tr><td valign=top style="padding-top: 14px;">&nbsp;</td><td valign=top style="padding-top: 14px;"><b>Session 2: Multiword expressions</b> (chair: Tim Baldwin)</td></tr>
<tr><td valign=top width=100>11:00&#8211;11:30</td><td valign=top align=left><a href="lawmwecxg2018/slides/Markantonatou.pdf" target="_blank"><i>Fixed Similes: Measuring aspects of the relation between MWE idiomatic semantics and syntactic flexibility</i></a><br>
Stella Markantonatou, Panagiotis Kouris and Yanis Maistros</td></tr>
<tr><td valign=top width=100>11:30&#8211;12:00</td><td valign=top align=left><a href="lawmwecxg2018/slides/Ramisch.pdf" target="_blank"><i>Edition 1.1 of the PARSEME Shared Task on Automatic Identification of Verbal Multiword Expressions </i></a><br>
Carlos Ramisch, Silvio Ricardo Cordeiro, Agata Savary, Veronika Vincze, Verginica Barbu Mititelu, Archna Bhatia, Maja Buljan, Marie Candito, Polona Gantar, Voula Giouli, Tunga Güngör, Abdelati Hawwari, Uxoa Iñurrieta, Jolanta Kovalevskaitė, Simon Krek, Timm Lichte, Chaya Liebeskind, Johanna Monti, Carla Parra Escartín, Behrang QasemiZadeh, Renata Ramisch, Nathan Schneider, Ivelina Stoyanova, Ashwini Vaidya and Abigail Walsh</td></tr>
<tr><td valign=top width=100>12:00&#8211;12:10</td><td valign=top align=left><a href="lawmwecxg2018/slides/Waszczuk.pdf" target="_blank"><i>TRAVERSAL at PARSEME Shared Task 2018: Identification of Verbal Multiword Expression using a Discriminative Tree-Structured Model</i></a><br>
Jakub Waszczuk</td></tr>
<tr><td valign=top width=100>12:10&#8211;12:20</td><td valign=top align=left><a href="lawmwecxg2018/slides/Stodden.pdf" target="_blank"><i>TRAPACC and TRAPACCS at PARSEME Shared Task 2018: Neural Transition Tagging of Verbal Multiword Expressions</i></a><br>
Regina Stodden, Behrang QasemiZadeh and Laura Kallmeyer</td></tr>
<tr><td valign=top width=100>12:20&#8211;12:30</td><td valign=top align=left><i><a href="lawmwecxg2018/slides/boosters-sharedtask.pdf" target="_blank">Poster boosters of 6 other shared task papers</a></i> (2 min. per poster)</td></tr>

<!------------------------------------------------>
<tr><td valign=top style="padding-top: 14px;">12:30&#8211;13:50</td><td valign=top style="padding-top: 14px;"><b><em>LUNCH BREAK</em></b></td></tr>

<!------------------------------------------------>
<tr><td valign=top style="padding-top: 14px;">13:50&#8211;15:50</td><td valign=top style="padding-top: 14px;"><b>Session 3: Posters</b> (chair: Stella Markantonatou; 7 research papers, 4 system papers)</td></tr>

<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>The RST Spanish-Chinese Treebank</i><br>
Shuyuan Cao, Iria da Cunha and Mikel Iruskieta</td></tr>
<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>The Other Side of the Coin: Unsupervised Disambiguation of Potentially Idiomatic Expressions by Contrasting Senses</i><br>
Hessel Haagsma, Malvina Nissim and Johan Bos</td></tr>
<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>Fine-grained termhood prediction for German compound terms using neural networks </i><br>
Anna Hätty and Sabine Schulte im Walde</td></tr>
<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>Verbal Multiword Expressions in Basque corpora </i><br>
Uxoa Iñurrieta, Itziar Aduriz, Ainara Estarrona, Itziar Gonzalez-Dios, Antton Gurrutxaga, Ruben Urizar and Iñaki Alegria</td></tr>
<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>Towards a Computational Lexicon for Moroccan Darija: Words, Idioms, and Constructions </i><br>
Jamal Laoudi, Claire Bonial, Lucia Donatelli, Stephen Tratz and Clare Voss</td></tr>
<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>Cooperating Tools for MWE Lexicon Management and Corpus Annotation</i><br>
Yuji Matsumoto, Akihiko Kato, Hiroyuki Shindo and Toshio Morita</td></tr>
<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>Developing and Evaluating Annotation Procedures for Twitter Data during Hazard Events</i><br>
Kevin Stowe, Martha Palmer, Jennings Anderson, Marina Kogan, Leysia Palen, Kenneth M. Anderson, Rebecca Morss, Julie Demuth and Heather Lazrus</td></tr>


<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>CRF-Seq and CRF-DepTree at PARSEME Shared Task 2018: Detecting Verbal MWEs using Sequential and Dependency-based Approaches</i><br>
Erwan Moreau, Ashjan Alsulaimani, Alfredo Maldonado and Carl Vogel</td></tr>
<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>Mumpitz at PARSEME Shared Task 2018: A Bidirectional LSTM for the Identification of Verbal Multiword Expressions </i><br>
Rafael Ehren, Timm Lichte and Younes Samih</td></tr>
<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>TRAVERSAL at PARSEME Shared Task 2018: Identification of Verbal Multiword Expression using a Discriminative Tree-Structured Model</i><br>
Jakub Waszczuk</td></tr>
<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>Veyn at PARSEME Shared Task 2018: Recurrent neural networks for VMWE identification </i><br>
Nicolas Zampieri, Manon Scholivet, Carlos Ramisch and Benoit Favre</td></tr>


<!------------------------------------------------>
<tr><td valign=top style="padding-top: 14px;">15:50&#8211;16:20</td><td valign=top style="padding-top: 14px;"><b><em>COFFEE BREAK</em></b></td></tr>
<!------------------------------------------------>

<tr><td valign=top style="padding-top: 14px;">16:20&#8211;18:00</td><td valign=top style="padding-top: 14px;"><b>Session 4: Posters</b> (chair: Stella Markantonatou; 6 research papers, 4 system papers)</td></tr>

<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>"Fingers in the Nose": Evaluating Speakers’ identification of Multi-Word Expressions Using a Slightly Gamified Crowdsourcing Platform</i><br>
Karën Fort, Bruno Guillaume, Matthieu Constant, Nicolas Lefèbvre and Yann-Alan Pilatte</td></tr>
<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>Do Character-level Neural Network Language Models Capture Knowledge of Multiword Expression Compositionality?</i><br>
Ali Hakimi Parizi and Paul Cook</td></tr>
<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>A Treebank for the Healthcare Domain</i><br>
Nganthoibi Oinam, Diwakar Mishra, Pinal Patel, Narayan Choudhary and Hitesh Desai</td></tr>
<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>All Roads Lead to UD: Converting Stanford and Penn Parses to English Universal Dependencies with Multilayer Annotations </i><br>
Siyao Peng and Amir Zeldes</td></tr>
<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>Constructing an Annotated Corpus of Verbal MWEs for English</i><br>
Abigail Walsh, Claire Bonial, Kristina Geeraert, John P. McCrae, Nathan Schneider and Clarissa Somers</td></tr>
<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>A syntax-based scheme for the annotation and segmentation of German spoken language interactions</i><br>
Swantje Westpfahl and Jan Gorisch</td></tr>


<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>Deep-BGT at PARSEME Shared Task 2018: Bidirectional LSTM-CRF Model for Verbal Multiword Expression Identification</i><br>
Gözde Berk, Berna Erden and Tunga Güngör</td></tr>
<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>GBD-NER at PARSEME Shared Task 2018: Multi-word Expression Detection using Bidirectional Long-Short-Term Memory Networks and graph-based decoding</i><br>
Tiberiu Boroș and Ruxandra Burtica</td></tr>
<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>TRAPACC and TRAPACCS at PARSEME Shared Task 2018: Neural Transition Tagging of Verbal Multiword Expressions</i><br>
Regina Stodden, Behrang QasemiZadeh and Laura Kallmeyer</td></tr>
<tr><td valign=top width=100>&nbsp;</td><td valign=top align=left><i>VarIDE at PARSEME Shared Task 2018: Are variants really as alike as two peas in a pod?</i><br>
Caroline Pasquer, Carlos Ramisch, Agata Savary and Jean-Yves Antoine</td></tr>
</table>

<!------------------------------------------------>
<!------------------------------------------------>
<!------------------------------------------------>
<hr>
<table cellspacing="0" cellpadding="3" border="0"><tr>
<td colspan=2 style="padding-top: 0px;"><h3>Sunday, August 26, 2018 (Peralta room)</h3></td></tr>

<!------------------------------------------------>
<tr><td valign=top style="padding-top: 14px;">&nbsp;</td><td valign=top style="padding-top: 14px;"><b>Session 5: Constructions</b> (chair: Nathan Schneider)</td></tr>
<tr><td valign=top width=100>09:00&#8211;10:00</td>
<td valign=top align=left><strong><a href="PHITE.php?sitesig=CONF&page=CONF_04_LAW-MWE-CxG_2018___lb__COLING__rb__&subpage=CONF_25_Keynote_Speakers#keynote-lori">Invited talk: <i>Annotation Schemes for Surface Construction Labeling</i></a></strong><br>
<strong>Lori Levin</strong></td></tr>
<tr><td valign=top width=100>10:00&#8211;10:30</td><td valign=top align=left><a href="lawmwecxg2018/slides/Groen.pdf" target="_blank"><i>The Interplay of Form and Meaning in Complex Medical Terms: Evidence from a Clinical Corpus</i></a><br>
Leonie Grön, Ann Bertels and Heylen Kris</td></tr>

<!------------------------------------------------>
<tr><td valign=top style="padding-top: 14px;">10:30&#8211;11:00</td><td valign=top style="padding-top: 14px;"><b><em>COFFEE BREAK</em></b></td></tr>
<!------------------------------------------------>

<tr><td valign=top style="padding-top: 14px;">&nbsp;</td><td valign=top style="padding-top: 14px;"><b>Session 6: Constructions</b> (chair: Amir Zeldes)</td></tr>
<tr><td valign=top width=100>12:00&#8211;12:30</td><td valign=top align=left><a href="lawmwecxg2018/slides/Bhattasali.pdf" target="_blank"><i>Processing MWEs: Neurocognitive Bases of Verbal MWEs and Lexical Cohesiveness within MWEs</i></a><br>
Shohini Bhattasali, Murielle Fabre and John Hale</td></tr>
<tr><td valign=top width=100>11:30&#8211;12:00</td><td valign=top align=left><a href="lawmwecxg2018/slides/Danlos.pdf" target="_blank"><i>Discourse and lexicons: lexemes, MWEs, grammatical constructions and compositional word combinations to signal discourse relations</i></a><br>
Laurence Danlos</td></tr>
<tr><td valign=top width=100>11:00&#8211;11:30</td><td valign=top align=left><a href="lawmwecxg2018/slides/Colson.pdf" target="_blank"><i>From Chinese word segmentation to extraction of constructions: two sides of the same algorithmic coin</i></a><br>
Jean-Pierre Colson</td></tr>

<!------------------------------------------------>
<tr><td valign=top style="padding-top: 14px;">12:30&#8211;13:50</td><td valign=top style="padding-top: 14px;"><b><em>LUNCH BREAK</em></b></td></tr>

<!------------------------------------------------>

<tr><td valign=top style="padding-top: 14px;">&nbsp;</td><td valign=top style="padding-top: 14px;"><b>Session 7: Linguistic annotation</b> (chair: Agata Savary) </td></tr>
<tr><td valign=top width=100>13:50&#8211;14:50</td>
<td valign=top align=left><strong>Invited talk: <i><a href="PHITE.php?sitesig=CONF&page=CONF_04_LAW-MWE-CxG_2018___lb__COLING__rb__&subpage=CONF_25_Keynote_Speakers#keynote-adam">From Lexical Functional Grammar to Enhanced Universal Dependencies</a></i></strong><br>
<strong>Adam Przepiórkowski</strong></td></tr>
<tr><td valign=top width=100>14:50&#8211;15:20</td><td valign=top align=left><a href="lawmwecxg2018/slides/Donatelli.pdf" target="_blank"><i>Annotation of Tense and Aspect Semantics for Sentential AMR</i></a><br>
 Lucia Donatelli, Michael Regan, William Croft and Nathan Schneider</td></tr>
<tr><td valign=top width=100>15:20&#8211;15:50</td><td valign=top align=left><a href="lawmwecxg2018/slides/Koehn.pdf" target="_blank"><i>An Annotated Corpus of Picture Stories Retold by Language Learners</i></a><br>
Christine Köhn and Arne Köhn</td></tr>

<!------------------------------------------------>
<tr><td valign=top style="padding-top: 14px;">15:50&#8211;16:20</td><td valign=top style="padding-top: 14px;"><b><em>COFFEE BREAK</em></b></td></tr>
<!------------------------------------------------>

<tr><td valign=top style="padding-top: 14px;">&nbsp;</td><td valign=top style="padding-top: 14px;"><b>Session 8: Linguistic annotation</b> (chair: Josef Ruppenhofer)</td></tr>
<tr><td valign=top width=100>16:20&#8211;16:40</td><td valign=top align=left><a href="lawmwecxg2018/slides/Boukaram.pdf" target="_blank"><i>Improving Domain Independent Question Parsing with Synthetic Treebanks </i></a><br>
Halim-Antoine Boukaram, Nizar Habash, Micheline Ziadee and Majd Sakr</td></tr>

<tr><td valign=top style="padding-top: 14px;">16:40&#8211;17:40</td><td valign=top style="padding-top: 14px;"><b>Business meeting</b> [<a href="lawmwecxg2018/slides/business-meeting.pdf" target="_blank">slides</a>] [<a href="lawmwecxg2018/minutes/LAW-MWE-CxG-business-meeting-minutes.pdf" target="_blank">minutes</a>] [<a href="https://docs.google.com/document/d/1-A-2qtM5J2OYTN56Bi3tnZiknK9B3VqmEzx5l0RxPI0/edit#heading=h.hpzk43x0tjq3" target="_blank">MWE research issues</a>]</td></tr>

</table>


<!-----------------------------------------
<h1>Papers selected in the research track</h1>

<ol>

<li>PROCESSING MWES: NEUROCOGNITIVE BASES OF VERBAL MWES AND LEXICAL COHESIVENESS WITHIN MWES<br />
Shohini Bhattasali, Murielle Fabre and John Hale

<li>IMPROVING DOMAIN INDEPENDENT QUESTION PARSING WITH SYNTHETIC TREEBANKS<br />
Halim-Antoine Boukaram, Nizar Habash, Micheline Ziadee and Majd Sakr

<li>THE RST SPANISH-CHINESE TREEBANK<br />
Shuyuan Cao, Iria da Cunha and Mikel Iruskieta

<li>FROM CHINESE WORD SEGMENTATION TO EXTRACTION OF CONSTRUCTIONS: TWO SIDES OF THE SAME ALGORITHMIC COIN<br />
Jean-Pierre Colson

<li>DISCOURSE AND LEXICONS: LEXEMES, MWES, GRAMMATICAL CONSTRUCTIONS AND COMPOSITIONAL WORD COMBINATIONS TO SIGNAL DISCOURSE RELATIONS<br />
Laurence Danlos

<li>ANNOTATION OF TENSE AND ASPECT SEMANTICS FOR SENTENTIAL AMR<br />
Lucia Donatelli, Michael Regan, William Croft and Nathan Schneider

<li>"FINGERS IN THE NOSE": EVALUATING THE SPEAKERS’ INTUITION IN IDENTIFYING MWES USING A SLIGHTLY GAMIFIED CROWDSOURCING PLATFORM<br />
Karën Fort, Bruno Guillaume, Matthieu Constant, Nicolas Lefèbvre and Yann-Alan Pilatte

<li>THE INTERPLAY OF FORM AND MEANING IN COMPLEX MEDICAL TERMS: EVIDENCE FROM A CLINICAL CORPUS<br />
Leonie Grön

<li>THE OTHER SIDE OF THE COIN: UNSUPERVISED DISAMBIGUATION OF POTENTIALLY IDIOMATIC EXPRESSIONS BY CONTRASTING SENSES<br />
Hessel Haagsma, Malvina Nissim and Johan Bos

<li>DO CHARACTER-LEVEL NEURAL NETWORK LANGUAGE MODELS CAPTURE KNOWLEDGE OF MULTIWORD EXPRESSION COMPOSITIONALITY?<br />
Ali Hakimi Parizi and Paul Cook

<li>FINE-GRAINED TERMHOOD PREDICTION FOR GERMAN COMPOUND TERMS USING NEURAL NETWORKS<br />
Anna Hätty and Sabine Schulte im Walde

<li>VERBAL MULTIWORD EXPRESSIONS IN BASQUE CORPORA<br />
Uxoa Iñurrieta, Itziar Aduriz, Ainara Estarrona, Itziar Gonzalez-Dios, Antton Gurrutxaga and Ruben Urizar

<li>AN ANNOTATED CORPUS OF COMIC STRIPS STORIES WRITTEN BY LANGUAGE LEARNERS<br />
Christine Köhn and Arne Köhn

<li>TOWARDS A COMPUTATIONAL LEXICON FOR MOROCCAN DARIJA: WORDS, IDIOMS, AND CONSTRUCTIONS<br />
Jamal Laoudi, Claire Bonial, Lucia Donatelli, Stephen Tratz and Clare Voss

<li>FIXED SIMILES: MEASURING ASPECTS OF THE RELATION BETWEEN MWE IDIOMATIC SEMANTICS AND SYNTACTIC FLEXIBILITY<br />
Stella Markantonatou, Panagiotis Kouris and Yanis Maistros

<li>COOPERATING TOOLS FOR MWE LEXICON MANAGEMENT AND CORPUS ANNOTATION<br />
Yuji Matsumoto, Akihiko Kato, Hiroyuki Shindo and Toshio Morita

<li>A TREEBANK FOR THE HEALTHCARE DOMAIN<br />
Nganthoibi Oinam, Diwakar Mishra, Pinal Patel, Narayan Choudhary and Hitesh Desai

<li>ALL ROADS LEAD TO UD: CONVERTING STANFORD AND PENN PARSES TO ENGLISH UNIVERSAL DEPENDENCIES WITH MULTILAYER ANNOTATIONS<br />
Siyao Peng and Amir Zeldes

<li>EDITION 1.1 OF THE PARSEME SHARED TASK ON AUTOMATIC IDENTIFICATION OF VERBAL MULTIWORD EXPRESSIONS<br />
Carlos Ramisch, Silvio Cordeiro, Agata Savary, Veronika Vincze, Verginica Barbu Mititelu, Archna Bhatia, Maja Buljan, Marie Candito, Carla Parra Escartín, Polona Gantar, Voula Giuoli, Tunga Gungor, Abdelati Hawwari, Usoa Iñurrieta, Jolanta Kovalevskaite, Simon Krek, Timm Lichte, Chaya Liebeskind, Johanna Monti, Behrang QasemiZadeh, Renata Ramisch, Nathan Schneider, Ivelina Stoyanova, Ashwini Vaidya and Abigail Walsh

<li>DEVELOPING AND EVALUATING ANNOTATION PROCEDURES FOR TWITTER DATA DURING HAZARD EVENTS<br />
Kevin Stowe, Martha Palmer, Leysia Palen, Kenneth M. Anderson, Jennings Anderson, Marina Kogan, Rebecca Morss, Julie Demuth and Heather Lazrus

<li>CONSTRUCTING AN ANNOTATED CORPUS OF VERBAL MWES FOR ENGLISH<br />
Abigail Walsh and Claire Bonial

<li>A SYNTAX-BASED SCHEME FOR THE ANNOTATION AND SEGMENTATION OF GERMAN SPOKEN LANGUAGE INTERACTIONS<br />
Swantje Westpfahl and Jan Gorisch

</ol>

<h1>Papers selected in the shared task track</h1>

<ol>
<li>DEEP-BGT AT PARSEME SHARED TASK 2018: BIDIRECTIONAL LSTM-CRF MODEL FOR VERBAL MULTIWORD EXPRESSION IDENTIFICATION<br />
Gözde Berk, Berna Erden and Tunga Gungor

<li>MULTI-WORD EXPRESSION DETECTION USING BIDIRECTIONAL LONG-SHORT-TERM MEMORY NETWORKS AND GRAPH-BASED DECODING.<br />
Tiberiu Boroș and Ruxandra Burtica

<li>MUMPITZ AT PARSEME SHARED TASK 2018: A BIDIRECTIONAL LSTM FOR THE IDENTIFICATION OF VERBAL MULTIWORD EXPRESSIONS<br />
Rafael Ehren, Timm Lichte and Younes Samih

<li>CRF-SEQ and CRF-DEPTREE AT PARSEME SHARED TASK 2018:: DETECTING VERBAL MWEs USING SEQUENTIAL AND DEPENDENCY-BASED APPROACHES
Erwan Moreau, Ashjan Alsulaimani, Alfredo Maldonado and Carl Vogel

<li>VARIDE AT PARSEME SHARED TASK 2018: ARE VARIANTS REALLY AS ALIKE AS TWO PEAS IN A POD?<br />
Caroline Pasquer, Agata Savary, Carlos Ramisch and Jean-Yves Antoine

<li>TRAPACC AND TRAPACCS AT PARSEME SHARED TASK 2018: A NEURAL TRANSITION SYSTEM FOR VMWE IDENTIFICATION<br />
Regina Stodden, Behrang QasemiZadeh and Laura Kallmeyer

<li>TRAVERSAL AT PARSEME SHARED TASK 2018: IDENTIFICATION OF MULTIWORD EXPRESSION USING A DISCRIMINATIVE TREE-STRUCTURED MODEL<br />
Jakub Waszczuk

<li>VEYN AT PARSEME SHARED TASK 2018: RECURRENT NEURAL NETWORKS FOR VMWE IDENTIFICATION<br />
Nicolas Zampieri, Manon Scholivet, Carlos Ramisch and Benoit Favre

</ol>
------------------------------------------------>

<!----------------------------------------->
<h1><a name="research-boosters"></a>Session 1: Poster boosters in the research track</h1>

<ol>

<!-- Treebanks and lexicons -->
<li>THE RST SPANISH-CHINESE TREEBANK<br />
Shuyuan Cao, Iria da Cunha and Mikel Iruskieta

<li>A TREEBANK FOR THE HEALTHCARE DOMAIN<br />
Nganthoibi Oinam, Diwakar Mishra, Pinal Patel, Narayan Choudhary and Hitesh Desai

<li>TOWARDS A COMPUTATIONAL LEXICON FOR MOROCCAN DARIJA: WORDS, IDIOMS, AND CONSTRUCTIONS<br />
Jamal Laoudi, Claire Bonial, Lucia Donatelli, Stephen Tratz and Clare Voss


<!-- Annotation procedures -->

<li>A SYNTAX-BASED SCHEME FOR THE ANNOTATION AND SEGMENTATION OF GERMAN SPOKEN LANGUAGE INTERACTIONS<br />
Swantje Westpfahl and Jan Gorisch

<li>ALL ROADS LEAD TO UD: CONVERTING STANFORD AND PENN PARSES TO ENGLISH UNIVERSAL DEPENDENCIES WITH MULTILAYER ANNOTATIONS<br />
Siyao Peng and Amir Zeldes

<li>COOPERATING TOOLS FOR MWE LEXICON MANAGEMENT AND CORPUS ANNOTATION<br />
Yuji Matsumoto, Akihiko Kato, Hiroyuki Shindo and Toshio Morita

<li>DEVELOPING AND EVALUATING ANNOTATION PROCEDURES FOR TWITTER DATA DURING HAZARD EVENTS<br />
Kevin Stowe, Martha Palmer, Leysia Palen, Kenneth M. Anderson, Jennings Anderson, Marina Kogan, Rebecca Morss, Julie Demuth and Heather Lazrus

<li>"FINGERS IN THE NOSE": EVALUATING THE SPEAKERS’ IDENTIFICATION OF MWES USING A SLIGHTLY GAMIFIED CROWDSOURCING PLATFORM<br />
Karën Fort, Bruno Guillaume, Matthieu Constant, Nicolas Lefèbvre and Yann-Alan Pilatte


<!-- MWE annotation -->
<li>VERBAL MULTIWORD EXPRESSIONS IN BASQUE CORPORA<br />
Uxoa Iñurrieta, Itziar Aduriz, Ainara Estarrona, Itziar Gonzalez-Dios, Antton Gurrutxaga and Ruben Urizar

<li>CONSTRUCTING AN ANNOTATED CORPUS OF VERBAL MWES FOR ENGLISH<br />
Abigail Walsh and Claire Bonial

<!-- MWE-hood -->
<li>THE OTHER SIDE OF THE COIN: UNSUPERVISED DISAMBIGUATION OF POTENTIALLY IDIOMATIC EXPRESSIONS BY CONTRASTING SENSES<br />
Hessel Haagsma, Malvina Nissim and Johan Bos

<li>DO CHARACTER-LEVEL NEURAL NETWORK LANGUAGE MODELS CAPTURE KNOWLEDGE OF MULTIWORD EXPRESSION COMPOSITIONALITY?<br />
Ali Hakimi Parizi and Paul Cook

<li>FINE-GRAINED TERMHOOD PREDICTION FOR GERMAN COMPOUND TERMS USING NEURAL NETWORKS<br />
Anna Hätty and Sabine Schulte im Walde


</ol>

<!----------------------------------------->
<h1><a name="st-boosters"></a>Session 2: Poster boosters in the shared task track</h1>

<ol>
<li>DEEP-BGT AT PARSEME SHARED TASK 2018: BIDIRECTIONAL LSTM-CRF MODEL FOR VERBAL MULTIWORD EXPRESSION IDENTIFICATION<br />
Gözde Berk, Berna Erden and Tunga Gungor

<li>GBD-NER AT PARSEME SHARED TASK 2018: MULTI-WORD EXPRESSION DETECTION USING BIDIRECTIONAL LONG-SHORT-TERM MEMORY NETWORKS AND GRAPH-BASED DECODING<br />
Tiberiu Boroș and Ruxandra Burtica

<li>MUMPITZ AT PARSEME SHARED TASK 2018: A BIDIRECTIONAL LSTM FOR THE IDENTIFICATION OF VERBAL MULTIWORD EXPRESSIONS<br />
Rafael Ehren, Timm Lichte and Younes Samih

<li>CRF-SEQ and CRF-DEPTREE AT PARSEME SHARED TASK 2018:: DETECTING VERBAL MWEs USING SEQUENTIAL AND DEPENDENCY-BASED APPROACHES<br />
Erwan Moreau, Ashjan Alsulaimani, Alfredo Maldonado and Carl Vogel

<li>VARIDE AT PARSEME SHARED TASK 2018: ARE VARIANTS REALLY AS ALIKE AS TWO PEAS IN A POD?<br />
Caroline Pasquer, Agata Savary, Carlos Ramisch and Jean-Yves Antoine

<li>VEYN AT PARSEME SHARED TASK 2018: RECURRENT NEURAL NETWORKS FOR VMWE IDENTIFICATION<br />
Nicolas Zampieri, Manon Scholivet, Carlos Ramisch and Benoit Favre

</ol>
<!------------------------------------------------>

