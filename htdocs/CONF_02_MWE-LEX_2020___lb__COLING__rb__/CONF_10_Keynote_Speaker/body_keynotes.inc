﻿<h2>Joint Workshop on Multiword Expressions and Electronic Lexicons (MWE-LEX 2020)</h2>

<h3>Workshop at <a href="http://coling2020.org/" target="_blank">COLING 2020</a> (<del>Barcelona, Spain</del> online), <del>September 14</del> December 13, 2020.</h3>

<p>Organized and sponsored by: <br/>
Special Interest Group on the Lexicon (<a href="http://www.siglex.org">SIGLEX</a>) of the Association for Computational Linguistics (<a href="https://www.aclweb.org/portal/">ACL</a>) <br/>
<a href="https://elex.is/">ELEXIS</a> - European Lexicographic Infrastructure. <br/><br/>
This joint event is the 16th edition of the <strong>Workshop on Multiword Expressions (<a href="?sitesig=CONF">MWE</a>)</strong>.
<br/>
<a href="https://twitter.com/mwe_workshop?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-count="false">Follow @mwe_workshop</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>

<p><small>Last updated: Nov 15, 2020</small></p>


<!-- ------------------------------------------------------------------------- -->

<h1>Generationary or: "How We Went beyond Sense Inventories and Learned to Gloss"</h1>

<p>by <strong><a href="http://wwwusers.di.uniroma1.it/~navigli">Roberto Navigli</a></strong> – Sapienza University of Rome (Italy) </p>

<table><tr>
  <td width="180" valign="top"><img src="images/Roberto.jpg" width="160" /></td>
  <td valign="top">
    <a href="http://wwwusers.di.uniroma1.it/~navigli">Roberto Navigli</a> is Professor of Computer Science at the Sapienza University of Rome, where he leads the <a href="http://nlp.uniroma1.it/">Sapienza NLP Group</a>. He is one of the few researchers to have received two prestigious ERC grants in computer science, namely an ERC Starting Grant on multilingual word sense disambiguation (2011-2016) and an ERC Consolidator Grant on multilingual language- and syntax-independent open-text unified representations (2017-2022). In 2015 he received the <a href="http://www.meta-net.eu/meta-prize">META prize</a> for groundbreaking work in overcoming language barriers with <a href="http://babelnet.org/">BabelNet</a>, a project also highlighted in <a href="https://www.theguardian.com/news/2018/feb/23/oxford-english-dictionary-can-worlds-biggest-dictionary-survive-internet">The Guardian</a> and <a href="http://wwwusers.di.uniroma1.it/~navigli/img/Redefining_the_modern_dictionary.png">Time magazine</a>, and winner of the Artificial Intelligence Journal prominent paper award 2017. He is the co-founder of <a href="http://babelscape.com/">Babelscape</a>, a successful Sapienza startup company which enables NLP in dozens of languages. He is a Program Chair of ACL 2021, the reference conference in NLP.
  </td>
</tr></table>

<p><strong>Abstract</strong></p>
<p>
In this talk I present Generationary, an approach that goes beyond the mainstream assumption that word senses can be represented as discrete items of a predefined inventory, and put forward a unified model which produces contextualized definitions for arbitrary lexical items, from words to phrases and even sentences. Generationary employs a novel span-based encoding scheme to fine-tune an English pre-trained Encoder-Decoder system and generate new definitions. Our model outperforms previous approaches in the generative task of Definition Modeling in many settings, but it also matches or surpasses the state of the art in discriminative tasks such as Word Sense Disambiguation and Word-in-Context. Finally, we show that Generationary benefits from training on definitions from multiple inventories, with strong gains across benchmarks, including a novel dataset of definitions for free adjective-noun phrases.
</p>

<!-- ------------------------------------------------------------------------- -->


<!--
<h3>TITLE</h3>
<p>by <strong><a href="http://" target="_blank">NAME</a></strong> – AFFILIATION</p>

<table><tr>
  <td width="160" valign="top"><img src="images/.jpg" width="120" /></td>
  <td valign="top">
    SHORT BIO
  </td>
</tr></table>

<p>
  ABSTRACT
</p>
<p><strong>References</strong></p>
<p>
  REFS
</p>
</p>
-->

<!-- ------------------------------------------------------------------------- -->


