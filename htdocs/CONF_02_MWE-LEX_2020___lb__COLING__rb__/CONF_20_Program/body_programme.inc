<h2>Joint Workshop on Multiword Expressions and Electronic Lexicons (MWE-LEX 2020)</h2>

<h3>Workshop at <a href="http://coling2020.org/" target="_blank">COLING 2020</a> (<del>Barcelona, Spain</del> online), <del>September 14</del> December 13, 2020.</h3>

<p>Organized and sponsored by: <br/>
Special Interest Group on the Lexicon (<a href="http://www.siglex.org">SIGLEX</a>) of the Association for Computational Linguistics (<a href="https://www.aclweb.org/portal/">ACL</a>) <br/>
<a href="https://elex.is/">ELEXIS</a> - European Lexicographic Infrastructure. <br/><br/>
This joint event is the 16th edition of the <strong>Workshop on Multiword Expressions (<a href="?sitesig=CONF">MWE</a>)</strong>.
<br/>
<a href="https://twitter.com/mwe_workshop?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-count="false">Follow @mwe_workshop</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>

<p><small>Last updated: Dec 02, 2020</small></p>


<!------------------------------------------------------------------------------>

<h1>Workshop program</h1>

<p>Videos for all MWE-LEX talks are now available on Underline for registered COLING2020 participants:</p>
<ul><li><a href="https://underline.io/events/54/">https://underline.io/events/54/</a></li></ul>

<p>Sunday, December 13, 2020 (Central European Time - UTC+1)</p>

<ul>
  <li>14:00–14:10 Welcoming and preparation</li>
  <li>14:10–14:40 <strong>Session 1: MWE resources and linguistics (30min)</strong> <br/>
  Chair: Jean-Pierre Colson, Co-chair: Antonios Anastasopoulos</li>
  <ul>
    <!--<li>14:10­–14:16 Elevator pitches</li>
    <li>14:16–14:24 -->
    <li>CollFrEn: Rich Bilingual English–French Collocation Resource (Long paper #20)<br/>
    <em>Beatriz Fisas, Luis Espinosa-Anke, Joan Codina-Filbá and Leo Wanner</em><br/>
    <a href="https://underline.io/events/54/sessions/1439/lecture/6495-collfren-rich-bilingual-english--french-collocation-resource">[Video]</a> 
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.1/">[Paper]</a></li>
    <!--<li>14:24–14:32--><li> Filling the ___-s in Finnish MWE lexicons (Long paper #39)<br/>
    <em>Frankie Robertson</em><br/>
    <a href="https://underline.io/events/54/sessions/1439/lecture/6507-filling-the-___-s-in-finnish-mwe-lexicons">[Video]</a> 
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.2/">[Paper]</a></li>
    <!--<li>14:32–14:36--><li> Hierarchy-aware Learning of Sequential Tool Usage via Semi-automatically Constructed Taxonomies (Short paper #19) <br/>
    <em>Nima Nabizadeh, Martin Heckmann and Dorothea Kolossa </em><br/>
    <a href="https://underline.io/events/54/sessions/1439/lecture/6494-hierarchy-aware-learning-of-sequential-tool-usage-via-semi-automatically-constructed-taxonomies">[Video]</a> 
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.3/">[Paper]</a></li>
    <!--<li>14:36–14:40--><li> Scalar vs. mereological conceptualizations of the N-BY-N and NUM-BY-NUM adverbials (Short paper #37) <br/>
    <em>Lucia Vlášková and Mojmír Dočekal</em><br/>
    <a href="https://underline.io/events/54/sessions/1439/lecture/6505-scalar-vs.-mereological-conceptualizations-of-the-n-by-n-and-num-by-num-adverbials">[Video]</a> 
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.4/">[Paper]</a></li>
  </ul>

  <li>14:40–14:50 Break (10min)</li>

  <li>14:50–15:20 <strong>Session 2: Verbal multiword expressions (30min)</strong> <br/>
  Chair: Archna Bhatia, Co-chair: Carole Tiberius</li>
  <ul>
    <!--<li>14:50­–14:56 Elevator pitches</li>
    <li>14:56–15:04--><li> Polish corpus of verbal multiword expressions (Long paper #36)<br/>
    <em>Agata Savary and Jakub Waszczuk</em><br/>
    <a href="https://underline.io/events/54/sessions/1439/lecture/6504-polish-corpus-of-verbal-multiword-expressions">[Video]</a> 
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.5/">[Paper]</a></li>
    <!--<li>15:04–15:12--><li> AlphaMWE: Construction of Multilingual Parallel Corpora with MWE Annotations (Long paper #14)<br/>
    <em>Lifeng Han, Gareth Jones and Alan Smeaton</em><br/>
    <a href="https://underline.io/events/54/sessions/1439/lecture/6492-alphamwe-construction-of-multilingual-parallel-corpora-with-mwe-annotations">[Video]</a> 
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.6/">[Paper]</a></li> 
    <!--<li>15:12–15:16--><li> Annotating Verbal MWEs in Irish for the PARSEME Shared Task 1.2 (Short paper #24)<br/>
    <em>Abigail Walsh, Teresa Lynn and Jennifer Foster</em><br/>
    <a href="https://underline.io/events/54/sessions/1439/lecture/6497-annotating-verbal-mwes-in-irish-for-the-parseme-shared-task-1.2">[Video]</a> 
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.7/">[Paper]</a></li> 
    <!--<li>15:16–15:20--><li> VMWE discovery: a comparative analysis between Literature and Twitter Corpora (Short paper #13)<br/>
    <em>Vivian Stamou, Artemis Xylogianni, Marilena Malli, Penny Takorou and Stella Markantonatou</em><br/>
    <a href="https://underline.io/events/54/sessions/1439/lecture/7006-vmwe-discovery-a-comparative-analysis-between-literature-and-twitter-corpora">[Video]</a> 
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.8/">[Paper]</a></li>
  </ul>

  <li>15:20–15:30 Break (10min)</li> 
  
  <li>15:30–16:30 <strong>Session 3: Keynote speech (60min)</strong> <br/>
  Chair: Jelena Mitrović, Co-chair: Agata Savary</li>
  <ul>
    <li>A close look at Generationary or: "How We Went beyond Sense Inventories and Learned to Gloss"
<br/>
    <em>Roberto Navigli</em><br/>
    <!--<a href="">[Video]</a> -->
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.9/">[Abstract]</a></li>
  </ul>

  <li>16:30–16:40 Break (10min)</li> 

  <li>16:40-17:10 <strong>Session 4: Processing multiword expressions (30min)</strong> <br/>
  Chair: Shiva Taslimipoor, Co-chair: Vivian Stamou</em></li>
  <ul>
    <!--<li>16:40­–16:46 Elevator pitches</li>
    <li>16:46–16:54--><li> Multi-word Expressions for Abusive Speech Detection in Serbian (Long paper #30)<br/>
    <em>Ranka Stanković, Jelena Mitrović, Danka Jokić and Cvetana Krstev</em><br/>
    <a href="https://underline.io/events/54/sessions/1439/lecture/6500-multi-word-expressions-for-abusive-speech-detection-in-serbian">[Video]</a> 
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.10/">[Paper]</a></li>
    <!--<li>16:54–17:02--><li> Disambiguation of Potentially Idiomatic Expressions with Contextual Embeddings (Long paper #33)<br/>
    <em>Murathan Kurfalı and Robert Östling</em><br/>
    <a href="https://underline.io/events/54/sessions/1439/lecture/6502-disambiguation-of-potentially-idiomatic-expressions-with-contextual-embeddings">[Video]</a> 
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.11/">[Paper]</a></li>
    <!--<li>17:02–17:06--><li> Comparing word2vec and GloVe for Automatic Measurement of MWE Compositionality (Short paper #22)<br/>
    <em>Thomas Pickard</em><br/>
    <a href="https://underline.io/events/54/sessions/1439/lecture/6496-comparing-word2vec-and-glove-for-automatic-measurement-of-mwe-compositionality">[Video]</a> 
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.12/">[Paper]</a></li>
    <!--<li>17:06–17:10--><li> Automatic detection of unexpected/erroneous collocations in learner corpus (Short paper #35)<br/>
    <em>Jen-Yu Li and Thomas Gaillat</em><br/>
    <a href="https://underline.io/events/54/sessions/1439/lecture/7007-automatic-detection-of-unexpecteddasherroneous-collocations-in-learner-corpus">[Video]</a> 
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.13/">[Paper]</a></li> 
  </ul>

  <li>17:10–17:20 Break (10min)</li>

  <li>17:20-18:00 <strong>Session 5: Shared task (40min)</strong> <br/>
  Chair: Ashwini Vaidya, Co-chair: Lifeng Han </li>
  <ul>
    <!--<li>17:20­–17:28 Elevator pitches</li>
    <li>17:28–17:36--><li> Edition 1.2 of the PARSEME Shared Task on Semi-supervised Identification of Verbal Multiword Expressions (Long paper #38) <br/>
    <em>Carlos Ramisch, Agata Savary, Bruno Guillaume, Jakub Waszczuk, Marie Candito, Ashwini Vaidya, Verginica Barbu Mititelu, Archna Bhatia, Uxoa Iñurrieta, Voula Giouli, Tunga Gungor, Menghan Jiang, Timm Lichte, Chaya Liebeskind, Johanna Monti, Renata Ramisch, Sara Stymne, Abigail Walsh and Hongzhi Xu</em><br/>
    <a href="https://underline.io/events/54/sessions/1439/lecture/6506-edition-1.2-of-the-parseme-shared-task-on-semi-supervised-identification-of-verbal-multiword-expressions">[Video]</a> 
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.14/">[Paper]</a></li>
    <!--<li>17:36–17:40--><li> HMSid and HMSid2 at PARSEME Shared Task 2020: Computational Corpus Linguistics and unseen-in-training MWEs (System paper #12)<br/>
    <em>Jean-Pierre Colson </em><br/>
    <a href="https://underline.io/events/54/sessions/1439/lecture/6491-hmsid-and-hmsid2-at-parseme-shared-task-2020-computational-corpus-linguistics-and-unseen-in-training-mwes-">[Video]</a> 
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.15/">[Paper]</a></li>
    <!--<li>17:40–17:44--><li> Seen2Unseen at PARSEME Shared Task 2020: All Roads do not Lead to Unseen Verb-Noun VMWEs (System paper #15)<br/>
    <em>Caroline Pasquer, Agata Savary, Carlos Ramisch and Jean-Yves Antoine </em><br/>
    <a href="https://underline.io/events/54/sessions/1439/lecture/6493-seen2unseen-at-parseme-shared-task-2020-all-roads-do-not-lead-to-unseen-verb-noun-vmwes">[Video]</a> 
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.16/">[Paper]</a></li>
    <!--<li>17:44–17:48--><li> ERMI at PARSEME Shared Task 2020: Embedding-Rich Multiword Expression Identification (System paper #27)<br/>
    <em>Zeynep Yirmibeşoglu and Tunga Güngör</em><br/>
    <a href="https://underline.io/events/54/sessions/1439/lecture/6498-ermi-at-parseme-shared-task-2020-embedding-rich-multiword-expression-identification">[Video]</a> 
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.17/">[Paper]</a></li>
    <!--<li>17:48–17:52--><li> TRAVIS at PARSEME Shared Task 2020: How good is (m)BERT at seeing the unseen? (System paper #29)<br/>
    <em>Murathan Kurfalı</em><br/>
    <a href="https://underline.io/events/54/sessions/1439/lecture/6499-travis-at-parseme-shared-task-2020-how-good-is-(m)bert-at-seeing-the-unseenquestion-">[Video]</a> 
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.18/">[Paper]</a></li>
    <!--<li>17:52–17:56--><li> MTLB-STRUCT @Parseme 2020: Capturing Unseen Multiword Expressions Using Multi-task Learning and Pre-trained Masked Language Models (System paper #32) <br/>
    <em>Shiva Taslimipoor, Sara Bahaadini and Ekaterina Kochmar</em><br/>
    <a href="https://underline.io/events/54/sessions/1439/lecture/6501-mtlb-struct-atparseme-2020-capturing-unseen-multiword-expressions-using-multi-task-learning-and-pre-trained-masked-language-models">[Video]</a> 
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.19/">[Paper]</a></li>
    <!--<li>17:56–18:00--><li> MultiVitaminBooster at PARSEME Shared Task 2020: Combining Window- and Dependency-Based Features with Multilingual Contextualised Word Embeddings for VMWE Detection (System paper #34)<br/>
    <em>Sebastian Gombert and Sabine Bartsch</em><br/>
    <a href="https://underline.io/events/54/sessions/1439/lecture/6503-multivitaminbooster-at-parseme-shared-task-2020">[Video]</a> 
    <a href="https://www.aclweb.org/anthology/2020.mwe-1.20/">[Paper]</a></li>
  </ul>

  <li>18:00–18:10 Break</li> 
  
  <li>18:10-19:00 <strong>Session 6: Section reporting, panel discussion (50min)</strong><br/>
  Chair: Carlos Ramisch, Co-chair: Agata Savary <br/>
  <a href="download/mwe-lex-community-discussion.pdf">[Slides]</a></li>

</ul>
