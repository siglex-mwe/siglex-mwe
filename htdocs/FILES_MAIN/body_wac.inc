<h2>Legacy page</h2>

<p>Please go to our new website <a href="https://multiword.org/">https://multiword.org/</a> for up-to-date information</p>

<h2>Software & Resources</h2>

<p>Please use the links on the left menu to access and download the <a href="?sitesig=FILES&page=FILES_20_Data_Sets">Data Sets</a> or <a href="?sitesig=FILES&page=FILES_30_Software">Software</a> resources. </p>

<h2>License</h2>

<ul>
<li> When not specified in the specific resource package, the MWE data sets released on this website by their respective authors are licensed under a <a target="_blank" rel="license" href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0 Unported License</a>.</li>
<li>The software tools are distributed by their authors and have their own license policy.</li>
</ul>

<h2>Data Set Submission Information</h2>

<p>Please send your resource package to the <a href="mailto:mwe2010workshop[[--aT--]]gmail.com">website maintainers</a>. Include the following information:</p>

<ul>
<li>Documentation including extraction information and annotation guidelines.</li>
<li>A Readme file including: package name, contact details of the contributor(s), submission date, source, description, file names.</li>
</ul> 

<p><a target="_blank" rel="license" href="http://creativecommons.org/licenses/by/3.0/"><img alt="Creative Commons Licence" style="border-width:0" src="http://i.creativecommons.org/l/by/3.0/88x31.png" /></a></p>


