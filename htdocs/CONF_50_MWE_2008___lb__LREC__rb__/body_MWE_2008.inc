<h2>Towards a Shared Task for Multiword Expressions (MWE 2008)</h2>

<h3>Workshop at the LREC 2008 Conference (Marrakech, Morocco)</h3>

<p>
	Endorsed by the ACL Special Interest Group on the Lexicon (SIGLEX)
</p>

<h1>Papers and slides</h1>

<p>Full proceedings: <a href="http://www.lrec-conf.org/proceedings/lrec2008/workshops/W20_Proceedings.pdf" target="_blank">PDF</a><br>
Presentation slides: <a href="http://sourceforge.net/project/showfiles.php?group_id=212049&package_id=286671" target="_blank">ZIP</a><br>Slides shared task overview: <a href="download/SharedTask2008.pdf" target="_blank">PDF</a></p>

<table cellspacing="0" cellpadding="5" border="0">
<tr><td valign=top></td>
<td valign=top><i>A Resource for Evaluating the Deep Lexical Acquisition of English Verb-Particle Constructions</i><br>
Timothy Baldwin</td></tr>

<tr><td valign=top></td>
<td valign=top><i>A lexicographic evaluation of German adjective-noun collocations</i><br>
Stefan Evert</td></tr>

<tr><td valign=top></td>
<td valign=top><i>Description of evaluation resource -- German PP-verb data</i><br>
Brigitte Krenn</td></tr>

<tr><td valign=top></td>
<td valign=top><i>Reference Data for Czech Collocation Extraction</i><br>
Pavel Pecina</td></tr>

<tr><td valign=top></td>
<td valign=top><i>A Lexicon of shallow-typed German-English MW-Expressions and a German Corpus of MW-Expressions annotated Sentences</i><br>
Dimitra Anastasiou and Michael Carl</td></tr>

<tr><td valign=top></td>
<td valign=top><i>The VNC-Tokens Dataset</i><br>
Paul Cook, Afsaneh Fazly and Suzanne Stevenson</td></tr>

<tr><td valign=top></td>
<td valign=top><i>Multi-Word Verbs of Estonian: a Database and a Corpus</i><br>
Heiki-Jaan Kaalep and Kadri Muischnek</td></tr>

<tr><td valign=top></td>
<td valign=top><i>A French Corpus Annotated for Multiword Nouns</i><br>
Eric Laporte, Takuya Nakamura and Stavroula Voyatzi</td></tr>

<tr><td valign=top></td>
<td valign=top><i>An Electronic Dictionary of French Multiword Adverbs</i><br>
Eric Laporte and Stavroula Voyatzi</td></tr>

<tr><td valign=top></td>
<td valign=top><i>Cranberry Expressions in English and in German</i><br>
Beata Trawinski, Manfred Sailer, Jan-Philipp Soehn, Lothar Lemnitzer and Frank Richter</td></tr>

<tr><td valign=top></td>
<td valign=top><i>Standardised Evaluation of English Noun Compound Interpretation</i><br>
Su Nam Kim and Timothy Baldwin</td></tr>

<tr><td valign=top></td>
<td valign=top><i>Interpreting Compound Nominalisations</i><br>
Jeremy Nicholson and Timothy Baldwin</td></tr>

<tr><td valign=top></td>
<td valign=top><i>Paraphrasing Verbs for Noun Compound Interpretation</i><br>
Preslav Nakov</td></tr>

<tr><td valign=top></td>
<td valign=top><i>An Evaluation of Methods for the Extraction of Multiword Expressions</i><br>
Carlos Ramisch, Paulo Schreiner, Marco Idiart and Aline Villavicencio</td></tr>

<tr><td valign=top></td>
<td valign=top><i>A Machine Learning Approach to Multiword Expression Extraction</i><br>
Pavel Pecina</td></tr>


</table> 

<h1>Programme committee</h1>

<ul>
	<li>Iñaki Alegria, <i>University of the Basque Country (Spain)</i></li>
	<li>Timothy Baldwin, <i>Stanford University (USA); U of Melbourne (Australia)</i></li>
	<li>Colin Bannard, <i>Max Planck Institute (Germany)</i></li>
	<li>Francis Bond, <i>NTT Communication Science Laboratories (Japan)</i></li>
	<li>Gaël Dias, <i>Beira Interior University (Portugal)</i></li>
	<li>Ulrich Heid, <i>Stuttgart University (Germany)</i></li>
	<li>Kyo Kageura, <i>University of Tokyo (Japan)</i></li>
	<li>Rosamund Moon, <i>University of Birmingham (UK)</i></li>
	<li>Diana McCarthy, <i>University of Sussex (UK)</i></li>
	<li>Eric Laporte, <i>University of Marne-la-Vallee (France)</i></li>
	<li>Preslav Nakov, <i>University of California, Berkeley (USA)</i></li>
	<li>Jan Odijk, <i>University of Utrecht (The Netherlands)</i></li>
	<li>Stephan Oepen, <i>Stanford University (USA); U of Oslo (Norway)</i></li>
	<li>Darren Pearce, <i>University of Sussex (UK)</i></li>
	<li>Pavel Pecina, <i>Charles University (Czech Republic)</i></li>
	<li>Scott Piao, <i>University of Manchester (UK)</i></li>
	<li>Violeta Seretan, <i>University of Geneva (Switzerland)</i></li>
	<li>Suzanne Stevenson	University of Toronto (Canada)</i></li>
	<li>Beata Trawinski, <i>University of Tuebingen (Germany)</i></li>
	<li>Kiyoko Uchiyama, <i>Keio University (Japan)</i></li>
	<li>Begoña Villada Moirón, <i>University of Groningen (The Netherlands)</i></li>
	<li>Aline Villavicencio, <i>Federal University of Rio Grande do Sul (Brazil)</i></li>
</ul>

<h1>Workshop chairs</h1>

<ul>
	<li>Nicole Grégoire, <i>University of Utrecht, The Netherlands</i></li>
	<li>Stefan Evert, <i>University of Osnabrück, Germany</i></li>
	<li>Brigitte Krenn, <i>Austrian Research Institute for Artificial Intelligence (ÖFAI), Austria</i></li>
</ul>