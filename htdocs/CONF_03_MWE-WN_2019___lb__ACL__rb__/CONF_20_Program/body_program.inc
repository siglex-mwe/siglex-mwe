﻿<h2>Joint Workshop on Multiword Expressions and WordNet (MWE-WN 2019)</h2>

<h3>Workshop at <a href="http://www.acl2019.org/" target="_blank">ACL 2019</a> (Florence, Italy), August 2nd, 2019</h3>

<p>Organized, sponsored and endorsed by the Global Wordnet Association (<a href="http://globalwordnet.org/" target="_blank">GWA</a>) and the Special Interest Group on the Lexicon (<a href="http://www.siglex.org" target="_blank">SIGLEX</a>) of the Association for Computational Linguistics (<a href="https://www.aclweb.org/portal/" target="_blank">ACL</a>).</p>

<p>This joint event is the 15th edition of the <strong>Workshop on Multiword Expressions (<a href="?sitesig=CONF" target="_blank">MWE</a>)</strong>, with a special focus on WordNet.</p>

<p><small>Last updated: August 19, 2019</small></p>

<h1>Proceedings</h1>
The <a href="https://www.aclweb.org/anthology/events/ws-2019/#w19-51">MWE-WN proceedings</a> are available on the ACL Anthology.

<h1>Selected papers</h1>

<p>37 papers (20 long and 17 short) were submitted to the <strong>research track</strong> of the workshop, and none to the dissemination track. 
12 long papers and 8 short ones were selected. 6 papers (1 short and 5 long) were selected as oral presentations and 14 (7 short and 7 long) as posters. 
The overall <strong>acceptance rate is 54%</strong>.</br >
</p>

<!--
<center>
<table cellspacing="0" cellpadding="0" border="0">
	<tr>
                <td colspan="3"><a href="index.html">ACL 2014 Proceedings Home</a> | <a href="http://acl2014.org">ACL 2014 WEBSITE</a> | <a href="http://www.aclweb.org/">ACL WEBSITE</a></td>
	</tr>
</table>
<br><br>
-->

<!--
<h1>Papers selected in the research track</h1>

<ol>

<li>A comparison of statistical association measures for identifying dependency-based collocations in various languages<br /> 
Marcos Garcia, Marcos García Salido and Margarita Alonso-Ramos


<li>12345: A database for Modern Greek multiword expressions<br />
Stella Markantonatou, Panagiotis Minos, George Zakis, Vassiliki Moutzouri and Maria Chantou


<li>Identification of Adjective-Noun Neologisms using Pretrained Language Models<br />
John Philip McCrae


<li>Confirming the Non-compositionality of Idioms for Sentiment Analysis<br />
Alyssa Hwang


<li>Unsupervised Compositional Translation of Multiword Expressions<br />
Pablo Gamallo and Marcos Garcia


<li>A Systematic Comparison of English Noun Compound Representations<br />
Vered Schwartz

<li>Neural Lemmatization of Multiword Expressions<br />
Marine Schmitt and Mathieu Constant


<li>Semantic Modelling of Adjective-Noun Collocations Using FrameNet<br />
Yana Strakatova and Erhard Hinrichs


<li>The Impact of Word Representations on Sequential Neural MWE Identification<br />
Nicolas Zampieri, Carlos Ramisch and Géraldine Damnati


<li>Hear about Verbal Multiword Expressions in the Bulgarian and the Romanian Wordnets Straight from the Horse's Mouth<br />
Verginica Barbu Mititelu, Ivelina Stoyanova, Svetlozara Leseva, Maria Mitrofan, Tsvetana Dimitrova and Maria Todorova


<li>The Romanian Corpus Annotated with Verbal Multiword Expressions
<br />
Verginica Barbu Mititelu, Mihaela Ionescu and Mihaela Plămadă-Onofrei


<li>Using OntoLex-Lemon for Representing and Interlinking German Multiword Expressions in OdeNet and MMORPH
<br />
Thierry Declerck and Melanie Siegel

<li>A Neural Graph-based Approach to Verbal MWE Identification<br />
Jakub Waszczuk, Rafael Ehren, Regina Stodden and Laura Kallmeyer

<li>Without lexicons, multiword expression identification will never fly: A position statement<br />
Agata Savary, Silvio Cordeiro and Carlos Ramisch


<li>Ilfhocail: A Lexicon of Irish MWEs<br />
Abigail Walsh, Teresa Lynn and Jennifer Foster


<li>Modeling MWEs in BTB-WN<br />
Laska Laskova, Petya Osenova, Kiril Simov, Ivajlo Radev and Zara Kancheva



<li>L2 Processing Advantages of Multiword Sequences: Evidence from Eye-Tracking<br />
Elma Kerz, Arndt Heilmann and Stella Neumann



<li>Cross-lingual Transfer Learning and Multitask Learning for Capturing Multiword Expressions<br />
Shiva Taslimipoor, Omid Rohanian and Le An Ha


<li>Evaluating Automatic Term Extraction Methods on Individual Documents<br />
Antonio Šajatović, Maja Buljan, Jan Šnajder and Bojana Dalbelo Bašić


<li>Novel Compound Predictor - Learning to Distinguish the Plausible from the Implausible<br />
Prajit Dhar and Lonneke van der Plas

</ol>
-->


<!---------------------------------->
<h1>Workshop Program (Fortezza da Basso, Hall 7)</h1>
<table cellspacing="0" cellpadding="3" border="0">

<!------------------------------------------------>
<tr><td valign=top style="padding-top: 2px;">8:55&#8211;09:00</td><td valign=top style="padding-top: 2px;"><em>Opening</em></td></tr>

<tr><td valign=top style="padding-top: 14px;">09:00&#8211;10:00</td><td valign=top style="padding-top: 14px;"><b>Session 1: Invited talk</b> (chair: Carlos Ramisch)</td></tr>
<tr><td valign=top width=100></td>
<td valign=top align=left><i>When the whole is greater than the sum of its parts: Multiword expressions and idiomaticity</i><br>
<strong>Aline Villavicencio</strong></td></tr>
<tr><td valign=top width=100>10:00&#8211;10:30</td>
<td valign=top align=left><i><b>Session 2: Poster boosters</b></i> (chair: Jelena Mitrović; 2 min. per poster)</tr>

<!------------------------------------------------>
<tr><td valign=top style="padding-top: 14px;">10:30&#8211;11:00</td><td valign=top style="padding-top: 14px;"><b><em>Coffee break</em></b></td></tr>
<!------------------------------------------------>

<tr><td valign=top style="padding-top: 14px;">11:00&#8211;12:30</td><td valign=top style="padding-top: 14px;"><b>Session 3: Posters</b> (chair: Verginica Barbu Mititelu)</td></tr>
<tr><td valign=top width=100></td><td valign=top align=left><i>The Romanian Corpus Annotated with Verbal Multiword Expressions</i><br>
Verginica Barbu Mititelu, Mihaela Ionescu and Mihaela Onofrei</td></tr>
<tr><td valign=top width=100></td><td valign=top align=left><i>Learning to Predict Novel Noun-Noun Compounds </i><br>
Prajit Dhar and Lonneke van der Plas</td></tr>
<tr><td valign=top width=100></td><td valign=top align=left><i>A comparison of statistical association measures for identifying dependency-based collocations in various languages</i><br>
Marcos Garcia, Marcos García Salido and Margarita Alonso-Ramos</td></tr>
<tr><td valign=top width=100></td><td valign=top align=left><i>Conﬁrming the Non-compositionality of Idioms for Sentiment Analysis</i><br>
Alyssa Hwang and Christopher Hidey</td></tr>
<tr><td valign=top width=100></td><td valign=top align=left><i>L2 Processing Advantages of Multiword Sequences: Evidence from Eye-Tracking</i><br>
Elma Kerz, Arndt Heilmann and Stella Neumann</td></tr>
<tr><td valign=top width=100></td><td valign=top align=left><i>Identiﬁcation of Adjective-Noun Neologisms using Pretrained Language Models </i><br>
John Philip McCrae</td></tr>
<tr><td valign=top width=100></td><td valign=top align=left><i>IDION: A database for Modern Greek multiword expressions.</i><br>
Stella Markantonatou, Panagiotis Minos, George Zakis, Vassiliki Moutzouri and Maria Chantou</td></tr>
<tr><td valign=top width=100></td><td valign=top align=left><i>Evaluating Automatic Term Extraction Methods on Individual Documents.</i><br>
Antonio Šajatović, Maja Buljan, Jan Šnajder and Bojana Dalbelo Bašić</td></tr>
<tr><td valign=top width=100></td><td valign=top align=left><i>A Systematic Comparison of English Noun Compound Representations</i><br>
Vered Shwarzt</td></tr>
<tr><td valign=top width=100></td><td valign=top align=left><i>Semantic Modelling of Adjective-Noun Collocations using Frame-Net</i><br>
Yana Strakatova and Erhard Hinrichs</td></tr>
<tr><td valign=top width=100></td><td valign=top align=left><i>Cross-lingual Transfer Learning and Multitask Learning for Capturing Multiword Expressions</i><br>
Shiva Taslimipoor, Omid Rohanian and Le An Ha</td></tr>
<tr><td valign=top width=100></td><td valign=top align=left><i>The Impact of Word Representations on Sequential Neural MWE Identiﬁcation</i><br>
Nicolas Zampieri, Carlos Ramisch and Geraldine Damnati</td></tr>
<tr><td valign=top width=100></td><td valign=top align=left><i>Ilfhocail: A Lexicon of Irish MWEs</i><br>
Abigail Walsh, Teresa Lynn and Jennifer Foster</td></tr>
<tr><td valign=top width=100></td><td valign=top align=left><i>A Neural Graph-based Approach to Verbal MWE Identiﬁcation </i><br>
Jakub Waszczuk, Rafael Ehren, Regina Stodden and Laura Kallmeyer</td></tr>

<tr><td valign=top style="padding-top: 14px;">12:30&#8211;14:00</td><td valign=top style="padding-top: 14px;"><b><i>Lunch break</i></b></td></tr>

<tr><td valign=top style="padding-top: 14px;"></td><td valign=top style="padding-top: 14px;"><b>Session 4: Multiword Expressions and WordNet</b> (chair: John Phillip McCrae)</td></tr>

<tr><td valign=top style="padding-top: 14px;">14:00&#8211;14:30</td><td valign=top style="padding-top: 14px;"><i>Hear about Verbal Multiword Expressions in the Bulgarian and the Romanian Wordnets Straight from the Horse’s Mouth</i></br>
Verginica Barbu Mititelu, Ivelina Stoyanova, Svetlozara Leseva, Maria Mitrofan, Tsvetana Dimitrova and Maria Todorova</td></tr>
<tr><td valign=top style="padding-top: 14px;">14:30&#8211;15:00</td><td valign=top style="padding-top: 14px;"><i>Modeling MWEs in BTB-WN</i></br>
Laska Laskova, Petya Osenova, Kiril Simov, Ivajlo Radev and Zara Kancheva</td></tr>
<tr><td valign=top style="padding-top: 14px;">15:00&#8211;15:30</td><td valign=top style="padding-top: 14px;"><i>Using OntoLex-Lemon for Representing and Interlinking German Multiword Expressions in OdeNet and MMORPH </i></br>
Thierry Declerck, Melanie Siegel and Stefania Racioppa</td></tr>

<tr><td valign=top style="padding-top: 14px;">15:30&#8211;16:00</td><td valign=top style="padding-top: 14px;"><i><b>Coffee break</b></i></br>

<tr><td valign=top style="padding-top: 14px;"></td><td valign=top style="padding-top: 14px;"><b>Session 5: Multiword Expressions - translation, lemmatization and identification</b> (chair: Stella Markantonatou)</br>
<tr><td valign=top style="padding-top: 14px;">16:00&#8211;16:30</td><td valign=top style="padding-top: 14px;"><i>Unsupervised Compositional Translation of Multiword Expressions</i></br>
Pablo Gamallo and Marcos Garcia</td></tr>
<tr><td valign=top style="padding-top: 14px;">16:30&#8211;16:50</td><td valign=top style="padding-top: 14px;"><i>Neural Lemmatization of Multiword Expressions</i></br>
Marine Schmitt and Mathieu Constant</td></tr>
<tr><td valign=top style="padding-top: 14px;">16:50&#8211;17:20</td><td valign=top style="padding-top: 14px;"><i>Without lexicons, multiword expression identiﬁcation will never ﬂy: A position statement </i></br>
Agata Savary, Silvio Cordeiro and Carlos Ramisch</td></tr>
<tr><td valign=top style="padding-top: 14px;">17:20&#8211;18:00</td><td valign=top style="padding-top: 14px;"><b>Session 6: Community discussion</b> (chair: Agata Savary)</br>
</i>News, future work, SIGLEX MWE section</i> [<a href="mwewn2019/slides/2019-08-02-MWE-WN-community-discussion.pdf">slides</a>]</td></tr>

</table>

