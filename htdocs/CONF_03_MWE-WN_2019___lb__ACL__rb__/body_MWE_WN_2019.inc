﻿<h2>Joint Workshop on Multiword Expressions and WordNet (MWE-WN 2019)</h2>

<h3>Workshop at <a href="http://www.acl2019.org/" target="_blank">ACL 2019</a> (Florence, Italy), August 2nd, 2019</h3>

<p>Organized and sponsored by the Special Interest Group on the Lexicon (<a href="http://www.siglex.org" target="_blank">SIGLEX</a>) of the Association for Computational Linguistics (<a href="https://www.aclweb.org/portal/" target="_blank">ACL</a>). Endorsed by the Global Wordnet Association (<a href="http://globalwordnet.org/" target="_blank">GWA</a>). This joint event is the 15th edition of the <strong>Workshop on Multiword Expressions (<a href="?sitesig=CONF" target="_blank">MWE</a>)</strong></p>

<p><small>Last updated: August 21, 2019</small></p>

<table><tr>
  <td height="90" valign="top"><a href="mwewn2019/photos/DSC_0124.JPG" target="_self"><img src="mwewn2019/photos/DSC_0124-small.JPG" height="90" /></a></td>
  <td height="90" valign="top"><a href="mwewn2019/photos/DSC_0113.JPG" target="_self"><img src="mwewn2019/photos/DSC_0113-small.JPG" height="90" /></a></td>
  <td height="90" valign="top"><a href="mwewn2019/photos/DSC_0114.JPG" target="_self"><img src="mwewn2019/photos/DSC_0114-small.JPG" height="90" /></a></td>
  <td height="90" valign="top"><a href="mwewn2019/photos/DSC_0116.JPG" target="_self"><img src="mwewn2019/photos/DSC_0116-small.JPG" height="90" /></a></td>
  <td height="90" valign="top"><a href="mwewn2019/photos/DSC_0118.JPG" target="_self"><img src="mwewn2019/photos/DSC_0118-small.JPG" height="90" /></a></td>
  <td height="90" valign="top"><a href="mwewn2019/photos/DSC_0119.JPG" target="_self"><img src="mwewn2019/photos/DSC_0119-small.JPG" height="90" /></a></td>
  <td height="90" valign="top"><a href="mwewn2019/photos/DSC_0120.JPG" target="_self"><img src="mwewn2019/photos/DSC_0120-small.JPG" height="90" /></a></td>
  <td height="90" valign="top"><a href="mwewn2019/photos/DSC_0135.JPG" target="_self"><img src="mwewn2019/photos/DSC_0135-small.JPG" height="90" /></a></td>
</tr></table>

<font color="red"><strong>NEWS:</strong></font>
<ul>
<li>[August 19]: The <a href="https://www.aclweb.org/anthology/events/ws-2019/#w19-51">MWE-WN proceedings</a> are available on the ACL Anthology.</li>
<li>[June 17]: The workshop <a href="http://multiword.sourceforge.net/PHITE.php?sitesig=CONF&page=CONF_03_MWE-WN_2019___lb__ACL__rb__&subpage=CONF_20_Program">program</a> is online.</li>
<li>[May 24]: The list of accepted papers has been published.</li>
<li>[Now 29]: The workshop proposal has been accepted and will be co-located with <a href="http://www.acl2019.org/" target="_blank">ACL 2019</a> in Florence.</li>


</ul>

<!------------------------------------->
<h1>Description</h1>

<p>As a joint event, this workshop proposal addresses two domains – multiword expressions and WordNet – with partly overlapping communities and research interests, but relatively divergent practices and terminologies.</p>

<p>Multiword expressions (MWEs) are word combinations, such as all of a sudden, a hot dog, to pay a visit or to pull one's leg, which exhibit lexical, syntactic, semantic, pragmatic and/or statistical idiosyncrasies. MWEs encompass closely related linguistic objects such as idioms, compounds, light verb constructions, rhetorical figures, institutionalised phrases or collocations. Modelling and computational aspects of MWEs have been covered by the Multiword Expression Workshop, organised over the past years by the <a href="http://multiword.sourceforge.net/" target="_blank">MWE section</a> of <a href="http://www.siglex.org" target="_blank">SIGLEX</a>. Because of their unpredictable behavior, and most prominently their non-compositional semantics, MWEs pose special problems in linguistic modelling (e.g. treebank annotation and grammar engineering), in NLP pipelines (e.g. when their orchestration with parsing is concerned), and in end-use applications (e.g. information extraction  or machine translation).</p>

<p>From its very beginning, WordNet has included MWEs, and linked their meanings into a shared network: talk, blab, sing, spill the beans, let the cat out of the bag, tattle, peach, babble, babble out, blab out “divulge confidential information or secrets”.  Indeed, over 50% of entries in the Princeton WordNet of English are MWEs and most other projects have a similarly high percentage. However, MWEs are generally encoded as a string, with no internal information about syntactic structure or compositionality.   Many suggestions for richer encodings have been made but not yet widely adopted, partly because of the cost of adding richer data to already large lexicons.</p>  

<p>For the above reasons, the MWE and WN communities propose to put forward a joint event, which should allow better convergences and scientific innovation. We will call for papers focusing on research related (but not limited) to the following topics.</p> 

<h3>Joint topics on MWEs and Wordnets</h3>
<ul> 
<li>Encoding MWEs in wordnets - how can we take advantage of the existing rich structure of wordnets?</li>
<li>Encoding MWEs in wordnets - consequences for a lexical-semantic organization of MWEs</li>
<li>Linking wordnets with existing MWE lexicons</li>
<li>Word sense disambiguation for single-word and multiword expressions </li>
<li>Cross-wordnet and cross-language comparisons of MWEs</li>
<li>MWEs in sense-annotated corpora</li>
<li>Semantic relations in wordnets related to MWEs</li>
</li>
</ul>

<h3>MWE-specific topics</h3>
<ul> 
<li>Computationally-applicable theoretical studies on MWEs and constructions in psycholinguistics, corpus linguistics and formal grammars</li>
<li>MWE and construction annotation in corpora and treebanks</li>
<li>MWE and construction representation in manually/automatically constructed lexical resources</li>
<li>Processing of MWEs and constructions in syntactic and semantic frameworks (e.g. CCG, CxG, HPSG, LFG, TAG, UD, etc.), and in end-user applications (e.g. information extraction, machine translation and summarization)</li>
<li>Original discovery and identification methods for MWEs and constructions</li>
<li>MWEs and constructions in language acquisition and in non-standard language (e.g. tweets, forums, spontaneous speech)</li>
<li>Evaluation of annotation and processing techniques for MWEs and constructions</li>
<li>Retrospective comparative analyses from the PARSEME shared tasks on automatic identification of MWEs</li>
</ul>
<p>Note that, with the intention to also perpetuate previous converging effects with the Construction Grammar community (see the  <a href="http://multiword.sourceforge.net/lawmwecxg2018/" target="_blank">LAW-MWE-CxG 2018</a> workshop), we extend the traditional MWE scope to grammatical constructions.</p>

<!------------------------------------->
<h1>Links to the PARSEME Shared Task on Automatic Verbal MWE Identification</h1>

<p>The two previous editions of the MWE workshop (in <a href="http://multiword.sourceforge.net/sharedtask2017" target="_blank">2017</a> and in <a href="http://multiword.sourceforge.net/sharedtask2018" target="_blank">2018</a>) featured the PARSEME Shared Task on Automatic Identification of Verbal Multiword Expressions. There is no edition planned for 2019, however we wish the MWE-WN 2019 workshop to play an important role in preparing a future edition (probably in 2020). Namely, we call for papers including retrospective and/or comparatives analyses of the results of previous shared task editions, as well as recommendations for corpus annotation enhancements.</p>

<!------------------------------------->
<h1 id="modalities"><a name="submissions"></a>Submission modalities</h1>

<p>There are two tracks:</p>
<ul>
<li>Regular <strong>research track</strong>, where the submissions must be substantially original.</li>
<li><strong>Dissemination track</strong>, which welcomes recent previously published work (or work accepted for publication), dedicated explicitly both to MWEs and WordNet.   This will be presented to encourage discussion, but only the abstract will appear in the proceedings.</li>
</ul>

<p>The regular <strong>research track</strong> submissions should follow one of the 2 formats:</p>
<ul>
<li><strong>Long papers</strong> (8 content pages + references): Long papers should report on solid and finished research including new experimental results, resources and/or techniques.</li>
<li><strong>Short papers</strong> (4 content pages + references): Short papers should report on small experiments, focused contributions, ongoing research, negative results and/or philosophical discussion.</li>
</ul>

<p>The decisions as to oral or poster presentations of the selected papers will be taken by the PC chairs. No distinction between papers presented orally and as posters is made in the workshop proceedings. There is no limit on the number of reference pages. Authors will be granted an <strong>extra page</strong> for the final version of their papers. The submission is <strong>double-blind</strong>, as understood by the ACL 2019 submission policy. The reported research should be substantially original. Papers available as preprints can also be submitted provided that they fulfil the conditions defined by the <a href="https://www.aclweb.org/portal/content/new-policies-submission-review-and-citation" target="_blank">ACL Policies for Submission, Review and Citation</a>. For both types of submissions in this track, the ACL 2019 templates should be used.</p>

<p>The <strong>dissemination track</strong> submissions are not anonymous, and they should not exceed one page, including the authors' names and affiliations, the mention of the original venue, the link to the original paper and a short explanation why the paper is relevant to MWEs and Wordnets workshop. If the original paper is not publicly available, it should also be submitted in a separate .pdf file but it does not have to follow the ACL 2019 template.</p>

<p>All papers should be submitted via the following  <a href="https://www.softconf.com/acl2019/mwewn" target="_blank">START space</a>. 
</p>

<p> The MWE-WN Workshop follows the ACL 2019 <a href="http://www.acl2019.org/EN/call-for-papers.xhtml" target="_blank">multiple submission policy</a>.

<p>Please choose the appropriate track (research/dissemination) and for research papers the submission modality (long/short).</p>

<!-- In accordance to EACL 2017 submission policy, this is a condition for accepting the paper for the reviewing process. Final versions of accepted papers will be submitted both in PDF and source LaTeX formats. -->

<!------------------------------------->
 
<h1>Important dates</h1>

<p>We follow the <strong>ACL 2019 workshop schedule:</strong></p>

<p>All deadlines are at 23:59 UTC-12 (anywhere in the world).</p>

<table>
<tr><td>May 1, 2019</td><td> Paper Submission due (Deadline extended)</td></tr>
<tr><td>May 24, 2019</td><td>Notification of Acceptance</td></tr>
<tr><td>June 3, 2019</td><td>Camera-ready papers due</td></tr>
<tr><td>August 2, 2019</td><td>MWE-WN 2019 Workshop</td></tr>
</table>

<!--
See also the important dates for the <a href="http://multiword.sourceforge.net/sharedtask2017" target="_blank">shared task</a> systems.
-->

<!------------------------------------->
<h1>Workshop Organizers and Program Committee Chairs</h1>
<ul>
<li><a href="http://www.racai.ro/en/about-us/racai-staff/verginica-barbu-mititelu/" target="_blank">Verginica Barbu Mititelu</a>, Romanian Academy Research Institute for Artificial Intelligence (Romania)</li>
<li><a href="http://www.ntu.edu.sg/home/fcbond/" target="_blank">Francis Bond</a>, Nanyang Technological University (Singapore) </li>
<li><a href="https://bit.ly/2NhL2lz" target="_blank">Jelena Mitrović</a>, University of Passau (Germany) </li>
<li><a href="https://sites.google.com/site/carlaparraescartin/" target="_blank">Carla Parra Escartín</a>, Unbabel, Lisbon (Portugal) </li>
<li><a href="http://www.info.univ-tours.fr/~savary/" target="_blank">Agata Savary</a>, University of Tours (France) </li>
</ul>


<!------------------------------------->
<!--
<h1>Publication Chairs</h1>
<ul>
<li><a href="https://www.slm.uni-hamburg.de/germanistik/personen/andresen.html" target="_blank">Melanie Andresen</a>, Hamburg University (Germany) </li>
<li><a href="http://www.info.univ-tours.fr/~savary/" target="_blank">Agata Savary</a>, University of Tours (France) </li>
</ul>
-->


<!------------------------------------->
<!--
<h1>Publicity Chairs</h1>
<ul>
<li><a href="http://nlp.cs.nyu.edu/people/meyers.html" target="_blank">Adam Meyers</a>, New York University (USA) </li>
<li><a href="http://www.info.univ-tours.fr/~savary/" target="_blank">Agata Savary</a>, University of Tours (France) </li>
</ul>
-->

<!------------------------------------->

<h1>Contact</h1>
<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwewn2019@gmail.com">mwewn2019@gmail.com</a></p>

<!------------------------------------->
<h1>Anti-harassment policy</h1>
<p>The workshop supports the ACL <a href="https://www.aclweb.org/adminwiki/index.php?title=Anti-Harassment_Policy" target=_blanc">anti-harassment policy</a>.</p>

