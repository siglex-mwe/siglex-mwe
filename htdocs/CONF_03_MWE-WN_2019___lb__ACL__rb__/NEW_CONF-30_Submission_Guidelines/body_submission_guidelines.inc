<h2>The 9th Workshop on Multiword Expressions (MWE 2013)</h2>

<h3>Workshop at NAACL 2013 (Atlanta, Georgia, USA), June 13/14, 2013</h3>

<p>Endorsed by the Special Interest Group on the Lexicon of the Association for Computational Linguistics (<a href="http://www.siglex.org" target="_blank">SIGLEX</a>)</p>

<p><small>Last updated: Apr 02, 2013</small></p>

<h1>Instructions for authors</h1>

<h2>Camera-ready submission</h2>

<p>Coming soon!</p>

<h2>Submission for review</h2>

<p>For MWE 2013, we will accept the following submission modalities:</p>

<ul>
<li><strong>Regular long papers (8 content pages + 1 page for references):</strong> Long papers should report on solid and finished research including new experimental results, resources and/or techniques.</li>
<li><strong>Regular short papers (4 content pages + 1 page for references):</strong> Short papers should report on small experiments, focused contributions, ongoing research, negative results and/or philosophical discussion.</li>
</ul>

<p>The reported research should be substantially original. The papers will be presented orally or as a posters. The decision as to which papers will be presented orally and which as poster will be made by the program committee based on the nature rather than on the quality of the work. All submissions must be in PDF format and must follow the NAACL 2013 formatting requirements (available at the <a href="http://naacl2013.naacl.org/CFP.aspx" target="_blank"/>NAACL 2013 website</a>). We strongly advise the use of the provided Word or LaTeX template files. </p>

<p>Reviewing will be double-blind, and thus no author information should be included in the papers; self-reference should be avoided as well. Papers that do not conform to these requirements will be rejected without review. Accepted papers will appear in the workshop proceedings, where no distinction will be made between papers presented orally or as posters.</p>

<p>Submissions should be made online through the START system at <a href="https://www.softconf.com/naacl2013/MWE2013/">https://www.softconf.com/naacl2013/MWE2013/</a>. Please notice that, this year, you should <strong>register a user account</strong> before you can submit a paper.</p>

<!--<p>More details about the submission procedure (e.g. online submission system) will be available soon.</p>-->

<h1>Important dates</h1>

<table>
<tr><td><span style="text-decoration:line-through">Mar 1, 2013</span></td><td><span style="text-decoration:line-through">Long &amp; short paper submission deadline 23:59 PDT (GMT-12)</span></td></tr>
<tr><td><span style="text-decoration:line-through">Mar 15, 2013</span></td><td><span style="text-decoration:line-through">Long &amp; short paper submission deadline 23:59 PDT (GMT-12)</span></td></tr>
<tr><td>Apr 12, 2013</td><td>Notification of acceptance</td></tr>
<tr><td>Apr 26, 2013</td><td>Camera-ready deadline</td></tr>
<tr><td>Jun 13/14, 2013</td><td>MWE 2013 Workshop</td></tr>
</table>

<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwe2013workshop at gmail.com">mwe2013workshop at gmail.com</a></p>

