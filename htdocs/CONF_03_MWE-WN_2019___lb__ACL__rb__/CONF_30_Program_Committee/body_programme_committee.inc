﻿<h2>Joint Workshop on Multiword Expressions and WordNet (MWE-WN 2019)</h2>

<h3>Workshop at <a href="http://www.acl2019.org/" target="_blank">ACL 2019</a> (Florence, Italy), August 2nd, 2019</h3>

<!--
<h3>Workshop at <a href="http://coling2018.org/" target="_blank">COLING 2018</a> (Santa Fe, USA), August 25-26, 2018</h3>
-->

<p>Organized, sponsored and endorsed by the Global Wordnet Association (<a href="http://globalwordnet.org/" target="_blank">GWA</a>) and the Special Interest Group on the Lexicon (<a href="http://www.siglex.org" target="_blank">SIGLEX</a>) of the Association for Computational Linguistics (<a href="https://www.aclweb.org/portal/" target="_blank">ACL</a>).</p>

<p>This joint event is the 15th edition of the <strong>Workshop on Multiword Expressions (<a href="?sitesig=CONF" target="_blank">MWE</a>)</strong>, with a special focus on WordNet.</p>

<p><small>Last updated: August 19, 2019</small></p>


<h1>PC chairs</h1>
<ul>
<li><a href="http://www.racai.ro/en/about-us/racai-staff/verginica-barbu-mititelu/" target="_blank">Verginica Barbu Mititelu</a>, Romanian Academy Research Institute for Artificial Intelligence (Romania)</li>
<li><a href="http://www.ntu.edu.sg/home/fcbond/" target="_blank">Francis Bond</a>, Nanyang Technological University (Singapore) </li>
<li><a href="https://bit.ly/2NhL2lz" target="_blank">Jelena Mitrović</a>, University of Passau (Germany) </li>
<li><a href="https://sites.google.com/site/carlaparraescartin/" target="_blank">Carla Parra Escartín</a>, Unbabel, Lisbon (Portugal) </li>
<li><a href="http://www.info.univ-tours.fr/~savary/" target="_blank">Agata Savary</a>, University of Tours (France) </li>
</ul>

<h1>Program Committee</h1>

<ul>
<li>Eneko Agirre, University of the Basque Country (Spain) </li>
<li>Tim Baldwin, University of Melbourne (Australia) </li>
<li>Archna Bhatia, Florida Institute for Human and Machine Cognition (USA) </li>
<li>Sonja Bosch, Department of African Languages, University of South Africa (South Africa) </li>
<li>Miriam Butt, Universität Konstanz (Germany) </li>
<li>Aoife Cahill, ETS (USA) </li>
<li>Marie Candito, Paris Diderot University (France) </li>
<li>Annalina Caputo, ADAPT Centre / Trinity College Dublin (Ireland) </li>
<li>Helena Caseli, Federal University of Sao Carlos (Brazil) </li>
<li>Anastasia Christofidou, Academy of Athens (Greece) </li>
<li>Matthieu Constant, Université de Lorraine (France) </li>
<li>Silvio Cordeiro, Federal University of Rio Grande do Sul (France) </li>
<li>Janos Csirik, University of Szeged (Hungary) </li>
<!-- <li>Niladri Sekhar Dash, Indian Statistical Institute (India) </li> didn't review his papers -->
<li>Gaël Dias, University of Caen Basse-Normandie (France) </li>
<li>Gülşen Eryiğit , Istanbul Technical University (Turkey) </li>
<li>Stefan Evert, FAU Erlangen-Nürnberg (Germany) </li>
<li>Christiane Fellbaum, Princeton University (USA) </li>
<li>Joaquim Ferreira da Silva, New University of Lisbon (Portugal) </li>
<li>Darja Fišer, University of Ljubljana (Slovenia) </li>
<li>Dan Flickinger, Stanford University (USA) </li>
<li>Aggeliki Fotopoulou, ILSP/RC "Athena" (Greece) </li>
<li>Voula Giouli, Institute for Language and Speech Processing (Greece) </li>
<li>Chikara Hashimoto, Yahoo!Japan (Japan) </li>
<li>Ales Horak, Masaryk University (Czech Republic) </li>
<li>Shu-Kai Hsieh, National Taiwan Normal University (Taiwan) </li>
<li>Hitoshi Isahara, Toyohashi University of Technology (Japan) </li>
<li>Kyo Kageura, University of Tokyo (Japan) </li>
<li>Diptesh Kanojia, IIT Bombay (India) </li>
<li>Kyoko Kanzaki, Toyohashi University of Technology (Japan) </li>
<li>Philipp Koehn, University of Edinburgh (UK) </li>
<li>Dimitris Kokkinakis, University of Gothenburg (Sweden) </li>
<li>Olga Kolesnikova, Instituto Politécnico Nacional (Mexico) </li>
<li>Ioannis Korkontzelos, Edge Hill University (UK) </li>
<li>Cvetana Krstev, University of Belgrade (Serbia) </li>
<li>Timm Lichte, University of Tübingen (Germany) </li>
<li>Irina Lobzhanidze, Ilia State University (Georgia)</li>
<li>Ismail el Maarouf, Adarga Ltd (UK) </li>
<li>Stella Markantonatou, Institute for Language and Speech Processing (Greece) </li>
<li>Hector Martínez Alonso, Thomson Reuters Labs (Canada) </li>
<li>John P. McCrae, National University of Ireland, Galway (Ireland) </li>
<li>Nurit Melnik, The Open University of Israel (Israel) </li>
<li>Gerard de Melo, Rutgers University (USA) </li>
<li>Johanna Monti, "L'Orientale" University of Naples (Italy) </li>
<li>Preslav Nakov, Qatar Computing Research Institute, HBKU (Qatar) </li>
<li>Joakim Nivre, Uppsala University (Sweden) </li>
<li>Jan Odijk, University of Utrecht (Netherlands) </li>
<li>Antoni Oliver Gonzales, Universitat Oberta de Catalunya (Spain) </li>
<li>Heili Orav, University of Tartu (Estonia) </li>
<li>Petya Osenova, Bulgarian Academy of Sciences (Bulgaria) </li>
<li>Haris Papageorgiou, Institute for Language and Speech Processing (Greece) </li>
<li>Yannick Parmentier, Université d'Orléans (France) </li>
<li>Agnieszka Patejuk, University of Oxford (UK); Institute of Computer Science, Polish Academy of Sciences (Poland) </li>
<li>Marie-Sophie Pausé, University of Paris 3 (France) </li>
<li>Adam Pease, Articulate Software (USA) </li>
<li>Pavel Pecina, Charles University (Czech Republic) </li>
<li>Bolette Pedersen, University of Copenhagen (Denmark) </li>
<li>Ted Pedersen, University of Minnesota (USA) </li>
<li>Scott Piao, Lancaster University (UK)</li>
<li>Maciej Piasecki, Wroclaw University of Technology (Poland)</li>
<li>Alain Polguère, Université de Lorraine (France) </li>
<li>Marten Postma, Vrije Universiteit Amsterdam (Netherlands) </li>
<li>Behrang QuasemiZadeh, University of Duesseldorf (Germany) </li>
<li>Alexandre Rademaker, IBM Research Brazil and EMAp/FGV (Brazil) </li>
<li>Carlos Ramisch, Aix Marseille University (France) </li>
<li>German Rigau, University of the Basque Country (Spain)</li>
<li>Mike Rosner, University of Malta (Malta) </li>
<li>Ewa Rudnicka, Wrocław University of Technology (Poland) </li>
<li>Manfred Sailer, Goethe-Universität Frankfurt am Main (Germany) </li>
<li>Federico Sangati, Independent researcher (Italy) </li>
<li>Kevin Scannell, Saint Louis University (USA) </li>
<li>Nathan Schneider, Georgetown University (USA) </li>
<li>Sabine Schulte im Walde, University of Stuttgart (Germany) </li>
<li>Kiril Simov, Bulgarian Academy of Sciences (Bulgaria) </li>
<li>Jan Šnajder, University of Zagreb (Croatia) </li>
<li>Ranka Stanković, University of Belgrade (Serbia) </li>
<li>Ivelina Stoyanova, Bulgarian Academy of Sciences (Bulgaria) </li>
<li>Stan Szpakowicz, University of Ottawa (Canada) </li>
<li>Beata Trawinski, Institut für Deutsche Sprache Mannheim (Germany) </li>
<li>Dan Tufiș, Romanina Academey (Romania) </li>
<li>Ruben Urizar, University of the Basque Country (Spain) </li>
<li>E Umamaheswari Vasanthakumar, Nanyang Technological University (Singapore) </li>
<li>Veronika Vincze, Hungarian Academy of Sciences (Hungary) </li>
<li>Piek Vossen, VU University Amsterdam. (Netherlands) </li>
<li>Shan Wang, University of Macau (China) </li>
<li>Jakub Waszczuk, University of Duesseldorf (Germany) </li>
<li>Marion Weller-Di Marco, University of Amsterdam (Netherlands) </li>

<!--
<h1>Contact</h1>

<p>For any inquiries regarding the workshop please send an email to <a href="mailto:mwe2017workshop at gmail.com">mwe2017workshop at gmail.com</a></p>
-->
