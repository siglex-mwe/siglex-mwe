<h2><a href="http://ixa2.si.ehu.es/starsem/">*SEM - First Joint Conference on Lexical and Computational Semantics</a></h2>

<p><strong>Deadline : March 30 2012 (UTC-11:00)</strong></p>

<h3>MWE 2012 becomes part of *SEM 2012</h3>

<p>We are pleased to announce that in 2012, the traditional yearly workshop on Multiword Expressions changes!</p>

<p>This year, the SIGLEX and SIGSEM communities are joining forces and we will have the first *SEM conference that attempts to bring together CL practitioners interested in semantics under the same roof. Therefore, there will be no MWE 2012 workshop, but instead we are joining the *SEM initiative and MWEs will be an area of the *SEM conference. The conference will be held in conjunction with NAACL 2012 in Montreal, Canada, on July 7-8.</p>

<p>To find out more about  the *SEM conference, visit the page <a href="http://ixa2.si.ehu.es/starsem/">ixa2.si.ehu.es/starsem/</a></p>

<p>As organisers of MWE 2011, we strongly encourage you to submit to *SEM. We believe that this is going to strengthen our community and give more visibility to our field. Please find below the CFP of the event, do not hesitate to spread the news in your institutions and research groups. We expect to see many papers on MWEs in the *SEM programme, we count on you!</p>

<p>Carlos Ramisch, Valia Kordoni and Aline Villavicenio</p>


