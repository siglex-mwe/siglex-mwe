<h3>2006</h3>
<ul>
<li>ACL workshop <a href="http://www.inf.ufrgs.br/~avillavicencio/mwe-acl06.html" target ="_blank"><em>Multiword Expressions: Identifying and Exploiting Underlying Properties</em></a> - Sydney, Australia. [<a href="http://acl.ldc.upenn.edu/W/W06/#W06-1200" target="_blank">Proceedings</a>]</li>
<li>EACL workshop <a href="http://ucrel.lancs.ac.uk/EACL06MWEmc/" target ="_blank"><em>Multi-word-expressions in a multilingual context</em></a> - Trento, Italy.</li>
<li><a href="http://kollokationen.bbaw.de/htm/collconf2_en.html" target ="_blank"><em>Collocations and idioms 2006: linguistic, computational, and psycholinguistic perspectives</em></a> - Berlin, Germany.</li>
</ul>

<h3>2004</h3>
<ul>
<li>ACL workshop <a href="http://www.cl.cam.ac.uk/~alk23/mwe04/mwe.html" target ="_blank"><em>Multiword Expressions: Integrating Processing</em></a> - Barcelona, Spain.</li>
</ul>

<h3>2003</h3>
<ul>
<li>ACL workshop <a href="http://www.cl.cam.ac.uk/~alk23/mwe/mwe.html" target ="_blank"><em>Multiword Expressions: Analysis, Acquisition and Treatment</em></a> - Sapporo, Japan.</li>
</ul>
