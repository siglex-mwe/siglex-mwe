#! /bin/bash
HERE="$(cd "$(dirname "$0")" && pwd)"

set -o nounset    # Using "$UNDEF" var raises error
set -o errexit    # Exit on error, do not continue quietly


usage() {
    echo "Usage: $(basename "$0") <sourceforge-username>"
    echo "Send website updates to HTTP server."
}
fail() {
    usage
    echo "$(basename "$0"): $1"; exit 1
}

# Handle optional args
while getopts :"h" FLAG; do
    case "$FLAG" in
        h)  usage; exit 0 ;;
        :)  fail "missing arg to -$OPTARG" ;;
        \?) fail "bad flag: -$OPTARG" ;;
        *)  fail "NOT IMPLEMENTED: -$FLAG" ;;
    esac
done
shift "$((OPTIND - 1))"

# Handle positional args
test "$#" -ne 1  && fail "expected 1 positional arg: sourceforge-username"
username="$1"; shift

############################################################


cd "$HERE"

echo "Warning: not showing directory permission modifications"
rsync -rlptC --exclude .git --stats --compress --del --progress -i htdocs ${username},multiword@web.sf.net: | sed '/^\.d\.\.\.p\.\.\.\.\./d'


git status

echo -e "\n\n\n\e[31m\e[01m             DO NOT FORGET to...\n\n\n"
echo -e "      GGG     IIII  TTTTTTTT      PPPPPP  UU   UU   SSSs  HH  HH"
echo -e "     GG  G     II      TT         PP   PP UU   UU  SS     HH  HH"
echo -e "     G         II      TT         PP  PP  UU   UU   SS    HH  HH"
echo -e "     G GGG     II      TT         PPPPPP  UU   UU    SS   HHHHHH"
echo -e "     G  GG     II      TT         PP      UU   UU     SS  HH  HH"
echo -e "     GG GG     II      TT         PP      UU   UU      SS HH  HH"
echo -e "      GGG     IIII     TT         PP       UUUUU    SSSS  HH  HH"
echo -e "\e[0m"
sleep 2
